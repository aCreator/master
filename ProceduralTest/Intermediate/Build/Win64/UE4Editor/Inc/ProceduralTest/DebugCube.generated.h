// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROCEDURALTEST_DebugCube_generated_h
#error "DebugCube.generated.h already included, missing '#pragma once' in DebugCube.h"
#endif
#define PROCEDURALTEST_DebugCube_generated_h

#define ProceduralTest_Source_ProceduralTest_DebugCube_h_14_RPC_WRAPPERS
#define ProceduralTest_Source_ProceduralTest_DebugCube_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define ProceduralTest_Source_ProceduralTest_DebugCube_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADebugCube(); \
	friend struct Z_Construct_UClass_ADebugCube_Statics; \
public: \
	DECLARE_CLASS(ADebugCube, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(ADebugCube)


#define ProceduralTest_Source_ProceduralTest_DebugCube_h_14_INCLASS \
private: \
	static void StaticRegisterNativesADebugCube(); \
	friend struct Z_Construct_UClass_ADebugCube_Statics; \
public: \
	DECLARE_CLASS(ADebugCube, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(ADebugCube)


#define ProceduralTest_Source_ProceduralTest_DebugCube_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADebugCube(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADebugCube) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADebugCube); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADebugCube); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADebugCube(ADebugCube&&); \
	NO_API ADebugCube(const ADebugCube&); \
public:


#define ProceduralTest_Source_ProceduralTest_DebugCube_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADebugCube(ADebugCube&&); \
	NO_API ADebugCube(const ADebugCube&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADebugCube); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADebugCube); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADebugCube)


#define ProceduralTest_Source_ProceduralTest_DebugCube_h_14_PRIVATE_PROPERTY_OFFSET
#define ProceduralTest_Source_ProceduralTest_DebugCube_h_11_PROLOG
#define ProceduralTest_Source_ProceduralTest_DebugCube_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_DebugCube_h_14_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_DebugCube_h_14_RPC_WRAPPERS \
	ProceduralTest_Source_ProceduralTest_DebugCube_h_14_INCLASS \
	ProceduralTest_Source_ProceduralTest_DebugCube_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProceduralTest_Source_ProceduralTest_DebugCube_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_DebugCube_h_14_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_DebugCube_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_DebugCube_h_14_INCLASS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_DebugCube_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProceduralTest_Source_ProceduralTest_DebugCube_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
