// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROCEDURALTEST_Room_generated_h
#error "Room.generated.h already included, missing '#pragma once' in Room.h"
#endif
#define PROCEDURALTEST_Room_generated_h

#define ProceduralTest_Source_ProceduralTest_Room_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execTestMethod) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TestMethod(); \
		P_NATIVE_END; \
	}


#define ProceduralTest_Source_ProceduralTest_Room_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execTestMethod) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TestMethod(); \
		P_NATIVE_END; \
	}


#define ProceduralTest_Source_ProceduralTest_Room_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARoom(); \
	friend struct Z_Construct_UClass_ARoom_Statics; \
public: \
	DECLARE_CLASS(ARoom, AVisibleObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(ARoom)


#define ProceduralTest_Source_ProceduralTest_Room_h_18_INCLASS \
private: \
	static void StaticRegisterNativesARoom(); \
	friend struct Z_Construct_UClass_ARoom_Statics; \
public: \
	DECLARE_CLASS(ARoom, AVisibleObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(ARoom)


#define ProceduralTest_Source_ProceduralTest_Room_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARoom(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARoom) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARoom); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARoom); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARoom(ARoom&&); \
	NO_API ARoom(const ARoom&); \
public:


#define ProceduralTest_Source_ProceduralTest_Room_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARoom(ARoom&&); \
	NO_API ARoom(const ARoom&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARoom); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARoom); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARoom)


#define ProceduralTest_Source_ProceduralTest_Room_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__wallMesh() { return STRUCT_OFFSET(ARoom, wallMesh); } \
	FORCEINLINE static uint32 __PPO__floorMesh() { return STRUCT_OFFSET(ARoom, floorMesh); } \
	FORCEINLINE static uint32 __PPO__ceilingMesh() { return STRUCT_OFFSET(ARoom, ceilingMesh); }


#define ProceduralTest_Source_ProceduralTest_Room_h_15_PROLOG
#define ProceduralTest_Source_ProceduralTest_Room_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_Room_h_18_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_Room_h_18_RPC_WRAPPERS \
	ProceduralTest_Source_ProceduralTest_Room_h_18_INCLASS \
	ProceduralTest_Source_ProceduralTest_Room_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProceduralTest_Source_ProceduralTest_Room_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_Room_h_18_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_Room_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_Room_h_18_INCLASS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_Room_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProceduralTest_Source_ProceduralTest_Room_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
