// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ProceduralTest/Connection.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConnection() {}
// Cross Module References
	PROCEDURALTEST_API UClass* Z_Construct_UClass_AConnection_NoRegister();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_AConnection();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_ProceduralTest();
// End Cross Module References
	void AConnection::StaticRegisterNativesAConnection()
	{
	}
	UClass* Z_Construct_UClass_AConnection_NoRegister()
	{
		return AConnection::StaticClass();
	}
	struct Z_Construct_UClass_AConnection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AConnection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ProceduralTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AConnection_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Connection.h" },
		{ "ModuleRelativePath", "Connection.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AConnection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AConnection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AConnection_Statics::ClassParams = {
		&AConnection::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AConnection_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AConnection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AConnection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AConnection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AConnection, 2516695457);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AConnection(Z_Construct_UClass_AConnection, &AConnection::StaticClass, TEXT("/Script/ProceduralTest"), TEXT("AConnection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AConnection);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
