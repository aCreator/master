// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AVisibilityManager;
#ifdef PROCEDURALTEST_Corridor_generated_h
#error "Corridor.generated.h already included, missing '#pragma once' in Corridor.h"
#endif
#define PROCEDURALTEST_Corridor_generated_h

#define ProceduralTest_Source_ProceduralTest_Corridor_h_20_RPC_WRAPPERS
#define ProceduralTest_Source_ProceduralTest_Corridor_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define ProceduralTest_Source_ProceduralTest_Corridor_h_20_EVENT_PARMS \
	struct Corridor_eventWasActivated_Parms \
	{ \
		AVisibilityManager* currentRoomVM; \
	}; \
	struct Corridor_eventWasDeactivated_Parms \
	{ \
		AVisibilityManager* currentRoomVM; \
	};


#define ProceduralTest_Source_ProceduralTest_Corridor_h_20_CALLBACK_WRAPPERS
#define ProceduralTest_Source_ProceduralTest_Corridor_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACorridor(); \
	friend struct Z_Construct_UClass_ACorridor_Statics; \
public: \
	DECLARE_CLASS(ACorridor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(ACorridor)


#define ProceduralTest_Source_ProceduralTest_Corridor_h_20_INCLASS \
private: \
	static void StaticRegisterNativesACorridor(); \
	friend struct Z_Construct_UClass_ACorridor_Statics; \
public: \
	DECLARE_CLASS(ACorridor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(ACorridor)


#define ProceduralTest_Source_ProceduralTest_Corridor_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACorridor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACorridor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACorridor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACorridor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACorridor(ACorridor&&); \
	NO_API ACorridor(const ACorridor&); \
public:


#define ProceduralTest_Source_ProceduralTest_Corridor_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACorridor(ACorridor&&); \
	NO_API ACorridor(const ACorridor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACorridor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACorridor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACorridor)


#define ProceduralTest_Source_ProceduralTest_Corridor_h_20_PRIVATE_PROPERTY_OFFSET
#define ProceduralTest_Source_ProceduralTest_Corridor_h_17_PROLOG \
	ProceduralTest_Source_ProceduralTest_Corridor_h_20_EVENT_PARMS


#define ProceduralTest_Source_ProceduralTest_Corridor_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_Corridor_h_20_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_Corridor_h_20_RPC_WRAPPERS \
	ProceduralTest_Source_ProceduralTest_Corridor_h_20_CALLBACK_WRAPPERS \
	ProceduralTest_Source_ProceduralTest_Corridor_h_20_INCLASS \
	ProceduralTest_Source_ProceduralTest_Corridor_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProceduralTest_Source_ProceduralTest_Corridor_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_Corridor_h_20_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_Corridor_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_Corridor_h_20_CALLBACK_WRAPPERS \
	ProceduralTest_Source_ProceduralTest_Corridor_h_20_INCLASS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_Corridor_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProceduralTest_Source_ProceduralTest_Corridor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
