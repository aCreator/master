// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROCEDURALTEST_Corner_generated_h
#error "Corner.generated.h already included, missing '#pragma once' in Corner.h"
#endif
#define PROCEDURALTEST_Corner_generated_h

#define ProceduralTest_Source_ProceduralTest_Corner_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execTestMethod) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TestMethod(); \
		P_NATIVE_END; \
	}


#define ProceduralTest_Source_ProceduralTest_Corner_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execTestMethod) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TestMethod(); \
		P_NATIVE_END; \
	}


#define ProceduralTest_Source_ProceduralTest_Corner_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACorner(); \
	friend struct Z_Construct_UClass_ACorner_Statics; \
public: \
	DECLARE_CLASS(ACorner, AVisibleObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(ACorner)


#define ProceduralTest_Source_ProceduralTest_Corner_h_14_INCLASS \
private: \
	static void StaticRegisterNativesACorner(); \
	friend struct Z_Construct_UClass_ACorner_Statics; \
public: \
	DECLARE_CLASS(ACorner, AVisibleObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(ACorner)


#define ProceduralTest_Source_ProceduralTest_Corner_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACorner(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACorner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACorner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACorner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACorner(ACorner&&); \
	NO_API ACorner(const ACorner&); \
public:


#define ProceduralTest_Source_ProceduralTest_Corner_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACorner(ACorner&&); \
	NO_API ACorner(const ACorner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACorner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACorner); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACorner)


#define ProceduralTest_Source_ProceduralTest_Corner_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__wallMesh() { return STRUCT_OFFSET(ACorner, wallMesh); } \
	FORCEINLINE static uint32 __PPO__floorMesh() { return STRUCT_OFFSET(ACorner, floorMesh); } \
	FORCEINLINE static uint32 __PPO__ceilingMesh() { return STRUCT_OFFSET(ACorner, ceilingMesh); }


#define ProceduralTest_Source_ProceduralTest_Corner_h_11_PROLOG
#define ProceduralTest_Source_ProceduralTest_Corner_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_Corner_h_14_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_Corner_h_14_RPC_WRAPPERS \
	ProceduralTest_Source_ProceduralTest_Corner_h_14_INCLASS \
	ProceduralTest_Source_ProceduralTest_Corner_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProceduralTest_Source_ProceduralTest_Corner_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_Corner_h_14_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_Corner_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_Corner_h_14_INCLASS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_Corner_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProceduralTest_Source_ProceduralTest_Corner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
