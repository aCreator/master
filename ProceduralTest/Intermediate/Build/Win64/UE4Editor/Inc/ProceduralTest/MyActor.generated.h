// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROCEDURALTEST_MyActor_generated_h
#error "MyActor.generated.h already included, missing '#pragma once' in MyActor.h"
#endif
#define PROCEDURALTEST_MyActor_generated_h

#define ProceduralTest_Source_ProceduralTest_MyActor_h_15_RPC_WRAPPERS
#define ProceduralTest_Source_ProceduralTest_MyActor_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define ProceduralTest_Source_ProceduralTest_MyActor_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyActor(); \
	friend struct Z_Construct_UClass_AMyActor_Statics; \
public: \
	DECLARE_CLASS(AMyActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(AMyActor)


#define ProceduralTest_Source_ProceduralTest_MyActor_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMyActor(); \
	friend struct Z_Construct_UClass_AMyActor_Statics; \
public: \
	DECLARE_CLASS(AMyActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(AMyActor)


#define ProceduralTest_Source_ProceduralTest_MyActor_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyActor(AMyActor&&); \
	NO_API AMyActor(const AMyActor&); \
public:


#define ProceduralTest_Source_ProceduralTest_MyActor_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyActor(AMyActor&&); \
	NO_API AMyActor(const AMyActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyActor)


#define ProceduralTest_Source_ProceduralTest_MyActor_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__wallMaterial() { return STRUCT_OFFSET(AMyActor, wallMaterial); } \
	FORCEINLINE static uint32 __PPO__mesh() { return STRUCT_OFFSET(AMyActor, mesh); } \
	FORCEINLINE static uint32 __PPO__editRoom() { return STRUCT_OFFSET(AMyActor, editRoom); } \
	FORCEINLINE static uint32 __PPO__roomHeight() { return STRUCT_OFFSET(AMyActor, roomHeight); } \
	FORCEINLINE static uint32 __PPO__roomWidth() { return STRUCT_OFFSET(AMyActor, roomWidth); } \
	FORCEINLINE static uint32 __PPO__roomLength() { return STRUCT_OFFSET(AMyActor, roomLength); } \
	FORCEINLINE static uint32 __PPO__position() { return STRUCT_OFFSET(AMyActor, position); } \
	FORCEINLINE static uint32 __PPO__editWall() { return STRUCT_OFFSET(AMyActor, editWall); } \
	FORCEINLINE static uint32 __PPO__wallStartPoint() { return STRUCT_OFFSET(AMyActor, wallStartPoint); } \
	FORCEINLINE static uint32 __PPO__wallEndPoint() { return STRUCT_OFFSET(AMyActor, wallEndPoint); } \
	FORCEINLINE static uint32 __PPO__wallHeight() { return STRUCT_OFFSET(AMyActor, wallHeight); } \
	FORCEINLINE static uint32 __PPO__wallThickness() { return STRUCT_OFFSET(AMyActor, wallThickness); } \
	FORCEINLINE static uint32 __PPO__numberOfDoors() { return STRUCT_OFFSET(AMyActor, numberOfDoors); } \
	FORCEINLINE static uint32 __PPO__doorWidth() { return STRUCT_OFFSET(AMyActor, doorWidth); } \
	FORCEINLINE static uint32 __PPO__doorHeight() { return STRUCT_OFFSET(AMyActor, doorHeight); }


#define ProceduralTest_Source_ProceduralTest_MyActor_h_12_PROLOG
#define ProceduralTest_Source_ProceduralTest_MyActor_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_MyActor_h_15_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_MyActor_h_15_RPC_WRAPPERS \
	ProceduralTest_Source_ProceduralTest_MyActor_h_15_INCLASS \
	ProceduralTest_Source_ProceduralTest_MyActor_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProceduralTest_Source_ProceduralTest_MyActor_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_MyActor_h_15_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_MyActor_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_MyActor_h_15_INCLASS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_MyActor_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProceduralTest_Source_ProceduralTest_MyActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
