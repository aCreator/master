// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROCEDURALTEST_Door_generated_h
#error "Door.generated.h already included, missing '#pragma once' in Door.h"
#endif
#define PROCEDURALTEST_Door_generated_h

#define ProceduralTest_Source_ProceduralTest_Door_h_19_RPC_WRAPPERS
#define ProceduralTest_Source_ProceduralTest_Door_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define ProceduralTest_Source_ProceduralTest_Door_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADoor(); \
	friend struct Z_Construct_UClass_ADoor_Statics; \
public: \
	DECLARE_CLASS(ADoor, AVisibleObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(ADoor)


#define ProceduralTest_Source_ProceduralTest_Door_h_19_INCLASS \
private: \
	static void StaticRegisterNativesADoor(); \
	friend struct Z_Construct_UClass_ADoor_Statics; \
public: \
	DECLARE_CLASS(ADoor, AVisibleObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(ADoor)


#define ProceduralTest_Source_ProceduralTest_Door_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADoor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADoor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADoor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADoor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADoor(ADoor&&); \
	NO_API ADoor(const ADoor&); \
public:


#define ProceduralTest_Source_ProceduralTest_Door_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADoor(ADoor&&); \
	NO_API ADoor(const ADoor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADoor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADoor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADoor)


#define ProceduralTest_Source_ProceduralTest_Door_h_19_PRIVATE_PROPERTY_OFFSET
#define ProceduralTest_Source_ProceduralTest_Door_h_16_PROLOG
#define ProceduralTest_Source_ProceduralTest_Door_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_Door_h_19_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_Door_h_19_RPC_WRAPPERS \
	ProceduralTest_Source_ProceduralTest_Door_h_19_INCLASS \
	ProceduralTest_Source_ProceduralTest_Door_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProceduralTest_Source_ProceduralTest_Door_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_Door_h_19_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_Door_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_Door_h_19_INCLASS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_Door_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProceduralTest_Source_ProceduralTest_Door_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
