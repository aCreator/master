// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ProceduralTest/Hallway.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHallway() {}
// Cross Module References
	PROCEDURALTEST_API UClass* Z_Construct_UClass_AHallway_NoRegister();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_AHallway();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_AVisibleObject();
	UPackage* Z_Construct_UPackage__Script_ProceduralTest();
	PROCEDURALTEST_API UFunction* Z_Construct_UFunction_AHallway_TestMethod();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_UMaterial_NoRegister();
	PROCEDURALMESHCOMPONENT_API UClass* Z_Construct_UClass_UProceduralMeshComponent_NoRegister();
// End Cross Module References
	void AHallway::StaticRegisterNativesAHallway()
	{
		UClass* Class = AHallway::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "TestMethod", &AHallway::execTestMethod },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AHallway_TestMethod_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AHallway_TestMethod_Statics::Function_MetaDataParams[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "Hallway.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AHallway_TestMethod_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AHallway, "TestMethod", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AHallway_TestMethod_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AHallway_TestMethod_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AHallway_TestMethod()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AHallway_TestMethod_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AHallway_NoRegister()
	{
		return AHallway::StaticClass();
	}
	struct Z_Construct_UClass_AHallway_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_test_MetaData[];
#endif
		static void NewProp_test_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_test;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__end_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp__end;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__start_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp__start;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__height_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp__height;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__corridorWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp__corridorWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__ceilingMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__ceilingMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__floorMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__floorMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__wallMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__wallMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ceilingMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ceilingMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_floorMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_floorMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_wallMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_wallMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AHallway_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AVisibleObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ProceduralTest,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AHallway_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AHallway_TestMethod, "TestMethod" }, // 954932883
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHallway_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Hallway.h" },
		{ "ModuleRelativePath", "Hallway.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHallway_Statics::NewProp_test_MetaData[] = {
		{ "Category", "Hallway" },
		{ "ModuleRelativePath", "Hallway.h" },
	};
#endif
	void Z_Construct_UClass_AHallway_Statics::NewProp_test_SetBit(void* Obj)
	{
		((AHallway*)Obj)->test = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AHallway_Statics::NewProp_test = { UE4CodeGen_Private::EPropertyClass::Bool, "test", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AHallway), &Z_Construct_UClass_AHallway_Statics::NewProp_test_SetBit, METADATA_PARAMS(Z_Construct_UClass_AHallway_Statics::NewProp_test_MetaData, ARRAY_COUNT(Z_Construct_UClass_AHallway_Statics::NewProp_test_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHallway_Statics::NewProp__end_MetaData[] = {
		{ "Category", "Hallway" },
		{ "ModuleRelativePath", "Hallway.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AHallway_Statics::NewProp__end = { UE4CodeGen_Private::EPropertyClass::Struct, "_end", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AHallway, _end), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_AHallway_Statics::NewProp__end_MetaData, ARRAY_COUNT(Z_Construct_UClass_AHallway_Statics::NewProp__end_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHallway_Statics::NewProp__start_MetaData[] = {
		{ "Category", "Hallway" },
		{ "ModuleRelativePath", "Hallway.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AHallway_Statics::NewProp__start = { UE4CodeGen_Private::EPropertyClass::Struct, "_start", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AHallway, _start), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_AHallway_Statics::NewProp__start_MetaData, ARRAY_COUNT(Z_Construct_UClass_AHallway_Statics::NewProp__start_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHallway_Statics::NewProp__height_MetaData[] = {
		{ "Category", "Hallway" },
		{ "ModuleRelativePath", "Hallway.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AHallway_Statics::NewProp__height = { UE4CodeGen_Private::EPropertyClass::Float, "_height", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AHallway, _height), METADATA_PARAMS(Z_Construct_UClass_AHallway_Statics::NewProp__height_MetaData, ARRAY_COUNT(Z_Construct_UClass_AHallway_Statics::NewProp__height_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHallway_Statics::NewProp__corridorWidth_MetaData[] = {
		{ "Category", "Hallway" },
		{ "ModuleRelativePath", "Hallway.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AHallway_Statics::NewProp__corridorWidth = { UE4CodeGen_Private::EPropertyClass::Float, "_corridorWidth", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AHallway, _corridorWidth), METADATA_PARAMS(Z_Construct_UClass_AHallway_Statics::NewProp__corridorWidth_MetaData, ARRAY_COUNT(Z_Construct_UClass_AHallway_Statics::NewProp__corridorWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHallway_Statics::NewProp__ceilingMaterial_MetaData[] = {
		{ "Category", "Hallway" },
		{ "ModuleRelativePath", "Hallway.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AHallway_Statics::NewProp__ceilingMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "_ceilingMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AHallway, _ceilingMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AHallway_Statics::NewProp__ceilingMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_AHallway_Statics::NewProp__ceilingMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHallway_Statics::NewProp__floorMaterial_MetaData[] = {
		{ "Category", "Hallway" },
		{ "ModuleRelativePath", "Hallway.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AHallway_Statics::NewProp__floorMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "_floorMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AHallway, _floorMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AHallway_Statics::NewProp__floorMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_AHallway_Statics::NewProp__floorMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHallway_Statics::NewProp__wallMaterial_MetaData[] = {
		{ "Category", "Hallway" },
		{ "ModuleRelativePath", "Hallway.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AHallway_Statics::NewProp__wallMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "_wallMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AHallway, _wallMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AHallway_Statics::NewProp__wallMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_AHallway_Statics::NewProp__wallMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHallway_Statics::NewProp_ceilingMesh_MetaData[] = {
		{ "Category", "Hallway" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Hallway.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AHallway_Statics::NewProp_ceilingMesh = { UE4CodeGen_Private::EPropertyClass::Object, "ceilingMesh", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000080009, 1, nullptr, STRUCT_OFFSET(AHallway, ceilingMesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AHallway_Statics::NewProp_ceilingMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_AHallway_Statics::NewProp_ceilingMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHallway_Statics::NewProp_floorMesh_MetaData[] = {
		{ "Category", "Hallway" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Hallway.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AHallway_Statics::NewProp_floorMesh = { UE4CodeGen_Private::EPropertyClass::Object, "floorMesh", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000080009, 1, nullptr, STRUCT_OFFSET(AHallway, floorMesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AHallway_Statics::NewProp_floorMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_AHallway_Statics::NewProp_floorMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AHallway_Statics::NewProp_wallMesh_MetaData[] = {
		{ "Category", "Hallway" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Hallway.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AHallway_Statics::NewProp_wallMesh = { UE4CodeGen_Private::EPropertyClass::Object, "wallMesh", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000080009, 1, nullptr, STRUCT_OFFSET(AHallway, wallMesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AHallway_Statics::NewProp_wallMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_AHallway_Statics::NewProp_wallMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AHallway_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHallway_Statics::NewProp_test,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHallway_Statics::NewProp__end,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHallway_Statics::NewProp__start,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHallway_Statics::NewProp__height,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHallway_Statics::NewProp__corridorWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHallway_Statics::NewProp__ceilingMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHallway_Statics::NewProp__floorMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHallway_Statics::NewProp__wallMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHallway_Statics::NewProp_ceilingMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHallway_Statics::NewProp_floorMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AHallway_Statics::NewProp_wallMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AHallway_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AHallway>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AHallway_Statics::ClassParams = {
		&AHallway::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_AHallway_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AHallway_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AHallway_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AHallway_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AHallway()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AHallway_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AHallway, 364029461);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AHallway(Z_Construct_UClass_AHallway, &AHallway::StaticClass, TEXT("/Script/ProceduralTest"), TEXT("AHallway"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AHallway);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
