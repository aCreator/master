// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ProceduralTest/DebugCube.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDebugCube() {}
// Cross Module References
	PROCEDURALTEST_API UClass* Z_Construct_UClass_ADebugCube_NoRegister();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_ADebugCube();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_ProceduralTest();
	ENGINE_API UClass* Z_Construct_UClass_UMaterial_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void ADebugCube::StaticRegisterNativesADebugCube()
	{
	}
	UClass* Z_Construct_UClass_ADebugCube_NoRegister()
	{
		return ADebugCube::StaticClass();
	}
	struct Z_Construct_UClass_ADebugCube_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_material_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_material;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_sphereVisual_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_sphereVisual;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADebugCube_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ProceduralTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADebugCube_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DebugCube.h" },
		{ "ModuleRelativePath", "DebugCube.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADebugCube_Statics::NewProp_material_MetaData[] = {
		{ "Category", "DebugCube" },
		{ "ModuleRelativePath", "DebugCube.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADebugCube_Statics::NewProp_material = { UE4CodeGen_Private::EPropertyClass::Object, "material", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ADebugCube, material), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADebugCube_Statics::NewProp_material_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADebugCube_Statics::NewProp_material_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADebugCube_Statics::NewProp_sphereVisual_MetaData[] = {
		{ "Category", "DebugCube" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "DebugCube.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADebugCube_Statics::NewProp_sphereVisual = { UE4CodeGen_Private::EPropertyClass::Object, "sphereVisual", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000080009, 1, nullptr, STRUCT_OFFSET(ADebugCube, sphereVisual), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADebugCube_Statics::NewProp_sphereVisual_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADebugCube_Statics::NewProp_sphereVisual_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ADebugCube_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADebugCube_Statics::NewProp_material,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADebugCube_Statics::NewProp_sphereVisual,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADebugCube_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADebugCube>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADebugCube_Statics::ClassParams = {
		&ADebugCube::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		Z_Construct_UClass_ADebugCube_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ADebugCube_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ADebugCube_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ADebugCube_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADebugCube()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADebugCube_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADebugCube, 3989547898);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADebugCube(Z_Construct_UClass_ADebugCube, &ADebugCube::StaticClass, TEXT("/Script/ProceduralTest"), TEXT("ADebugCube"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADebugCube);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
