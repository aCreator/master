// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROCEDURALTEST_Testing_generated_h
#error "Testing.generated.h already included, missing '#pragma once' in Testing.h"
#endif
#define PROCEDURALTEST_Testing_generated_h

#define ProceduralTest_Source_ProceduralTest_Testing_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execTestMethod) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TestMethod(); \
		P_NATIVE_END; \
	}


#define ProceduralTest_Source_ProceduralTest_Testing_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execTestMethod) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TestMethod(); \
		P_NATIVE_END; \
	}


#define ProceduralTest_Source_ProceduralTest_Testing_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATesting(); \
	friend struct Z_Construct_UClass_ATesting_Statics; \
public: \
	DECLARE_CLASS(ATesting, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(ATesting)


#define ProceduralTest_Source_ProceduralTest_Testing_h_14_INCLASS \
private: \
	static void StaticRegisterNativesATesting(); \
	friend struct Z_Construct_UClass_ATesting_Statics; \
public: \
	DECLARE_CLASS(ATesting, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(ATesting)


#define ProceduralTest_Source_ProceduralTest_Testing_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATesting(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATesting) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATesting); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATesting); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATesting(ATesting&&); \
	NO_API ATesting(const ATesting&); \
public:


#define ProceduralTest_Source_ProceduralTest_Testing_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATesting(ATesting&&); \
	NO_API ATesting(const ATesting&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATesting); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATesting); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATesting)


#define ProceduralTest_Source_ProceduralTest_Testing_h_14_PRIVATE_PROPERTY_OFFSET
#define ProceduralTest_Source_ProceduralTest_Testing_h_11_PROLOG
#define ProceduralTest_Source_ProceduralTest_Testing_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_Testing_h_14_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_Testing_h_14_RPC_WRAPPERS \
	ProceduralTest_Source_ProceduralTest_Testing_h_14_INCLASS \
	ProceduralTest_Source_ProceduralTest_Testing_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProceduralTest_Source_ProceduralTest_Testing_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_Testing_h_14_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_Testing_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_Testing_h_14_INCLASS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_Testing_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProceduralTest_Source_ProceduralTest_Testing_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
