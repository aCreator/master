// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ACorridor;
class ADoor;
#ifdef PROCEDURALTEST_PathCreator_generated_h
#error "PathCreator.generated.h already included, missing '#pragma once' in PathCreator.h"
#endif
#define PROCEDURALTEST_PathCreator_generated_h

#define ProceduralTest_Source_ProceduralTest_PathCreator_h_26_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FROOMSTRUCT_Statics; \
	PROCEDURALTEST_API static class UScriptStruct* StaticStruct();


#define ProceduralTest_Source_ProceduralTest_PathCreator_h_84_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execInitialize) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Initialize(); \
		P_NATIVE_END; \
	}


#define ProceduralTest_Source_ProceduralTest_PathCreator_h_84_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execInitialize) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Initialize(); \
		P_NATIVE_END; \
	}


#define ProceduralTest_Source_ProceduralTest_PathCreator_h_84_EVENT_PARMS \
	struct PathCreator_eventActivateCorridor_Parms \
	{ \
		ACorridor* corridor; \
		ADoor* currentDoor; \
	}; \
	struct PathCreator_eventDeactivateCorridor_Parms \
	{ \
		ACorridor* corridor; \
		ADoor* currentDoor; \
	}; \
	struct PathCreator_eventDestroyCorridor_Parms \
	{ \
		ACorridor* corridor; \
	}; \
	struct PathCreator_eventPathCalculatedEvent_Parms \
	{ \
		ACorridor* corridor; \
	};


#define ProceduralTest_Source_ProceduralTest_PathCreator_h_84_CALLBACK_WRAPPERS
#define ProceduralTest_Source_ProceduralTest_PathCreator_h_84_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPathCreator(); \
	friend struct Z_Construct_UClass_APathCreator_Statics; \
public: \
	DECLARE_CLASS(APathCreator, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(APathCreator)


#define ProceduralTest_Source_ProceduralTest_PathCreator_h_84_INCLASS \
private: \
	static void StaticRegisterNativesAPathCreator(); \
	friend struct Z_Construct_UClass_APathCreator_Statics; \
public: \
	DECLARE_CLASS(APathCreator, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(APathCreator)


#define ProceduralTest_Source_ProceduralTest_PathCreator_h_84_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APathCreator(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APathCreator) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APathCreator); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APathCreator); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APathCreator(APathCreator&&); \
	NO_API APathCreator(const APathCreator&); \
public:


#define ProceduralTest_Source_ProceduralTest_PathCreator_h_84_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APathCreator(APathCreator&&); \
	NO_API APathCreator(const APathCreator&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APathCreator); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APathCreator); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APathCreator)


#define ProceduralTest_Source_ProceduralTest_PathCreator_h_84_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__runOnStart() { return STRUCT_OFFSET(APathCreator, runOnStart); } \
	FORCEINLINE static uint32 __PPO__trackingSpaceSize() { return STRUCT_OFFSET(APathCreator, trackingSpaceSize); } \
	FORCEINLINE static uint32 __PPO__wallThickness() { return STRUCT_OFFSET(APathCreator, wallThickness); } \
	FORCEINLINE static uint32 __PPO__corridorWidth() { return STRUCT_OFFSET(APathCreator, corridorWidth); } \
	FORCEINLINE static uint32 __PPO__corridorHeight() { return STRUCT_OFFSET(APathCreator, corridorHeight); } \
	FORCEINLINE static uint32 __PPO__wallHeight() { return STRUCT_OFFSET(APathCreator, wallHeight); } \
	FORCEINLINE static uint32 __PPO___amountOfDoors() { return STRUCT_OFFSET(APathCreator, _amountOfDoors); } \
	FORCEINLINE static uint32 __PPO__doorHeight() { return STRUCT_OFFSET(APathCreator, doorHeight); } \
	FORCEINLINE static uint32 __PPO__doorWidth() { return STRUCT_OFFSET(APathCreator, doorWidth); } \
	FORCEINLINE static uint32 __PPO__doorSpeed() { return STRUCT_OFFSET(APathCreator, doorSpeed); } \
	FORCEINLINE static uint32 __PPO__doorOpeningDistance() { return STRUCT_OFFSET(APathCreator, doorOpeningDistance); } \
	FORCEINLINE static uint32 __PPO___minimumDistanceBetweenDoors() { return STRUCT_OFFSET(APathCreator, _minimumDistanceBetweenDoors); } \
	FORCEINLINE static uint32 __PPO__rooms() { return STRUCT_OFFSET(APathCreator, rooms); } \
	FORCEINLINE static uint32 __PPO__addDoorFrame() { return STRUCT_OFFSET(APathCreator, addDoorFrame); } \
	FORCEINLINE static uint32 __PPO___corridorCeilingMaterial() { return STRUCT_OFFSET(APathCreator, _corridorCeilingMaterial); } \
	FORCEINLINE static uint32 __PPO___corridorWallMaterial() { return STRUCT_OFFSET(APathCreator, _corridorWallMaterial); } \
	FORCEINLINE static uint32 __PPO___corridorFloorMaterial() { return STRUCT_OFFSET(APathCreator, _corridorFloorMaterial); } \
	FORCEINLINE static uint32 __PPO__trackingAreaMaterial() { return STRUCT_OFFSET(APathCreator, trackingAreaMaterial); } \
	FORCEINLINE static uint32 __PPO__minimumCornerAmount() { return STRUCT_OFFSET(APathCreator, minimumCornerAmount); } \
	FORCEINLINE static uint32 __PPO__maximumCornerAmount() { return STRUCT_OFFSET(APathCreator, maximumCornerAmount); } \
	FORCEINLINE static uint32 __PPO__spawnCorridor() { return STRUCT_OFFSET(APathCreator, spawnCorridor); } \
	FORCEINLINE static uint32 __PPO__showAlgorithmVisualization() { return STRUCT_OFFSET(APathCreator, showAlgorithmVisualization); } \
	FORCEINLINE static uint32 __PPO__showNodes() { return STRUCT_OFFSET(APathCreator, showNodes); } \
	FORCEINLINE static uint32 __PPO__allowZigZags() { return STRUCT_OFFSET(APathCreator, allowZigZags); } \
	FORCEINLINE static uint32 __PPO__randomizeZigZagLength() { return STRUCT_OFFSET(APathCreator, randomizeZigZagLength); } \
	FORCEINLINE static uint32 __PPO__randomRoom() { return STRUCT_OFFSET(APathCreator, randomRoom); } \
	FORCEINLINE static uint32 __PPO__randomizeDirections() { return STRUCT_OFFSET(APathCreator, randomizeDirections); } \
	FORCEINLINE static uint32 __PPO__singleRoomTest() { return STRUCT_OFFSET(APathCreator, singleRoomTest); } \
	FORCEINLINE static uint32 __PPO__addDoor() { return STRUCT_OFFSET(APathCreator, addDoor); } \
	FORCEINLINE static uint32 __PPO__solutionsToCompare() { return STRUCT_OFFSET(APathCreator, solutionsToCompare); } \
	FORCEINLINE static uint32 __PPO__randomCorridorTest() { return STRUCT_OFFSET(APathCreator, randomCorridorTest); } \
	FORCEINLINE static uint32 __PPO__fixedCorridorTest() { return STRUCT_OFFSET(APathCreator, fixedCorridorTest); } \
	FORCEINLINE static uint32 __PPO__upperLimitOnly() { return STRUCT_OFFSET(APathCreator, upperLimitOnly); } \
	FORCEINLINE static uint32 __PPO__lowerLimitOnly() { return STRUCT_OFFSET(APathCreator, lowerLimitOnly); } \
	FORCEINLINE static uint32 __PPO__rebuildMap() { return STRUCT_OFFSET(APathCreator, rebuildMap); } \
	FORCEINLINE static uint32 __PPO__testRuns() { return STRUCT_OFFSET(APathCreator, testRuns); } \
	FORCEINLINE static uint32 __PPO__runMultipleTests() { return STRUCT_OFFSET(APathCreator, runMultipleTests); } \
	FORCEINLINE static uint32 __PPO__corridorLengthLowerLimit() { return STRUCT_OFFSET(APathCreator, corridorLengthLowerLimit); } \
	FORCEINLINE static uint32 __PPO__zigZagCorridorLengthLowerLimit() { return STRUCT_OFFSET(APathCreator, zigZagCorridorLengthLowerLimit); } \
	FORCEINLINE static uint32 __PPO__hallwayMinimumLength() { return STRUCT_OFFSET(APathCreator, hallwayMinimumLength); } \
	FORCEINLINE static uint32 __PPO__stepSize() { return STRUCT_OFFSET(APathCreator, stepSize); } \
	FORCEINLINE static uint32 __PPO__cornerCost() { return STRUCT_OFFSET(APathCreator, cornerCost); } \
	FORCEINLINE static uint32 __PPO__removeEverything() { return STRUCT_OFFSET(APathCreator, removeEverything); } \
	FORCEINLINE static uint32 __PPO__nodeTest() { return STRUCT_OFFSET(APathCreator, nodeTest); }


#define ProceduralTest_Source_ProceduralTest_PathCreator_h_81_PROLOG \
	ProceduralTest_Source_ProceduralTest_PathCreator_h_84_EVENT_PARMS


#define ProceduralTest_Source_ProceduralTest_PathCreator_h_84_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_PathCreator_h_84_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_PathCreator_h_84_RPC_WRAPPERS \
	ProceduralTest_Source_ProceduralTest_PathCreator_h_84_CALLBACK_WRAPPERS \
	ProceduralTest_Source_ProceduralTest_PathCreator_h_84_INCLASS \
	ProceduralTest_Source_ProceduralTest_PathCreator_h_84_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProceduralTest_Source_ProceduralTest_PathCreator_h_84_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_PathCreator_h_84_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_PathCreator_h_84_RPC_WRAPPERS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_PathCreator_h_84_CALLBACK_WRAPPERS \
	ProceduralTest_Source_ProceduralTest_PathCreator_h_84_INCLASS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_PathCreator_h_84_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProceduralTest_Source_ProceduralTest_PathCreator_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
