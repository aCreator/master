// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ProceduralTest/Corner.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCorner() {}
// Cross Module References
	PROCEDURALTEST_API UClass* Z_Construct_UClass_ACorner_NoRegister();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_ACorner();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_AVisibleObject();
	UPackage* Z_Construct_UPackage__Script_ProceduralTest();
	PROCEDURALTEST_API UFunction* Z_Construct_UFunction_ACorner_TestMethod();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_UMaterial_NoRegister();
	PROCEDURALMESHCOMPONENT_API UClass* Z_Construct_UClass_UProceduralMeshComponent_NoRegister();
// End Cross Module References
	void ACorner::StaticRegisterNativesACorner()
	{
		UClass* Class = ACorner::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "TestMethod", &ACorner::execTestMethod },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ACorner_TestMethod_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACorner_TestMethod_Statics::Function_MetaDataParams[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "Corner.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACorner_TestMethod_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACorner, "TestMethod", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACorner_TestMethod_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ACorner_TestMethod_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACorner_TestMethod()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACorner_TestMethod_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ACorner_NoRegister()
	{
		return ACorner::StaticClass();
	}
	struct Z_Construct_UClass_ACorner_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_test_MetaData[];
#endif
		static void NewProp_test_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_test;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__end_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp__end;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__start_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp__start;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__height_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp__height;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__corridorWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp__corridorWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__ceilingMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__ceilingMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__floorMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__floorMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__wallMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__wallMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ceilingMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ceilingMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_floorMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_floorMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_wallMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_wallMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACorner_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AVisibleObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ProceduralTest,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ACorner_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ACorner_TestMethod, "TestMethod" }, // 3549628974
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorner_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Corner.h" },
		{ "ModuleRelativePath", "Corner.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorner_Statics::NewProp_test_MetaData[] = {
		{ "Category", "Corner" },
		{ "ModuleRelativePath", "Corner.h" },
	};
#endif
	void Z_Construct_UClass_ACorner_Statics::NewProp_test_SetBit(void* Obj)
	{
		((ACorner*)Obj)->test = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ACorner_Statics::NewProp_test = { UE4CodeGen_Private::EPropertyClass::Bool, "test", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ACorner), &Z_Construct_UClass_ACorner_Statics::NewProp_test_SetBit, METADATA_PARAMS(Z_Construct_UClass_ACorner_Statics::NewProp_test_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorner_Statics::NewProp_test_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorner_Statics::NewProp__end_MetaData[] = {
		{ "Category", "Corner" },
		{ "ModuleRelativePath", "Corner.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ACorner_Statics::NewProp__end = { UE4CodeGen_Private::EPropertyClass::Struct, "_end", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ACorner, _end), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ACorner_Statics::NewProp__end_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorner_Statics::NewProp__end_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorner_Statics::NewProp__start_MetaData[] = {
		{ "Category", "Corner" },
		{ "ModuleRelativePath", "Corner.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ACorner_Statics::NewProp__start = { UE4CodeGen_Private::EPropertyClass::Struct, "_start", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ACorner, _start), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ACorner_Statics::NewProp__start_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorner_Statics::NewProp__start_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorner_Statics::NewProp__height_MetaData[] = {
		{ "Category", "Corner" },
		{ "ModuleRelativePath", "Corner.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ACorner_Statics::NewProp__height = { UE4CodeGen_Private::EPropertyClass::Float, "_height", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ACorner, _height), METADATA_PARAMS(Z_Construct_UClass_ACorner_Statics::NewProp__height_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorner_Statics::NewProp__height_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorner_Statics::NewProp__corridorWidth_MetaData[] = {
		{ "Category", "Corner" },
		{ "ModuleRelativePath", "Corner.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ACorner_Statics::NewProp__corridorWidth = { UE4CodeGen_Private::EPropertyClass::Float, "_corridorWidth", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ACorner, _corridorWidth), METADATA_PARAMS(Z_Construct_UClass_ACorner_Statics::NewProp__corridorWidth_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorner_Statics::NewProp__corridorWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorner_Statics::NewProp__ceilingMaterial_MetaData[] = {
		{ "Category", "Corner" },
		{ "ModuleRelativePath", "Corner.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACorner_Statics::NewProp__ceilingMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "_ceilingMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ACorner, _ceilingMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACorner_Statics::NewProp__ceilingMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorner_Statics::NewProp__ceilingMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorner_Statics::NewProp__floorMaterial_MetaData[] = {
		{ "Category", "Corner" },
		{ "ModuleRelativePath", "Corner.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACorner_Statics::NewProp__floorMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "_floorMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ACorner, _floorMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACorner_Statics::NewProp__floorMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorner_Statics::NewProp__floorMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorner_Statics::NewProp__wallMaterial_MetaData[] = {
		{ "Category", "Corner" },
		{ "ModuleRelativePath", "Corner.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACorner_Statics::NewProp__wallMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "_wallMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ACorner, _wallMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACorner_Statics::NewProp__wallMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorner_Statics::NewProp__wallMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorner_Statics::NewProp_ceilingMesh_MetaData[] = {
		{ "Category", "Corner" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Corner.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACorner_Statics::NewProp_ceilingMesh = { UE4CodeGen_Private::EPropertyClass::Object, "ceilingMesh", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000080009, 1, nullptr, STRUCT_OFFSET(ACorner, ceilingMesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACorner_Statics::NewProp_ceilingMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorner_Statics::NewProp_ceilingMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorner_Statics::NewProp_floorMesh_MetaData[] = {
		{ "Category", "Corner" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Corner.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACorner_Statics::NewProp_floorMesh = { UE4CodeGen_Private::EPropertyClass::Object, "floorMesh", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000080009, 1, nullptr, STRUCT_OFFSET(ACorner, floorMesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACorner_Statics::NewProp_floorMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorner_Statics::NewProp_floorMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorner_Statics::NewProp_wallMesh_MetaData[] = {
		{ "Category", "Corner" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Corner.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACorner_Statics::NewProp_wallMesh = { UE4CodeGen_Private::EPropertyClass::Object, "wallMesh", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000080009, 1, nullptr, STRUCT_OFFSET(ACorner, wallMesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACorner_Statics::NewProp_wallMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorner_Statics::NewProp_wallMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ACorner_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorner_Statics::NewProp_test,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorner_Statics::NewProp__end,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorner_Statics::NewProp__start,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorner_Statics::NewProp__height,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorner_Statics::NewProp__corridorWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorner_Statics::NewProp__ceilingMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorner_Statics::NewProp__floorMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorner_Statics::NewProp__wallMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorner_Statics::NewProp_ceilingMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorner_Statics::NewProp_floorMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorner_Statics::NewProp_wallMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACorner_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACorner>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACorner_Statics::ClassParams = {
		&ACorner::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ACorner_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ACorner_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ACorner_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ACorner_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACorner()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACorner_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACorner, 3040900082);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACorner(Z_Construct_UClass_ACorner, &ACorner::StaticClass, TEXT("/Script/ProceduralTest"), TEXT("ACorner"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACorner);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
