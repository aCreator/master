// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROCEDURALTEST_ProceduralTestGameModeBase_generated_h
#error "ProceduralTestGameModeBase.generated.h already included, missing '#pragma once' in ProceduralTestGameModeBase.h"
#endif
#define PROCEDURALTEST_ProceduralTestGameModeBase_generated_h

#define ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_15_RPC_WRAPPERS
#define ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAProceduralTestGameModeBase(); \
	friend struct Z_Construct_UClass_AProceduralTestGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AProceduralTestGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(AProceduralTestGameModeBase)


#define ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAProceduralTestGameModeBase(); \
	friend struct Z_Construct_UClass_AProceduralTestGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AProceduralTestGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(AProceduralTestGameModeBase)


#define ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProceduralTestGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProceduralTestGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProceduralTestGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProceduralTestGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProceduralTestGameModeBase(AProceduralTestGameModeBase&&); \
	NO_API AProceduralTestGameModeBase(const AProceduralTestGameModeBase&); \
public:


#define ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProceduralTestGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProceduralTestGameModeBase(AProceduralTestGameModeBase&&); \
	NO_API AProceduralTestGameModeBase(const AProceduralTestGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProceduralTestGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProceduralTestGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProceduralTestGameModeBase)


#define ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_12_PROLOG
#define ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_15_RPC_WRAPPERS \
	ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_15_INCLASS \
	ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProceduralTest_Source_ProceduralTest_ProceduralTestGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
