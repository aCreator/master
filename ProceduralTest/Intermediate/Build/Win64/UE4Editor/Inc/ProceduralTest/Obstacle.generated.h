// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROCEDURALTEST_Obstacle_generated_h
#error "Obstacle.generated.h already included, missing '#pragma once' in Obstacle.h"
#endif
#define PROCEDURALTEST_Obstacle_generated_h

#define ProceduralTest_Source_ProceduralTest_Obstacle_h_18_RPC_WRAPPERS
#define ProceduralTest_Source_ProceduralTest_Obstacle_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define ProceduralTest_Source_ProceduralTest_Obstacle_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAObstacle(); \
	friend struct Z_Construct_UClass_AObstacle_Statics; \
public: \
	DECLARE_CLASS(AObstacle, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(AObstacle)


#define ProceduralTest_Source_ProceduralTest_Obstacle_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAObstacle(); \
	friend struct Z_Construct_UClass_AObstacle_Statics; \
public: \
	DECLARE_CLASS(AObstacle, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProceduralTest"), NO_API) \
	DECLARE_SERIALIZER(AObstacle)


#define ProceduralTest_Source_ProceduralTest_Obstacle_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AObstacle(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AObstacle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AObstacle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AObstacle); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AObstacle(AObstacle&&); \
	NO_API AObstacle(const AObstacle&); \
public:


#define ProceduralTest_Source_ProceduralTest_Obstacle_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AObstacle(AObstacle&&); \
	NO_API AObstacle(const AObstacle&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AObstacle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AObstacle); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AObstacle)


#define ProceduralTest_Source_ProceduralTest_Obstacle_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__mesh() { return STRUCT_OFFSET(AObstacle, mesh); } \
	FORCEINLINE static uint32 __PPO___height() { return STRUCT_OFFSET(AObstacle, _height); } \
	FORCEINLINE static uint32 __PPO__obstacleVisual() { return STRUCT_OFFSET(AObstacle, obstacleVisual); }


#define ProceduralTest_Source_ProceduralTest_Obstacle_h_15_PROLOG
#define ProceduralTest_Source_ProceduralTest_Obstacle_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_Obstacle_h_18_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_Obstacle_h_18_RPC_WRAPPERS \
	ProceduralTest_Source_ProceduralTest_Obstacle_h_18_INCLASS \
	ProceduralTest_Source_ProceduralTest_Obstacle_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProceduralTest_Source_ProceduralTest_Obstacle_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProceduralTest_Source_ProceduralTest_Obstacle_h_18_PRIVATE_PROPERTY_OFFSET \
	ProceduralTest_Source_ProceduralTest_Obstacle_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_Obstacle_h_18_INCLASS_NO_PURE_DECLS \
	ProceduralTest_Source_ProceduralTest_Obstacle_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProceduralTest_Source_ProceduralTest_Obstacle_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
