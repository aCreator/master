// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ProceduralTest/Room.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRoom() {}
// Cross Module References
	PROCEDURALTEST_API UClass* Z_Construct_UClass_ARoom_NoRegister();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_ARoom();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_AVisibleObject();
	UPackage* Z_Construct_UPackage__Script_ProceduralTest();
	PROCEDURALTEST_API UFunction* Z_Construct_UFunction_ARoom_TestMethod();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
	ENGINE_API UClass* Z_Construct_UClass_UMaterial_NoRegister();
	PROCEDURALMESHCOMPONENT_API UClass* Z_Construct_UClass_UProceduralMeshComponent_NoRegister();
// End Cross Module References
	void ARoom::StaticRegisterNativesARoom()
	{
		UClass* Class = ARoom::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "TestMethod", &ARoom::execTestMethod },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ARoom_TestMethod_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARoom_TestMethod_Statics::Function_MetaDataParams[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARoom_TestMethod_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARoom, "TestMethod", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARoom_TestMethod_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARoom_TestMethod_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARoom_TestMethod()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARoom_TestMethod_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ARoom_NoRegister()
	{
		return ARoom::StaticClass();
	}
	struct Z_Construct_UClass_ARoom_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_stepSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_stepSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__maxLoops_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp__maxLoops;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__floorSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp__floorSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__minimumDistanceBetweenDoors_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp__minimumDistanceBetweenDoors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__doorSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp__doorSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__doorWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp__doorWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__doorHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp__doorHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__height_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp__height;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__corridorWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp__corridorWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__wallThickness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp__wallThickness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_id_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_randomPosition_MetaData[];
#endif
		static void NewProp_randomPosition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_randomPosition;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_createMap_MetaData[];
#endif
		static void NewProp_createMap_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_createMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_addDoor_MetaData[];
#endif
		static void NewProp_addDoor_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_addDoor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_showDoorPositions_MetaData[];
#endif
		static void NewProp_showDoorPositions_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_showDoorPositions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_test_MetaData[];
#endif
		static void NewProp_test_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_test;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__roomColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp__roomColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ceilingMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ceilingMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_floorMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_floorMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_wallMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_wallMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ceilingMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ceilingMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_floorMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_floorMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_wallMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_wallMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARoom_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AVisibleObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ProceduralTest,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ARoom_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ARoom_TestMethod, "TestMethod" }, // 2153124672
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Room.h" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp_stepSize_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp_stepSize = { UE4CodeGen_Private::EPropertyClass::Float, "stepSize", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ARoom, stepSize), METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp_stepSize_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp_stepSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp__maxLoops_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp__maxLoops = { UE4CodeGen_Private::EPropertyClass::Int, "_maxLoops", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ARoom, _maxLoops), METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp__maxLoops_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp__maxLoops_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp__floorSize_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp__floorSize = { UE4CodeGen_Private::EPropertyClass::Struct, "_floorSize", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ARoom, _floorSize), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp__floorSize_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp__floorSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp__minimumDistanceBetweenDoors_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp__minimumDistanceBetweenDoors = { UE4CodeGen_Private::EPropertyClass::Float, "_minimumDistanceBetweenDoors", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ARoom, _minimumDistanceBetweenDoors), METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp__minimumDistanceBetweenDoors_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp__minimumDistanceBetweenDoors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp__doorSpeed_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp__doorSpeed = { UE4CodeGen_Private::EPropertyClass::Float, "_doorSpeed", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ARoom, _doorSpeed), METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp__doorSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp__doorSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp__doorWidth_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp__doorWidth = { UE4CodeGen_Private::EPropertyClass::Float, "_doorWidth", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ARoom, _doorWidth), METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp__doorWidth_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp__doorWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp__doorHeight_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp__doorHeight = { UE4CodeGen_Private::EPropertyClass::Float, "_doorHeight", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ARoom, _doorHeight), METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp__doorHeight_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp__doorHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp__height_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp__height = { UE4CodeGen_Private::EPropertyClass::Float, "_height", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ARoom, _height), METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp__height_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp__height_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp__corridorWidth_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp__corridorWidth = { UE4CodeGen_Private::EPropertyClass::Float, "_corridorWidth", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ARoom, _corridorWidth), METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp__corridorWidth_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp__corridorWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp__wallThickness_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp__wallThickness = { UE4CodeGen_Private::EPropertyClass::Float, "_wallThickness", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ARoom, _wallThickness), METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp__wallThickness_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp__wallThickness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp_id_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp_id = { UE4CodeGen_Private::EPropertyClass::Int, "id", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ARoom, id), METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp_id_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp_id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp_randomPosition_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	void Z_Construct_UClass_ARoom_Statics::NewProp_randomPosition_SetBit(void* Obj)
	{
		((ARoom*)Obj)->randomPosition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp_randomPosition = { UE4CodeGen_Private::EPropertyClass::Bool, "randomPosition", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ARoom), &Z_Construct_UClass_ARoom_Statics::NewProp_randomPosition_SetBit, METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp_randomPosition_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp_randomPosition_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp_createMap_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	void Z_Construct_UClass_ARoom_Statics::NewProp_createMap_SetBit(void* Obj)
	{
		((ARoom*)Obj)->createMap = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp_createMap = { UE4CodeGen_Private::EPropertyClass::Bool, "createMap", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ARoom), &Z_Construct_UClass_ARoom_Statics::NewProp_createMap_SetBit, METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp_createMap_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp_createMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp_addDoor_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	void Z_Construct_UClass_ARoom_Statics::NewProp_addDoor_SetBit(void* Obj)
	{
		((ARoom*)Obj)->addDoor = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp_addDoor = { UE4CodeGen_Private::EPropertyClass::Bool, "addDoor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ARoom), &Z_Construct_UClass_ARoom_Statics::NewProp_addDoor_SetBit, METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp_addDoor_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp_addDoor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp_showDoorPositions_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	void Z_Construct_UClass_ARoom_Statics::NewProp_showDoorPositions_SetBit(void* Obj)
	{
		((ARoom*)Obj)->showDoorPositions = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp_showDoorPositions = { UE4CodeGen_Private::EPropertyClass::Bool, "showDoorPositions", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ARoom), &Z_Construct_UClass_ARoom_Statics::NewProp_showDoorPositions_SetBit, METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp_showDoorPositions_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp_showDoorPositions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp_test_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	void Z_Construct_UClass_ARoom_Statics::NewProp_test_SetBit(void* Obj)
	{
		((ARoom*)Obj)->test = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp_test = { UE4CodeGen_Private::EPropertyClass::Bool, "test", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ARoom), &Z_Construct_UClass_ARoom_Statics::NewProp_test_SetBit, METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp_test_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp_test_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp__roomColor_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp__roomColor = { UE4CodeGen_Private::EPropertyClass::Struct, "_roomColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ARoom, _roomColor), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp__roomColor_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp__roomColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp_ceilingMaterial_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp_ceilingMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "ceilingMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ARoom, ceilingMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp_ceilingMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp_ceilingMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp_floorMaterial_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp_floorMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "floorMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ARoom, floorMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp_floorMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp_floorMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp_wallMaterial_MetaData[] = {
		{ "Category", "Room" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp_wallMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "wallMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ARoom, wallMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp_wallMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp_wallMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp_ceilingMesh_MetaData[] = {
		{ "Category", "Room" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp_ceilingMesh = { UE4CodeGen_Private::EPropertyClass::Object, "ceilingMesh", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000080009, 1, nullptr, STRUCT_OFFSET(ARoom, ceilingMesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp_ceilingMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp_ceilingMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp_floorMesh_MetaData[] = {
		{ "Category", "Room" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp_floorMesh = { UE4CodeGen_Private::EPropertyClass::Object, "floorMesh", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000080009, 1, nullptr, STRUCT_OFFSET(ARoom, floorMesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp_floorMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp_floorMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARoom_Statics::NewProp_wallMesh_MetaData[] = {
		{ "Category", "Room" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Room.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARoom_Statics::NewProp_wallMesh = { UE4CodeGen_Private::EPropertyClass::Object, "wallMesh", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000080009, 1, nullptr, STRUCT_OFFSET(ARoom, wallMesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::NewProp_wallMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::NewProp_wallMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ARoom_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp_stepSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp__maxLoops,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp__floorSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp__minimumDistanceBetweenDoors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp__doorSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp__doorWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp__doorHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp__height,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp__corridorWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp__wallThickness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp_id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp_randomPosition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp_createMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp_addDoor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp_showDoorPositions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp_test,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp__roomColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp_ceilingMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp_floorMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp_wallMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp_ceilingMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp_floorMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARoom_Statics::NewProp_wallMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARoom_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARoom>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ARoom_Statics::ClassParams = {
		&ARoom::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ARoom_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ARoom_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ARoom_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARoom()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ARoom_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ARoom, 814580979);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARoom(Z_Construct_UClass_ARoom, &ARoom::StaticClass, TEXT("/Script/ProceduralTest"), TEXT("ARoom"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARoom);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
