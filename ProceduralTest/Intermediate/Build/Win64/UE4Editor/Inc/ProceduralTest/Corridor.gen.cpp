// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ProceduralTest/Corridor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCorridor() {}
// Cross Module References
	PROCEDURALTEST_API UClass* Z_Construct_UClass_ACorridor_NoRegister();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_ACorridor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_ProceduralTest();
	PROCEDURALTEST_API UFunction* Z_Construct_UFunction_ACorridor_WasActivated();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_AVisibilityManager_NoRegister();
	PROCEDURALTEST_API UFunction* Z_Construct_UFunction_ACorridor_WasDeactivated();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_ADoorFrame_NoRegister();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_ADoor_NoRegister();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_ARoom_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterial_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	static FName NAME_ACorridor_WasActivated = FName(TEXT("WasActivated"));
	void ACorridor::WasActivated(AVisibilityManager* currentRoomVM)
	{
		Corridor_eventWasActivated_Parms Parms;
		Parms.currentRoomVM=currentRoomVM;
		ProcessEvent(FindFunctionChecked(NAME_ACorridor_WasActivated),&Parms);
	}
	static FName NAME_ACorridor_WasDeactivated = FName(TEXT("WasDeactivated"));
	void ACorridor::WasDeactivated(AVisibilityManager* currentRoomVM)
	{
		Corridor_eventWasDeactivated_Parms Parms;
		Parms.currentRoomVM=currentRoomVM;
		ProcessEvent(FindFunctionChecked(NAME_ACorridor_WasDeactivated),&Parms);
	}
	void ACorridor::StaticRegisterNativesACorridor()
	{
	}
	struct Z_Construct_UFunction_ACorridor_WasActivated_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_currentRoomVM;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ACorridor_WasActivated_Statics::NewProp_currentRoomVM = { UE4CodeGen_Private::EPropertyClass::Object, "currentRoomVM", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(Corridor_eventWasActivated_Parms, currentRoomVM), Z_Construct_UClass_AVisibilityManager_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ACorridor_WasActivated_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACorridor_WasActivated_Statics::NewProp_currentRoomVM,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACorridor_WasActivated_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Corridor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACorridor_WasActivated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACorridor, "WasActivated", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020800, sizeof(Corridor_eventWasActivated_Parms), Z_Construct_UFunction_ACorridor_WasActivated_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ACorridor_WasActivated_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACorridor_WasActivated_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ACorridor_WasActivated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACorridor_WasActivated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACorridor_WasActivated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ACorridor_WasDeactivated_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_currentRoomVM;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ACorridor_WasDeactivated_Statics::NewProp_currentRoomVM = { UE4CodeGen_Private::EPropertyClass::Object, "currentRoomVM", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(Corridor_eventWasDeactivated_Parms, currentRoomVM), Z_Construct_UClass_AVisibilityManager_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ACorridor_WasDeactivated_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACorridor_WasDeactivated_Statics::NewProp_currentRoomVM,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACorridor_WasDeactivated_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Corridor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACorridor_WasDeactivated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACorridor, "WasDeactivated", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020800, sizeof(Corridor_eventWasDeactivated_Parms), Z_Construct_UFunction_ACorridor_WasDeactivated_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ACorridor_WasDeactivated_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACorridor_WasDeactivated_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ACorridor_WasDeactivated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACorridor_WasDeactivated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACorridor_WasDeactivated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ACorridor_NoRegister()
	{
		return ACorridor::StaticClass();
	}
	struct Z_Construct_UClass_ACorridor_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_playerInside_MetaData[];
#endif
		static void NewProp_playerInside_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_playerInside;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__endRoomVM_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__endRoomVM;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__startRoomVM_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__startRoomVM;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__endDoorFrame_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__endDoorFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__startDoorFrame_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__startDoorFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__endDoor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__endDoor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__startDoor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__startDoor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__endRoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__endRoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__startRoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__startRoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__corridorCeilingMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__corridorCeilingMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__corridorWallMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__corridorWallMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__corridorFloorMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__corridorFloorMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__corridorPoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp__corridorPoints;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp__corridorPoints_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACorridor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ProceduralTest,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ACorridor_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ACorridor_WasActivated, "WasActivated" }, // 2448624532
		{ &Z_Construct_UFunction_ACorridor_WasDeactivated, "WasDeactivated" }, // 2074522524
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorridor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Corridor.h" },
		{ "ModuleRelativePath", "Corridor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorridor_Statics::NewProp_playerInside_MetaData[] = {
		{ "Category", "Corridor" },
		{ "ModuleRelativePath", "Corridor.h" },
		{ "ToolTip", "AVisibilityManager* _endDoorVM;\nAVisibilityManager* _startDoorVM;" },
	};
#endif
	void Z_Construct_UClass_ACorridor_Statics::NewProp_playerInside_SetBit(void* Obj)
	{
		((ACorridor*)Obj)->playerInside = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ACorridor_Statics::NewProp_playerInside = { UE4CodeGen_Private::EPropertyClass::Bool, "playerInside", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ACorridor), &Z_Construct_UClass_ACorridor_Statics::NewProp_playerInside_SetBit, METADATA_PARAMS(Z_Construct_UClass_ACorridor_Statics::NewProp_playerInside_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorridor_Statics::NewProp_playerInside_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorridor_Statics::NewProp__endRoomVM_MetaData[] = {
		{ "Category", "Corridor" },
		{ "ModuleRelativePath", "Corridor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACorridor_Statics::NewProp__endRoomVM = { UE4CodeGen_Private::EPropertyClass::Object, "_endRoomVM", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(ACorridor, _endRoomVM), Z_Construct_UClass_AVisibilityManager_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACorridor_Statics::NewProp__endRoomVM_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorridor_Statics::NewProp__endRoomVM_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorridor_Statics::NewProp__startRoomVM_MetaData[] = {
		{ "Category", "Corridor" },
		{ "ModuleRelativePath", "Corridor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACorridor_Statics::NewProp__startRoomVM = { UE4CodeGen_Private::EPropertyClass::Object, "_startRoomVM", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(ACorridor, _startRoomVM), Z_Construct_UClass_AVisibilityManager_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACorridor_Statics::NewProp__startRoomVM_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorridor_Statics::NewProp__startRoomVM_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorridor_Statics::NewProp__endDoorFrame_MetaData[] = {
		{ "Category", "Corridor" },
		{ "ModuleRelativePath", "Corridor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACorridor_Statics::NewProp__endDoorFrame = { UE4CodeGen_Private::EPropertyClass::Object, "_endDoorFrame", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(ACorridor, _endDoorFrame), Z_Construct_UClass_ADoorFrame_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACorridor_Statics::NewProp__endDoorFrame_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorridor_Statics::NewProp__endDoorFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorridor_Statics::NewProp__startDoorFrame_MetaData[] = {
		{ "Category", "Corridor" },
		{ "ModuleRelativePath", "Corridor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACorridor_Statics::NewProp__startDoorFrame = { UE4CodeGen_Private::EPropertyClass::Object, "_startDoorFrame", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(ACorridor, _startDoorFrame), Z_Construct_UClass_ADoorFrame_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACorridor_Statics::NewProp__startDoorFrame_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorridor_Statics::NewProp__startDoorFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorridor_Statics::NewProp__endDoor_MetaData[] = {
		{ "Category", "Corridor" },
		{ "ModuleRelativePath", "Corridor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACorridor_Statics::NewProp__endDoor = { UE4CodeGen_Private::EPropertyClass::Object, "_endDoor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(ACorridor, _endDoor), Z_Construct_UClass_ADoor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACorridor_Statics::NewProp__endDoor_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorridor_Statics::NewProp__endDoor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorridor_Statics::NewProp__startDoor_MetaData[] = {
		{ "Category", "Corridor" },
		{ "ModuleRelativePath", "Corridor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACorridor_Statics::NewProp__startDoor = { UE4CodeGen_Private::EPropertyClass::Object, "_startDoor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(ACorridor, _startDoor), Z_Construct_UClass_ADoor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACorridor_Statics::NewProp__startDoor_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorridor_Statics::NewProp__startDoor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorridor_Statics::NewProp__endRoom_MetaData[] = {
		{ "Category", "Corridor" },
		{ "ModuleRelativePath", "Corridor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACorridor_Statics::NewProp__endRoom = { UE4CodeGen_Private::EPropertyClass::Object, "_endRoom", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(ACorridor, _endRoom), Z_Construct_UClass_ARoom_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACorridor_Statics::NewProp__endRoom_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorridor_Statics::NewProp__endRoom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorridor_Statics::NewProp__startRoom_MetaData[] = {
		{ "Category", "Corridor" },
		{ "ModuleRelativePath", "Corridor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACorridor_Statics::NewProp__startRoom = { UE4CodeGen_Private::EPropertyClass::Object, "_startRoom", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(ACorridor, _startRoom), Z_Construct_UClass_ARoom_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACorridor_Statics::NewProp__startRoom_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorridor_Statics::NewProp__startRoom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorridor_Statics::NewProp__corridorCeilingMaterial_MetaData[] = {
		{ "Category", "Corridor" },
		{ "ModuleRelativePath", "Corridor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACorridor_Statics::NewProp__corridorCeilingMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "_corridorCeilingMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ACorridor, _corridorCeilingMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACorridor_Statics::NewProp__corridorCeilingMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorridor_Statics::NewProp__corridorCeilingMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorridor_Statics::NewProp__corridorWallMaterial_MetaData[] = {
		{ "Category", "Corridor" },
		{ "ModuleRelativePath", "Corridor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACorridor_Statics::NewProp__corridorWallMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "_corridorWallMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ACorridor, _corridorWallMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACorridor_Statics::NewProp__corridorWallMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorridor_Statics::NewProp__corridorWallMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorridor_Statics::NewProp__corridorFloorMaterial_MetaData[] = {
		{ "Category", "Corridor" },
		{ "ModuleRelativePath", "Corridor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACorridor_Statics::NewProp__corridorFloorMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "_corridorFloorMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(ACorridor, _corridorFloorMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACorridor_Statics::NewProp__corridorFloorMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorridor_Statics::NewProp__corridorFloorMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACorridor_Statics::NewProp__corridorPoints_MetaData[] = {
		{ "Category", "Corridor" },
		{ "ModuleRelativePath", "Corridor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ACorridor_Statics::NewProp__corridorPoints = { UE4CodeGen_Private::EPropertyClass::Array, "_corridorPoints", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(ACorridor, _corridorPoints), METADATA_PARAMS(Z_Construct_UClass_ACorridor_Statics::NewProp__corridorPoints_MetaData, ARRAY_COUNT(Z_Construct_UClass_ACorridor_Statics::NewProp__corridorPoints_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ACorridor_Statics::NewProp__corridorPoints_Inner = { UE4CodeGen_Private::EPropertyClass::Struct, "_corridorPoints", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ACorridor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorridor_Statics::NewProp_playerInside,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorridor_Statics::NewProp__endRoomVM,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorridor_Statics::NewProp__startRoomVM,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorridor_Statics::NewProp__endDoorFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorridor_Statics::NewProp__startDoorFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorridor_Statics::NewProp__endDoor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorridor_Statics::NewProp__startDoor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorridor_Statics::NewProp__endRoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorridor_Statics::NewProp__startRoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorridor_Statics::NewProp__corridorCeilingMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorridor_Statics::NewProp__corridorWallMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorridor_Statics::NewProp__corridorFloorMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorridor_Statics::NewProp__corridorPoints,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACorridor_Statics::NewProp__corridorPoints_Inner,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACorridor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACorridor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACorridor_Statics::ClassParams = {
		&ACorridor::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ACorridor_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ACorridor_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ACorridor_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ACorridor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACorridor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACorridor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACorridor, 2436280271);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACorridor(Z_Construct_UClass_ACorridor, &ACorridor::StaticClass, TEXT("/Script/ProceduralTest"), TEXT("ACorridor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACorridor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
