// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ProceduralTest/PathCreator.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePathCreator() {}
// Cross Module References
	PROCEDURALTEST_API UScriptStruct* Z_Construct_UScriptStruct_FROOMSTRUCT();
	UPackage* Z_Construct_UPackage__Script_ProceduralTest();
	ENGINE_API UClass* Z_Construct_UClass_UMaterial_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_AVisibleObject_NoRegister();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_ACeilingLight_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_APathCreator_NoRegister();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_APathCreator();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	PROCEDURALTEST_API UFunction* Z_Construct_UFunction_APathCreator_ActivateCorridor();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_ADoor_NoRegister();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_ACorridor_NoRegister();
	PROCEDURALTEST_API UFunction* Z_Construct_UFunction_APathCreator_DeactivateCorridor();
	PROCEDURALTEST_API UFunction* Z_Construct_UFunction_APathCreator_DestroyCorridor();
	PROCEDURALTEST_API UFunction* Z_Construct_UFunction_APathCreator_Initialize();
	PROCEDURALTEST_API UFunction* Z_Construct_UFunction_APathCreator_PathCalculatedEvent();
// End Cross Module References
class UScriptStruct* FROOMSTRUCT::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PROCEDURALTEST_API uint32 Get_Z_Construct_UScriptStruct_FROOMSTRUCT_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FROOMSTRUCT, Z_Construct_UPackage__Script_ProceduralTest(), TEXT("ROOMSTRUCT"), sizeof(FROOMSTRUCT), Get_Z_Construct_UScriptStruct_FROOMSTRUCT_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FROOMSTRUCT(FROOMSTRUCT::StaticStruct, TEXT("/Script/ProceduralTest"), TEXT("ROOMSTRUCT"), false, nullptr, nullptr);
static struct FScriptStruct_ProceduralTest_StaticRegisterNativesFROOMSTRUCT
{
	FScriptStruct_ProceduralTest_StaticRegisterNativesFROOMSTRUCT()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("ROOMSTRUCT")),new UScriptStruct::TCppStructOps<FROOMSTRUCT>);
	}
} ScriptStruct_ProceduralTest_StaticRegisterNativesFROOMSTRUCT;
	struct Z_Construct_UScriptStruct_FROOMSTRUCT_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ceilingMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ceilingMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_floorMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_floorMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_wallMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_wallMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_props_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_props;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_props_Key_KeyProp;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_props_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_lightBlueprint_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_lightBlueprint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_roomColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_roomColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_doorColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_doorColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_connections_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_connections;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_connections_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_size_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_size;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FROOMSTRUCT>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_ceilingMaterial_MetaData[] = {
		{ "Category", "ROOMSTRUCT" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_ceilingMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "ceilingMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(FROOMSTRUCT, ceilingMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_ceilingMaterial_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_ceilingMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_floorMaterial_MetaData[] = {
		{ "Category", "ROOMSTRUCT" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_floorMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "floorMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(FROOMSTRUCT, floorMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_floorMaterial_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_floorMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_wallMaterial_MetaData[] = {
		{ "Category", "ROOMSTRUCT" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_wallMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "wallMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(FROOMSTRUCT, wallMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_wallMaterial_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_wallMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_props_MetaData[] = {
		{ "Category", "ROOMSTRUCT" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_props = { UE4CodeGen_Private::EPropertyClass::Map, "props", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0014000000000001, 1, nullptr, STRUCT_OFFSET(FROOMSTRUCT, props), METADATA_PARAMS(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_props_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_props_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_props_Key_KeyProp = { UE4CodeGen_Private::EPropertyClass::Struct, "props_Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000001, 1, nullptr, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_props_ValueProp = { UE4CodeGen_Private::EPropertyClass::Class, "props", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0004000000000001, 1, nullptr, 1, Z_Construct_UClass_AVisibleObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_lightBlueprint_MetaData[] = {
		{ "Category", "ROOMSTRUCT" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_lightBlueprint = { UE4CodeGen_Private::EPropertyClass::Class, "lightBlueprint", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0014000000000001, 1, nullptr, STRUCT_OFFSET(FROOMSTRUCT, lightBlueprint), Z_Construct_UClass_ACeilingLight_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_lightBlueprint_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_lightBlueprint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_roomColor_MetaData[] = {
		{ "Category", "ROOMSTRUCT" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_roomColor = { UE4CodeGen_Private::EPropertyClass::Struct, "roomColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(FROOMSTRUCT, roomColor), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_roomColor_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_roomColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_doorColor_MetaData[] = {
		{ "Category", "ROOMSTRUCT" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_doorColor = { UE4CodeGen_Private::EPropertyClass::Struct, "doorColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(FROOMSTRUCT, doorColor), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_doorColor_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_doorColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_connections_MetaData[] = {
		{ "Category", "ROOMSTRUCT" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_connections = { UE4CodeGen_Private::EPropertyClass::Array, "connections", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(FROOMSTRUCT, connections), METADATA_PARAMS(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_connections_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_connections_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_connections_Inner = { UE4CodeGen_Private::EPropertyClass::Int, "connections", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_size_MetaData[] = {
		{ "Category", "ROOMSTRUCT" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_size = { UE4CodeGen_Private::EPropertyClass::Struct, "size", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(FROOMSTRUCT, size), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_size_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_size_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_ceilingMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_floorMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_wallMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_props,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_props_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_props_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_lightBlueprint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_roomColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_doorColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_connections,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_connections_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::NewProp_size,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ProceduralTest,
		nullptr,
		&NewStructOps,
		"ROOMSTRUCT",
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		sizeof(FROOMSTRUCT),
		alignof(FROOMSTRUCT),
		Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::PropPointers),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FROOMSTRUCT()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FROOMSTRUCT_CRC();
		UPackage* Outer = Z_Construct_UPackage__Script_ProceduralTest();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ROOMSTRUCT"), sizeof(FROOMSTRUCT), Get_Z_Construct_UScriptStruct_FROOMSTRUCT_CRC(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FROOMSTRUCT_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FROOMSTRUCT_CRC() { return 1797761093U; }
	static FName NAME_APathCreator_ActivateCorridor = FName(TEXT("ActivateCorridor"));
	void APathCreator::ActivateCorridor(ACorridor* corridor, ADoor* currentDoor)
	{
		PathCreator_eventActivateCorridor_Parms Parms;
		Parms.corridor=corridor;
		Parms.currentDoor=currentDoor;
		ProcessEvent(FindFunctionChecked(NAME_APathCreator_ActivateCorridor),&Parms);
	}
	static FName NAME_APathCreator_DeactivateCorridor = FName(TEXT("DeactivateCorridor"));
	void APathCreator::DeactivateCorridor(ACorridor* corridor, ADoor* currentDoor)
	{
		PathCreator_eventDeactivateCorridor_Parms Parms;
		Parms.corridor=corridor;
		Parms.currentDoor=currentDoor;
		ProcessEvent(FindFunctionChecked(NAME_APathCreator_DeactivateCorridor),&Parms);
	}
	static FName NAME_APathCreator_DestroyCorridor = FName(TEXT("DestroyCorridor"));
	void APathCreator::DestroyCorridor(ACorridor* corridor)
	{
		PathCreator_eventDestroyCorridor_Parms Parms;
		Parms.corridor=corridor;
		ProcessEvent(FindFunctionChecked(NAME_APathCreator_DestroyCorridor),&Parms);
	}
	static FName NAME_APathCreator_PathCalculatedEvent = FName(TEXT("PathCalculatedEvent"));
	void APathCreator::PathCalculatedEvent(ACorridor* corridor)
	{
		PathCreator_eventPathCalculatedEvent_Parms Parms;
		Parms.corridor=corridor;
		ProcessEvent(FindFunctionChecked(NAME_APathCreator_PathCalculatedEvent),&Parms);
	}
	void APathCreator::StaticRegisterNativesAPathCreator()
	{
		UClass* Class = APathCreator::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Initialize", &APathCreator::execInitialize },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APathCreator_ActivateCorridor_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_currentDoor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_corridor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APathCreator_ActivateCorridor_Statics::NewProp_currentDoor = { UE4CodeGen_Private::EPropertyClass::Object, "currentDoor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(PathCreator_eventActivateCorridor_Parms, currentDoor), Z_Construct_UClass_ADoor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APathCreator_ActivateCorridor_Statics::NewProp_corridor = { UE4CodeGen_Private::EPropertyClass::Object, "corridor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(PathCreator_eventActivateCorridor_Parms, corridor), Z_Construct_UClass_ACorridor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APathCreator_ActivateCorridor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APathCreator_ActivateCorridor_Statics::NewProp_currentDoor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APathCreator_ActivateCorridor_Statics::NewProp_corridor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APathCreator_ActivateCorridor_Statics::Function_MetaDataParams[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APathCreator_ActivateCorridor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APathCreator, "ActivateCorridor", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020800, sizeof(PathCreator_eventActivateCorridor_Parms), Z_Construct_UFunction_APathCreator_ActivateCorridor_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APathCreator_ActivateCorridor_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APathCreator_ActivateCorridor_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APathCreator_ActivateCorridor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APathCreator_ActivateCorridor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APathCreator_ActivateCorridor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APathCreator_DeactivateCorridor_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_currentDoor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_corridor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APathCreator_DeactivateCorridor_Statics::NewProp_currentDoor = { UE4CodeGen_Private::EPropertyClass::Object, "currentDoor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(PathCreator_eventDeactivateCorridor_Parms, currentDoor), Z_Construct_UClass_ADoor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APathCreator_DeactivateCorridor_Statics::NewProp_corridor = { UE4CodeGen_Private::EPropertyClass::Object, "corridor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(PathCreator_eventDeactivateCorridor_Parms, corridor), Z_Construct_UClass_ACorridor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APathCreator_DeactivateCorridor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APathCreator_DeactivateCorridor_Statics::NewProp_currentDoor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APathCreator_DeactivateCorridor_Statics::NewProp_corridor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APathCreator_DeactivateCorridor_Statics::Function_MetaDataParams[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APathCreator_DeactivateCorridor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APathCreator, "DeactivateCorridor", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020800, sizeof(PathCreator_eventDeactivateCorridor_Parms), Z_Construct_UFunction_APathCreator_DeactivateCorridor_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APathCreator_DeactivateCorridor_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APathCreator_DeactivateCorridor_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APathCreator_DeactivateCorridor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APathCreator_DeactivateCorridor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APathCreator_DeactivateCorridor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APathCreator_DestroyCorridor_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_corridor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APathCreator_DestroyCorridor_Statics::NewProp_corridor = { UE4CodeGen_Private::EPropertyClass::Object, "corridor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(PathCreator_eventDestroyCorridor_Parms, corridor), Z_Construct_UClass_ACorridor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APathCreator_DestroyCorridor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APathCreator_DestroyCorridor_Statics::NewProp_corridor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APathCreator_DestroyCorridor_Statics::Function_MetaDataParams[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APathCreator_DestroyCorridor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APathCreator, "DestroyCorridor", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020800, sizeof(PathCreator_eventDestroyCorridor_Parms), Z_Construct_UFunction_APathCreator_DestroyCorridor_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APathCreator_DestroyCorridor_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APathCreator_DestroyCorridor_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APathCreator_DestroyCorridor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APathCreator_DestroyCorridor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APathCreator_DestroyCorridor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APathCreator_Initialize_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APathCreator_Initialize_Statics::Function_MetaDataParams[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APathCreator_Initialize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APathCreator, "Initialize", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APathCreator_Initialize_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APathCreator_Initialize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APathCreator_Initialize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APathCreator_Initialize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APathCreator_PathCalculatedEvent_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_corridor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APathCreator_PathCalculatedEvent_Statics::NewProp_corridor = { UE4CodeGen_Private::EPropertyClass::Object, "corridor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(PathCreator_eventPathCalculatedEvent_Parms, corridor), Z_Construct_UClass_ACorridor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APathCreator_PathCalculatedEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APathCreator_PathCalculatedEvent_Statics::NewProp_corridor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APathCreator_PathCalculatedEvent_Statics::Function_MetaDataParams[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APathCreator_PathCalculatedEvent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APathCreator, "PathCalculatedEvent", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020800, sizeof(PathCreator_eventPathCalculatedEvent_Parms), Z_Construct_UFunction_APathCreator_PathCalculatedEvent_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APathCreator_PathCalculatedEvent_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APathCreator_PathCalculatedEvent_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APathCreator_PathCalculatedEvent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APathCreator_PathCalculatedEvent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APathCreator_PathCalculatedEvent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APathCreator_NoRegister()
	{
		return APathCreator::StaticClass();
	}
	struct Z_Construct_UClass_APathCreator_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_nodeTest_MetaData[];
#endif
		static void NewProp_nodeTest_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_nodeTest;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_removeEverything_MetaData[];
#endif
		static void NewProp_removeEverything_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_removeEverything;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_cornerCost_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_cornerCost;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_stepSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_stepSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_hallwayMinimumLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_hallwayMinimumLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_zigZagCorridorLengthLowerLimit_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_zigZagCorridorLengthLowerLimit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_corridorLengthLowerLimit_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_corridorLengthLowerLimit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_runMultipleTests_MetaData[];
#endif
		static void NewProp_runMultipleTests_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_runMultipleTests;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_testRuns_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_testRuns;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_rebuildMap_MetaData[];
#endif
		static void NewProp_rebuildMap_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_rebuildMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_lowerLimitOnly_MetaData[];
#endif
		static void NewProp_lowerLimitOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_lowerLimitOnly;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_upperLimitOnly_MetaData[];
#endif
		static void NewProp_upperLimitOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_upperLimitOnly;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fixedCorridorTest_MetaData[];
#endif
		static void NewProp_fixedCorridorTest_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_fixedCorridorTest;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_randomCorridorTest_MetaData[];
#endif
		static void NewProp_randomCorridorTest_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_randomCorridorTest;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_solutionsToCompare_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_solutionsToCompare;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_addDoor_MetaData[];
#endif
		static void NewProp_addDoor_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_addDoor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_singleRoomTest_MetaData[];
#endif
		static void NewProp_singleRoomTest_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_singleRoomTest;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_randomizeDirections_MetaData[];
#endif
		static void NewProp_randomizeDirections_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_randomizeDirections;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_randomRoom_MetaData[];
#endif
		static void NewProp_randomRoom_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_randomRoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_randomizeZigZagLength_MetaData[];
#endif
		static void NewProp_randomizeZigZagLength_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_randomizeZigZagLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_allowZigZags_MetaData[];
#endif
		static void NewProp_allowZigZags_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_allowZigZags;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_showNodes_MetaData[];
#endif
		static void NewProp_showNodes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_showNodes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_showAlgorithmVisualization_MetaData[];
#endif
		static void NewProp_showAlgorithmVisualization_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_showAlgorithmVisualization;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_spawnCorridor_MetaData[];
#endif
		static void NewProp_spawnCorridor_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_spawnCorridor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_maximumCornerAmount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_maximumCornerAmount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_minimumCornerAmount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_minimumCornerAmount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_trackingAreaMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_trackingAreaMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__corridorFloorMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__corridorFloorMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__corridorWallMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__corridorWallMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__corridorCeilingMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp__corridorCeilingMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_addDoorFrame_MetaData[];
#endif
		static void NewProp_addDoorFrame_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_addDoorFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_rooms_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_rooms;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_rooms_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__minimumDistanceBetweenDoors_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp__minimumDistanceBetweenDoors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_doorOpeningDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_doorOpeningDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_doorSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_doorSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_doorWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_doorWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_doorHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_doorHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp__amountOfDoors_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp__amountOfDoors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_wallHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_wallHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_corridorHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_corridorHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_corridorWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_corridorWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_wallThickness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_wallThickness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_trackingSpaceSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_trackingSpaceSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_runOnStart_MetaData[];
#endif
		static void NewProp_runOnStart_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_runOnStart;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_curvedCorridors_MetaData[];
#endif
		static void NewProp_curvedCorridors_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_curvedCorridors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APathCreator_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ProceduralTest,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APathCreator_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APathCreator_ActivateCorridor, "ActivateCorridor" }, // 2485270717
		{ &Z_Construct_UFunction_APathCreator_DeactivateCorridor, "DeactivateCorridor" }, // 4209905483
		{ &Z_Construct_UFunction_APathCreator_DestroyCorridor, "DestroyCorridor" }, // 58031733
		{ &Z_Construct_UFunction_APathCreator_Initialize, "Initialize" }, // 1836335447
		{ &Z_Construct_UFunction_APathCreator_PathCalculatedEvent, "PathCalculatedEvent" }, // 623128108
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PathCreator.h" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_nodeTest_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_nodeTest_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->nodeTest = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_nodeTest = { UE4CodeGen_Private::EPropertyClass::Bool, "nodeTest", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_nodeTest_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_nodeTest_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_nodeTest_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_removeEverything_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_removeEverything_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->removeEverything = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_removeEverything = { UE4CodeGen_Private::EPropertyClass::Bool, "removeEverything", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_removeEverything_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_removeEverything_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_removeEverything_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_cornerCost_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_cornerCost = { UE4CodeGen_Private::EPropertyClass::Float, "cornerCost", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, cornerCost), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_cornerCost_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_cornerCost_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_stepSize_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_stepSize = { UE4CodeGen_Private::EPropertyClass::Float, "stepSize", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, stepSize), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_stepSize_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_stepSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_hallwayMinimumLength_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_hallwayMinimumLength = { UE4CodeGen_Private::EPropertyClass::Float, "hallwayMinimumLength", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, hallwayMinimumLength), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_hallwayMinimumLength_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_hallwayMinimumLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_zigZagCorridorLengthLowerLimit_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_zigZagCorridorLengthLowerLimit = { UE4CodeGen_Private::EPropertyClass::Float, "zigZagCorridorLengthLowerLimit", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, zigZagCorridorLengthLowerLimit), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_zigZagCorridorLengthLowerLimit_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_zigZagCorridorLengthLowerLimit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_corridorLengthLowerLimit_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_corridorLengthLowerLimit = { UE4CodeGen_Private::EPropertyClass::Float, "corridorLengthLowerLimit", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, corridorLengthLowerLimit), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_corridorLengthLowerLimit_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_corridorLengthLowerLimit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_runMultipleTests_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_runMultipleTests_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->runMultipleTests = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_runMultipleTests = { UE4CodeGen_Private::EPropertyClass::Bool, "runMultipleTests", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_runMultipleTests_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_runMultipleTests_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_runMultipleTests_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_testRuns_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_testRuns = { UE4CodeGen_Private::EPropertyClass::Int, "testRuns", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, testRuns), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_testRuns_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_testRuns_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_rebuildMap_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_rebuildMap_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->rebuildMap = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_rebuildMap = { UE4CodeGen_Private::EPropertyClass::Bool, "rebuildMap", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_rebuildMap_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_rebuildMap_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_rebuildMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_lowerLimitOnly_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_lowerLimitOnly_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->lowerLimitOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_lowerLimitOnly = { UE4CodeGen_Private::EPropertyClass::Bool, "lowerLimitOnly", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_lowerLimitOnly_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_lowerLimitOnly_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_lowerLimitOnly_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_upperLimitOnly_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_upperLimitOnly_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->upperLimitOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_upperLimitOnly = { UE4CodeGen_Private::EPropertyClass::Bool, "upperLimitOnly", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_upperLimitOnly_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_upperLimitOnly_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_upperLimitOnly_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_fixedCorridorTest_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_fixedCorridorTest_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->fixedCorridorTest = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_fixedCorridorTest = { UE4CodeGen_Private::EPropertyClass::Bool, "fixedCorridorTest", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_fixedCorridorTest_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_fixedCorridorTest_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_fixedCorridorTest_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_randomCorridorTest_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_randomCorridorTest_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->randomCorridorTest = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_randomCorridorTest = { UE4CodeGen_Private::EPropertyClass::Bool, "randomCorridorTest", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_randomCorridorTest_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_randomCorridorTest_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_randomCorridorTest_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_solutionsToCompare_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_solutionsToCompare = { UE4CodeGen_Private::EPropertyClass::Int, "solutionsToCompare", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, solutionsToCompare), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_solutionsToCompare_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_solutionsToCompare_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_addDoor_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_addDoor_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->addDoor = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_addDoor = { UE4CodeGen_Private::EPropertyClass::Bool, "addDoor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_addDoor_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_addDoor_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_addDoor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_singleRoomTest_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_singleRoomTest_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->singleRoomTest = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_singleRoomTest = { UE4CodeGen_Private::EPropertyClass::Bool, "singleRoomTest", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_singleRoomTest_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_singleRoomTest_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_singleRoomTest_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_randomizeDirections_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_randomizeDirections_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->randomizeDirections = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_randomizeDirections = { UE4CodeGen_Private::EPropertyClass::Bool, "randomizeDirections", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_randomizeDirections_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_randomizeDirections_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_randomizeDirections_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_randomRoom_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_randomRoom_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->randomRoom = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_randomRoom = { UE4CodeGen_Private::EPropertyClass::Bool, "randomRoom", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_randomRoom_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_randomRoom_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_randomRoom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_randomizeZigZagLength_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_randomizeZigZagLength_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->randomizeZigZagLength = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_randomizeZigZagLength = { UE4CodeGen_Private::EPropertyClass::Bool, "randomizeZigZagLength", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_randomizeZigZagLength_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_randomizeZigZagLength_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_randomizeZigZagLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_allowZigZags_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_allowZigZags_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->allowZigZags = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_allowZigZags = { UE4CodeGen_Private::EPropertyClass::Bool, "allowZigZags", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_allowZigZags_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_allowZigZags_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_allowZigZags_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_showNodes_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_showNodes_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->showNodes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_showNodes = { UE4CodeGen_Private::EPropertyClass::Bool, "showNodes", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_showNodes_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_showNodes_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_showNodes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_showAlgorithmVisualization_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_showAlgorithmVisualization_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->showAlgorithmVisualization = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_showAlgorithmVisualization = { UE4CodeGen_Private::EPropertyClass::Bool, "showAlgorithmVisualization", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_showAlgorithmVisualization_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_showAlgorithmVisualization_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_showAlgorithmVisualization_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_spawnCorridor_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_spawnCorridor_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->spawnCorridor = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_spawnCorridor = { UE4CodeGen_Private::EPropertyClass::Bool, "spawnCorridor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_spawnCorridor_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_spawnCorridor_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_spawnCorridor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_maximumCornerAmount_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_maximumCornerAmount = { UE4CodeGen_Private::EPropertyClass::Int, "maximumCornerAmount", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, maximumCornerAmount), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_maximumCornerAmount_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_maximumCornerAmount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_minimumCornerAmount_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_minimumCornerAmount = { UE4CodeGen_Private::EPropertyClass::Int, "minimumCornerAmount", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, minimumCornerAmount), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_minimumCornerAmount_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_minimumCornerAmount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_trackingAreaMaterial_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_trackingAreaMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "trackingAreaMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, trackingAreaMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_trackingAreaMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_trackingAreaMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp__corridorFloorMaterial_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp__corridorFloorMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "_corridorFloorMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, _corridorFloorMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp__corridorFloorMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp__corridorFloorMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp__corridorWallMaterial_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp__corridorWallMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "_corridorWallMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, _corridorWallMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp__corridorWallMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp__corridorWallMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp__corridorCeilingMaterial_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp__corridorCeilingMaterial = { UE4CodeGen_Private::EPropertyClass::Object, "_corridorCeilingMaterial", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, _corridorCeilingMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp__corridorCeilingMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp__corridorCeilingMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_addDoorFrame_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_addDoorFrame_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->addDoorFrame = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_addDoorFrame = { UE4CodeGen_Private::EPropertyClass::Bool, "addDoorFrame", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_addDoorFrame_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_addDoorFrame_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_addDoorFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_rooms_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_rooms = { UE4CodeGen_Private::EPropertyClass::Array, "rooms", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, rooms), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_rooms_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_rooms_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_rooms_Inner = { UE4CodeGen_Private::EPropertyClass::Struct, "rooms", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UScriptStruct_FROOMSTRUCT, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp__minimumDistanceBetweenDoors_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp__minimumDistanceBetweenDoors = { UE4CodeGen_Private::EPropertyClass::Float, "_minimumDistanceBetweenDoors", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, _minimumDistanceBetweenDoors), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp__minimumDistanceBetweenDoors_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp__minimumDistanceBetweenDoors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_doorOpeningDistance_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_doorOpeningDistance = { UE4CodeGen_Private::EPropertyClass::Float, "doorOpeningDistance", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, doorOpeningDistance), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_doorOpeningDistance_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_doorOpeningDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_doorSpeed_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_doorSpeed = { UE4CodeGen_Private::EPropertyClass::Float, "doorSpeed", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, doorSpeed), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_doorSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_doorSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_doorWidth_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_doorWidth = { UE4CodeGen_Private::EPropertyClass::Float, "doorWidth", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, doorWidth), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_doorWidth_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_doorWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_doorHeight_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_doorHeight = { UE4CodeGen_Private::EPropertyClass::Float, "doorHeight", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, doorHeight), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_doorHeight_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_doorHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp__amountOfDoors_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp__amountOfDoors = { UE4CodeGen_Private::EPropertyClass::Int, "_amountOfDoors", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, _amountOfDoors), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp__amountOfDoors_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp__amountOfDoors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_wallHeight_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_wallHeight = { UE4CodeGen_Private::EPropertyClass::Float, "wallHeight", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, wallHeight), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_wallHeight_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_wallHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_corridorHeight_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_corridorHeight = { UE4CodeGen_Private::EPropertyClass::Float, "corridorHeight", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, corridorHeight), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_corridorHeight_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_corridorHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_corridorWidth_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_corridorWidth = { UE4CodeGen_Private::EPropertyClass::Float, "corridorWidth", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, corridorWidth), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_corridorWidth_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_corridorWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_wallThickness_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_wallThickness = { UE4CodeGen_Private::EPropertyClass::Float, "wallThickness", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, wallThickness), METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_wallThickness_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_wallThickness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_trackingSpaceSize_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_trackingSpaceSize = { UE4CodeGen_Private::EPropertyClass::Struct, "trackingSpaceSize", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(APathCreator, trackingSpaceSize), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_trackingSpaceSize_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_trackingSpaceSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_runOnStart_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_runOnStart_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->runOnStart = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_runOnStart = { UE4CodeGen_Private::EPropertyClass::Bool, "runOnStart", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_runOnStart_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_runOnStart_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_runOnStart_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APathCreator_Statics::NewProp_curvedCorridors_MetaData[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "PathCreator.h" },
	};
#endif
	void Z_Construct_UClass_APathCreator_Statics::NewProp_curvedCorridors_SetBit(void* Obj)
	{
		((APathCreator*)Obj)->curvedCorridors = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APathCreator_Statics::NewProp_curvedCorridors = { UE4CodeGen_Private::EPropertyClass::Bool, "curvedCorridors", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APathCreator), &Z_Construct_UClass_APathCreator_Statics::NewProp_curvedCorridors_SetBit, METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::NewProp_curvedCorridors_MetaData, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::NewProp_curvedCorridors_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APathCreator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_nodeTest,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_removeEverything,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_cornerCost,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_stepSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_hallwayMinimumLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_zigZagCorridorLengthLowerLimit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_corridorLengthLowerLimit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_runMultipleTests,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_testRuns,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_rebuildMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_lowerLimitOnly,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_upperLimitOnly,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_fixedCorridorTest,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_randomCorridorTest,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_solutionsToCompare,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_addDoor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_singleRoomTest,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_randomizeDirections,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_randomRoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_randomizeZigZagLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_allowZigZags,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_showNodes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_showAlgorithmVisualization,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_spawnCorridor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_maximumCornerAmount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_minimumCornerAmount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_trackingAreaMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp__corridorFloorMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp__corridorWallMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp__corridorCeilingMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_addDoorFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_rooms,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_rooms_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp__minimumDistanceBetweenDoors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_doorOpeningDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_doorSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_doorWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_doorHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp__amountOfDoors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_wallHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_corridorHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_corridorWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_wallThickness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_trackingSpaceSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_runOnStart,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APathCreator_Statics::NewProp_curvedCorridors,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APathCreator_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APathCreator>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APathCreator_Statics::ClassParams = {
		&APathCreator::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_APathCreator_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_APathCreator_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APathCreator_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APathCreator()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APathCreator_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APathCreator, 3971941569);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APathCreator(Z_Construct_UClass_APathCreator, &APathCreator::StaticClass, TEXT("/Script/ProceduralTest"), TEXT("APathCreator"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APathCreator);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
