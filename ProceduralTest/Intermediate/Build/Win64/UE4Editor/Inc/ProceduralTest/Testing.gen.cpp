// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ProceduralTest/Testing.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTesting() {}
// Cross Module References
	PROCEDURALTEST_API UClass* Z_Construct_UClass_ATesting_NoRegister();
	PROCEDURALTEST_API UClass* Z_Construct_UClass_ATesting();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_ProceduralTest();
	PROCEDURALTEST_API UFunction* Z_Construct_UFunction_ATesting_TestMethod();
// End Cross Module References
	void ATesting::StaticRegisterNativesATesting()
	{
		UClass* Class = ATesting::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "TestMethod", &ATesting::execTestMethod },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ATesting_TestMethod_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATesting_TestMethod_Statics::Function_MetaDataParams[] = {
		{ "Category", "PathCreator" },
		{ "ModuleRelativePath", "Testing.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATesting_TestMethod_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATesting, "TestMethod", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATesting_TestMethod_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ATesting_TestMethod_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATesting_TestMethod()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATesting_TestMethod_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ATesting_NoRegister()
	{
		return ATesting::StaticClass();
	}
	struct Z_Construct_UClass_ATesting_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_startTest_MetaData[];
#endif
		static void NewProp_startTest_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_startTest;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATesting_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ProceduralTest,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ATesting_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ATesting_TestMethod, "TestMethod" }, // 3841160456
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATesting_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Testing.h" },
		{ "ModuleRelativePath", "Testing.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATesting_Statics::NewProp_startTest_MetaData[] = {
		{ "Category", "Testing" },
		{ "ModuleRelativePath", "Testing.h" },
	};
#endif
	void Z_Construct_UClass_ATesting_Statics::NewProp_startTest_SetBit(void* Obj)
	{
		((ATesting*)Obj)->startTest = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ATesting_Statics::NewProp_startTest = { UE4CodeGen_Private::EPropertyClass::Bool, "startTest", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ATesting), &Z_Construct_UClass_ATesting_Statics::NewProp_startTest_SetBit, METADATA_PARAMS(Z_Construct_UClass_ATesting_Statics::NewProp_startTest_MetaData, ARRAY_COUNT(Z_Construct_UClass_ATesting_Statics::NewProp_startTest_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATesting_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATesting_Statics::NewProp_startTest,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATesting_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATesting>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATesting_Statics::ClassParams = {
		&ATesting::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ATesting_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ATesting_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ATesting_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ATesting_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATesting()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATesting_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATesting, 1306696488);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATesting(Z_Construct_UClass_ATesting, &ATesting::StaticClass, TEXT("/Script/ProceduralTest"), TEXT("ATesting"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATesting);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
