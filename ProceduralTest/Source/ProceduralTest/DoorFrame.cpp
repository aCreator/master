// Fill out your copyright notice in the Description page of Project Settings.

#include "DoorFrame.h"
#include "Door.h"

ADoorFrame::ADoorFrame() {
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent* root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Object"));
	root->SetupAttachment(RootComponent);

	wallMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("WallMesh"));
	SetRootComponent(root);
	wallMesh->bUseAsyncCooking = true;
}

void ADoorFrame::Init(int roomID, FVector position, float frameWidth, float frameHeight, UMaterial * material, ADoor * door)
{
	Init(roomID, position, frameWidth, frameHeight, material, door->_doorWidth, door->_height, door->_forward);
}

void ADoorFrame::Init(int roomID, FVector position, float frameWidth, float frameHeight, UMaterial * material, float doorWidth, float doorHeight, FVector doorForward)
{
	deactivateCollisions = true;
	if (FMath::Abs(doorForward.X) > 0) {
		_width = 0;
		_length = frameWidth;
	}
	else {
		_width = frameWidth;
		_length = 0;
	}
	_roomID = roomID;
	_position = position;
	_frameWidth = frameWidth;
	_frameHeight = frameHeight;
	_material = material;
	SetActorLocation(_position);
	wallMesh->SetWorldLocation(_position);
	_doorWidth = doorWidth;
	_doorHeight = doorHeight;
	_doorForward = doorForward;
	CreateMesh();
}

void ADoorFrame::CreateMesh()
{
	wallMesh->SetMaterial(0, _material);
	meshCreator.SetMesh(wallMesh);
	meshCreator.ClearMesh();

	FVector wallDirection = FVector(-_doorForward.Y, _doorForward.X, 0);
	FVector p1 = - wallDirection * _frameWidth / 2.0f;
	FVector p2 = - wallDirection * _doorWidth / 2.0f;
	FVector p3 = p2;
	p3.Z += _doorHeight;

	FVector p4 = wallDirection * _doorWidth / 2.0f;
	FVector p5 = p4;
	p4.Z += _doorHeight;

	FVector p6 = wallDirection * _frameWidth / 2.0f;
	Wall firstWall = meshCreator.CreateWall(p1, p2, _frameHeight);
	float topBitHeight = _frameHeight - _doorHeight;
	Wall topWall = meshCreator.CreateWall(p3, p4, topBitHeight, firstWall);
	Wall lastWall = meshCreator.CreateWall(p5, p6, _frameHeight, topWall);
	meshCreator.AddWallToMesh(firstWall);
	meshCreator.AddWallToMesh(topWall);
	meshCreator.AddWallToMesh(lastWall);
	meshCreator.GenerateMesh();
	SetActorEnableCollision(false);
}

#if WITH_EDITOR  
void ADoorFrame::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	//UE_LOG(LogTemp, Warning, TEXT("butnc"));
	//mesh = GetOwner()->FindComponentByClass<UProceduralMeshComponent>();

	//Get the name of the property that was changed  
	FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(ADoorFrame, test))){
		Init(_roomID, GetActorLocation(), _frameWidth, _frameHeight, _material, _doorWidth, _doorHeight, _doorForward);
	}

	// Call the base class version  
	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif