// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MeshCreator.h"
#include "VisibleObject.h"
#include "Hallway.generated.h"

UCLASS()
class PROCEDURALTEST_API AHallway : public AVisibleObject
{
	GENERATED_BODY()
	
private:
	MeshCreator meshCreator;
	UPROPERTY(EditAnywhere)
		UProceduralMeshComponent* wallMesh = nullptr;
	UPROPERTY(EditAnywhere)
		UProceduralMeshComponent* floorMesh = nullptr;
	UPROPERTY(EditAnywhere)
		UProceduralMeshComponent* ceilingMesh = nullptr;

	void MakeWallsIntangibleToggle();

public:	
	// Sets default values for this actor's properties
	AHallway();
	
	FVector _direction;
	UFUNCTION(BlueprintCallable, Category = "PathCreator")
		void TestMethod();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterial * _wallMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterial * _floorMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterial * _ceilingMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float _corridorWidth = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float _height = 200;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector _start;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector _end = FVector(500,0,0);
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool test;
	void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void Init(FVector start, FVector end, float width, float height, UMaterial * floorMaterial, UMaterial * wallMaterial, UMaterial * ceilingMaterial);
	void CreateMesh();
};
