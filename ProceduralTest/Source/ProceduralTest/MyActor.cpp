// Fill out your copyright notice in the Description page of Project Settings.

#include "MyActor.h"
#include "ProceduralMeshComponent.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"

TArray<FVector> vertices;
TArray<int32> Triangles;
TArray<FVector> normals;
TArray<FVector2D> UV0;
TArray<FProcMeshTangent> tangents;
TArray<FLinearColor> vertexColors;
int triangleIndex = 0;
float uvSize = 100;

// Sets default values
AMyActor::AMyActor()
{
	//GetOwner()->FindComponentByClass<UInputComponent>();
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	SetRootComponent(mesh);
	/**
	*	Create/replace a section for this procedural mesh component.
	*	@param	SectionIndex		Index of the section to create or replace.
	*	@param	Vertices			Vertex buffer of all vertex positions to use for this mesh section.
	*	@param	Triangles			Index buffer indicating which vertices make up each triangle. Length must be a multiple of 3.
	*	@param	Normals				Optional array of normal vectors for each vertex. If supplied, must be same length as Vertices array.
	*	@param	UV0					Optional array of texture co-ordinates for each vertex. If supplied, must be same length as Vertices array.
	*	@param	VertexColors		Optional array of colors for each vertex. If supplied, must be same length as Vertices array.
	*	@param	Tangents			Optional array of tangent vector for each vertex. If supplied, must be same length as Vertices array.
	*	@param	bCreateCollision	Indicates whether collision should be created for this section. This adds significant cost.
	*/
	//UFUNCTION(BlueprintCallable, Category = "Components|ProceduralMesh", meta = (AutoCreateRefTerm = "Normals,UV0,VertexColors,Tangents"))
	//	void CreateMeshSection(int32 SectionIndex, const TArray<FVector>& Vertices, const TArray<int32>& Triangles, const TArray<FVector>& Normals,
	// const TArray<FVector2D>& UV0, const TArray<FColor>& VertexColors, const TArray<FProcMeshTangent>& Tangents, bool bCreateCollision);

	// New in UE 4.17, multi-threaded PhysX cooking.
	mesh->bUseAsyncCooking = true;
}

// This is called when actor is spawned (at runtime or when you drop it into the world in editor)
void AMyActor::PostActorCreated()
{
	UE_LOG(LogTemp, Warning, TEXT("Post Actor Created"));
	Super::PostActorCreated();
	//CreateTriangle(FVector(0, 0, 0), FVector(0, 200, 0), FVector(0, 50, 100), FVector2D(0.0f, 0.0f), FVector2D(2.0f, 0), FVector2D(0.5f, 1.0f));
	//CreateWall(FVector(0, 0, 0), FVector(0, 200, 0), 100);
	//CreateSimpleFloor(FVector(0, 0, 0), FVector(100, 100, 0), FVector2D(0, 0), FVector2D(10, 10));
	CreateSimpleRoom(FVector(0, 0, 0), FVector(200, 200, 0), 100);
	GenerateMesh();
}

void AMyActor::CreateSimpleHallway(FVector p1, FVector p2, float height) {
	CreateWall(p1, FVector(p1.X, p2.Y, 0), height);
	CreateWall(p1, FVector(p2.X, p1.Y, 0), height);
	CreateWall(p2, FVector(p2.X, p1.Y, 0), height);
	CreateWall(p2, FVector(p1.X, p2.Y, 0), height);

	CreateSimpleFloor(p1, p2);
	CreateSimpleFloor(FVector(p1.X, p1.Y, p1.Z + height), FVector(p2.X, p2.Y, p2.Z + height));
}

void AMyActor::ClearMesh()
{
	vertices.Empty();
	tangents.Empty();
	UV0.Empty();
	Triangles.Empty();
	normals.Empty();
	vertexColors.Empty();
	triangleIndex = 0;
	mesh->ClearAllMeshSections();
}

void AMyActor::CreateTriangle(FVector p1, FVector p2, FVector p3, FVector2D uv1, FVector2D uv2, FVector2D uv3, FVector tangent, FVector normal) {

	vertices.Add(p1);
	vertices.Add(p2);
	vertices.Add(p3);

	Triangles.Add(triangleIndex);
	triangleIndex++;
	Triangles.Add(triangleIndex);
	triangleIndex++;
	Triangles.Add(triangleIndex);
	triangleIndex++;

	/*FVector normal = GetNormal(p1, p2, p3);/*p2 - p1;//Calculating the normal of the wall
	float temp = normal.X;
	normal.X = normal.Y;
	normal.Y = -temp;
	normal.Normalize();
	*/
	normals.Add(-normal); //Adding the normal of the wall for each vertex
	normals.Add(-normal);
	normals.Add(-normal);

	tangent.Y = normal.X;
	tangent.X = -normal.Y;

	UV0.Add(uv1);
	UV0.Add(uv2);
	UV0.Add(uv3);

	tangents.Add(FProcMeshTangent(tangent.X, tangent.Y, tangent.Z));
	tangents.Add(FProcMeshTangent(tangent.X, tangent.Y, tangent.Z));
	tangents.Add(FProcMeshTangent(tangent.X, tangent.Y, tangent.Z));

	vertexColors.Add(FColor(100, 100, 100, 100));
	vertexColors.Add(FColor(100, 100, 100, 100));
	vertexColors.Add(FColor(100, 100, 100, 100));
}

void AMyActor::CreateTriangle(FVector p1, FVector p2, FVector p3, FVector2D uv1, FVector2D uv2, FVector2D uv3)
{
	FVector normal = GetNormal(p1, p2, p3);

	FVector tangent;
	tangent.Y = normal.X;
	tangent.X = -normal.Y;

	CreateTriangle(p1, p2, p3, uv1, uv2, uv3, tangent, normal);
}

Wall AMyActor::CreateWall(FVector p1, FVector p2, float roomHeight, Wall startWall) {
	FVector p3 = FVector(p2.X, p2.Y, p2.Z + roomHeight);
	FVector p4 = FVector(p1.X, p1.Y, p1.Z + roomHeight);
	float heightDifference = p1.Y + roomHeight - startWall.p3.Y;
	//FVector2D startUV = startWall.uv3;
	//startUV = FVector2D(startUV.X, startUV.Y - roomHeight / uvSize);
	FVector2D startUV = startWall.uv3;
	startUV = FVector2D(startUV.X, startUV.Y - heightDifference / uvSize);


	float wallLength = (p1 - p2).Size();
	FVector2D uv1 = startUV;
	FVector2D uv2 = FVector2D(wallLength, 0) / uvSize + startUV;
	FVector2D uv3 = FVector2D(wallLength, roomHeight) / uvSize + startUV;
	FVector2D uv4 = FVector2D(0, roomHeight) / uvSize + startUV;


	FVector normal = GetNormal(p1, p2, p3);

	FVector tangent;
	tangent.Y = normal.X;
	tangent.X = -normal.Y;


	Wall bla = Wall(p1, p2, p3, p4, normal, tangent, uv1, uv2, uv3, uv4);
	return bla;
}

Wall AMyActor::CreateSimpleFloor(FVector middlePosition, FVector2D dimensions, bool up)
{
	FVector p1 = FVector(middlePosition.X - dimensions.X / 2.0f, middlePosition.Y - dimensions.Y / 2.0f, middlePosition.Z);
	FVector p2 = FVector(middlePosition.X + dimensions.X / 2.0f, middlePosition.Y + dimensions.Y / 2.0f, middlePosition.Z);
	FVector p3 = FVector(p1.X, p2.Y, middlePosition.Z);
	FVector p4 = FVector(p2.X, p1.Y, middlePosition.Z);
	if (!up)
		return Wall(CreateSimpleFloor(p1, p2));
	else
		return Wall(CreateSimpleFloor(p3, p4));
}

void AMyActor::AddWallToMesh(Wall wall) {
	if (wall.thickness == 0) {
		//lower triangle
		//back
		//CreateTriangle(wall.p1, wall.p2, wall.p3, wall.uv1, wall.uv2, wall.uv3, wall.tangent, wall.normal);
		//front
		CreateTriangle(wall.p2, wall.p1, wall.p3, wall.uv2, wall.uv1, wall.uv3, wall.tangent, wall.normal);

		//upper triangle
		//back
		//CreateTriangle(wall.p3, wall.p4, wall.p1, wall.uv3, wall.uv4, wall.uv1, wall.tangent, wall.normal);
		//front
		CreateTriangle(wall.p3, wall.p1, wall.p4, wall.uv3, wall.uv1, wall.uv4, wall.tangent, wall.normal);
	}
	else {
		Wall wallFront = Wall(wall);
		wallFront.normal = wall.normal;
		wallFront.p1 = wall.p1 + (wall.normal * wall.thickness / 2.0f);
		wallFront.p2 = wall.p2 + (wall.normal * wall.thickness / 2.0f);
		wallFront.p3 = wall.p3 + (wall.normal * wall.thickness / 2.0f);
		wallFront.p4 = wall.p4 + (wall.normal * wall.thickness / 2.0f);

		wallFront.thickness = 0;

		AddWallToMesh(wallFront);

		Wall wallBack = Wall(wall);
		wallBack.normal = wall.normal;
		wallBack.p1 = wall.p1 - (wall.normal * wall.thickness / 2.0f);
		wallBack.p2 = wall.p2 - (wall.normal * wall.thickness / 2.0f);
		wallBack.p3 = wall.p3 - (wall.normal * wall.thickness / 2.0f);
		wallBack.p4 = wall.p4 - (wall.normal * wall.thickness / 2.0f);

		wallBack.thickness = 0;

		AddWallToMesh(wallBack);

		float wallHeight = wall.p4.Z - wall.p1.Z;

		//add ridges
		AddWallToMesh(CreateWall(wallBack.p1, wallFront.p1, wallHeight));
		AddWallToMesh(CreateWall(wallFront.p2, wallBack.p2, wallHeight));

		FVector bottomPosition = wallBack.p1 + (wallFront.p2 - wallBack.p1) / 2.0f;
		FVector2D dimensions;
		dimensions.X = FMath::Abs(wallBack.p1.X - wallFront.p2.X);
		dimensions.Y = FMath::Abs(wallBack.p1.Y - wallFront.p2.Y);
		Wall bottom = CreateSimpleFloor(bottomPosition, dimensions, false);
		AddWallToMesh(bottom);

		FVector topPosition = wallBack.p3 + (wallFront.p4 - wallBack.p3) / 2.0f;
		Wall top = CreateSimpleFloor(topPosition, dimensions, true);
		AddWallToMesh(top);
	}
}

Wall AMyActor::CreateWall(FVector p1, FVector p2, float roomHeight){
	return CreateWall(p1, p2, roomHeight, Wall());
}

FVector AMyActor::GetNormal(FVector v1, FVector v2, FVector v3)
{
	FVector a, b;
	a = v1 - v2;
	b = v1 - v3;
	FVector normal = FVector::CrossProduct(a, b);
	normal.Normalize();
	return normal;
}

Wall AMyActor::CreateSimpleFloor(FVector p1, FVector p2) {
	float floorLength = fabs(p1.Y - p2.Y);
	float floorWidth = fabs(p1.X - p2.X);

	FVector2D uv1 = FVector2D(0, 0) / uvSize;
	FVector2D uv2 = FVector2D(floorLength, 0) / uvSize;
	FVector2D uv3 = FVector2D(floorLength, floorWidth) / uvSize;
	FVector2D uv4 = FVector2D(0, floorWidth) / uvSize;
	FVector p3 = p2;
	p2 = FVector(p1.X, p2.Y, p2.Z);
	FVector p4 = FVector(p3.X, p1.Y, p1.Z);

	FVector normal = GetNormal(p1, p2, FVector(p1.X, p2.Y, p1.Z));
	
	FVector tangent;
	tangent.Y = normal.X;
	tangent.X = -normal.Y;
	
	//normal = FVector(0.0f, 0.0f, -1.0f);

	
	tangent = p2 - p1;
	tangent.Normalize();

	//lower triangle
	//front
	//CreateTriangle(p1, p2, p3, uv1, uv2, uv3, tangent, normal);
	//back
	//CreateTriangle(p2, p1, p3, uv2, uv1, uv3, -tangent, -normal);

	//upper triangle
	//front
	//CreateTriangle(p4, p1, p3, uv4, uv1, uv3, tangent, normal);
	//back
	//CreateTriangle(p4, p3, p1, uv4, uv3, uv1, -tangent, -normal);

	return Wall(p1, p2, p3, p4, normal, tangent, uv1, uv2, uv3, uv4);
}

void AMyActor::CreateWallWithCorners(FVector p1, FVector p2, float height, Wall startWall) {
	CreateWall(p1, FVector(p1.X, p2.Y, p2.Z), height, startWall);
	CreateWall(p1, FVector(p2.X, p1.Y, p1.Z), height, startWall);
	CreateWall(p2, FVector(p2.X, p1.Y, p2.Z), height, startWall);
	CreateWall(p2, FVector(p1.X, p2.Y, p1.Z), height, startWall);

	CreateSimpleFloor(p1, p2);
	CreateSimpleFloor(FVector(p1.X, p1.Y, p1.Z + height), FVector(p2.X, p2.Y, p2.Z + height));
}

void AMyActor::CreateSimpleRoom(FVector p1, FVector p2, float height) {
	CreateWallsWithDoors(p1, FVector(p1.X, p2.Y, 0), height, wallThickness, numberOfDoors, doorHeight, doorWidth);
	CreateWallsWithDoors(p1, FVector(p2.X, p1.Y, 0), height, wallThickness, numberOfDoors, doorHeight, doorWidth);
	CreateWallsWithDoors(p2, FVector(p2.X, p1.Y, 0), height, wallThickness, numberOfDoors, doorHeight, doorWidth);
	CreateWallsWithDoors(p2, FVector(p1.X, p2.Y, 0), height, wallThickness, numberOfDoors, doorHeight, doorWidth);

	AddWallToMesh(CreateSimpleFloor(p2, p1));
	AddWallToMesh(CreateSimpleFloor(FVector(p1.X, p1.Y, p1.Z + height), FVector(p2.X, p2.Y, p2.Z + height)));
}

Wall AMyActor::CreateWall(FVector p1, FVector p2, float roomHeight, float thickness, Wall startWall) {
	/*FVector2D startUV = startWall.uv2;
	float wallLength = (p1 - p2).Size();
	FVector2D uv = FVector2D(wallLength, roomHeight) / uvSize + startUV;

	FVector p3 = FVector(p2.X, p2.Y, p2.Z + roomHeight);
	FVector normal = GetNormal(p1, p2, p3);
	FVector newP1 = p1 - (normal * thickness / 2.0f);
	FVector newP2 = p2 + (normal * thickness / 2.0f);
	CreateWallWithCorners(newP1, newP2, roomHeight, startUV);
	*/
	Wall wall = CreateWall(p1, p2, roomHeight, startWall);
	wall.thickness = thickness;
	return wall;
}

Wall AMyActor::CreateWall(FVector p1, FVector p2, float roomHeight, float thickness) {
	return CreateWall(p1, p2, roomHeight, thickness, Wall());
}

void AMyActor::CreateWallsWithDoors(FVector p1, FVector p2, float roomHeight, float thickness, int numberOfDoors, float doorHeight, float doorWidth) {
	FVector wallVector = p2 - p1;
	float wallLength = wallVector.Size();
	FVector wallDirection = wallVector;
	wallDirection.Normalize();
	FVector wallStartPoint = p1;
	float wallStart = thickness;
	
	while (wallLength - thickness * 2 < numberOfDoors * doorWidth) {
		numberOfDoors--;
	}
	Wall previousWall = Wall();
	//UE_LOG(LogTemp, Warning, TEXT("previousUV: %f"), previousUV.X);

	while (numberOfDoors > 0) {
		if (wallLength - thickness * 2 < numberOfDoors * doorWidth)
			break;
		float doorStart = FMath::FRandRange(wallStart, wallStart + wallLength/numberOfDoors - (thickness + doorWidth));
		FVector doorStartPoint = p1 + wallDirection * doorStart;
		previousWall = CreateWall(wallStartPoint, doorStartPoint, roomHeight, thickness, previousWall);
		//UE_LOG(LogTemp, Warning, TEXT("previousUV: %f"), previousUV.X);
		wallStartPoint = doorStartPoint + wallDirection * doorWidth;
		wallStart = doorStart + doorWidth;
		wallLength -= wallStart;
		numberOfDoors--;
		AddWallToMesh(previousWall);

		//add area above door
		previousWall = CreateWall(doorStartPoint + FVector(0 ,0 ,doorHeight) , wallStartPoint + FVector(0, 0, doorHeight), roomHeight - doorHeight, thickness, previousWall);
		AddWallToMesh(previousWall);
	}
	AddWallToMesh(CreateWall(wallStartPoint, p2, roomHeight, thickness, previousWall));
}

void AMyActor::GenerateMesh()
{
	//UMaterialInstanceDynamic* dynamicMaterial = UMaterialInstanceDynamic::Create();
	mesh->CreateMeshSection_LinearColor(0, vertices, Triangles, normals, UV0, vertexColors, tangents, true);

	// Enable collision data
	mesh->ContainsPhysicsTriMeshData(true);
}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();
	//GetOwner()->SetActorLabel("Banana");
}

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

#if WITH_EDITOR  
void AMyActor::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	//UE_LOG(LogTemp, Warning, TEXT("butnc"));
	//mesh = GetOwner()->FindComponentByClass<UProceduralMeshComponent>();

	//Get the name of the property that was changed  
	FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(AMyActor, editWall))) {
		editRoom = false;
	}

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(AMyActor, editRoom))) {
		editWall = false;
	}
	if (editRoom) {
		ClearMesh();
		CreateSimpleRoom(FVector(0, 0, 0), FVector(roomLength, roomWidth, 0), roomHeight);
		GenerateMesh();
	}
	if (editWall) {
		ClearMesh();
		//CreateWall(wallStartPoint, wallEndPoint, wallHeight, wallThickness, numberOfDoors, doorHeight, doorWidth);
		Wall uv = CreateWall(wallStartPoint, wallEndPoint, wallHeight, wallThickness, Wall());
		AddWallToMesh(uv);
		/*FVector vector = wallEndPoint - wallStartPoint;
		//uv = CreateWall(wallEndPoint + FVector(0,0,wallHeight*2/3), wallEndPoint + vector + FVector(0, 0, wallHeight * 2 / 3), wallHeight / 3, wallThickness, FVector2D(uv.X, uv.Y - wallHeight/300));
		uv = CreateWall(wallEndPoint, wallEndPoint + vector, wallHeight, uv);
		AddWallToMesh(uv);
		AddWallToMesh(CreateWall(wallEndPoint + vector, wallEndPoint + vector * 2, wallHeight, uv));
		//CreateWall(wallEndPoint, wallEndPoint + vector, wallHeight, wallThickness, FVector2D(uv.X, 0));
		*/
		GenerateMesh();
	}
	mesh->SetMaterial(0, wallMaterial);

	// Call the base class version  
	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif