// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "NodeMap.h"
#include "Runtime/Engine/Classes/Engine/World.h"

struct DOORMAP;
struct DOORPOS {
	FIntVector position;
	FIntVector forward;
	TArray<FVector> possibleRoomPositions;
	DOORPOS(FIntVector pos, FIntVector direction)
	{
		position = pos;
		forward = direction;
	}
};

/**
 * 
 */
class PROCEDURALTEST_API RoomTemplate
{
private:
	TSharedPtr<DOORMAP> doorMap;
	TArray<DOORPOS> possibleDoorPositions;

public:
	RoomTemplate();
	RoomTemplate(UWorld * world, NodeMap * nodeMap, FVector size, float corridorWidth, float doorWidth, float doorHeight, float minimumDistanceBetweenDoors, float wallThickness, int minimumSpaceForBreakout);
	~RoomTemplate();

	UWorld * world; 
	FVector _size;
	FVector trackingSpaceSize; 
	float corridorWidth; 
	float doorWidth; 
	float doorHeight; 
	float minimumDistanceBetweenDoors; 
	float wallThickness;
	float stepSize;
	int minimumSpaceForBreakout;
	FVector firstNodePos;
	NodeMap * nodeMap;
	TArray<FVector> possibleRoomPositions;


	void FindAllPossibleRoomAndDoorPositions(FVector trackingSpaceSize, float stepSize);
	void GetPossibleDoors(FVector position);
	bool CheckIfValidDoorPosition(FIntVector doorPos, FIntVector doorDirection);
	TArray<FVector> & GetPossibleRoomPositions(FIntVector doorPos, FIntVector doorDir);
	void ShowAllDoorPositions();
	FVector GetClosestRoomPosition(FVector point);
};
