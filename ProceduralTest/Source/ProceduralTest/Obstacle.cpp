// Fill out your copyright notice in the Description page of Project Settings.

#include "Obstacle.h"
#include "Runtime/Engine/Classes/Engine/World.h"

// Sets default values
AObstacle::AObstacle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	USceneComponent* root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Object"));
	root->SetupAttachment(RootComponent);
	SetRootComponent(root);

	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("FloorMesh"));
	mesh->bUseAsyncCooking = true;
	mesh->SetupAttachment(root);

	obstacleVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Debug Sphere Visualisation"));
	obstacleVisual->SetupAttachment(root);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> obstacleVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube"));
	if (obstacleVisualAsset.Succeeded()) {
		obstacleVisual->SetStaticMesh(obstacleVisualAsset.Object);
		obstacleVisual->AddWorldOffset(FVector(0, 0, -50));
	}
}

FBoxSphereBounds AObstacle::GetBoundingBox()
{

	return obstacleVisual->CalcBounds(GetTransform());
}

void AObstacle::AddTestSphere(FVector position)
{
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	UWorld* world = GetWorld();
	ADebugSphere* debugSphere;
	debugSphere = GetWorld()->SpawnActor<ADebugSphere>(position, Rotation, SpawnInfo);
	debugSphere->SetActorScale3D(FVector(0.2f, 0.2f, 0.2f));
	debugSphere->SetActorLabel(*FString("Test Sphere"));
	actorsToDestroy.Add(debugSphere);
}

// Called when the game starts or when spawned
void AObstacle::BeginPlay()
{
	Super::BeginPlay();
	
}

void AObstacle::CreateMesh()
{
	if (corners.Num() < 2)
		return;
	meshCreator.SetMesh(mesh);
	meshCreator.ClearMesh();
	mesh->SetWorldLocation(FVector(0,0,0));

	FVector p1;
	FVector p2;
	for (int i = 0; i < corners.Num() - 1; i++)
	{
		p1 = corners[i];
		p2 = corners[i+1];
		CreateWallMesh(p1, p2);
	}
	p1 = corners[corners.Num() - 1];
	p2 = corners[0];
	CreateWallMesh(p1, p2);
	meshCreator.GenerateMesh();
}

void AObstacle::CreateWallMesh(FVector p1, FVector p2)
{
	Wall wall = meshCreator.CreateWall(p1 , p2 , _height);
	meshCreator.AddWallToMesh(wall, true);
}

#if WITH_EDITOR  
void AObstacle::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	//UE_LOG(LogTemp, Warning, TEXT("butnc"));
	//mesh = GetOwner()->FindComponentByClass<UProceduralMeshComponent>();

	//Get the name of the property that was changed  
	FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	/*if ((PropertyName == GET_MEMBER_NAME_CHECKED(AObstacle, corners))) {
		CreateMesh();
	}*/

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(AObstacle, _width)) ||
		(PropertyName == GET_MEMBER_NAME_CHECKED(AObstacle, _height)) ||
		(PropertyName == GET_MEMBER_NAME_CHECKED(AObstacle, _length)) ||
		(PropertyName == GET_MEMBER_NAME_CHECKED(AObstacle, reset))) {
		FTransform transform = GetTransform();
		FVector scale = transform.GetScale3D();
		scale.X = _width / 100.0f;
		scale.Y = _length / 100.0f;
		scale.Z = _height / 100.0f;

		transform.SetScale3D(scale);
		SetActorTransform(transform);
		//CreateMesh();
		FBoxSphereBounds box = GetBoundingBox();

		UE_LOG(LogTemp, Warning, TEXT("Box size X: %f, Y: %f, Z: %f"), box.BoxExtent.X, box.BoxExtent.Y, box.BoxExtent.Z);
		UE_LOG(LogTemp, Warning, TEXT("Box position X: %f, Y: %f, Z: %f"), box.GetBox().GetCenter().X, box.GetBox().GetCenter().Y, box.GetBox().GetCenter().Z);

	}
	if ((PropertyName == GET_MEMBER_NAME_CHECKED(AObstacle, test)))
	{
		FBoxSphereBounds box = GetBoundingBox();
		TArray<AActor*> FoundActors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AObstacle::StaticClass(), FoundActors);
		for (int i = 0; i < FoundActors.Num(); i++)
		{
			AObstacle* o = (AObstacle *) FoundActors[i];
			if (FBoxSphereBounds::BoxesIntersect(o->GetBoundingBox(), box)) {
				UE_LOG(LogTemp, Warning, TEXT("Intersections are happening"));
			}
			else {
				UE_LOG(LogTemp, Warning, TEXT("no intersections here"));
			}
		}
	}

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(AObstacle, showBoundingBox)))
	{
		if (showBoundingBox) {
			FBoxSphereBounds box = GetBoundingBox();
			for (size_t i = 0; i < 2; i++)
			{
				AddTestSphere(box.GetBoxExtrema(i));
			}
		}
		else {
			for (int i = 0; i < actorsToDestroy.Num(); i++) {
				actorsToDestroy[i]->Destroy();
			}
			actorsToDestroy.Empty();
		}
	}
	// Call the base class version  
	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif

// Called every frame
void AObstacle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

