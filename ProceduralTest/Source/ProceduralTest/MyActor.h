// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProceduralMeshComponent.h"
#include "Wall.h"
#include "Runtime/Engine/Classes/Materials/Material.h"
#include "GameFramework/Actor.h"
#include "MyActor.generated.h"

UCLASS()
class PROCEDURALTEST_API AMyActor : public AActor
{
	GENERATED_BODY()
private:
	void CreateTriangle(FVector p1, FVector p2, FVector p3, FVector2D uv1, FVector2D uv2, FVector2D uv3);
	void CreateTriangle(FVector p1, FVector p2, FVector p3, FVector2D uv1, FVector2D uv2, FVector2D uv3, FVector tangent, FVector normal);
	Wall CreateWall(FVector p1, FVector p2, float roomHeight);
	Wall CreateWall(FVector p1, FVector p2, float roomHeight, Wall startWall);
	Wall CreateWall(FVector p1, FVector p2, float roomHeight, float thickness);
	Wall CreateWall(FVector p1, FVector p2, float roomHeight, float thickness, Wall startWall);
	void AddWallToMesh(Wall wall);
	void CreateWallWithCorners(FVector p1, FVector p2, float roomHeight, Wall startWall);
	void CreateWallsWithDoors(FVector p1, FVector p2, float roomHeight, float thickness, int numberOfDoors, float doorHeight, float doorWidth);

	Wall CreateSimpleFloor(FVector p1, FVector p2);
	Wall CreateSimpleFloor(FVector middlePosition, FVector2D dimensions, bool up);
	void CreateSimpleRoom(FVector p1, FVector p2, float height);
	void CreateSimpleHallway(FVector p1, FVector p2, float height);
	virtual void ClearMesh();
	FVector GetNormal(FVector v1, FVector v2, FVector v3);

	UPROPERTY(EditAnywhere) 
		UMaterial * wallMaterial;
	UPROPERTY(EditAnywhere)
		UProceduralMeshComponent* mesh = nullptr;

	UPROPERTY(EditAnywhere)
		bool editRoom = false;
	UPROPERTY(EditAnywhere)
		float roomHeight = 100;
	UPROPERTY(EditAnywhere)
		float roomWidth = 200;
	UPROPERTY(EditAnywhere)
		float roomLength = 200;
	UPROPERTY(EditAnywhere)
		FVector position;

	UPROPERTY(EditAnywhere)
		bool editWall = false;
	UPROPERTY(EditAnywhere)
		FVector wallStartPoint = FVector(0, 0, 0);
	UPROPERTY(EditAnywhere)
		FVector wallEndPoint = FVector(0, 200, 0);
	UPROPERTY(EditAnywhere)
		float wallHeight = 100;
	UPROPERTY(EditAnywhere)
		float wallThickness = 1;
	UPROPERTY(EditAnywhere)
		int numberOfDoors = 2;
	UPROPERTY(EditAnywhere)
		float doorWidth = 40.0f;
	UPROPERTY(EditAnywhere)
		float doorHeight = 80;

public:	
	// Sets default values for this actor's properties
	AMyActor();
	void GenerateMesh();
	void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void PostActorCreated() override;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
