// Fill out your copyright notice in the Description page of Project Settings.

#include "Room.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Public/DrawDebugHelpers.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Obstacle.h"
#include "DebugSphere.h"

bool ARoom::CheckIfFree()
{
	boundingBox = GetBoundingBox();

	bool intersects = false;
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AObstacle::StaticClass(), FoundActors);
	//UE_LOG(LogTemp, Warning, TEXT("Start intersection test"));

	for (int i = 0; i < FoundActors.Num(); i++)
	{
		AObstacle* o = (AObstacle *)FoundActors[i];
		if (FBoxSphereBounds::BoxesIntersect(o->GetBoundingBox(), boundingBox)) {
			intersects = true;
			//UE_LOG(LogTemp, Warning, TEXT("Intersections are happening"));
		}
		else {
			//UE_LOG(LogTemp, Warning, TEXT("no intersections here"));
		}
	}

	//FBoxSphereBounds::BoxesIntersect(boundingBox, )
	return !intersects;
}

struct DOOR {
	FIntVector position;
	FIntVector forward;

	FVector truePosition;
	FVector trueDirection;
	DOOR(FIntVector pos, FIntVector direction, FVector truePosition, FVector trueDirection)
		: position(pos), forward(direction), trueDirection(trueDirection), truePosition(truePosition)
	{}
};

bool operator==(const DOOR &n1, const DOOR &n2) {
	return n1.position == n2.position && n1.forward == n2.forward;
}

TArray<DOOR> possibleDoorPositions;

void ARoom::AddDoors()
{
	FRotator Rotation(0.0f, 0.0f, 0.0f);

	FActorSpawnParameters SpawnInfo;
	UWorld* world = GetWorld();



	FVector Location = _position + FVector(0.0f, _length / 2.0f, 0.0f);
	ADoor* doorRight = GetWorld()->SpawnActor<ADoor>(Location, Rotation, SpawnInfo);
	//doorRight->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
	doorRight->Init(0, Location, FVector(0, 1, 0), roomTemplate->nodeMap->GetPositionNodeArray(Location));
	doorsFace.Add(Location);
	_doors.Add(doorRight);
	//doorRight->SetActorLabel("Door ");

	Location = _position + FVector(0.0f, -_length / 2.0f, 0.0f);
	ADoor* doorLeft = GetWorld()->SpawnActor<ADoor>(Location, Rotation, SpawnInfo);
	//doorLeft->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
	doorLeft->Init(1, Location, FVector(0, -1, 0), roomTemplate->nodeMap->GetPositionNodeArray(Location));
	doorsFace.Add(Location);
	_doors.Add(doorLeft);

	Location = _position + FVector(_width / 2.0f, 0.0f, 0.0f);
	ADoor* doorUp = GetWorld()->SpawnActor<ADoor>(Location, Rotation, SpawnInfo);
	//doorUp->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
	doorUp->Init(2, Location, FVector(1, 0, 0), roomTemplate->nodeMap->GetPositionNodeArray(Location));
	doorsFace.Add(Location);
	_doors.Add(doorUp);

	Location = _position + FVector(-_width / 2.0f, 0.0f, 0.0f);
	ADoor* doorDown = GetWorld()->SpawnActor<ADoor>(Location, Rotation, SpawnInfo);
	//doorDown->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
	doorDown->Init(3, Location, FVector(-1, 0, 0), roomTemplate->nodeMap->GetPositionNodeArray(Location));
	doorsFace.Add(Location);
	_doors.Add(doorDown);
}

void ARoom::AddDoors(int doorsAmount)
{
	if (roomTemplate == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("find possible door positions first"));
		return;
	}
	int minimumDistance = roomTemplate->minimumSpaceForBreakout;
	float minDistance = _doorWidth + _minimumDistanceBetweenDoors;
	minDistance *= minDistance;
	
	for (int i = 0; i < doorsAmount; i++)
	{
		if (possibleDoorPositions.Num() == 0)
			return;
		int idForPosition = FMath::FRandRange(0, possibleDoorPositions.Num()-1);
		DOOR doorPos = possibleDoorPositions[idForPosition];
		//check if door can be reached

		FIntVector intDirection(doorPos.trueDirection.X, doorPos.trueDirection.Y, doorPos.trueDirection.Z);

		FIntVector checkPos = doorPos.position + intDirection * ((_corridorWidth / stepSize));
		if (roomTemplate != nullptr && roomTemplate->nodeMap->CheckIfBreakOutNecessary(checkPos, doorPos.forward, minimumDistance, false)) {
			possibleDoorPositions.RemoveAt(idForPosition);
			i--;
			continue;
		}

		if (!CheckIfDoorIsLegal(doorPos.truePosition, doorPos.trueDirection)) {
			possibleDoorPositions.RemoveAt(idForPosition);
			i--;
			continue;
		}
		AddDoor(doorPos);
		
		//if (FMath::Abs(doorPos.forward.X) > 0) {
		//	doorPos.position.X = _position.X + _width / 2.0f * doorPos.forward.X;
		//}
		//else {
		//	doorPos.position.Y = _position.Y + _length / 2.0f * doorPos.forward.Y;
		//}
		//AddDoor(doorPos);
	}
	
}

void ARoom::VisualizePossibleDoors()
{
	for (int i = 0; i < possibleDoorPositions.Num(); i++)
	{
		DOOR door = possibleDoorPositions[i];
		FColor color = FColor::Cyan;
		DrawDebugSolidBox(GetWorld(), door.truePosition, FVector::OneVector * stepSize / 6.0f, color, false, 3, 0);
	}
	for (int i = 0; i < _doors.Num(); i++) {
		ADoor * door = _doors[i];
		DrawDebugSphere(GetWorld(), door->GetActorLocation(), stepSize, 26, FColor::Turquoise, false, 3, 0);
	}
}

void ARoom::AddDoor(DOOR & doorPos)
{
	AdjustDoorToRoomWalls(doorPos);
	float minDistance = FMath::Square(_doorWidth + _minimumDistanceBetweenDoors);
	//DrawDebugSphere(GetWorld(), doorPos.truePosition, stepSize, 26, FColor::Turquoise, false, 10, 0);

	for (int i = 0; i < possibleDoorPositions.Num(); i++) {
		DOOR tempDoor = possibleDoorPositions[i];
		if (tempDoor.forward == doorPos.forward) {
			if (FVector::DistSquared(tempDoor.truePosition, doorPos.truePosition) < minDistance) {
				possibleDoorPositions.RemoveAt(i);
				i--;
			}
		}
	}

	FRotator Rotation(0.0f, 0.0f, 0.0f);

	FActorSpawnParameters SpawnInfo;
	UWorld* world = GetWorld();
	ADoor* door = GetWorld()->SpawnActor<ADoor>(doorPos.truePosition, Rotation, SpawnInfo);
	//door->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
	door->Init(_doors.Num() + 1, doorPos.truePosition, doorPos.trueDirection, doorPos.position, _doorHeight, _doorWidth, _doorSpeed);

	doorsFace.Add(doorPos.truePosition);
	_doors.Add(door);
}

void ARoom::AdjustDoorToRoomWalls(DOOR & doorPos)
{
	if (FMath::Abs(doorPos.trueDirection.X) > FMath::Abs(doorPos.trueDirection.Y))
		if (doorPos.trueDirection.X < 0)
			doorPos.truePosition.X = _position.X - _width / 2.0f + _wallThickness / 2.0f;
		else
			doorPos.truePosition.X = _position.X + _width / 2.0f - _wallThickness / 2.0f;
	else
		if (doorPos.trueDirection.Y < 0)
			doorPos.truePosition.Y = _position.Y - _length / 2.0f + _wallThickness / 2.0f;
		else
			doorPos.truePosition.Y = _position.Y + _length / 2.0f - _wallThickness / 2.0f;
}

void ARoom::ChangeDoorColor(FLinearColor color)
{
	for (int i = 0; i < _doors.Num(); i++)
	{
		ADoor* door = _doors[i];
		door->ChangeColor(color);
	}
}

bool ARoom::CheckIfDoorIsLegal(FVector doorPos, FVector forward) {
	
	float minDistance = FMath::Square(_doorWidth + _minimumDistanceBetweenDoors);
	for (int i = 0; i < _doors.Num(); i++)
	{
		if (_doors[i]->_forward.Equals(forward, 0.1) && FVector::DistSquared(_doors[i]->_position, doorPos) < minDistance) {
			return false;
		}
	}
	return true;
}

void ARoom::FindObstacles()
{
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AObstacle::StaticClass(), obstacles);
}

// Sets default values
ARoom::ARoom()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	USceneComponent* root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Object"));
	root->SetupAttachment(RootComponent);
	SetRootComponent(root);

	ceilingMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("CeilingMesh"));
	ceilingMesh->bUseAsyncCooking = true;
	ceilingMesh->SetupAttachment(RootComponent);

	wallMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("WallMesh"));
	wallMesh->bUseAsyncCooking = true;
	wallMesh->SetupAttachment(RootComponent);

	floorMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("FloorMesh"));
	floorMesh->bUseAsyncCooking = true;
	floorMesh->SetupAttachment(RootComponent);

	//lightBlockMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("FloorMesh"));
	//lightBlockMesh->bUseAsyncCooking = true;
	//lightBlockMesh->SetupAttachment(RootComponent);


	//RoomInit(1, 200, 200, FVector(3000, 3000, 0), 20);
	//InitializeMesh(wallMesh);
	//InitializeMesh(floorMesh);
	//InitializeMesh(ceilingMesh);

	//meshCreator.SetMesh(wallMesh);

}

void ARoom::InitializeMesh(UProceduralMeshComponent * mesh)
{
	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	SetRootComponent(mesh);
	mesh->bUseAsyncCooking = true;
}

ADoor * ARoom::getRandomDoor()
{
	if (_doors.Num() == 0) {
		AddDoors(4);
		CreateMesh();
	}

	if (_doors.Num() == 0) {
		AddDoors();
		CreateMesh();
	}
	int doorInt = FMath::RandRange(0, _doors.Num()-1);
	return _doors[doorInt];
}

ADoor * ARoom::getClosestDoor(FVector pos)
{
	float closestDistance = TNumericLimits< float >::Max();
	ADoor * closestDoor = nullptr;
	for (ADoor* d : _doors) {
		float currentDistance = FVector::DistSquared2D(pos, d->_position);
		if (closestDistance > currentDistance) {
			closestDistance = currentDistance;
			closestDoor = d;
		}
	}
	return closestDoor;
}

FBoxSphereBounds ARoom::GetBoundingBox()
{
	FVector* points = new FVector[2];
	FVector sizeVector = FVector(_width, _length, _height) / 2.0f;


	points[0] = _position + sizeVector;
	//points[1] = position - normal * corridorWidth / 2.0f;
	//points[2] = endPoint + normal * corridorWidth / 2.0f;
	points[1] = _position - sizeVector;

	boundingBox = FBoxSphereBounds(points, 2);
	boundingBox = boundingBox.ExpandBy(_corridorWidth);
	return boundingBox;
}

void ARoom::FreeNodesTest(TArray<FVector> freeNodes)
{
	/*for (int i = 0; i < freeNodes.Num(); i++)
	{
		//DOORPOS door(freeNodes[i], FVector::ForwardVector);
		if (GetPossibleRoomPositions(door.position, door.forward).Num() > 0) {
	//		UE_LOG(LogTemp, Warning, TEXT("possible door found x: %f, y: %f, z: %f"), door.position.X, door.position.Y, door.position.Z);
			//DrawDebugSphere(GetWorld(), door.position, stepSize / 2.0f, 26, FColor::Turquoise, false, 20, 0);
			//DrawDebugSphere(GetWorld(), door.position + stepSize / 2.0f * door.forward, 20, 26, FColor::Purple, false, 20, 0);

		}
		//DOORPOS door1(freeNodes[i], FVector::ForwardVector * -1.0f);
		if (GetPossibleRoomPositions(door1.position, door1.forward).Num() > 0) {
	//		UE_LOG(LogTemp, Warning, TEXT("possible door found x: %f, y: %f, z: %f"), door1.position.X, door1.position.Y, door1.position.Z);
			//DrawDebugSphere(GetWorld(), door1.position, stepSize / 2.0f, 26, FColor::Turquoise, false, 20, 0);
			//DrawDebugSphere(GetWorld(), door1.position + stepSize / 2.0f * door1.forward, 20, 26, FColor::Purple, false, 20, 0);

		}
		//DOORPOS door2(freeNodes[i], FVector::RightVector);
		if (GetPossibleRoomPositions(door2.position, door2.forward).Num() > 0) {
	//		UE_LOG(LogTemp, Warning, TEXT("possible door found x: %f, y: %f, z: %f"), door2.position.X, door2.position.Y, door2.position.Z);
			DrawDebugSphere(GetWorld(), door2.position, stepSize / 2.0f, 26, FColor::Turquoise, false, 20, 0);
			DrawDebugSphere(GetWorld(), door2.position + stepSize / 2.0f * door2.forward, 20, 26, FColor::Purple, false, 20, 0);

		}
		//DOORPOS door3(freeNodes[i], FVector::RightVector * -1.0f);
		if (GetPossibleRoomPositions(door3.position, door3.forward).Num() > 0) {
	//		UE_LOG(LogTemp, Warning, TEXT("possible door found x: %f, y: %f, z: %f"), door3.position.X, door3.position.Y, door3.position.Z);
			DrawDebugSphere(GetWorld(), door3.position, stepSize / 2.0f, 26, FColor::Turquoise, false, 20, 0);
			DrawDebugSphere(GetWorld(), door3.position + stepSize / 2.0f * door3.forward, 20, 26, FColor::Purple, false, 20, 0);

		}
	}*/

//	UE_LOG(LogTemp, Warning, TEXT("POSSIBLE DOOR NUMS: %i"), possibleDoorPositions.Num());
//	UE_LOG(LogTemp, Warning, TEXT("freeNodes NUMS: %i"), freeNodes.Num());

}

void ARoom::RoomInitRandomPosition(int ID, RoomTemplate * roomTemplate, bool createMesh, int amountOfDoors, float doorSpeed)
{
	RoomInit(ID, roomTemplate);
	ChooseRandomPosition();
	
	possibleDoorPositions.Empty();
	AddPossibleDoors(_position);
	AddDoors(amountOfDoors);
	if (createMesh)
		CreateMesh();
}

void ARoom::RoomInitFixedPosition(int ID, RoomTemplate * roomTemplate, bool createMesh, FVector position, int amountOfDoors, float doorSpeed)
{
	_doorSpeed = doorSpeed;
	RoomInit(ID, roomTemplate);
	_position = position;
	SetActorLocation(_position);
	wallMesh->SetWorldLocation(_position);
	ceilingMesh->SetWorldLocation(_position);
	floorMesh->SetWorldLocation(_position);

	_position = GetActorLocation();

	possibleDoorPositions.Empty();
	AddPossibleDoors(_position);
	if (createMesh)
		CreateMesh();
	AddDoors(amountOfDoors);
	if (createMesh)
		CreateMesh();
}

void ARoom::RoomInitFixedPosition(int ID, RoomTemplate * roomTemplate, bool createMesh, FVector position, int amountOfDoors, float doorSpeed, FIntVector entranceDoorDir, FIntVector entranceDoorPos)
{
	FVector doorTruePosition = roomTemplate->nodeMap->GetRealPosition(entranceDoorPos);
	DOOR entranceDoor(entranceDoorPos, entranceDoorDir, doorTruePosition, FVector(entranceDoorDir.X, entranceDoorDir.Y, 0));

	_doorSpeed = doorSpeed;
	RoomInitFixedPosition(ID, roomTemplate, createMesh, position, 0, doorSpeed);
	AddDoor(entranceDoor);
	AddDoors(amountOfDoors - 1);
	if (createMesh)
		CreateMesh();
}

void ARoom::TestMethod()
{
	CreateMesh();
}

void ARoom::RoomInit(int ID, RoomTemplate * roomTemplate)
{
	id = ID;
	SetActorLabel(*FString("Room " + FString::FromInt(ID)));
	//UE_LOG(LogTemp, Warning, TEXT("Room id: %f"), ID);

	_corridorWidth = roomTemplate->corridorWidth;
	_width = roomTemplate->_size.X;
	_length = roomTemplate->_size.Y;
	_height = roomTemplate->_size.Z;
	_wallThickness = roomTemplate->wallThickness;
	_doorHeight = roomTemplate->doorHeight;
	_doorWidth = roomTemplate->doorWidth;
	_minimumDistanceBetweenDoors = roomTemplate->minimumDistanceBetweenDoors;

	possibleRoomPositions = roomTemplate->possibleRoomPositions;
	//doorMap = templateRoom->doorMap;

	//possibleDoorPositions = templateRoom->possibleDoorPositions;
	_floorSize = roomTemplate->trackingSpaceSize;
	this->roomTemplate = roomTemplate;
}

void ARoom::ChooseRandomPosition()
{
	if (possibleRoomPositions.Num() < 1) {
		UE_LOG(LogTemp, Warning, TEXT("initialize room positions!"));
		return;
	}
	int idForPosition = FMath::FRandRange(0, possibleRoomPositions.Num()-1);
	_position = possibleRoomPositions[idForPosition];
	SetActorLocation(_position);
	wallMesh->SetWorldLocation(_position);
	ceilingMesh->SetWorldLocation(_position);
	floorMesh->SetWorldLocation(_position);

	_position = GetActorLocation();
}

TArray<ADoor*> ARoom::GetDoorsRangedByDistance(FVector point)
{
	float dist = 10000.0f;

	TArray<ADoor*> doorss;
	int32 doorsNum = _doors.Num();

	for (int32 i = 0; i < doorsNum; i++)
	{
		float d = FVector::Distance(point, _doors[i]->_position);
		if (d < dist && d != 0)
		{
			doorss.Insert(_doors[i], 0);
			dist = d;
		}
		else
		{
			for (int ii = 0; ii < doorss.Num(); ii++)
			{
				if (d < FVector::Distance(point, doorss[ii]->_position))
				{
					doorss.Insert(_doors[i], ii);
					break;
				}
			}
			if (!doorss.Contains(_doors[i]))
				doorss.Add(_doors[i]);
		}
	}

	return doorss;
}

void ARoom::SetRotation(ADoor * door, FVector doorFace)
{
	for (int i = 0; i<door->possibleDoorFaces.Num(); i++)
	{
		if (door->possibleDoorFaces[i] == (doorFace - _position))
		{
			_yRotation = door->possibleRotations[i];
			UE_LOG(LogTemp, Warning, TEXT("ROTATION SET TO %f"), _yRotation);
			break;
		}
	}
}

void ARoom::SetPotentialDoorFaces(ADoor * door)
{
	TArray<float> possibleRotations;
	possibleRotations.Add(0.0f);
	possibleRotations.Add(90.0f);
	possibleRotations.Add(180.0f);
	possibleRotations.Add(270.0f);

	door->possibleRotations = possibleRotations;

	if (door->possibleDoorFaces.Num() > 0)
		door->possibleDoorFaces.Empty();
	for (int i = 0; i<door->possibleRotations.Num(); i++)
	{
		FVector f = GetDoorFaceFromDoorPos(door, possibleRotations[i]);
		door->possibleDoorFaces.Add(f);
	}
}

FVector ARoom::GetDoorFaceFromDoorPos(ADoor* d, float ZRot)
{
	FVector doorPos = d->_position;

	float doorFaceX = 0.0f;
	float doorFaceY = 0.0f;

	//Vector3 doorPosW = doorPos + position;
	// if the door is on the right
	if (FMath::IsNearlyEqual(doorPos.X, _length / 2.0f)) // (0,0) is coord center
	{
		doorFaceX = _length / 2.0f + _corridorWidth / 2.0f; // in room coord
		doorFaceY = doorPos.Y;
	}
	else if (FMath::IsNearlyEqual(doorPos.X, -_length / 2.0f)) // if the door is on the left side
	{
		doorFaceX = -_length / 2.0f - _corridorWidth / 2.0f; // in room coord
		doorFaceY = doorPos.Y;
	}
	else if (FMath::IsNearlyEqual(doorPos.Y, _width / 2.0f)) // on the upper side
	{
		doorFaceY = _width / 2.0f + _corridorWidth / 2.0f; // in room coord
		doorFaceX = doorPos.X;
	}
	else if (FMath::IsNearlyEqual(doorPos.Y, -_width / 2.0f)) // on the lower side
	{
		doorFaceY = -_width / 2.0f - _corridorWidth / 2.0f; // in room coord
		doorFaceX = doorPos.X;
	}

	float x = doorFaceX*FMath::Cos(ZRot*PI / 180.0f) + doorFaceY*FMath::Sin(ZRot*PI / 180.0f);
	float y = -doorFaceX*FMath::Sin(ZRot*PI / 180.0f) + doorFaceY*FMath::Cos(ZRot*PI / 180.0f);

	FVector v = FVector(x, y, doorPos.Z);
	return v;
}

void ARoom::CreateMesh()
{
	FVector p1 = FVector(-_width / 2.f, -_length / 2.f, 0);
	FVector p2 = FVector(_width / 2.f, _length / 2.f, 0);

	meshCreator.currentColor = _roomColor;
	CreateFloorMesh();
	CreateCeilingMesh();
	CreateWallMeshes(p1, p2);

	boundingBox = GetBoundingBox();
}

void ARoom::CreateFloorMesh()
{
	meshCreator.SetMesh(floorMesh);
	meshCreator.ClearMesh();

	floorMaterialDynamic = UMaterialInstanceDynamic::Create(floorMaterial, floorMesh);
	floorMaterialDynamic->SetVectorParameterValue("Color", _roomColor);
	floorMesh->SetMaterial(0, floorMaterialDynamic);

	meshCreator.AddWallToMesh(meshCreator.CreateSimpleFloor(FVector::ZeroVector, FVector2D(_width, _length), true));
	meshCreator.GenerateMesh();
}

void ARoom::CreateCeilingMesh()
{
	FVector2D dimensions = FVector2D(_width - _wallThickness * 2.0f, _length - _wallThickness * 2.0f);

	FVector middlePoint = FVector::ZeroVector;
	middlePoint.Z = _height;

	meshCreator.SetMesh(ceilingMesh);
	meshCreator.ClearMesh();
	ceilingMaterialDynamic = UMaterialInstanceDynamic::Create(ceilingMaterial, ceilingMesh);
	ceilingMaterialDynamic->SetVectorParameterValue("Color", _roomColor);
	ceilingMesh->SetMaterial(0, ceilingMaterialDynamic);

	meshCreator.AddWallToMesh(meshCreator.CreateSimpleFloor(middlePoint, dimensions, false));
	meshCreator.GenerateMesh();
}

void ARoom::CreateWallMeshes(FVector p1, FVector p2)
{
	FVector p3 = p2;
	p2 = FVector(p1.X, p3.Y, p3.Z);
	FVector p4 = FVector(p3.X, p1.Y, p1.Z);

	meshCreator.SetMesh(wallMesh);
	meshCreator.ClearMesh();
	//wallMaterial->TwoSided = 1;
	wallMaterialDynamic = UMaterialInstanceDynamic::Create(wallMaterial,wallMesh);
	wallMaterialDynamic->SetVectorParameterValue("Color", _roomColor);
	wallMesh->SetMaterial(0, wallMaterialDynamic);
	
	CreateWallMesh(p1, p2, _wallThickness);
	CreateWallMesh(p2, p3, _wallThickness);
	CreateWallMesh(p3, p4, _wallThickness);
	CreateWallMesh(p4, p1, _wallThickness);
	meshCreator.GenerateMesh();
	wallMesh->bCastShadowAsTwoSided = 1;
}



void ARoom::CreateWallMesh(FVector p1, FVector p2, float wallThickness)
{
	FVector wallVector = p2 - p1;

	FVector wallDirection = wallVector;
	wallDirection.Normalize();

	//p1 += wallDirection * _wallThickness;
	//p2 -= wallDirection * _wallThickness;

	FVector point1 = p1;// +wallDirection * _wallThickness;
	FVector point2 = p2;// -wallDirection * _wallThickness;

	FVector normal = FVector(-wallDirection.Y, wallDirection.X, 0);
	TArray<ADoor*> doorsForWall;
	TArray<float> doorDistances;
	for (int i = 0; i < _doors.Num(); i++)
	{
		if (_doors[i]->_forward.Equals(normal, 0.1f)) {
			doorsForWall.Add(_doors[i]);
			float distanceToP1;
			if (FMath::Abs(wallDirection.X) > FMath::Abs(wallDirection.Y)) {
				distanceToP1 = FMath::Abs(point1.X - (_doors[i]->_position.X - _position.X));
			}
			else {
				distanceToP1 = FMath::Abs(point1.Y - (_doors[i]->_position.Y - _position.Y));
			}
			if(!doorDistances.Contains(distanceToP1))
				doorDistances.HeapPush(distanceToP1);
		}
	}
	doorDistances.Sort();
	FVector currentPoint = point1;
	Wall previousWall = Wall();
	float doorDistance = 0;

	for (int i = 0; i < doorDistances.Num(); i++)
	{
		/*if (doorDistances[i] <= doorDistance) {
			UE_LOG(LogTemp, Warning, TEXT("doorDistance smaller than previous : %f"), doorDistance);
			continue;
		}*/
		doorDistance = doorDistances[i];

		doorDistance -= _doorWidth / 2.0f;

		/*if (doorDistance < 0)
			UE_LOG(LogTemp, Warning, TEXT("doorDistance too small : %f"), doorDistance);
*/
		FVector nextPoint = point1 + wallDirection * doorDistance;

		/*FVector currentWalldir = nextPoint - currentPoint;
		currentWalldir.Normalize();
		if(!currentWalldir.Equals(wallDirection, 0.1))
			UE_LOG(LogTemp, Warning, TEXT("wrong direction firstWall"));
			*/

		previousWall = meshCreator.CreateWall(currentPoint, nextPoint, _height, wallThickness, previousWall);
		
		//First Wall
		meshCreator.AddWallToMesh(previousWall, true);
		
		currentPoint = nextPoint + FVector(0, 0, _doorHeight);
		nextPoint = point1 + wallDirection * (doorDistance + _doorWidth);
		/*
		currentWalldir = nextPoint + FVector(0, 0, _doorHeight) - currentPoint;
		currentWalldir.Normalize();
		
		if (!currentWalldir.Equals(wallDirection, 0.1))
			UE_LOG(LogTemp, Warning, TEXT("wrong direction topWall"));
			*/
		previousWall = meshCreator.CreateWall(currentPoint, nextPoint + FVector(0, 0, _doorHeight), _height - _doorHeight, wallThickness, previousWall);
		
		//Wall above Door
		meshCreator.AddWallToMesh(previousWall, true);
		currentPoint = nextPoint;
	}
	/*FVector currentWalldir = p2 - currentPoint;
	currentWalldir.Normalize();

	if (!currentWalldir.Equals(wallDirection, 0.1))
		UE_LOG(LogTemp, Warning, TEXT("wrong direction lastWall"));*/
	meshCreator.AddWallToMesh(meshCreator.CreateWall(currentPoint, point2, _height, wallThickness, previousWall), true);

}

bool ARoom::IsBetween(FVector a, FVector b, FVector pointToCheck)
{
	if (!IsOnLine(a, b, pointToCheck))
		return false;

	float dotproduct = FVector::DotProduct((b - a), (pointToCheck - b));

	if (dotproduct < 0.0f)
		return false;

	float squareLengthAC = (a - pointToCheck).SizeSquared();
	if (dotproduct > squareLengthAC)
		return false;

	return true;
}

bool ARoom::IsOnLine(FVector a, FVector b, FVector pointToCheck)
{
	FVector crossproduct = FVector::CrossProduct((b - a), (pointToCheck - a));
	if (FMath::IsNearlyEqual(crossproduct.X, 0.0f) &&
		FMath::IsNearlyEqual(crossproduct.Y, 0.0f) &&
		FMath::IsNearlyEqual(crossproduct.Z, 0.0f)) {
		return false;
	}
	return true;
}

FIntVector ARoom::GetPositionNodeArray(FVector pos)
{
	FVector temp = pos - firstNodePos;
	FVector arrayPos = temp / stepSize;

	return FIntVector(arrayPos.X, arrayPos.Y, 0);
}

FVector ARoom::GetRealPosition(FIntVector pos) {

	FVector temp(pos.X, pos.Y, 0);
	temp *= stepSize;
	return temp + firstNodePos;
}

void ARoom::AddPossibleDoors(FVector position)
{
	int doorMinimumDistanceNAmount = (_doorWidth + _minimumDistanceBetweenDoors) / stepSize;
	float doorMinimumDistance = doorMinimumDistanceNAmount * stepSize;

	//if(!doorMap.IsValid())
		//doorMap = TSharedPtr<DOORMAP>(new DOORMAP(nodeArrayHeight, nodeArrayWidth));

//	UE_LOG(LogTemp, Warning, TEXT("adding possible Doors"));

	int nodesAmountWidth = (_width / 2.0f) / (stepSize);
	int nodesAmountlength = (_length / 2.0f) / (stepSize);

	float left = position.X - nodesAmountWidth * stepSize;
	float right = position.X + nodesAmountWidth * stepSize;

	float up = position.Y - nodesAmountlength * stepSize;
	float down = position.Y + nodesAmountlength * stepSize;
	
	FVector leftUp(left, up, 0.0f);
	FVector rightDown(right, down, 0.0f);
	FColor color;
	int loopx = 0;
	for (float x = left + doorMinimumDistance; x <= right - doorMinimumDistance; x += stepSize)
	{
		color = FColor::Blue;
		loopx++;
		if (loopx > 500) {
			UE_LOG(LogTemp, Warning, TEXT("Too many loops x"));
			break;
		}
		float yPos = up;
		FVector dir = FVector::RightVector * -1;
		FIntVector dirInt = FIntVector(0, -1, 0);

		for (int i = 0; i <= 1; i++)
		{
			if (i == 1) {
				yPos = down;
				dir *= -1.0f;
				dirInt *= -1;
				color = FColor::Cyan;
			}

			if ((FMath::Abs(x) + stepSize / 2.0f) > (_floorSize.X / 2.0f) || (FMath::Abs(yPos) + stepSize / 2.0f) > (_floorSize.Y / 2.0f))
				continue;

			//doorMap->AddDoor(GetPositionNodeArray(FVector(x, yPos, 0)), dirInt);
			//DrawDebugSolidBox(GetWorld(), FVector(x, yPos, 0) + dir * stepSize / 3.0f, FVector::OneVector * stepSize / 6.0f, color, false, 10, 0);

			
			DOOR door(roomTemplate->nodeMap->GetPositionNodeArray(FVector(x, yPos, 0)), dirInt, FVector(x, yPos, 0), dir);
			possibleDoorPositions.Add(door);
			
			/*DOORPOS door(FVector(x, yPos, 0), dir);
			if (possibleDoorPositions.Contains(door)) {
				int existingID = possibleDoorPositions.Find(door);
				possibleDoorPositions[existingID].possibleRoomPositions.Add(position);
				//DrawDebugSolidBox(GetWorld(), door.position + FVector::UpVector * stepSize / 2.0f * possibleDoorPositions[existingID].possibleRoomPositions.Num(), FVector::OneVector * stepSize / 2.0f - FVector::OneVector, FColor::Emerald, false, 10, 0);
				
	//			UE_LOG(LogTemp, Warning, TEXT("adding to existing Door x: %f y: %f"), x, yPos);

			}
			else
			{
				door.possibleRoomPositions.Add(position);
				possibleDoorPositions.Add(door);
				//DrawDebugSolidBox(GetWorld(), door.position + FVector::UpVector * stepSize, FVector::OneVector * stepSize / 2.0f - FVector::OneVector, FColor::Purple, false, 10, 0);
	//			UE_LOG(LogTemp, Warning, TEXT("adding new Door position x: %f y: %f"), x, yPos);

			}*/
		}
	}

	int loopy = 0;
	int yIntPos = leftUp.Y;
	for (float y = up + doorMinimumDistance; y <= down - doorMinimumDistance; y += stepSize)
	{
		color = FColor::Emerald;
		loopy++;
		if (loopy > 500) {
			UE_LOG(LogTemp, Warning, TEXT("Too many loops y"));
			break;
		}
		float xPos = left;

		FVector dir = FVector::ForwardVector  * -1;
		FIntVector dirInt = FIntVector(-1, 0, 0);

		for (int i = 0; i <= 1; i++)
		{
			if (i == 1) {
				xPos = right;
				dir *= -1.0f;
				dirInt * -1;
				color = FColor::Green;
			}

			if (FMath::Abs(xPos) + stepSize / 2.0f > _floorSize.X / 2.0f || FMath::Abs(y) + stepSize / 3.0f > _floorSize.Y / 2.0f)
				continue;
			
			//doorMap->AddDoor(GetPositionNodeArray(FVector(xPos, y, 0)), dirInt);
			//DrawDebugSolidBox(GetWorld(), FVector(xPos, y, 0) + dir * stepSize / 4.0f, FVector::OneVector * stepSize / 2.0f, color, false, 10, 0);
			//DrawDebugSolidBox(GetWorld(), FVector(xPos, y, 0) + dir * stepSize / 3.0f, FVector::OneVector * stepSize / 6.0f, color, false, 10, 0);
			DOOR door(roomTemplate->nodeMap->GetPositionNodeArray(FVector(xPos, y, 0)), dirInt, FVector(xPos, y, 0), dir);
			possibleDoorPositions.Add(door);
			/*DOORPOS door(FVector(xPos, y, 0), dir);
			if (possibleDoorPositions.Contains(door)) {
				int existingID = possibleDoorPositions.Find(door);
				possibleDoorPositions[existingID].possibleRoomPositions.Add(position);
				//DrawDebugSolidBox(GetWorld(), door.position + FVector::UpVector * stepSize * possibleDoorPositions[existingID].possibleRoomPositions.Num(), FVector::UpVector * stepSize / 2.0f - FVector::OneVector, FColor::Emerald, false, 10, 0);
				//UE_LOG(LogTemp, Warning, TEXT("adding to existing Door x: %f y: %f"), xPos, y);
			}
			else
			{
				door.possibleRoomPositions.Add(position);
				possibleDoorPositions.Add(door);
				//DrawDebugSolidBox(GetWorld(), door.position + FVector::UpVector * stepSize, FVector::OneVector * stepSize / 2.0f - FVector::OneVector, FColor::Purple, false, 10, 0);
				//UE_LOG(LogTemp, Warning, TEXT("adding new Door position x: %f y: %f"), xPos, y);
			}*/
		}
		yIntPos++;
	}
}

bool ARoom::CheckIfFree(FVector size, FVector position)
{
	FVector absPosition = FVector(FMath::Abs(position.X), FMath::Abs(position.Y), 0);
	if (absPosition.X + size.X / 2.0f + _corridorWidth> _floorSize.X / 2.0f ||
		absPosition.Y + size.Y / 2.0f + _corridorWidth> _floorSize.Y / 2.0f)
		return false;

	FVector* points = new FVector[2];
	points[0] = position + size / 2.0f + FVector(_corridorWidth, _corridorWidth, 0);
	points[1] = position - size / 2.0f - FVector(_corridorWidth, _corridorWidth, 0);
	FBoxSphereBounds boundingBox(points, 2);

	for (int i = 0; i < obstacles.Num(); i++)
	{
		AObstacle* o = (AObstacle *)obstacles[i];
		if (FBoxSphereBounds::BoxesIntersect(o->GetBoundingBox(), boundingBox)) {
			//DrawDebugBox(GetWorld(), position, size / 2.0f - FVector::OneVector, FColor::Black, 10, 0, 0);
			//DrawDebugSolidBox(GetWorld(), position, size / 2.0f - FVector::OneVector, FColor::Black, false, 10, 0);
			//DrawDebugSolidBox(GetWorld(), position, FVector::OneVector * stepSize / 2.0f - FVector::OneVector, FColor::Black, false, 10, 0);
			return false;
		}
	}
	//DrawDebugBox(GetWorld(), position, size / 2.0f - FVector::OneVector, FColor::Blue, 10, 0, 0);
	//DrawDebugSolidBox(GetWorld(), position, size / 2.0f - FVector::OneVector, FColor::Blue, false, 10, 0);
	//DrawDebugSolidBox(GetWorld(), position, FVector::OneVector * stepSize / 2.0f - FVector::OneVector, FColor::Magenta , false, 10, 0);

	return true;
}

// Called when the game starts or when spawned
void ARoom::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARoom::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


#if WITH_EDITOR  
void ARoom::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	//UE_LOG(LogTemp, Warning, TEXT("butnc"));
	//mesh = GetOwner()->FindComponentByClass<UProceduralMeshComponent>();

	//Get the name of the property that was changed  
	FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;
	if ((PropertyName == GET_MEMBER_NAME_CHECKED(ARoom, createMap))) {
		//FindAllPossibleRoomPositions();
	}
	if ((PropertyName == GET_MEMBER_NAME_CHECKED(ARoom, addDoor))) {
		AddDoors(1);
		//VisualizePossibleDoors();
		CreateMesh();
	}
	if ((PropertyName == GET_MEMBER_NAME_CHECKED(ARoom, test))) {
		for (int i = 0; i < _doors.Num(); i++)
		{
			_doors[i]->Destroy();
		}
		_doors.Empty();
		FVector position = GetActorLocation();

		if (roomTemplate != nullptr) {
			delete roomTemplate->nodeMap;
			delete roomTemplate;
		}
		//RoomTemplate * temop = new RoomTemplate(GetWorld(), nodeMap, FVector(_width, _length, _height), _corridorWidth, _doorWidth, _doorHeight, _minimumDistanceBetweenDoors, _wallThickness, _corridorWidth * 2 / stepSize);
		//RoomInitFixedPosition(id, temop, true, position, 0, _doorSpeed);
		//SetActorLocation(position);
		//CheckIfFree();
	}
	if ((PropertyName == GET_MEMBER_NAME_CHECKED(ARoom, showDoorPositions))) {
		/*int minimumDistance = roomTemplate->minimumSpaceForBreakout;

		for (int i = 0; i < _doors.Num(); i++)
		{
			ADoor* door = _doors[i];
			FIntVector intDirection(door->_forward.X, door->_forward.Y, door->_forward.Z);
			FIntVector checkPos = door->_arrayPosition + intDirection * (_corridorWidth / stepSize);

			roomTemplate->nodeMap->CheckIfBreakOutNecessary(checkPos, intDirection, minimumDistance, true);

		}*/
		VisualizePossibleDoors();
	}
	if ((PropertyName == GET_MEMBER_NAME_CHECKED(ARoom, randomPosition))) {
		for (int i = 0; i < _doors.Num(); i++)
		{
			_doors[i]->Destroy();
		}
		_doors.Empty();
		//RoomInitRandomPosition(id, this, true);
	}

	// Call the base class version  
	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif

void ARoom::Destroy() 
{
	//UE_LOG(LogTemp, Warning, TEXT("Room Destroy"));
	for (ADoor* door : _doors) {
		if(door != nullptr)
			door->Destroy();
	}
	Super::Destroy();
}
