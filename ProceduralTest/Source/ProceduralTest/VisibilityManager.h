// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "VisibleObject.h"
#include "VisibilityManager.generated.h"

class ACorridor;

/**
 * 
 */
UCLASS()
class PROCEDURALTEST_API AVisibilityManager : public ATriggerBox
{
	GENERATED_BODY()

protected:

	virtual void BeginPlay() override;
	
public:
	bool uninitialized = true;
	FVector _position;
	FVector _boxExtent;
	FVector _dir;
	float _width = 0;
	float _length = 0;

	ACorridor * corridor = nullptr;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool active = true;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool playerInside = false;
	
	TArray<AVisibilityManager*> neighbours;

	TArray<AVisibleObject*> mainObjects;

	AVisibilityManager();

	void Init(FVector position, float width, float length, FVector direction);
	void AddNeighbour(AVisibilityManager* newNeighbour, bool addSelf);
	void AddMainObject(AVisibleObject* object, bool setName);
	void PlayerEntered();
	void PlayerLeft();

	UFUNCTION(BlueprintCallable)
		void Activate();

	UFUNCTION(BlueprintCallable)
		void Deactivate();

	void ActivateAllNeighbours();
	void ActivateAllNeighboursInDirection();
	void DectivateAllNeighboursWithException(AVisibilityManager * exception);

	void DeactivateAllNeighbours();

	void DeactivateAllEmptyNeighbours();
	void DeactivateAllEmptyNeighboursInDifferentDirections();
	void Destroy();

	UFUNCTION(BlueprintCallable)
		void ActivateAllMainObjects();

	UFUNCTION(BlueprintCallable)
		void DeactivateAllMainObjects();

	void ShowDebugBoxIfActive();

	// overlap begin function
	UFUNCTION()
		virtual void OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor);

	// overlap end function
	UFUNCTION()
		virtual void OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor);

	
	
};
