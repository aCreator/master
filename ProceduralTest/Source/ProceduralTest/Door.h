// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Hallway.h"
#include "Components/TimelineComponent.h"
#include "VisibleObject.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Door.generated.h"
class APathCreator;
class ACorridor;
class AVisibilityManager;
class ADoorFrame;
UCLASS()
class PROCEDURALTEST_API ADoor : public AVisibleObject
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADoor();
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* doorVisual;


	void Init(int id, FVector position, FVector forward, FIntVector arrayPosition);
	void Init(int id, FVector position, FVector forward, FIntVector arrayPosition, float height, float width, float speed);
	
	void Open();
	void Close();

	int _roomInID = 0;
	int _roomToID = -1;
	float _doorWidth = 0;
	float _speed = 10;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool open = false;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool closed = true;
	UPROPERTY(BlueprintReadWrite)
		AVisibilityManager * _room = nullptr;
	AVisibleObject * _doorFrame = nullptr;
	ACorridor * _corridor = nullptr;
	APathCreator * _pathCreator = nullptr;

	UPROPERTY(EditAnywhere)
		FVector _forward;
	UPROPERTY(EditAnywhere)
		float _height;
//	UFUNCTION(BlueprintCallable)
		void ActivateObject();
//	UFUNCTION(BlueprintCallable)
		void DeactivateObject();
	FIntVector _arrayPosition;
	TArray<AActor*> _hallWay;
	void ChangeColor(FLinearColor color);

	TArray<FVector> possibleDoorFaces;
	TArray<float> possibleRotations; // almost 4, correspond to possible door face positions

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UMaterialInstanceDynamic* myMaterialDynamic;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void Destroy();

};
