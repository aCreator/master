// Fill out your copyright notice in the Description page of Project Settings.

#include "Wall.h"

Wall::Wall()
{
}

Wall::Wall(FVector p1,
	FVector p2,
	FVector p3,
	FVector p4,
	FVector normal,
	FVector tangent,

	FVector2D uv1,
	FVector2D uv2,
	FVector2D uv3,
	FVector2D uv4)
{
	this->p1 = p1;
	this->p2 = p2;
	this->p3 = p3;
	this->p4 = p4;
	this->normal = normal;
	this->tangent = tangent;
	
	this->uv1 = uv1;
	this->uv2 = uv2;
	this->uv3 = uv3;
	this->uv4 = uv4;
}

Wall::Wall(FVector p1, FVector p2, FVector p3, FVector p4, FVector normal, FVector tangent, FVector2D uv1, FVector2D uv2, FVector2D uv3, FVector2D uv4, float thickness)
: Wall(p1,p2,p3,p4,normal,tangent, uv1, uv2, uv3, uv4)
{
	this->thickness = thickness;
}

Wall::~Wall()
{
}
