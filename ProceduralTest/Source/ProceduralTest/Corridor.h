// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Hallway.h"
#include "Corner.h"
#include "Room.h"
#include "VisibilityManager.h"
#include "DoorFrame.h"
#include "Corridor.generated.h"




UCLASS()
class PROCEDURALTEST_API ACorridor : public AActor
{
	GENERATED_BODY()
	
private:
	UWorld * world;
	FVector lastDir = FVector::ZeroVector;
	FVector penultimateDir = FVector::ZeroVector;
	bool _addVisibilityManager = true;
public:	
	// Sets default values for this actor's properties
	ACorridor();
	void InitCorridor(TArray<FVector> corridorPoints, float corridorWidth, float corridorHeight, UMaterial * corridorFloorMaterial, UMaterial * corridorWallMaterial, UMaterial * corridorCeilingMaterial);
	void CreateCorridor(bool addVisibilityManager);
	void AddHallway(AHallway* hallway, bool lastHallway);
	void AddCorner(ACorner* corner);
	void ActivateCorridor(AVisibilityManager * currentRoomVM);
	UFUNCTION(BlueprintImplementableEvent)
		void WasActivated(AVisibilityManager * currentRoomVM);
	void DeactivateCorridor(AVisibilityManager * currentRoomVM);
	UFUNCTION(BlueprintImplementableEvent)
		void WasDeactivated(AVisibilityManager * currentRoomVM);


	TArray<AHallway*> hallways;
	TArray<ACorner*> corners;
	TArray<AVisibilityManager*> hallwayManagers;
	UPROPERTY(BlueprintReadWrite)
		TArray<FVector> _corridorPoints;

	float _corridorWidth;
	float _corridorHeight;
	UPROPERTY(BlueprintReadWrite)
		UMaterial * _corridorFloorMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterial * _corridorWallMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterial * _corridorCeilingMaterial;

	UPROPERTY(BlueprintReadWrite)
		ARoom * _startRoom;
	UPROPERTY(BlueprintReadWrite)
		ARoom * _endRoom;

	UPROPERTY(BlueprintReadWrite)
		ADoor * _startDoor;
	UPROPERTY(BlueprintReadWrite)
		ADoor * _endDoor;

	UPROPERTY(BlueprintReadWrite)
		ADoorFrame * _startDoorFrame;
	UPROPERTY(BlueprintReadWrite)
		ADoorFrame * _endDoorFrame;

	UPROPERTY(BlueprintReadWrite)
		AVisibilityManager* _startRoomVM;
	UPROPERTY(BlueprintReadWrite)
		AVisibilityManager* _endRoomVM;


	//AVisibilityManager* _endDoorVM;
	//AVisibilityManager* _startDoorVM;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool playerInside = false;
	
	void AddRooms(ARoom * startRoom, AVisibilityManager* startRoomVM, ARoom * endRoom, AVisibilityManager* endRoomVM);
	void AddDoors(ADoor * startDoor, ADoor* endDoor, bool spawnDoorFrame, float minimumDistanceBetweenDoors);
	void PlayerEnteredCorridorBit();
	void PlayerLeftCorridorBit();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void Destroy();
	
	
};
