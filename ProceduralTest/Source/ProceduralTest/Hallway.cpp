// Fill out your copyright notice in the Description page of Project Settings.

#include "Hallway.h"
#include "Engine/Engine.h"


void AHallway::MakeWallsIntangibleToggle()
{
	deactivateCollisions = !deactivateCollisions;
	SetActorEnableCollision(!deactivateCollisions);
}

// Sets default values
AHallway::AHallway()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	USceneComponent* root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Object"));
	root->SetupAttachment(RootComponent);
	SetRootComponent(root);

	ceilingMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("CeilingMesh"));
	ceilingMesh->bUseAsyncCooking = true;
	ceilingMesh->SetupAttachment(RootComponent);

	wallMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("WallMesh"));
	wallMesh->bUseAsyncCooking = true;
	wallMesh->SetupAttachment(RootComponent);

	floorMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("FloorMesh"));
	floorMesh->bUseAsyncCooking = true;
	floorMesh->SetupAttachment(RootComponent);


}

void AHallway::TestMethod()
{
	Init(_start, _end, _corridorWidth, _height, _floorMaterial, _wallMaterial, _ceilingMaterial);
}

#if WITH_EDITOR  
void AHallway::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	//UE_LOG(LogTemp, Warning, TEXT("butnc"));
	//mesh = GetOwner()->FindComponentByClass<UProceduralMeshComponent>();

	//Get the name of the property that was changed  
	FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(AHallway, test))
		|| 
		PropertyName == GET_MEMBER_NAME_CHECKED(AHallway, _end)
		||
		PropertyName == GET_MEMBER_NAME_CHECKED(AHallway, _start)
		||
		PropertyName == GET_MEMBER_NAME_CHECKED(AHallway, _height)
		||
		PropertyName == GET_MEMBER_NAME_CHECKED(AHallway, _corridorWidth)) {
		TestMethod();
	}

	// Call the base class version  
	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif

// Called when the game starts or when spawned
void AHallway::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AHallway::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AHallway::Init(FVector start, FVector end, float width, float height, UMaterial * floorMaterial, UMaterial * wallMaterial, UMaterial * ceilingMaterial)
{
	_floorMaterial = floorMaterial;
	_wallMaterial = wallMaterial;
	_ceilingMaterial = ceilingMaterial;

	_corridorWidth = width;
	_height = height;
	_start = start;
	_end = end;

	float corridorLength = FVector::Dist2D(start, end);
	_direction = end - start;
	if (FMath::Abs(_direction.X) > 0) {
		_width = corridorLength;
		_length = _corridorWidth;
	}
	else {
		_length = corridorLength;
		_width = _corridorWidth;
	}
	_position = start + (end - start) / 2.0f;
	SetActorLocation(_position);
	wallMesh->SetWorldLocation(_position);
	ceilingMesh->SetWorldLocation(_position);
	floorMesh->SetWorldLocation(_position);
	
	CreateMesh();
}

void AHallway::CreateMesh()
{
	FVector relativeStart = _start - _position;
	FVector relativeEnd = _end - _position;

	wallMesh->SetMaterial(0, _wallMaterial);
	floorMesh->SetMaterial(0, _floorMaterial);
	ceilingMesh->SetMaterial(0, _ceilingMaterial);

	meshCreator.SetMesh(wallMesh);
	meshCreator.ClearMesh();

	Wall wall = meshCreator.CreateWall(relativeStart, relativeEnd, _height);
	//meshCreator.AddWallToMesh(wall);

	Wall wallFront = Wall(wall);
	wallFront.normal = -wall.normal;
	wallFront.p1 = wall.p2 + (wall.normal * _corridorWidth / 2.0f);
	wallFront.p2 = wall.p1 + (wall.normal * _corridorWidth / 2.0f);
	wallFront.p3 = wall.p4 + (wall.normal * _corridorWidth / 2.0f);
	wallFront.p4 = wall.p3 + (wall.normal * _corridorWidth / 2.0f);

	wallFront.thickness = 0;

	meshCreator.AddWallToMesh(wallFront);

	Wall wallBack = Wall(wall);
	wallBack.normal = wall.normal;
	wallBack.p1 = wall.p1 - (wall.normal * _corridorWidth / 2.0f);
	wallBack.p2 = wall.p2 - (wall.normal * _corridorWidth / 2.0f);
	wallBack.p3 = wall.p3 - (wall.normal * _corridorWidth / 2.0f);
	wallBack.p4 = wall.p4 - (wall.normal * _corridorWidth / 2.0f);

	wallBack.thickness = 0;

	meshCreator.AddWallToMesh(wallBack);

	meshCreator.GenerateMesh();

	//create floor
	FVector hallwayVector = _end - _start;
	
	FVector2D dimensions;

	if (FMath::Abs(hallwayVector.X) > FMath::Abs(hallwayVector.Y)) {
		dimensions.X = FMath::Abs(hallwayVector.X);
		dimensions.Y = _corridorWidth;
	}
	else
	{
		dimensions.X = _corridorWidth;
		dimensions.Y = FMath::Abs(hallwayVector.Y);
	}
	meshCreator.SetMesh(floorMesh);
	meshCreator.ClearMesh();
	meshCreator.AddWallToMesh(meshCreator.CreateSimpleFloor(FVector::ZeroVector, dimensions, true));
	meshCreator.GenerateMesh();
	
	//create ceiling
	meshCreator.SetMesh(ceilingMesh);
	meshCreator.ClearMesh();
	meshCreator.AddWallToMesh(meshCreator.CreateSimpleFloor(FVector(0, 0, _height), dimensions, false));
	meshCreator.GenerateMesh();

}

