// Fill out your copyright notice in the Description page of Project Settings.

#include "VisibilityManager.h"
#include "DrawDebugHelpers.h"
#include "EngineGlobals.h"
#include "Corridor.h"
#include "Runtime/Engine/Classes/Components/ShapeComponent.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodySetup.h"
#include "Engine/Engine.h"


AVisibilityManager::AVisibilityManager()
{
	OnActorBeginOverlap.AddDynamic(this, &AVisibilityManager::OnOverlapBegin);
	OnActorEndOverlap.AddDynamic(this, &AVisibilityManager::OnOverlapEnd);
}

void AVisibilityManager::Init(FVector position, float width, float length, FVector direction)
{
	uninitialized = false;
	_dir = direction;
	_position = position;
	_width = width;
	_length = length;
	_width = 5;
	_length = 5;
	_width = width;
	_length = length;
	/*FTransform transform = GetTransform();
	FVector scale = transform.GetScale3D();
	scale.X = _width / 100.0f;
	scale.Y = _length / 100.0f;

	transform.SetScale3D(scale);
	transform.SetLocation(position);
	SetActorTransform(transform);
	*/
	UShapeComponent * bla = GetCollisionComponent();
	UBoxComponent * ble = (UBoxComponent *)GetCollisionComponent();
	_boxExtent = FVector(width / 2.0f, length / 2.0f, 80);
	ble->SetBoxExtent(_boxExtent);
	//DrawDebugBox(GetWorld(), GetActorLocation(), GetComponentsBoundingBox().GetExtent(), FColor::Purple, true, 5, 0, 5);
}

void AVisibilityManager::AddNeighbour(AVisibilityManager * newNeighbour, bool addSelf)
{
	if (!neighbours.Contains(newNeighbour)) {
		neighbours.Add(newNeighbour);
	}
	if (addSelf && !newNeighbour->neighbours.Contains(this)) {
		newNeighbour->neighbours.Add(this);
	}
}

void AVisibilityManager::AddMainObject(AVisibleObject * object, bool setName)
{
	mainObjects.Add(object);
	if(setName)
		SetActorLabel(*FString("VisibilityManager " + object->GetActorLabel()));
}

void AVisibilityManager::PlayerEntered()
{
	playerInside = true;
	ActivateAllMainObjects();
	ActivateAllNeighbours();
	if (corridor != nullptr)
		corridor->PlayerEnteredCorridorBit();
	for (AVisibilityManager* vism : neighbours)
	{
		vism->ActivateAllNeighboursInDirection();
	}

}

void AVisibilityManager::PlayerLeft()
{


	playerInside = false;
	//DeactivateAllEmptyNeighbours();
	DeactivateAllEmptyNeighboursInDifferentDirections();
	Activate();
	ActivateAllMainObjects();
	if (corridor != nullptr)
		corridor->PlayerLeftCorridorBit();
}

void AVisibilityManager::Activate()
{
	active = true;
}

void AVisibilityManager::Deactivate()
{
	playerInside = false;
	active = false;
}

void AVisibilityManager::ActivateAllNeighbours()
{
	for (AVisibilityManager* vism : neighbours)
	{
		vism->ActivateAllMainObjects();
		vism->Activate();
	}
}

void AVisibilityManager::ActivateAllNeighboursInDirection()
{
	if (neighbours.Num() <= 1)
		return;

	if (neighbours[0]->_dir.Equals(neighbours[1]->_dir, 0.1)) {
		for (AVisibilityManager* vism : neighbours)
		{
			if (!vism->active) {
				vism->ActivateAllMainObjects();
				vism->Activate();
				vism->ActivateAllNeighboursInDirection();
			}
		}
	}

}

void AVisibilityManager::DectivateAllNeighboursWithException(AVisibilityManager * exception)
{
	for (AVisibilityManager* vism : neighbours)
	{
		if (vism != exception) {
			vism->DeactivateAllMainObjects();
			vism->Deactivate();
			vism->DectivateAllNeighboursWithException(this);
		}
	}
}

void AVisibilityManager::DeactivateAllNeighbours()
{
	for (AVisibilityManager* vism : neighbours)
	{
		vism->DeactivateAllMainObjects();
		vism->Deactivate();
	}
}

void AVisibilityManager::DeactivateAllEmptyNeighbours()
{
	for (AVisibilityManager* vism : neighbours)
	{
		if (!vism->playerInside) {
			if (vism->active) {
				vism->DeactivateAllMainObjects();
				vism->Deactivate();
				vism->DeactivateAllEmptyNeighbours();
			}
		}
	}
}

void AVisibilityManager::DeactivateAllEmptyNeighboursInDifferentDirections()
{
	if (neighbours.Num() <= 1)
		return;
	if (neighbours[0]->_dir.Equals(FVector::ZeroVector, 0.1) || neighbours[1]->_dir.Equals(FVector::ZeroVector, 0.1)) {
		//one is a room
		for (AVisibilityManager* vism : neighbours)
		{
			if (vism->_dir.Equals(FVector::ZeroVector, 0.1) && !vism->playerInside) {
				vism->DeactivateAllMainObjects();
				vism->Deactivate();
			}
		}
		return;
	}
	if (!neighbours[0]->_dir.Equals(neighbours[1]->_dir, 0.1)) {
		for (AVisibilityManager* vism : neighbours)
		{
			if (!vism->playerInside) {
				vism->DeactivateAllMainObjects();
				vism->Deactivate();
				vism->DectivateAllNeighboursWithException(this);
			}
		}
	}

}

void AVisibilityManager::ActivateAllMainObjects()
{
	if (this == nullptr)
		return;
	if (uninitialized)
		return;
	if (!IsValidLowLevel())
		return;
	for (AVisibleObject* viso : mainObjects)
	{
		if (viso->IsValidLowLevel())
			viso->ActivateObject();
	}
}

void AVisibilityManager::DeactivateAllMainObjects()
{
	if (this == nullptr)
		return;
	if (!IsValidLowLevel())
		return;
	if (uninitialized)
		return;
	for (AVisibleObject* viso : mainObjects)
	{
		if (viso->IsValidLowLevel())
			viso->DeactivateObject();
	}
}

void AVisibilityManager::ShowDebugBoxIfActive()
{
	if (active)
		DrawDebugBox(GetWorld(), GetActorLocation(), GetComponentsBoundingBox().GetExtent(), FColor::Purple, true, 1, 0, 5);
}

// Called when the game starts or when spawned
void AVisibilityManager::BeginPlay()
{
	Super::BeginPlay();
	APlayerController* myCharacter = GetWorld()->GetFirstPlayerController();
	myCharacter->InputComponent->BindAction("ViewDebugCollisions", IE_Pressed, this, &AVisibilityManager::ShowDebugBoxIfActive);

	//UShapeComponent * bla = GetCollisionComponent();
	//UBoxComponent * ble = (UBoxComponent *)GetCollisionComponent();
	//_position = _position;
	//ble->SetBoxExtent(_boxExtent);

}

void AVisibilityManager::OnOverlapBegin(AActor * OverlappedActor, AActor * OtherActor)
{
	if (uninitialized)
		return;
	if (!active)
		return;
	if (OtherActor && (OtherActor != this))
	{

		if (OtherActor->ActorHasTag("Player")) {
			//GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Green, TEXT("Overlap Begin"));
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("Overlapped Actor = %s"), *OverlappedActor->GetName()));
			PlayerEntered();

		}
	}

}

void AVisibilityManager::OnOverlapEnd(AActor * OverlappedActor, AActor * OtherActor)
{
	if (uninitialized)
		return;
	if (!active)
		return;
	if (OtherActor && OtherActor != this)
	{
		if (OtherActor->ActorHasTag("Player")) {
			//GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Blue, TEXT("Overlap Ended"));
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::Printf(TEXT("%s has left the Trigger Box"), *OtherActor->GetName()));
			PlayerLeft();

		}
	}

}

void AVisibilityManager::Destroy()
{
	//UE_LOG(LogTemp, Warning, TEXT("Room Destroy"));
	for (AVisibleObject* v : mainObjects) {
		if (v != nullptr && v->IsValidLowLevel()) {
			v->Destroy();
		}
	}
	Super::Destroy();
}
