// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VisibleObject.h"
#include "GameFramework/Actor.h"
#include "CeilingLight.generated.h"

UCLASS()
class PROCEDURALTEST_API ACeilingLight : public AVisibleObject
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACeilingLight();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void ChangeLightColor(FColor color);


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
