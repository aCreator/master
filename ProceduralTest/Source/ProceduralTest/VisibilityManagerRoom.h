// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VisibilityManager.h"
#include "Room.h"
#include "VisibilityManagerRoom.generated.h"

class APathCreator;

/**
 * 
 */
UCLASS()
class PROCEDURALTEST_API AVisibilityManagerRoom : public AVisibilityManager
{
	GENERATED_BODY()
	

public:
	void Init(ARoom* room, APathCreator * pathCreator);
	APathCreator * _pathCreator = nullptr;
	ARoom* _room;

	// overlap begin function
	//UFUNCTION()
		void OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor);
	// overlap end function
	//UFUNCTION()
		void OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor);

};
