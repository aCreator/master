// Fill out your copyright notice in the Description page of Project Settings.

#include "DebugSphere.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"

// Sets default values
ADebugSphere::ADebugSphere()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	USceneComponent* root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Object"));
	root->SetupAttachment(RootComponent);
	SetRootComponent(root);

	sphereVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Debug Sphere Visualisation"));
	sphereVisual->SetupAttachment(root);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere")); 
	if (SphereVisualAsset.Succeeded()) { 
		sphereVisual->SetStaticMesh(SphereVisualAsset.Object);
		sphereVisual->AddWorldOffset(FVector(0,0,-50));
		myMaterialDynamic = UMaterialInstanceDynamic::Create(sphereVisual->GetMaterial(0), sphereVisual);
		myMaterialDynamic->SetVectorParameterValue("Color", FLinearColor::Green);
		sphereVisual->SetMaterial(0, myMaterialDynamic);
	}
	SetActorEnableCollision(false);
}

void ADebugSphere::ChangeColorIntensity(float intensity)
{
	FLinearColor color = FLinearColor::LerpUsingHSV(FLinearColor::Green, FLinearColor::Red, intensity);
	myMaterialDynamic->SetVectorParameterValue("Color", color);

}

// Called when the game starts or when spawned
void ADebugSphere::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADebugSphere::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

