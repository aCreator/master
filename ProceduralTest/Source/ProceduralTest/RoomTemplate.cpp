// Fill out your copyright notice in the Description page of Project Settings.

#include "RoomTemplate.h"
#include "Obstacle.h"
#include "Runtime/Engine/Public/DrawDebugHelpers.h"

RoomTemplate::RoomTemplate()
{
}

RoomTemplate::RoomTemplate(UWorld * world, NodeMap * nodeMap, FVector size, float corridorWidth, float doorWidth, float doorHeight, float minimumDistanceBetweenDoors, float wallThickness, int minimumSpaceForBreakout)
	: world(world), nodeMap(nodeMap), _size(size), corridorWidth(corridorWidth), doorWidth(doorWidth), doorHeight(doorHeight), minimumDistanceBetweenDoors(minimumDistanceBetweenDoors), wallThickness(wallThickness), minimumSpaceForBreakout(minimumSpaceForBreakout)
{

}

RoomTemplate::~RoomTemplate()
{
}


bool operator==(const DOORPOS &n1, const DOORPOS &n2) {
	return n1.position == n2.position && n1.forward == n2.forward;
}

struct DOORMAP {
	bool* nodeMap100;
	bool* nodeMap010;
	bool* nodeMapN00;
	bool* nodeMap0N0;

	bool initialized;
	int _height;
	int _width;
	DOORMAP() { initialized = false; };

	DOORMAP(int height, int width) {
		nodeMap100 = new bool[height * width];
		nodeMap010 = new bool[height * width];
		nodeMapN00 = new bool[height * width];
		nodeMap0N0 = new bool[height * width];
		_height = height;
		_width = width;
		initialized = true;

	}

	void Destroy() {
		delete[] nodeMap100;
		delete[] nodeMap010;
		delete[] nodeMapN00;
		delete[] nodeMap0N0;
	}

	bool IsDoorPresent(FIntVector position, FIntVector direction) {
		if (!initialized)
			return false;

		if (position.X + _width * position.Y > _height * _width || position.Y < 0 || position.X < 0) {
			//		UE_LOG(LogTemp, Warning, TEXT("Door position illegal x: %i y: %i"), position.X, position.Y);
			//		UE_LOG(LogTemp, Warning, TEXT("height: %i width: %i"), _height, _width);
			return false;
		}
		if (direction.X == 1 && direction.Y == 0) {
			return !nodeMap100[position.X + _width * position.Y];
		}
		else if (direction.X == 0 && direction.Y == 1) {
			return !nodeMap010[position.X + _width * position.Y];
		}
		else if (direction.X == -1 && direction.Y == 0) {
			return !nodeMapN00[position.X + _width * position.Y];
		}
		else if (direction.X == 0 && direction.Y == -1) {
			return !nodeMap0N0[position.X + _width * position.Y];
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("failed add dir x: %i y: %i z: %i isfree: %d"), direction.X, direction.Y, direction.Z, nodeMap100[position.X + _width * position.Y]);
			return false;
		}
		return false;
	}

	void AddDoor(FIntVector position, FIntVector direction) {
		if (!initialized)
			return;

		if (position.X + _width * position.Y > _height * _width || position.Y < 0 || position.X < 0) {
			//	UE_LOG(LogTemp, Warning, TEXT("Door position illegal x: %i y: %i"), position.X, position.Y);
			//	UE_LOG(LogTemp, Warning, TEXT("height: %i width: %i"), _height, _width);
			return;
		}
		if (direction.X == 1 && direction.Y == 0) {
			//	UE_LOG(LogTemp, Warning, TEXT("node map x: %i y: %i isfree: %d"), position.X, position.Y, nodeMap100[position.X + _width * position.Y]);
			nodeMap100[position.X + _width * position.Y] = false;
		}
		else if (direction.X == 0 && direction.Y == 1) {
			//	UE_LOG(LogTemp, Warning, TEXT("node map x: %i y: %i isfree: %d"), position.X, position.Y, nodeMap100[position.X + _width * position.Y]);
			nodeMap010[position.X + _width * position.Y] = false;
		}
		else if (direction.X == -1 && direction.Y == 0) {
			//	UE_LOG(LogTemp, Warning, TEXT("node map x: %i y: %i isfree: %d"), position.X, position.Y, nodeMap100[position.X + _width * position.Y]);
			nodeMapN00[position.X + _width * position.Y] = false;
		}
		else if (direction.X == 0 && direction.Y == -1) {
			//	UE_LOG(LogTemp, Warning, TEXT("node map x: %i y: %i isfree: %d"), position.X, position.Y, nodeMap100[position.X + _width * position.Y]);
			nodeMap0N0[position.X + _width * position.Y] = false;
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("failed add dir x: %i y: %i z: %i"), direction.X, direction.Y, direction.Z);
		}
	}
};

/*void AddDoors(TArray<DOORPOS> &doors) {
	for (int i = 0; i < doors.Num(); i++)
	{
		DOORPOS door = doors[i];
		FVector position = 
		if (possibleDoorPositions.Contains(door)) {
			int existingID = possibleDoorPositions.Find(door);
			possibleDoorPositions[existingID].possibleRoomPositions.Add(position);
			//DrawDebugSolidBox(GetWorld(), door.position + FVector::UpVector * stepSize / 2.0f * possibleDoorPositions[existingID].possibleRoomPositions.Num(), FVector::OneVector * stepSize / 2.0f - FVector::OneVector, FColor::Emerald, false, 10, 0);

			//			UE_LOG(LogTemp, Warning, TEXT("adding to existing Door x: %f y: %f"), x, yPos);

		}
		else
		{
			door.possibleRoomPositions.Add(position);
			possibleDoorPositions.Add(door);
	}
}*/
void RoomTemplate::FindAllPossibleRoomAndDoorPositions(FVector trackingSpaceSize, float stepSize)
{
	this->trackingSpaceSize = trackingSpaceSize;
	this->stepSize = stepSize;
	TArray<AActor*> foundActors;
	UGameplayStatics::GetAllActorsOfClass(world, AObstacle::StaticClass(), foundActors);
	
	possibleRoomPositions.Empty();
	possibleDoorPositions.Empty();

	doorMap = TSharedPtr<DOORMAP>(new DOORMAP(nodeMap->nodeArrayDimensions.X, nodeMap->nodeArrayDimensions.Y));

	int i = 0;
	int j = 0;

	for (int i = 0; i < nodeMap->nodeArrayDimensions.X; i++)
	{
		for (int j = 0; j < nodeMap->nodeArrayDimensions.Y; j++)
		{
			FVector realPosition = nodeMap->GetRealPosition(FIntVector(i, j, 0));
			bool free = nodeMap->CheckIfFree(foundActors, FVector(_size.X + (corridorWidth + stepSize * 2.0f) * 2.0f, _size.Y + (corridorWidth + stepSize * 2.0f) * 2.0f, 0), realPosition, false);
			if (free) {
				possibleRoomPositions.Add(realPosition);
				GetPossibleDoors(realPosition);
				//doorMap->AddDoor(nodeMap->GetPositionNodeArray(FVector(x, yPos, 0)), dirInt);

			}
		}
	}
	//ShowAllDoorPositions();
}

void RoomTemplate::GetPossibleDoors(FVector position)
{

	int doorMinimumDistanceNAmount = (doorWidth + minimumDistanceBetweenDoors) / stepSize;
	float doorMinimumDistance = doorMinimumDistanceNAmount * stepSize;

	if (!doorMap.IsValid())
		doorMap = TSharedPtr<DOORMAP>(new DOORMAP(nodeMap->nodeArrayDimensions.X, nodeMap->nodeArrayDimensions.Y));

	//	UE_LOG(LogTemp, Warning, TEXT("adding possible Doors"));

	int nodesAmountWidth = (_size.X / 2.0f) / (stepSize);
	int nodesAmountlength = (_size.Y / 2.0f) / (stepSize);

	float left = position.X - nodesAmountWidth * stepSize;
	float right = position.X + nodesAmountWidth * stepSize;

	float up = position.Y - nodesAmountlength * stepSize;
	float down = position.Y + nodesAmountlength * stepSize;

	FVector leftUp(left, up, 0.0f);
	FVector rightDown(right, down, 0.0f);
	FColor color = FColor::Blue;

	int loopx = 0;
	for (float x = left + doorMinimumDistance; x <= right - doorMinimumDistance; x += stepSize)
	{
		color = FColor::Blue;
		loopx++;
		if (loopx > 500) {
			UE_LOG(LogTemp, Warning, TEXT("Too many loops x"));
			break;
		}
		float yPos = up;
		FVector dir = FVector::RightVector * -1;
		FIntVector dirInt = FIntVector(0, -1, 0);

		for (int i = 0; i <= 1; i++)
		{
			if (i == 1) {
				yPos = down;
				dir *= -1.0f;
				dirInt = FIntVector(0, 1, 0);;
				color = FColor::Cyan;
			}

			if ((FMath::Abs(x) + stepSize / 2.0f) > (trackingSpaceSize.X / 2.0f) || (FMath::Abs(yPos) + stepSize / 2.0f) > (trackingSpaceSize.Y / 2.0f))
				continue;
			dirInt = FIntVector(dir.X, dir.Y, dir.Z);

			doorMap->AddDoor(nodeMap->GetPositionNodeArray(FVector(x, yPos, 0)), dirInt);
			
			DOORPOS door(nodeMap->GetPositionNodeArray(FVector(x, yPos, 0)), dirInt);
			if (possibleDoorPositions.Contains(door)) {
				int existingID = possibleDoorPositions.Find(door);
				possibleDoorPositions[existingID].possibleRoomPositions.Add(position);
				//DrawDebugSolidBox(GetWorld(), door.position + FVector::UpVector * stepSize / 2.0f * possibleDoorPositions[existingID].possibleRoomPositions.Num(), FVector::OneVector * stepSize / 2.0f - FVector::OneVector, FColor::Emerald, false, 10, 0);

				//			UE_LOG(LogTemp, Warning, TEXT("adding to existing Door x: %f y: %f"), x, yPos);

			}
			else
			{
				//DrawDebugSolidBox(world, FVector(x, yPos, 0), FVector::OneVector * stepSize / 6.0f, FColor::Silver, false, 10, 0);
				FVector temp(dirInt.X, dirInt.Y, 0);

				//DrawDebugSolidBox(world, FVector(x, yPos, 0) + temp * stepSize / 3.0f, FVector::OneVector * stepSize / 6.0f, color, false, 10, 0);
				door.possibleRoomPositions.Add(position);
				possibleDoorPositions.Add(door);
				//DrawDebugSolidBox(GetWorld(), door.position + FVector::UpVector * stepSize, FVector::OneVector * stepSize / 2.0f - FVector::OneVector, FColor::Purple, false, 10, 0);
				//			UE_LOG(LogTemp, Warning, TEXT("adding new Door position x: %f y: %f"), x, yPos);

			}
		}
	}

	int loopy = 0;
	int yIntPos = leftUp.Y;
	for (float y = up + doorMinimumDistance; y <= down - doorMinimumDistance; y += stepSize)
	{
		color = FColor::Emerald;
		loopy++;
		if (loopy > 500) {
			UE_LOG(LogTemp, Warning, TEXT("Too many loops y"));
			break;
		}
		float xPos = left;

		FVector dir = FVector::ForwardVector  * -1;
		FIntVector dirInt = FIntVector(-1, 0, 0);

		for (int i = 0; i <= 1; i++)
		{
			if (i == 1) {
				xPos = right;
				dir *= -1.0f;
				dirInt * -1;
				color = FColor::Green;
			}

			if (FMath::Abs(xPos) + stepSize / 2.0f > trackingSpaceSize.X / 2.0f || FMath::Abs(y) + stepSize / 2.0f > trackingSpaceSize.Y / 2.0f)
				continue;
			dirInt = FIntVector(dir.X, dir.Y, dir.Z);

			doorMap->AddDoor(nodeMap->GetPositionNodeArray(FVector(xPos, y, 0)), dirInt);
			

			DOORPOS door(nodeMap->GetPositionNodeArray(FVector(xPos, y, 0)), dirInt);
			if (possibleDoorPositions.Contains(door)) {
				int existingID = possibleDoorPositions.Find(door);
				possibleDoorPositions[existingID].possibleRoomPositions.Add(position);
				//DrawDebugSolidBox(GetWorld(), door.position + FVector::UpVector * stepSize * possibleDoorPositions[existingID].possibleRoomPositions.Num(), FVector::UpVector * stepSize / 2.0f - FVector::OneVector, FColor::Emerald, false, 10, 0);
				//UE_LOG(LogTemp, Warning, TEXT("adding to existing Door x: %f y: %f"), xPos, y);
			}
			else
			{
				//DrawDebugSolidBox(world, FVector(xPos, y, 0), FVector::OneVector * stepSize / 6.0f, FColor::Silver, false, 10, 0);
				FVector temp(dirInt.X, dirInt.Y, 0);
				//DrawDebugSolidBox(world, FVector(xPos, y, 0) + temp * stepSize / 3.0f, FVector::OneVector * stepSize / 6.0f, color, false, 10, 0);

				door.possibleRoomPositions.Add(position);
				possibleDoorPositions.Add(door);
				//DrawDebugSolidBox(GetWorld(), door.position + FVector::UpVector * stepSize, FVector::OneVector * stepSize / 2.0f - FVector::OneVector, FColor::Purple, false, 10, 0);
				//UE_LOG(LogTemp, Warning, TEXT("adding new Door position x: %f y: %f"), xPos, y);
			}
		}
		yIntPos++;
	}
}

bool RoomTemplate::CheckIfValidDoorPosition(FIntVector doorPos, FIntVector doorDirection)
{
	if (!doorMap.IsValid()) {
		UE_LOG(LogTemp, Warning, TEXT("door Map not initialized"));
		return false;
	}
	if (!doorMap->initialized) {
		UE_LOG(LogTemp, Warning, TEXT("door Map not initialized"));
		return false;
	}
	return doorMap->IsDoorPresent(doorPos, doorDirection);
}
TArray<FVector> emptyArray;
TArray<FVector> & RoomTemplate::GetPossibleRoomPositions(FIntVector doorPos, FIntVector doorDir)
{
	if (possibleDoorPositions.Num() == 0) {
		UE_LOG(LogTemp, Warning, TEXT("please build map first"));
		return emptyArray;
	}
	DOORPOS door(doorPos, doorDir);
	if (possibleDoorPositions.Contains(door)) {
		int id = possibleDoorPositions.Find(door);
		return possibleDoorPositions[id].possibleRoomPositions;
	}
	return emptyArray;
}

void RoomTemplate::ShowAllDoorPositions()
{
	for (int i = 0; i < possibleDoorPositions.Num(); i++)
	{
		DOORPOS& door = possibleDoorPositions[i];
		FVector temp(possibleDoorPositions[i].forward.X, possibleDoorPositions[i].forward.Y, 0);
		FColor color = FColor::Silver;

		if (temp.X == 1.0f)
			color = FColor::Green;
		else if (temp.X == -1.0f)
			color = FColor::Emerald;
		else if (temp.Y == 1.0f)
			color = FColor::Cyan;
		else if (temp.Y == -1.0f)
			color = FColor::Blue;
		DrawDebugSolidBox(world, nodeMap->GetRealPosition(door.position), FVector::OneVector * stepSize / 6.0f, FColor::Silver, false, 10, 0);
		DrawDebugSolidBox(world, nodeMap->GetRealPosition(door.position) + temp * stepSize / 3.0f, FVector::OneVector * stepSize / 6.0f, color, false, 10, 0);


	}
}

FVector RoomTemplate::GetClosestRoomPosition(FVector point)
{
	FVector closestPoint = possibleRoomPositions[0];
	float closestDistance = FVector::DistSquared(point, closestPoint);
	//get closest Point
	for (int i = 0; i < possibleRoomPositions.Num() - 1; i++)
	{
		float distance = FVector::DistSquared(point, possibleRoomPositions[i]);
		if (distance < closestDistance) {
			closestDistance = distance;
			closestPoint = possibleRoomPositions[i];
		}
	}

	return closestPoint;
}
