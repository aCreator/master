// Fill out your copyright notice in the Description page of Project Settings.

#include "Corridor.h"
#include "VisibilityManager.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/Engine.h"

ACorridor::ACorridor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	USceneComponent* root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Object"));
	root->SetupAttachment(RootComponent);
	SetRootComponent(root);
	world = GetWorld();
}

void ACorridor::InitCorridor(TArray<FVector> corridorPoints, float corridorWidth, float corridorHeight, UMaterial * corridorFloorMaterial, UMaterial * corridorWallMaterial, UMaterial * corridorCeilingMaterial)
{
	_corridorPoints = corridorPoints;
	_corridorWidth = corridorWidth;
	_corridorHeight = corridorHeight;

	_corridorCeilingMaterial = corridorCeilingMaterial;
	_corridorWallMaterial = corridorWallMaterial;
	_corridorFloorMaterial = corridorFloorMaterial;
}

void ACorridor::CreateCorridor(bool addVisibilityManager)
{
	_addVisibilityManager = addVisibilityManager;
	
	FVector Location(0.0f, 0.0f, 0.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;

	FVector previousPoint = _corridorPoints[0];
	FVector previousDirection;
	for (int i = 1; i < _corridorPoints.Num(); i++) {
		AHallway* hallWay = GetWorld()->SpawnActor<AHallway>(Location, Rotation, SpawnInfo);
		FVector hallwayDirection = _corridorPoints[i] - previousPoint;
		hallwayDirection.Normalize();

		FVector hallwayStart = previousPoint;
		if (i > 1)
			hallwayStart += hallwayDirection * _corridorWidth / 2.0f;
		FVector hallwayEnd = _corridorPoints[i];
		if (i < _corridorPoints.Num() - 1)
			hallwayEnd -= hallwayDirection * _corridorWidth / 2.0f;

		hallWay->Init(hallwayStart, hallwayEnd, _corridorWidth, _corridorHeight, _corridorFloorMaterial, _corridorWallMaterial, _corridorCeilingMaterial);

		AddHallway(hallWay, i == (_corridorPoints.Num() - 1));

		previousPoint = _corridorPoints[i];
		if (i == _corridorPoints.Num() - 1)
			break;
		FVector previousDirection = -hallwayDirection;
		FVector nextDirection = _corridorPoints[i] - _corridorPoints[i + 1];
		nextDirection.Normalize();
		if (i < _corridorPoints.Num() - 1) {
			ACorner* corner = GetWorld()->SpawnActor<ACorner>(Location, Rotation, SpawnInfo);
			corner->Init(previousPoint, -nextDirection, previousDirection, _corridorWidth, _corridorHeight, _corridorFloorMaterial, _corridorWallMaterial, _corridorCeilingMaterial);
			AddCorner(corner);
		}
	}
	if(_startRoomVM != nullptr)
		_startRoomVM->PlayerEntered();
}


// Sets default values


void ACorridor::AddHallway(AHallway * hallway, bool lastHallway)
{
	hallways.Add(hallway);
	hallway->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	float width = hallway->_width;
	float length = hallway->_length;
	FVector dir = hallway->_direction;
	dir.Normalize();
	FVector position = hallway->_position;
	float distance = hallway->_corridorWidth * 2;
	if (hallwayManagers.Num() == 0) {
		distance = hallway->_corridorWidth;
		position += dir * hallway->_corridorWidth / 2.0f;
	}
	if (lastHallway) {
		distance = hallway->_corridorWidth;
		position -= dir * hallway->_corridorWidth / 2.0f;
	}

	if (FMath::Abs(dir.X) < FMath::Abs(dir.Y)) {
		length += distance;
	}
	else {
		width += distance;
	}

	if (!_addVisibilityManager)
		return;

	AVisibilityManager* vism = GetWorld()->SpawnActor<AVisibilityManager>(position, Rotation, SpawnInfo);
	vism->Init(hallway->_position, width, length, dir);
	vism->AddMainObject(hallway, true);

	if (corners.Num() > 0) {
		vism->AddMainObject(corners.Last(), false);
	}

	if (hallwayManagers.Num() > 0) {
		vism->AddNeighbour(hallwayManagers.Last(), true);
	}
	else {
		//_startDoorVM->AddNeighbour(vism, false);
		//vism->AddDoor(_startDoorVM);
		vism->AddMainObject(_startDoor, false);
		vism->AddMainObject(_startDoorFrame, false);
		//vism->AddNeighbour(_startRoomVM, false);
	}
	if (lastHallway) {
		//_endDoorVM->AddNeighbour(vism, false);
		//vism->AddDoor(_endDoorVM);
		vism->AddMainObject(_endDoor, false);
		vism->AddMainObject(_endDoorFrame, false);
		//vism->AddNeighbour(_endRoomVM, false);
	}

	vism->Deactivate();
	vism->DeactivateAllMainObjects();

	vism->corridor = this;
	vism->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);

	hallwayManagers.Add(vism);
}

void ACorridor::AddCorner(ACorner * corner)
{
	corners.Add(corner);
	corner->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
	if (hallwayManagers.Num() > 0) {
		AVisibilityManager * vism = hallwayManagers.Last();
		vism->AddMainObject(corner, false);
	}
}

void ACorridor::ActivateCorridor(AVisibilityManager * currentRoomVM)
{
	WasActivated(currentRoomVM);
	
	if (hallwayManagers.Num() == 0)
		return;
	AVisibilityManager * firstHallwayManager;

	if (currentRoomVM == _startRoomVM)
		firstHallwayManager = hallwayManagers[0];
	else
		firstHallwayManager = hallwayManagers.Last();

	firstHallwayManager->PlayerEntered();

	playerInside = false;
}

void ACorridor::DeactivateCorridor(AVisibilityManager * currentRoomVM)
{
	WasDeactivated(currentRoomVM);

	if (hallwayManagers.Num() == 0)
		return;

	for (AVisibilityManager* vism : hallwayManagers) {
		vism->DeactivateAllMainObjects();
		vism->Deactivate();
	}
	currentRoomVM->ActivateAllMainObjects();

	if (_endRoomVM != currentRoomVM) {
		_endRoomVM->DeactivateAllMainObjects();
		_endRoomVM->Deactivate();
	}
	else {
		_startRoomVM->DeactivateAllMainObjects();
		_startRoomVM->Deactivate();
	}

}


void ACorridor::Destroy()
{
	for (AActor* h : hallways) {
		if (h != nullptr)
			h->Destroy();
	}
	for (AActor* c : corners) {
		if (c != nullptr)
			c->Destroy();
	}
	for (AActor* hm : hallwayManagers) {
		if (hm != nullptr)
			hm->Destroy();
	}

	if (_startDoorFrame != nullptr)
		_startDoorFrame->Destroy();

	if (_endDoorFrame != nullptr)
		_endDoorFrame->Destroy();

	/*if (_endDoorVM != nullptr) {
		_endDoorVM->Destroy();
	}

	if (_startDoorVM != nullptr) {
		_startDoorVM->Destroy();
	}
	
	/*if (_endRoomVM != nullptr) {
		_endRoomVM->Destroy();
	}	

	if (_startRoomVM != nullptr) {
		_startRoomVM->Destroy();
	}*/

	Super::Destroy();
}

void ACorridor::AddRooms(ARoom * startRoom, AVisibilityManager* startRoomVM, ARoom * endRoom, AVisibilityManager* endRoomVM)
{
	_startRoom = startRoom;
	_endRoom = endRoom;
	_endRoomVM = endRoomVM;
	_startRoomVM = startRoomVM;
	SetActorLabel(*FString("Corridor Room " + FString::FromInt(startRoom->id) + " to Room " + FString::FromInt(endRoom->id)));
	endRoomVM->Deactivate();
	endRoomVM->DeactivateAllMainObjects();
}

void ACorridor::AddDoors(ADoor * startDoor, ADoor * endDoor, bool spawnDoorFrame, float minimumDistanceBetweenDoors)
{
	_endDoor = endDoor;
	_endDoor->_corridor = this;
	_startRoomVM->AddMainObject(startDoor, false);
	_endRoomVM->AddMainObject(endDoor, false);

	endDoor->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
	startDoor->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);

	//_endDoorVM = GetDoorVisibilityManager(_endRoom, _endRoomVM, endDoor);
	//_endDoorVM->Deactivate();
	//_endDoorVM->DeactivateAllMainObjects();
	//_endDoorVM->DeactivateAllNeighbours();
	_startDoor = startDoor;
	_startDoor->_corridor = this;
	//_startDoorVM = GetDoorVisibilityManager(_startRoom, _startRoomVM, startDoor);
	if (spawnDoorFrame) {
		FRotator Rotation(0.0f, 0.0f, 0.0f);
		FActorSpawnParameters SpawnInfo;
		if (_startDoor != nullptr) {
			float frameWidth = FMath::Max(_corridorWidth, (_startDoor->_doorWidth + minimumDistanceBetweenDoors));
			bool wasActive = _startDoor->active;
			if (!wasActive)
				_startDoor->ActivateObject();
			_startDoorFrame = GetWorld()->SpawnActor<ADoorFrame>(_corridorPoints[0], Rotation, SpawnInfo);
			_startDoorFrame->Init(_startDoor->_roomInID, _corridorPoints[0], frameWidth, _corridorHeight, _corridorWallMaterial, _startDoor);
			_startDoor->_doorFrame = _startDoorFrame;
			_startDoorFrame->AttachToActor(_startDoor, FAttachmentTransformRules::KeepRelativeTransform);
			if (!wasActive)
				_startDoor->DeactivateObject();
			//_startDoorVM->AddMainObject(_startDoorFrame);
		}
		if (_endDoor != nullptr) {

			float frameWidth = FMath::Max(_corridorWidth, (_endDoor->_doorWidth + minimumDistanceBetweenDoors));
			bool wasActive = _endDoor->active;
			if (!wasActive)
				_endDoor->ActivateObject();
			_endDoorFrame = GetWorld()->SpawnActor<ADoorFrame>(_corridorPoints.Last(), Rotation, SpawnInfo);
			_endDoorFrame->Init(_endDoor->_roomInID, _corridorPoints.Last(), frameWidth, _corridorHeight, _corridorWallMaterial, _endDoor);
			_endDoor->_doorFrame = _endDoorFrame;
			_endDoorFrame->AttachToActor(_endDoor, FAttachmentTransformRules::KeepRelativeTransform);
			if (!wasActive)
				_endDoor->DeactivateObject();
			//_endDoorVM->AddMainObject(_endDoorFrame);
		}
		_startRoomVM->AddMainObject(_startDoorFrame, false);
		_endRoomVM->AddMainObject(_endDoorFrame, false);
	}
	_endDoor->_room = _endRoomVM;
	_startDoor->_room = _startRoomVM;
}

void ACorridor::PlayerEnteredCorridorBit()
{
	if (!playerInside) {
		/*//Deactivate all neigbouring corridors
		TArray<AActor*> foundActors;
		UGameplayStatics::GetAllActorsOfClass(world, ACorridor::StaticClass(), foundActors);

		for (AActor* c : foundActors) {

			//((ACorridor*)c)->_startDoorVM->active = false;
			//((ACorridor*)c)->_endDoorVM->active = false;
		}*/
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("Entered corridor %i to %i"), _startRoom->id, _endRoom->id));

	}
	playerInside = true;
}

void ACorridor::PlayerLeftCorridorBit()
{
	for (AVisibilityManager* hm : hallwayManagers) {
		if (hm->playerInside)
			//do nothing, player still in corridor
			return;
	}
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Turquoise, FString::Printf(TEXT("Left corridor %i to %i"), _startRoom->id, _endRoom->id));

	playerInside = false;
	//Activate all neigbouring corridors
/*	TArray<AActor*> foundActors;
	UGameplayStatics::GetAllActorsOfClass(world, ACorridor::StaticClass(), foundActors);

	for (AActor* c : foundActors) {

		//((ACorridor*) c)->_startDoorVM->active = true;
		//((ACorridor*)c)->_endDoorVM->active = true;
	}*/
}

// Called when the game starts or when spawned
void ACorridor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACorridor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

