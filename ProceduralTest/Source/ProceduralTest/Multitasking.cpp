// Fill out your copyright notice in the Description page of Project Settings.
#include "Multitasking.h"
#include "Engine.h"

Multitasking* Multitasking::Runnable = NULL;

Multitasking::Multitasking(APathCreator *p)
	: StopTaskCounter(0)
{
	//Link to where data should be stored
	pathCreator = p;
	const bool bAutoDeleteSelf = false;
	const bool bAutoDeleteRunnable = false;
	Thread = FRunnableThread::Create(this, TEXT("ThreadX"), 0, TPri_BelowNormal); //windows default = 8mb for thread, could specify more
}

Multitasking::~Multitasking()
{
	delete Thread;
	Thread = NULL;
}

//Init
bool Multitasking::Init()
{
	//Init the Data 
	return true;
}

//Run
uint32 Multitasking::Run()
{
	//Initial wait before starting
	FPlatformProcess::Sleep(0.03);
	UE_LOG(LogTemp, Warning, TEXT("Multitask Test"));
	DoTestLoop();
	Shutdown();
	return 0;
}

//stop
void Multitasking::Stop()
{
	StopTaskCounter.Increment();
}

Multitasking* Multitasking::JoyInit(APathCreator *p)
{
	//Create new instance of thread if it does not exist
	//        and the platform supports multi threading!
	if (!Runnable && FPlatformProcess::SupportsMultithreading())
	{
		Runnable = new Multitasking(p);
	}
	return Runnable;
}

void Multitasking::DoTestLoop() {
	for (int i = 0; i < 30; i++) {
		FPlatformProcess::Sleep(1.0f);
		if (pathCreator == nullptr) {
			UE_LOG(LogTemp, Warning, TEXT("pathCreator is nullptr"));
		}
		else {
			//pathCreator->CreateRandomRoomWithPath();
		}
		UE_LOG(LogTemp, Warning, TEXT("Multitask Test Loop"));
	}
}

void Multitasking::EnsureCompletion()
{
	Stop();
	Thread->WaitForCompletion();
}

void Multitasking::Shutdown()
{
	if (Runnable)
	{
		Runnable->EnsureCompletion();
		delete Runnable;
		Runnable = NULL;
	}
}

bool Multitasking::IsThreadFinished()
{
	if (Runnable) return Runnable->IsFinished();
	return true;
}