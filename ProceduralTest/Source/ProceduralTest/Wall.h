// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class PROCEDURALTEST_API Wall
{
public:
	FVector p1 = FVector(0, 0, 0);
	FVector p2 = FVector(0, 0, 0);
	FVector p3 = FVector(0, 0, 0);
	FVector p4 = FVector(0, 0, 0);

	FVector normal = FVector(0, 0, 0);
	FVector tangent = FVector(0, 0, 0);

	FVector2D uv1 = FVector2D(0, 0);
	FVector2D uv2 = FVector2D(0, 0);
	FVector2D uv3 = FVector2D(0, 0);
	FVector2D uv4 = FVector2D(0, 0);

	float thickness = 0;

	Wall();

	Wall(FVector p1,
		FVector p2,
		FVector p3,
		FVector p4,
		FVector normal,
		FVector tangent,

		FVector2D uv1,
		FVector2D uv2,
		FVector2D uv3,
		FVector2D uv4);

	Wall(FVector p1,
		FVector p2,
		FVector p3,
		FVector p4,
		FVector normal,
		FVector tangent,

		FVector2D uv1,
		FVector2D uv2,
		FVector2D uv3,
		FVector2D uv4,

		float thickness);

	~Wall();
};
