// Fill out your copyright notice in the Description page of Project Settings.

#include "NodeMap.h"
#include "Obstacle.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Public/DrawDebugHelpers.h"

NodeMap::NodeMap(UWorld * world)
{
	this->world = world;
}

NodeMap::NodeMap() {

}


NodeMap::~NodeMap()
{
}

FVector NodeMap::GetClosestPossiblePoint(const FVector& position)
{

	FVector closestPoint = possibleNodes[0];
	float closestDistance = FVector::DistSquared(position, possibleNodes[0]);
	//get closest Point
	for (int i = 0; i < possibleNodes.Num() - 1; i++)
	{
		float distance = FVector::DistSquared(position, possibleNodes[i]);
		if (distance < closestDistance) {
			closestDistance = distance;
			closestPoint = possibleNodes[i];
		}
	}

	return closestPoint;
}

bool NodeMap::CheckIfNodeIsValid(const FIntVector& nodePosition)
{
	if (nodePosition.X >= nodeArrayDimensions.X || nodePosition.Y >= nodeArrayDimensions.Y || nodePosition.X < 0 || nodePosition.Y < 0)
		return false;
	int arrayPosition = GetArrayPosition(nodePosition);
	return nodeArray[arrayPosition];
}

int NodeMap::GetArrayPosition(const FIntVector& nodePosition)
{
	return nodePosition.X + nodeArrayDimensions.X * nodePosition.Y;
}

int NodeMap::NodeRayCast(const FIntVector& position, const FIntVector& direction)
{
	return NodeRayCast(position, direction, false);
}

int NodeMap::NodeRayCast(const FIntVector & position, const FIntVector & direction, bool visualize)
{
	FIntVector currentPos = position;
	int length = 0;
	while (CheckIfNodeIsValid(currentPos)) {
		if (visualize)
			DrawDebugSolidBox(world, GetRealPosition(currentPos), FVector(corridorWidth / 2.0f), FColor::Green, false, 10, 0);
		currentPos += direction;
		length++;
	}
	if (visualize)
		DrawDebugSolidBox(world, GetRealPosition(currentPos), FVector(corridorWidth/2.0f), FColor::Red, false, 10, 0);
	return length;
}

bool NodeMap::CheckIfBreakOutNecessary(const FIntVector& position, const FIntVector& forward, const int& minimumDistance)
{
	return CheckIfBreakOutNecessary(position, forward, minimumDistance, false);
}

bool NodeMap::CheckIfBreakOutNecessary(const FIntVector & position, const FIntVector & forward, const int & minimumDistance, bool visualize)
{
	FIntVector checkDirection = FIntVector(forward.Y, forward.X, 0);
	int length = NodeRayCast(position, checkDirection, visualize);
	if (length > minimumDistance)
		return false;

	checkDirection *= -1;
	length = NodeRayCast(position, checkDirection, visualize);
	if (length > minimumDistance)
		return false;
	return true;
}

FIntVector NodeMap::GetPositionNodeArray(const FVector& pos)
{
	FVector temp = pos - firstNodePos;
	FVector arrayPos = temp / stepSize;

	return FIntVector(arrayPos.X, arrayPos.Y, 0);
}

FVector NodeMap::GetRealPosition(const FIntVector& pos)
{
	FVector temp(pos.X, pos.Y, 0);
	temp *= stepSize;
	return temp + firstNodePos;
}



void NodeMap::CreateNodeMap(const float& stepSize, const float& corridorWidth, const FVector& trackingSpaceSize)
{
	this->stepSize = stepSize;
	this->trackingSpaceSize = trackingSpaceSize;
	float xOffset = FMath::Fmod(trackingSpaceSize.X, stepSize);
	float yOffset = FMath::Fmod(trackingSpaceSize.Y, stepSize);
	this->corridorWidth = corridorWidth;
	//UE_LOG(LogTemp, Warning, TEXT("offset x: %f y: %f"), xOffset, yOffset);
	nodeTrackingSpaceSize = FVector(trackingSpaceSize.X - xOffset, trackingSpaceSize.Y - yOffset, trackingSpaceSize.Z);
	float nodesAmountX = nodeTrackingSpaceSize.X / stepSize;
	float nodesAmountY = nodeTrackingSpaceSize.Y / stepSize;
	//UE_LOG(LogTemp, Warning, TEXT("nodesAmount x: %f y: %f"), nodesAmountX, nodesAmountY);

	nodeArrayDimensions = FIntVector(nodesAmountX, nodesAmountY, 0);
	if(nodeArray != nullptr)
		delete[] nodeArray;
	nodeArray = new bool[nodeArrayDimensions.X * nodeArrayDimensions.Y];

	xOffset = xOffset / 2.0f;
	yOffset = yOffset / 2.0f;

	firstNodePos.X = -1.0f * trackingSpaceSize.X / 2.0f + stepSize / 2.0f + xOffset;
	firstNodePos.Y = -1.0f * trackingSpaceSize.Y / 2.0f + stepSize / 2.0f + yOffset;
	firstNodePos.Z = 0.0f;

	int i = 0;
	int j = 0;
	for (float x = -1.0f * trackingSpaceSize.X / 2.0f + stepSize / 2.0f + xOffset; x < trackingSpaceSize.X / 2.0f - stepSize / 2.0f; x += stepSize)
	{

		j = 0;
		for (float y = -1.0f * trackingSpaceSize.Y / 2.0f + stepSize / 2.0f + yOffset; y < trackingSpaceSize.Y / 2.0f - stepSize / 2.0f; y += stepSize)
		{
			TArray<AActor*> foundActors;
			UGameplayStatics::GetAllActorsOfClass(world, AObstacle::StaticClass(), foundActors);

			FVector position = FVector(x, y, 0.0f);
			bool free = CheckIfFree(foundActors, FVector(corridorWidth, corridorWidth, corridorWidth), position, false);
			if (free)
				possibleNodes.Add(position);

			nodeArray[i + nodeArrayDimensions.X * j] = free;
			j++;
		}
		i++;
	}
	//UE_LOG(LogTemp, Warning, TEXT("i: %i j: %i"), i, j);

}

bool NodeMap::CheckIfFree(const TArray<AActor*>& foundActors, const FVector& size, const FVector& position, const bool& addVisualization)
{
	FVector absPosition = FVector(FMath::Abs(position.X), FMath::Abs(position.Y), 0);
	if (absPosition.X + size.X / 2.0f > trackingSpaceSize.X / 2.0f ||
		absPosition.Y + size.Y / 2.0f > trackingSpaceSize.Y / 2.0f)
		return false;

	FVector* points = new FVector[2];
	points[0] = position + size / 2.0f;
	points[1] = position - size / 2.0f;
	FBoxSphereBounds boundingBox(points, 2);

	for (int i = 0; i < foundActors.Num(); i++)
	{
		AObstacle* o = (AObstacle *)foundActors[i];
		if (FBoxSphereBounds::BoxesIntersect(o->GetBoundingBox(), boundingBox)) {
			//if(addVisualization)
			//	AddTestCube(boundingBox, FLinearColor::Black, 0.1f, "not free", false);
			if (addVisualization)
				DrawDebugSolidBox(world, position, FVector(size.X / 2.0f, size.Y / 2.0f, 4.0f) - FVector::OneVector * 3, FColor::Black, false, 10, 0);

			return false;
		}
	}
	//if (addVisualization)
	//AddTestCube(boundingBox, FLinearColor::Blue, 0.15f, "free", false);
	if (addVisualization)
		DrawDebugSolidBox(world, position, FVector(size.X / 2.0f, size.Y / 2.0f, 4.0f) - FVector::OneVector * 3, FColor::Blue, false, 10, 0);

	return true;
}
