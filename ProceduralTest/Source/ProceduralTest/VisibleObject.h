// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VisibleObject.generated.h"

UCLASS()
class PROCEDURALTEST_API AVisibleObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVisibleObject();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool deactivateCollisions = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool active = true;
	UPROPERTY(EditAnywhere)
		float _length = 0;
	UPROPERTY(EditAnywhere)
		float _width = 0;
	UPROPERTY(EditAnywhere)
		FVector _position = FVector::ZeroVector;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable)
		virtual void ActivateObject();
	UFUNCTION(BlueprintCallable)
		virtual void DeactivateObject();
	
};
