// Fill out your copyright notice in the Description page of Project Settings.

#include "MeshCreator.h"
#include "KismetProceduralMeshLibrary.h"

MeshCreator::MeshCreator()
{
}

MeshCreator::~MeshCreator()
{
}

void MeshCreator::CreateSimpleHallway(FVector p1, FVector p2, float height) {
	CreateWall(p1, FVector(p1.X, p2.Y, 0), height);
	CreateWall(p1, FVector(p2.X, p1.Y, 0), height);
	CreateWall(p2, FVector(p2.X, p1.Y, 0), height);
	CreateWall(p2, FVector(p1.X, p2.Y, 0), height);

	CreateSimpleFloor(p1, p2, true);
	CreateSimpleFloor(FVector(p1.X, p1.Y, p1.Z + height), FVector(p2.X, p2.Y, p2.Z + height), false);
}

void MeshCreator::ClearMesh()
{
	vertices.Empty();
	tangents.Empty();
	UV0.Empty();
	Triangles.Empty();
	normals.Empty();
	vertexColors.Empty();
	triangleIndex = 0;
	mesh->ClearAllMeshSections();
}

void MeshCreator::CreateTriangle(FVector p1, FVector p2, FVector p3, FVector2D uv1, FVector2D uv2, FVector2D uv3, FVector tangent, FVector normal) {

	vertices.Add(p1);
	vertices.Add(p2);
	vertices.Add(p3);

	Triangles.Add(triangleIndex);
	triangleIndex++;
	Triangles.Add(triangleIndex);
	triangleIndex++;
	Triangles.Add(triangleIndex);
	triangleIndex++;

	/*FVector normal = GetNormal(p1, p2, p3);/*p2 - p1;//Calculating the normal of the wall
	float temp = normal.X;
	normal.X = normal.Y;
	normal.Y = -temp;
	normal.Normalize();
	*/
	normals.Add(normal); //Adding the normal of the wall for each vertex
	normals.Add(normal);
	normals.Add(normal);

	UV0.Add(uv1);
	UV0.Add(uv2);
	UV0.Add(uv3);

	tangents.Add(FProcMeshTangent(tangent.X, tangent.Y, tangent.Z));
	tangents.Add(FProcMeshTangent(tangent.X, tangent.Y, tangent.Z));
	tangents.Add(FProcMeshTangent(tangent.X, tangent.Y, tangent.Z));

	vertexColors.Add(currentColor);
	vertexColors.Add(currentColor);
	vertexColors.Add(currentColor);
}

void MeshCreator::CreateTriangle(FVector p1, FVector p2, FVector p3, FVector2D uv1, FVector2D uv2, FVector2D uv3)
{
	FVector normal = GetNormal(p1, p2, p3);

	FVector tangent;
	tangent.Y = normal.X;
	tangent.X = -normal.Y;

	CreateTriangle(p1, p2, p3, uv1, uv2, uv3, tangent, normal);
}

Wall MeshCreator::CreateWall(FVector p1, FVector p2, float roomHeight, Wall startWall) {
	FVector p3 = FVector(p2.X, p2.Y, p2.Z + roomHeight);
	FVector p4 = FVector(p1.X, p1.Y, p1.Z + roomHeight);
	
	float heightDifference = p1.Y + roomHeight - startWall.p3.Y;
	
	//float heightDifference = p1.Y + roomHeight - startWall.p3.Y;

	
//	FVector2D startUV = startWall.uv3;
//	startUV = FVector2D(startUV.X, startUV.Y - roomHeight / uvSize);
	FVector2D startUV = startWall.uv3;
	startUV = FVector2D(startUV.X, startUV.Y - heightDifference / uvSize);
	//FVector2D startUV;
	//startUV.X = p4.X - startWall.p4.X;
	//startUV.Y = p4.Z - startWall.p4.Z;
	//startUV / uvSize;
	//startUV.X = 0;
	//startUV.Y = startWall.uv3.Y;
	float wallLength = (p1 - p2).Size();
	FVector2D uv1 = startUV;
	FVector2D uv2 = FVector2D(wallLength, 0) / uvSize + startUV;
	FVector2D uv3 = FVector2D(wallLength, roomHeight) / uvSize + startUV;
	FVector2D uv4 = FVector2D(0, roomHeight) / uvSize + startUV;


	FVector normal = GetNormal(p1, p2, p3);

	FVector tangent;
	tangent.Y = normal.X;
	tangent.X = -normal.Y;


	Wall bla = Wall(p1, p2, p3, p4, normal, tangent, uv1, uv2, uv3, uv4);
	return bla;
}

void MeshCreator::AddWallToMesh(Wall wall) {
	AddWallToMesh(wall, false);
}

void MeshCreator::AddWallToMesh(Wall wall, bool addBothSides) {
	if (wall.thickness == 0) {
		//front
		//lower triangle
		CreateTriangle(wall.p2, wall.p1, wall.p3, wall.uv2, wall.uv1, wall.uv3, wall.tangent, wall.normal);
		//upper triangle
		CreateTriangle(wall.p3, wall.p1, wall.p4, wall.uv3, wall.uv1, wall.uv4, wall.tangent, wall.normal);
		if (addBothSides) {
			//back
			//lower triangle
			CreateTriangle(wall.p1, wall.p2, wall.p3, wall.uv1, wall.uv2, wall.uv3, wall.tangent, wall.normal);
			//upper triangle
			CreateTriangle(wall.p3, wall.p4, wall.p1, wall.uv3, wall.uv4, wall.uv1, wall.tangent, wall.normal);
		}
	}
	else {
		Wall wallFront = Wall(wall);
		wallFront.normal = wall.normal;
		wallFront.p1 = wall.p1 + (wall.normal * wall.thickness);
		wallFront.p2 = wall.p2 + (wall.normal * wall.thickness);
		wallFront.p3 = wall.p3 + (wall.normal * wall.thickness);
		wallFront.p4 = wall.p4 + (wall.normal * wall.thickness);

		wallFront.thickness = 0;

		AddWallToMesh(wallFront);

		Wall wallBack = Wall(wall);
		wallBack.normal = wall.normal;
		wallBack.p1 = wall.p1;
		wallBack.p2 = wall.p2;
		wallBack.p3 = wall.p3;
		wallBack.p4 = wall.p4;

		wallBack.thickness = 0;
		if (addBothSides) {
			AddWallToMesh(wallBack);
		}
		float wallHeight = wall.p4.Z - wall.p1.Z;

		//add ridges
		AddWallToMesh(CreateWall(wallBack.p1, wallFront.p1, wallHeight));
		AddWallToMesh(CreateWall(wallFront.p2, wallBack.p2, wallHeight));

		FVector bottomPosition = wallBack.p1 + (wallFront.p2 - wallBack.p1) / 2.0f;
		FVector2D dimensions;
		dimensions.X = FMath::Abs(wallBack.p1.X - wallFront.p2.X);
		dimensions.Y = FMath::Abs(wallBack.p1.Y - wallFront.p2.Y);
		Wall bottom = CreateSimpleFloor(bottomPosition, dimensions, false);
		AddWallToMesh(bottom);

		FVector topPosition = wallBack.p3 + (wallFront.p4 - wallBack.p3) / 2.0f;
		Wall top = CreateSimpleFloor(topPosition, dimensions, true);
		//AddWallToMesh(top);
	}
}

Wall MeshCreator::CreateWall(FVector p1, FVector p2, float roomHeight) {
	return CreateWall(p1, p2, roomHeight, Wall());
}

FVector MeshCreator::GetNormal(FVector v1, FVector v2, FVector v3)
{
	FVector a, b;
	a = v1 - v2;
	b = v1 - v3;
	FVector normal = FVector::CrossProduct(a, b);
	normal.Normalize();
	return normal;
}

Wall MeshCreator::CreateSimpleFloor(FVector middlePosition, FVector2D dimensions, bool up)
{
	FVector p1 = FVector(middlePosition.X - dimensions.X / 2.0f, middlePosition.Y - dimensions.Y / 2.0f, middlePosition.Z);
	FVector p2 = FVector(middlePosition.X + dimensions.X / 2.0f, middlePosition.Y + dimensions.Y / 2.0f, middlePosition.Z);
	FVector p3 = FVector(p1.X, p2.Y, middlePosition.Z);
	FVector p4 = FVector(p2.X, p1.Y, middlePosition.Z);
	if(!up)
		return Wall(CreateSimpleFloor(p1, p2, up));
	else
		return Wall(CreateSimpleFloor(p3, p4, up));
}

Wall MeshCreator::CreateSimpleFloor(FVector p1, FVector p2, bool up) {
	float floorLength = fabs(p1.Y - p2.Y);
	float floorWidth = fabs(p1.X - p2.X);

	FVector2D uv1 = FVector2D(0, 0) / uvSize;
	FVector2D uv2 = FVector2D(floorLength, 0) / uvSize;
	FVector2D uv3 = FVector2D(floorLength, floorWidth) / uvSize;
	FVector2D uv4 = FVector2D(0, floorWidth) / uvSize;
	FVector p3 = p2;
	p2 = FVector(p1.X, p2.Y, p2.Z);
	FVector p4 = FVector(p3.X, p1.Y, p1.Z);

	FVector normal = GetNormal(p1, p2, FVector(p1.X, p2.Y, p1.Z));
	normal = normal * -1;
	FVector tangent;
	tangent.Y = normal.X;
	tangent.X = -normal.Y;
	if(up)
		normal = FVector(0.0f, 0.0f, 1.0f);
	else
		normal = FVector(0.0f, 0.0f, -1.0f);


	tangent = p2 - p1;
	tangent.Normalize();

	//lower triangle
	//front
	//CreateTriangle(p1, p2, p3, uv1, uv2, uv3, tangent, normal);
	//back
	//CreateTriangle(p2, p1, p3, uv2, uv1, uv3, -tangent, -normal);

	//upper triangle
	//front
	//CreateTriangle(p4, p1, p3, uv4, uv1, uv3, tangent, normal);
	//back
	//CreateTriangle(p4, p3, p1, uv4, uv3, uv1, -tangent, -normal);

	return Wall(p1, p2, p3, p4, normal, tangent, uv1, uv2, uv3, uv4);
}

void MeshCreator::CreateWallWithCorners(FVector p1, FVector p2, float height, Wall startWall) {
	CreateWall(p1, FVector(p1.X, p2.Y, p2.Z), height, startWall);
	CreateWall(p1, FVector(p2.X, p1.Y, p1.Z), height, startWall);
	CreateWall(p2, FVector(p2.X, p1.Y, p2.Z), height, startWall);
	CreateWall(p2, FVector(p1.X, p2.Y, p1.Z), height, startWall);

	CreateSimpleFloor(p1, p2, true);
	CreateSimpleFloor(FVector(p1.X, p1.Y, p1.Z + height), FVector(p2.X, p2.Y, p2.Z + height), false);
}

void MeshCreator::CreateSimpleRoom(FVector p1, FVector p2, float height) {
	CreateWallsWithDoors(p1, FVector(p1.X, p2.Y, 0), height, wallThickness, numberOfDoors, doorHeight, doorWidth);
	CreateWallsWithDoors(p1, FVector(p2.X, p1.Y, 0), height, wallThickness, numberOfDoors, doorHeight, doorWidth);
	CreateWallsWithDoors(p2, FVector(p2.X, p1.Y, 0), height, wallThickness, numberOfDoors, doorHeight, doorWidth);
	CreateWallsWithDoors(p2, FVector(p1.X, p2.Y, 0), height, wallThickness, numberOfDoors, doorHeight, doorWidth);

	AddWallToMesh(CreateSimpleFloor(p2, p1, true));
	AddWallToMesh(CreateSimpleFloor(FVector(p1.X, p1.Y, p1.Z + height), FVector(p2.X, p2.Y, p2.Z + height), false));
}

Wall MeshCreator::CreateWall(FVector p1, FVector p2, float roomHeight, float thickness, Wall startWall) {
	/*FVector2D startUV = startWall.uv2;
	float wallLength = (p1 - p2).Size();
	FVector2D uv = FVector2D(wallLength, roomHeight) / uvSize + startUV;

	FVector p3 = FVector(p2.X, p2.Y, p2.Z + roomHeight);
	FVector normal = GetNormal(p1, p2, p3);
	FVector newP1 = p1 - (normal * thickness / 2.0f);
	FVector newP2 = p2 + (normal * thickness / 2.0f);
	CreateWallWithCorners(newP1, newP2, roomHeight, startUV);
	*/
	Wall wall = CreateWall(p1, p2, roomHeight, startWall);
	wall.thickness = thickness;
	return wall;
}

Wall MeshCreator::CreateWall(FVector p1, FVector p2, float roomHeight, float thickness) {
	return CreateWall(p1, p2, roomHeight, thickness, Wall());
}

void MeshCreator::CreateWallsWithDoors(FVector p1, FVector p2, float roomHeight, float thickness, int numberOfDoors, float doorHeight, float doorWidth) {
	FVector wallVector = p2 - p1;
	float wallLength = wallVector.Size();
	FVector wallDirection = wallVector;
	wallDirection.Normalize();
	FVector wallStartPoint = p1;
	float wallStart = thickness;

	while (wallLength - thickness * 2 < numberOfDoors * doorWidth) {
		numberOfDoors--;
	}
	Wall previousWall = Wall();
	//UE_LOG(LogTemp, Warning, TEXT("previousUV: %f"), previousUV.X);

	while (numberOfDoors > 0) {
		if (wallLength - thickness * 2 < numberOfDoors * doorWidth)
			break;
		float doorStart = FMath::FRandRange(wallStart, wallStart + wallLength / numberOfDoors - (thickness + doorWidth));
		FVector doorStartPoint = p1 + wallDirection * doorStart;
		previousWall = CreateWall(wallStartPoint, doorStartPoint, roomHeight, thickness, previousWall);
		//UE_LOG(LogTemp, Warning, TEXT("previousUV: %f"), previousUV.X);
		wallStartPoint = doorStartPoint + wallDirection * doorWidth;
		wallStart = doorStart + doorWidth;
		wallLength -= wallStart;
		numberOfDoors--;
		AddWallToMesh(previousWall);

		//add area above door
		previousWall = CreateWall(doorStartPoint + FVector(0, 0, doorHeight), wallStartPoint + FVector(0, 0, doorHeight), roomHeight - doorHeight, thickness, previousWall);
		AddWallToMesh(previousWall);
	}
	AddWallToMesh(CreateWall(wallStartPoint, p2, roomHeight, thickness, previousWall));
}

void MeshCreator::GenerateMesh()
{
	TArray<FVector> generatedNormals;
	TArray<FProcMeshTangent> generatedTangents;
	UKismetProceduralMeshLibrary::CalculateTangentsForMesh(vertices, Triangles, UV0, generatedNormals, generatedTangents);
	//generated normals and tangents
	//mesh->CreateMeshSection_LinearColor(0, vertices, Triangles, generatedNormals, UV0, vertexColors, generatedTangents, true);
	
	//generated normals
	//mesh->CreateMeshSection_LinearColor(0, vertices, Triangles, generatedNormals, UV0, vertexColors, tangents, true);
	
	//generated tangents
	//mesh->CreateMeshSection_LinearColor(0, vertices, Triangles, normals, UV0, vertexColors, generatedTangents, true);

	//previously defined tangents and normals
	mesh->CreateMeshSection_LinearColor(0, vertices, Triangles, normals, UV0, vertexColors, tangents, true);

	// Enable collision data
	mesh->ContainsPhysicsTriMeshData(true);
}

void MeshCreator::SetMesh(UProceduralMeshComponent * mesh)
{
	this->mesh = mesh;
}

void MeshCreator::SetMaterial(UMaterial * wallMaterial) {
	mesh->SetMaterial(0, wallMaterial);
}

