// Fill out your copyright notice in the Description page of Project Settings.

#include "Testing.h"
#include "PathCreator.h"
#include "EngineUtils.h"
#include "Engine/Engine.h"
#include "GameFramework/Actor.h"
#include "Hallway.h"

// Sets default values
ATesting::ATesting()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATesting::BeginPlay()
{
	Super::BeginPlay();
	TestMethod();

}

// Called every frame
void ATesting::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

//https://answers.unrealengine.com/questions/289453/get-all-actors-of-class-in-c.html
template<typename T>
void FindAllActors(UWorld* World, TArray<T*>& Out)
{
	for (TActorIterator<AActor> It(World, T::StaticClass()); It; ++It)
	{
		T* Actor = Cast<T>(*It);
		if (Actor && !Actor->IsPendingKill())
		{
			Out.Add(Actor);
		}
	}
}

void ATesting::TestMethod()
{
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	AHallway* hallway = GetWorld()->SpawnActor<AHallway>(GetActorLocation(), Rotation);
	hallway->TestMethod();
}

#if WITH_EDITOR  
void ATesting::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	//UE_LOG(LogTemp, Warning, TEXT("butnc"));
	//mesh = GetOwner()->FindComponentByClass<UProceduralMeshComponent>();

	//Get the name of the property that was changed  
	FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(ATesting, startTest))) {
		/*TArray<APathCreator*> pathCreators;
		FindAllActors(GetWorld(), pathCreators);
		if (pathCreators.Num() > 0) {
			pathCreator = pathCreators[0];
			Multitasking* thread = Multitasking::JoyInit(pathCreator);

			//pathCreator->CreateRandomRoomWithPath();
		}*/

		TestMethod();
	}
	// Call the base class version  
	Super::PostEditChangeProperty(PropertyChangedEvent);
}

#endif
