// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Engine/World.h"

/**
 * 
 */
class PROCEDURALTEST_API NodeMap
{
public:
	NodeMap(UWorld * world);
	NodeMap();

	~NodeMap();
	bool *nodeArray = nullptr;
	FVector trackingSpaceSize;
	FVector nodeTrackingSpaceSize;
	FIntVector nodeArrayDimensions;
	FVector firstNodePos;
	UWorld * world;
	TArray<FVector> possibleNodes;
	float stepSize;
	float corridorWidth;

	///Find all positions where corridor can be
	void CreateNodeMap(const float& stepSize, const float& corridorWidth, const FVector& trackingSpaceSize);
	bool CheckIfFree(const TArray<AActor*>& foundActors, const FVector& size, const FVector& position, const bool& addVisualization);
	FVector GetClosestPossiblePoint(const FVector& position);
	bool CheckIfNodeIsValid(const FIntVector& nodePosition);
	int GetArrayPosition(const FIntVector& nodePosition);
	int NodeRayCast(const FIntVector& position, const FIntVector& direction);
	int NodeRayCast(const FIntVector& position, const FIntVector& direction, bool visualize);

	bool CheckIfBreakOutNecessary(const FIntVector& position, const FIntVector& forward, const int& minimumDistance);
	bool CheckIfBreakOutNecessary(const FIntVector& position, const FIntVector& forward, const int& minimumDistance, bool visualize);

	FIntVector GetPositionNodeArray(const FVector& pos);
	FVector GetRealPosition(const FIntVector& pos);
};
