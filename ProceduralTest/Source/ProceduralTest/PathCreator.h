// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NodeMap.h"
#include "MeshCreator.h"
#include "RoomTemplate.h"
#include "Corridor.h"
#include "CeilingLight.h"
#include "VisibilityManagerRoom.h"
#include "GameFramework/Actor.h"
#include "PathCreator.generated.h"

class ADoor;
class ARoom;
class ACorridor;
class ADebugSphere;
class UProceduralMeshComponent;
struct NODE;
struct NODECONTAINER;
struct SOLUTIONSCORE;

USTRUCT()
struct FROOMSTRUCT {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
		FVector size;

	UPROPERTY(EditAnywhere)
		TArray<int> connections;

	UPROPERTY(EditAnywhere)
		FColor doorColor;

	UPROPERTY(EditAnywhere)
		FColor roomColor = FColor::White;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class ACeilingLight> lightBlueprint;

	UPROPERTY(EditAnywhere)
		TMap<FVector, TSubclassOf<class AVisibleObject>> props;

	UPROPERTY(EditAnywhere)
		UMaterial * wallMaterial;

	UPROPERTY(EditAnywhere)
		UMaterial * floorMaterial;

	UPROPERTY(EditAnywhere)
		UMaterial * ceilingMaterial;

	int id = -1;

	RoomTemplate * rTemplate = nullptr;
	ARoom * room = nullptr;
	AVisibilityManagerRoom * roomVM = nullptr;
	TArray<ACorridor *> corridors;

	void Clean() {
		if (room != nullptr) {
			room->Destroy();
			room = nullptr;
		}
		if (roomVM != nullptr) {
			roomVM->Destroy();
			roomVM = nullptr;
		}
		for (int i = 0; i < corridors.Num(); i++) {
			ACorridor* c = corridors[i];
			if(c!= nullptr)
				c->Destroy();
		}
		corridors.Empty();

	}
};

UCLASS()
class PROCEDURALTEST_API APathCreator : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool curvedCorridors = false;
private:
	UPROPERTY(EditAnywhere)
		bool runOnStart = true;
	UPROPERTY(EditAnywhere)
		FVector trackingSpaceSize = FVector(3000,3000,0);
	UPROPERTY(EditAnywhere)
		float wallThickness = 1.0f;
	UPROPERTY(EditAnywhere)
		float corridorWidth = 200.f;
	UPROPERTY(EditAnywhere)
		float corridorHeight = 200.f;
	UPROPERTY(EditAnywhere)
		float wallHeight = 250.f;
	UPROPERTY(EditAnywhere)
		int _amountOfDoors = 4;
	UPROPERTY(EditAnywhere)
		float doorHeight = 190.f;
	UPROPERTY(EditAnywhere)
		float doorWidth = 85.f;
	UPROPERTY(EditAnywhere)
		float doorSpeed = 10.f;
	UPROPERTY(EditAnywhere)
		float doorOpeningDistance = 85.f;
	UPROPERTY(EditAnywhere)
		float _minimumDistanceBetweenDoors = 1.0f;

	UPROPERTY(EditAnywhere)
		TArray<FROOMSTRUCT> rooms;
	UPROPERTY(EditAnywhere)
		bool addDoorFrame = false;
	UPROPERTY(EditAnywhere)
		UMaterial * _corridorCeilingMaterial;
	UPROPERTY(EditAnywhere)
		UMaterial * _corridorWallMaterial;
	UPROPERTY(EditAnywhere)
		UMaterial * _corridorFloorMaterial;
	UPROPERTY(EditAnywhere)
		UMaterial * trackingAreaMaterial;

	UPROPERTY(EditAnywhere)
		int minimumCornerAmount = 2;
	UPROPERTY(EditAnywhere)
		int maximumCornerAmount = 99;
	UPROPERTY(EditAnywhere)
		bool spawnCorridor;
	UPROPERTY(EditAnywhere)
		bool showAlgorithmVisualization;
	UPROPERTY(EditAnywhere)
		bool showNodes;
	UPROPERTY(EditAnywhere)
		bool allowZigZags;
	UPROPERTY(EditAnywhere)
		bool randomizeZigZagLength;
	UPROPERTY(EditAnywhere)
		bool randomRoom;
	UPROPERTY(EditAnywhere)
		bool randomizeDirections;
	UPROPERTY(EditAnywhere)
		bool singleRoomTest;
	UPROPERTY(EditAnywhere)
		bool addDoor;
	UPROPERTY(EditAnywhere)
		int solutionsToCompare = 50;
	UPROPERTY(EditAnywhere)
		bool randomCorridorTest;
	UPROPERTY(EditAnywhere)
		bool fixedCorridorTest;
	UPROPERTY(EditAnywhere)
		bool upperLimitOnly = false;
	UPROPERTY(EditAnywhere)
		bool lowerLimitOnly = false;
	UPROPERTY(EditAnywhere)
		bool rebuildMap = false;
	UPROPERTY(EditAnywhere)
		int testRuns = 2;
	UPROPERTY(EditAnywhere)
		bool runMultipleTests = false;
	UPROPERTY(EditAnywhere)
		float corridorLengthLowerLimit = 600.f;
	UPROPERTY(EditAnywhere)
		float zigZagCorridorLengthLowerLimit = 200.f;
	UPROPERTY(EditAnywhere)
		float hallwayMinimumLength = 1000.f;
	UPROPERTY(EditAnywhere)
		float stepSize = 100.0f;
	UPROPERTY(EditAnywhere)
		float cornerCost = 100.0f;
	UPROPERTY(EditAnywhere)
		bool removeEverything = false;
	UPROPERTY(EditAnywhere)
		bool nodeTest = false;

	float middle;
	float minimum = 1.f;//3f;//component for checking the borders of the tracking space - DON'T CHANGE!

	void GenerateIpoint(int i, FVector* arr, FVector trackingAreaSize, float corridorWidth);
	bool CheckPntDist(FVector pnt1, FVector pnt2);
	TArray<FVector> CalcWay(ADoor* doorStart, ARoom* roomA, ARoom* roomB, ADoor* destDoor);
	TArray<FVector> CreatePathPoints(FVector startPoint, FVector startDirection);

	TArray<FVector> CreateRandomPathPoints(FVector startPoint, FVector startDirection, float cornerAmount);
	TArray<FVector> CreateRandomPathPoints(FIntVector startPoint, FIntVector startDirection, int cornerAmount);

	TArray<FVector> CreateEndPathPoints(FVector startPoint, FVector startDirection);
	TArray<FVector> CreatePathPointsToPointNoLimit(FVector startPoint, FVector startDirection, FVector endPoint, FVector endDirection);
	TArray<FVector> CreatePathPointsToPoint(FVector startPoint, FVector startDirection, FVector endPoint, FVector endDirection);
	TArray<FVector> CreatePathPointsToPossibleDoor(FVector startPoint, FVector startDirection, RoomTemplate* exampleRoom);
	TArray<FVector> CreatePathPointsToPossibleDoorNoLimit(FVector startPoint, FVector startDirection, FROOMSTRUCT & startRoom, FROOMSTRUCT & endRoom, FVector & endRoomPosition, FIntVector & endDoorPosition, FIntVector & endDoorDirection);
	TArray<FVector> CreatePathPointsToPossibleDoorNoLimit(FIntVector startPoint, FIntVector startDirection, FROOMSTRUCT & startRoom, FROOMSTRUCT & endRoom, FVector & endRoomPosition, FIntVector & endDoorPosition, FIntVector & endDoorDirection);

	TArray<FIntVector> GetCardinalDirections();

	NODE* ExpandNode(TSharedPtr<NODE>currentNode, FIntVector direction);
	void ExpandNode(TSharedPtr<NODE>currentNode, TArray<NODE> &openlist);
	float GetSolutionScore(TSharedPtr<NODE> node);

	TSharedPtr<NODE> FindBetterSolution(TSharedPtr<NODE> node1, TSharedPtr<NODE> node2);


	TArray<FVector> RemoveRedundantPoints(TArray<FVector> wayPoints);

	TArray<FVector> BreakOut(TArray<FVector> wayPoints, FVector previousWayVector, FVector wayVector);
	bool TryBreakThrough(FIntVector &currentPoint, FIntVector wayVector, int lowerLimit);
	bool TryFittingCorridor(FIntVector &currentPoint, int corridorDistance, FIntVector wayVector, FIntVector previousWayVector, int lowerLimit, TArray<FVector> & wayPoints, TArray<FIntVector> & wayIntPoints);

	bool CheckIfBreakOutNecessary(FVector position, FVector wayVector, float minimumDistance, float widthCheck);
	FBoxSphereBounds GetClosestObstacle(FVector direction, FVector position);

	float GetCorridorDistanceToObstacle(FVector direction, FVector position);
	float GetDistanceToObstacle(FVector direction, FVector position, float width);

	bool PointInsideRoom(ARoom* room, FVector pointToCheck);
	bool PointInTheRoomOrBehind(ARoom* room, FVector targetEntry, FVector pointToCheck, float ignoreFactor);
	bool DoorChoice(FVector* arr, int maxPnum, ARoom* room, ADoor* destDoorID);
	bool CheckAddPnt(ARoom* roomA, FVector doorA, ARoom* roomB, TArray<FVector> listV, int pListNum, int cnt, int maxPnum, FVector old);
	bool DoesWayGoBack(TArray<FVector> listV);
	bool TryNextDoor(FVector* arr, int maxPnum, ARoom* room, FVector prevDoor, ADoor* destDoor);
	bool CheckIfRoomFitsAfterWayPoint(FVector wayVector, FVector position);
	bool CheckIfFree(FVector size, FVector position, bool addVisualization);
	bool CheckIfCorridorPointFree(FIntVector point);

	bool CheckIfPointInsideRoom(FVector roomPosition, float roomWidth, float roomLength, FVector point);
	bool CheckIfCorridorCrossesRoom(FVector roomPosition, float roomWidth, float roomLength, TSharedPtr<NODE> endNode);
	bool CheckIfCorridorCrossesRoomOverAllRoomPositions(float roomWidth, float roomLength, TSharedPtr<NODE> endNode, TArray<FVector> & roomPositions, FVector & endRoomPosition);
	bool CheckIfPointTouchesCorridor(TSharedPtr<NODE> node, FIntVector point);

	//bool CheckIfRoomColidesWithObstacles(FVector position, FVector roomDimensions);
	float GetDistanceToTrackingAreaBorder(FVector direction, FVector position);
	FVector GetDirectionWithMoreSpace(FVector direction, FVector position);
	void RunTests();


	TArray<AActor*> actorsToDestroy;
	TArray<AActor*> actorsToManuallyDestroy;

	ACorridor* _corridor = nullptr;

	MeshCreator meshCreator;
	APlayerController* player = nullptr;

	FROOMSTRUCT* currentRoomFS = nullptr;
	ACorridor* _currentCorridor = nullptr;
	AVisibilityManager* _closestDoorVM = nullptr;

	AVisibilityManagerRoom* _currentRoomVM = nullptr;
	AVisibilityManagerRoom* _nextRoomVM = nullptr;

	ADoor* _closestDoor = nullptr;

	ARoom* _currentRoom = nullptr;
	ARoom* _nextRoom = nullptr;

	NodeMap* nodeMap = nullptr;
	FVector nodeTrackingSpaceSize = FVector(3000, 3000, 0);

	ADebugSphere* testCorridorStart = nullptr;
	ADebugSphere* testCorridorEnd = nullptr;
	
	TMap<FVector, bool> corridorMap;
	TArray<FVector> ExtractWaypoints(TSharedPtr<NODE> lastNode);

	void GetFreeNodes(float stepsize);

	void AdjustWayPointsToPoints(TArray<FVector> & corridorPoints, FVector startPoint, FVector endPoint);

	void ExpandRoom(FROOMSTRUCT &roomstruct);
	void RemoveCorridors(ACorridor * doNotRemove, FROOMSTRUCT & roomstruct);
	ADoor* CreatePathAndRoom(FROOMSTRUCT & startRoom, FROOMSTRUCT & endRoom, ADoor* startDoor);

public:	
	// Sets default values for this actor's properties
	APathCreator();
	UFUNCTION(BlueprintCallable, Category = "PathCreator")
		void Initialize();
	UFUNCTION(BlueprintImplementableEvent, Category = "PathCreator")
		void PathCalculatedEvent(ACorridor * corridor);
	UFUNCTION(BlueprintImplementableEvent, Category = "PathCreator")
		void DestroyCorridor(ACorridor * corridor);
	UFUNCTION(BlueprintImplementableEvent, Category = "PathCreator")
		void ActivateCorridor(ACorridor * corridor, ADoor * currentDoor);
	UFUNCTION(BlueprintImplementableEvent, Category = "PathCreator")
		void DeactivateCorridor(ACorridor * corridor, ADoor * currentDoor);
	UProceduralMeshComponent* mesh = nullptr;
	ACorridor * CreatePathMesh(TArray<FVector> corridorPoints, ARoom * currentRoom, AVisibilityManagerRoom * currentRoomVM, ARoom * nextRoom, AVisibilityManagerRoom * nextRoomVM, bool AddVisibilityManager);
	void AdjustCorridorPointsToRooms(TArray<FVector> & corridorPoints, ARoom * startRoom, ARoom * endRoom);
	void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent);
	void FixedCorridorTest();
	void CreateRandomRoomWithPath();
	bool CreateRandomRoomWithPath(bool createMesh);

	void CreateRandomRoom();
	void AddDoor();
	void DoNothing();
	void RebuildMap();
	void AddTestSphere(FVector position, FString name);	
	void AddTestSphere(FVector position, FString name, float intensity);

	void AdjustRoomsList();

	void AddTestCube(FBoxSphereBounds box, FLinearColor color, float height, FString name, bool destroy);
	void AddTestCube(FBoxSphereBounds box, float intensity, float height, FString name);
	
	void CreateRoomClosestToPlayer(FROOMSTRUCT & roomStruct);
	void CreateRoom(FROOMSTRUCT & roomStruct, FVector roomPos, FIntVector doorIntPos, FIntVector doorIntDir, int doorToRoomID, int doorInRoomID);

	void AddPropsToRoom(FROOMSTRUCT & roomStruct);

	ARoom * CreateRoom(bool createMesh, int id, RoomTemplate * roomTemplate, int amountOfdoors);
	ARoom * CreateRoom(bool createMesh, int id, FVector position, RoomTemplate * roomTemplate, int amountOfdoors, UMaterial * floorMaterial, UMaterial * wallMaterial, UMaterial * ceilingMaterial, FColor roomColor);
	ARoom * CreateRoom(bool createMesh, int id, FVector position, RoomTemplate * roomTemplate, int amountOfDoors, FIntVector endDoorDirectionInt, FIntVector endDoorPositionInt, UMaterial * floorMaterial, UMaterial * wallMaterial, UMaterial * ceilingMaterial, FColor roomColor);
	AVisibilityManagerRoom * CreateVisibilityManagerForRoom(ARoom * room);


	//UFUNCTION()
	void PlayerEnteredRoom(int roomID);
	ACorridor * GetCurrentCorridor(FROOMSTRUCT * room, ADoor * door);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
