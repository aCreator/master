// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Room.h"
#include "Door.h"
#include "GameFramework/Actor.h"
#include "Connection.generated.h"

UCLASS()
class PROCEDURALTEST_API AConnection : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AConnection();
	//ARoom roomFrom;
	//ARoom roomTo;
	TArray<FVector> way;
	//Door destDoor;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
