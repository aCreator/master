// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Door.h"
#include "MeshCreator.h"
#include "RoomTemplate.h"
#include "NodeMap.h"
#include "VisibleObject.h"
#include "Room.generated.h"
struct DOOR;

UCLASS()
class PROCEDURALTEST_API ARoom : public AVisibleObject
{
	GENERATED_BODY()

private:
	MeshCreator meshCreator;
	UPROPERTY(EditAnywhere)
		UProceduralMeshComponent* wallMesh = nullptr;
	UPROPERTY(EditAnywhere)
		UProceduralMeshComponent* floorMesh = nullptr;
	UPROPERTY(EditAnywhere)
		UProceduralMeshComponent* ceilingMesh = nullptr;

	UMaterialInstanceDynamic * wallMaterialDynamic;
	UMaterialInstanceDynamic * floorMaterialDynamic;
	UMaterialInstanceDynamic * ceilingMaterialDynamic;

	USceneComponent* main;
	FBoxSphereBounds boundingBox;
	bool CheckIfFree(FVector size, FVector position);
	bool CheckIfFree();
	void AddDoors();

	TArray<AActor*> obstacles;
	void FindObstacles();
public:	
	UPROPERTY(EditAnywhere)
		UMaterial * wallMaterial;
	UPROPERTY(EditAnywhere)
		UMaterial * floorMaterial;
	UPROPERTY(EditAnywhere)
		UMaterial * ceilingMaterial;
	UPROPERTY(EditAnywhere)
		FColor _roomColor = FColor::White;

	UPROPERTY(EditAnywhere)
		bool test;
	UPROPERTY(EditAnywhere)
		bool showDoorPositions;
	UPROPERTY(EditAnywhere)
		bool addDoor;
	UPROPERTY(EditAnywhere)
		bool createMap;
	UPROPERTY(EditAnywhere)
		bool randomPosition;
	UPROPERTY(EditAnywhere)
		int id;
	UPROPERTY(EditAnywhere)
		float _wallThickness = 1.0f;
	UPROPERTY(EditAnywhere)
		float _corridorWidth = 100;
	UPROPERTY(EditAnywhere)
		float _height = 250;
	UPROPERTY(EditAnywhere)
		float _doorHeight = 190;
	UPROPERTY(EditAnywhere)
		float _doorWidth = 85;
	UPROPERTY(EditAnywhere)
		float _doorSpeed = 10;
	UPROPERTY(EditAnywhere)
		float _minimumDistanceBetweenDoors = 1.0f;
	UPROPERTY(EditAnywhere)
		FVector _floorSize = FVector(1310,1160,0);
	UPROPERTY(EditAnywhere)
		int _maxLoops = 1000;
	UPROPERTY(EditAnywhere)
		float stepSize = 20;

	TArray<ARoom*> connectedRooms;
	TArray<FVector> possibleRoomPositions;
	FIntVector GetPositionNodeArray(FVector pos);
	//bool CheckIfValidDoorPosition(FIntVector doorPos, FIntVector doorDirection);
	FVector GetRealPosition(FIntVector pos);

	float _yRotation;
	float _y_scale = 1.4f;
	float _middle = 1.55f / _y_scale; // height = y-coordinate where the room origine is set

	// Sets default values for this actor's properties
	ARoom();
	void RoomInitRandomPosition(int ID, RoomTemplate* roomTemplate, bool createMesh, int amountOfDoors, float doorSpeed);
	void RoomInitFixedPosition(int ID, RoomTemplate* roomTemplate, bool createMesh, FVector position, int amountOfDoors, float doorSpeed);
	void RoomInitFixedPosition(int ID, RoomTemplate* roomTemplate, bool createMesh, FVector position, int amountOfDoors, float doorSpeed, FIntVector entranceDoorDir, FIntVector entranceDoorPos);
	UFUNCTION(BlueprintCallable, Category = "PathCreator")
		void TestMethod();
	void RoomInit(int ID, RoomTemplate* roomTemplate);
	void AddDoors(int doorsAmount);
	void VisualizePossibleDoors();

	void ChooseRandomPosition();
	TArray<ADoor*> GetDoorsRangedByDistance(FVector point);
	TArray<FVector> doorsFace;
	TArray<ADoor*> _doors;
	RoomTemplate * roomTemplate = nullptr;
	void SetRotation(ADoor* door, FVector doorFace);
	void SetPotentialDoorFaces(ADoor* door);
	FVector GetDoorFaceFromDoorPos(ADoor* d, float ZRot);
	void CreateFloorMesh();
	void CreateWallMeshes(FVector p1, FVector p2);
	void CreateWallMesh(FVector p1, FVector p2, float wallThickness);
	bool IsBetween(FVector a, FVector b, FVector pointToCheck);
	bool IsOnLine(FVector a, FVector b, FVector pointToCheck);
	void CreateCeilingMesh();
	//void FindAllPossibleRoomPositions();
	void AddPossibleDoors(FVector position);
	void AddDoor(DOOR & doorPos);
	void AdjustDoorToRoomWalls(DOOR & doorPos);
	void ChangeDoorColor(FLinearColor color);

	bool CheckIfDoorIsLegal(FVector doorPos, FVector forward);

	void InitializeMesh(UProceduralMeshComponent* mesh);
	ADoor* getRandomDoor();
	ADoor* getClosestDoor(FVector pos);

	FBoxSphereBounds GetBoundingBox();

	FVector firstNodePos;
	void FreeNodesTest(TArray<FVector> freeNodes);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent);
	void Destroy();
	void CreateMesh();

};
