// Fill out your copyright notice in the Description page of Project Settings.

#include "VisibleObject.h"


// Sets default values
AVisibleObject::AVisibleObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	_position = GetActorLocation();
}

// Called when the game starts or when spawned
void AVisibleObject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AVisibleObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AVisibleObject::ActivateObject()
{
	if (!active) {
		SetActorLocation(_position);
	}
	active = true;
	/*
	// Hides visible components
	SetActorHiddenInGame(false);
	// Disables collision components
	if (!deactivateCollisions)
		SetActorEnableCollision(true);

	// Stops the Actor from ticking
	SetActorTickEnabled(true);
	*/
}

void AVisibleObject::DeactivateObject()
{
	if (active) {
		SetActorLocation(GetActorLocation() + FVector::UpVector * -100000);
	}
	active = false;
	/*
	// Hides visible components
	SetActorHiddenInGame(true);

	// Disables collision components
	SetActorEnableCollision(false);

	// Stops the Actor from ticking
	SetActorTickEnabled(false);*/
}

