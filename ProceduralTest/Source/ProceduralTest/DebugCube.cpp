// Fill out your copyright notice in the Description page of Project Settings.

#include "DebugCube.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"

// Sets default values
ADebugCube::ADebugCube()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	USceneComponent* root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Object"));
	root->SetupAttachment(RootComponent);
	SetRootComponent(root);

	sphereVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Debug Sphere Visualisation"));
	sphereVisual->SetupAttachment(root);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube"));
	if (SphereVisualAsset.Succeeded()) {
		sphereVisual->SetStaticMesh(SphereVisualAsset.Object);
		sphereVisual->AddWorldOffset(FVector(0, 0, -50));
		myMaterialDynamic = UMaterialInstanceDynamic::Create(sphereVisual->GetMaterial(0), sphereVisual);
		myMaterialDynamic->SetVectorParameterValue("Color", FLinearColor::Green);
		sphereVisual->SetMaterial(0, myMaterialDynamic);
	}
	SetActorEnableCollision(false);
}

void ADebugCube::ChangeColorIntensity(float intensity)
{
	FLinearColor color = FLinearColor::LerpUsingHSV(FLinearColor::Green, FLinearColor::Red, intensity);
	myMaterialDynamic->SetVectorParameterValue("Color", color);

}

void ADebugCube::ChangeColor(FLinearColor color)
{
	myMaterialDynamic->SetVectorParameterValue("Color", color);
}

// Called when the game starts or when spawned
void ADebugCube::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ADebugCube::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

#if WITH_EDITOR  
void ADebugCube::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	//UE_LOG(LogTemp, Warning, TEXT("butnc"));
	//mesh = GetOwner()->FindComponentByClass<UProceduralMeshComponent>();

	//Get the name of the property that was changed  
	FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(ADebugCube, material))) {
	//	myMaterialDynamic = UMaterialInstanceDynamic::Create(material, sphereVisual);
	//	myMaterialDynamic->SetVectorParameterValue("Color", FLinearColor(0.5f, 0.1f, 0.2f, 0.3f));
	//	sphereVisual->SetMaterial(0, myMaterialDynamic);
	}
	

	// Call the base class version  
	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif
