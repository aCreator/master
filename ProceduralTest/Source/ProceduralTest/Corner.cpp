// Fill out your copyright notice in the Description page of Project Settings.

#include "Corner.h"
#include "Engine/Engine.h"

// Sets default values
ACorner::ACorner()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	USceneComponent* root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Object"));
	root->SetupAttachment(RootComponent);
	SetRootComponent(root);

	ceilingMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("CeilingMesh"));
	ceilingMesh->bUseAsyncCooking = true;
	ceilingMesh->SetupAttachment(RootComponent);

	wallMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("WallMesh"));
	wallMesh->bUseAsyncCooking = true;
	wallMesh->SetupAttachment(RootComponent);

	floorMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("FloorMesh"));
	floorMesh->bUseAsyncCooking = true;
	floorMesh->SetupAttachment(RootComponent);
	//SetActorScale3D(FVector::OneVector);

}

void ACorner::TestMethod()
{
	Init(GetActorLocation(), _start, _end, _corridorWidth, _height, _floorMaterial, _wallMaterial, _ceilingMaterial);
}

#if WITH_EDITOR  
void ACorner::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	//UE_LOG(LogTemp, Warning, TEXT("butnc"));
	//mesh = GetOwner()->FindComponentByClass<UProceduralMeshComponent>();

	//Get the name of the property that was changed  
	FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(ACorner, test))
		||
		PropertyName == GET_MEMBER_NAME_CHECKED(ACorner, _end)
		||
		PropertyName == GET_MEMBER_NAME_CHECKED(ACorner, _start)
		||
		PropertyName == GET_MEMBER_NAME_CHECKED(ACorner, _height)
		||
		PropertyName == GET_MEMBER_NAME_CHECKED(ACorner, _corridorWidth)) {
		TestMethod();
	}

	// Call the base class version  
	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif

// Called when the game starts or when spawned
void ACorner::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACorner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACorner::Init(FVector position, FVector start, FVector end, float width, float height, UMaterial * floorMaterial, UMaterial * wallMaterial, UMaterial * ceilingMaterial)
{
	_floorMaterial = floorMaterial;
	_wallMaterial = wallMaterial;
	_ceilingMaterial = ceilingMaterial;

	_corridorWidth = width;
	_height = height;
	_start = start;
	_start.Normalize();
	_end = end;
	_end.Normalize();
	_width = width;
	_length = width;
	_position = position;
	SetActorLocation(_position);
	wallMesh->SetWorldLocation(_position);
	ceilingMesh->SetWorldLocation(_position);
	floorMesh->SetWorldLocation(_position);

	CreateMesh();
}


Wall ACorner::CreateWallInDirection(FVector direction)
{
	direction.Normalize();
	FVector middle = direction * _corridorWidth / 2.0f;
	FVector forward = FVector(direction.Y, direction.X, 0);
	FVector wallStart = middle + forward * _corridorWidth / 2.0f;
	FVector wallEnd = middle - forward * _corridorWidth / 2.0f;

	Wall wall = meshCreator.CreateWall(wallStart, wallEnd, _height);
	return wall;
}

void ACorner::MakeWallsIntangibleToggle()
{
	wallsIntangible = !wallsIntangible;
	SetActorEnableCollision(!wallsIntangible);
}

void ACorner::CreateMesh()
{
	wallMesh->SetMaterial(0, _wallMaterial);
	floorMesh->SetMaterial(0, _floorMaterial);
	ceilingMesh->SetMaterial(0, _ceilingMaterial);

	//create wall
	meshCreator.SetMesh(wallMesh);
	meshCreator.ClearMesh();
	FVector p1 = FVector(-_corridorWidth / 2.f, -_corridorWidth / 2.f, 0);
	FVector p2 = FVector(_corridorWidth / 2.f, -_corridorWidth / 2.f, 0);
	FVector p3 = FVector(_corridorWidth / 2.f, _corridorWidth / 2.f, 0);
	FVector p4 = FVector(-_corridorWidth / 2.f, _corridorWidth / 2.f, 0);

	Wall wall = meshCreator.CreateWall(p1, p4, _height);
	if ((wall.normal - _start).IsNearlyZero(0.01f) || (wall.normal - _end).IsNearlyZero(0.01f))
		meshCreator.AddWallToMesh(wall);
	wall = meshCreator.CreateWall(p4, p3, _height);
	if ((wall.normal - _start).IsNearlyZero(0.01f) || (wall.normal - _end).IsNearlyZero(0.01f))
		meshCreator.AddWallToMesh(wall);
	wall = meshCreator.CreateWall(p3, p2, _height);
	if ((wall.normal - _start).IsNearlyZero(0.01f) || (wall.normal - _end).IsNearlyZero(0.01f))
		meshCreator.AddWallToMesh(wall);
	wall = meshCreator.CreateWall(p2, p1, _height);
	if ((wall.normal - _start).IsNearlyZero(0.01f) || (wall.normal - _end).IsNearlyZero(0.01f))
		meshCreator.AddWallToMesh(wall);

	//meshCreator.AddWallToMesh(CreateWallInDirection(-_start));
	//meshCreator.AddWallToMesh(CreateWallInDirection(-_end));

	meshCreator.GenerateMesh();

	//create floor
	FVector corner = (_start + _end) * _corridorWidth / 2.0f;
	meshCreator.SetMesh(floorMesh);
	meshCreator.ClearMesh();
	meshCreator.AddWallToMesh(meshCreator.CreateSimpleFloor(FVector::ZeroVector, FVector2D(_corridorWidth, _corridorWidth), true));
	meshCreator.GenerateMesh();

	//create ceiling
	corner.Z = _height;
	FVector corner2 = -corner;
	corner2.Z = _height;
	meshCreator.SetMesh(ceilingMesh);
	meshCreator.ClearMesh();
	FVector ceilingPosition = _position;
	ceilingPosition.Z += _height;
	meshCreator.AddWallToMesh(meshCreator.CreateSimpleFloor(FVector(0, 0, _height) , FVector2D(_corridorWidth, _corridorWidth), false));
	meshCreator.GenerateMesh();

}

