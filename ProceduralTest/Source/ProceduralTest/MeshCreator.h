// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProceduralMeshComponent.h"
#include "Runtime/Engine/Classes/Materials/Material.h"
#include "Wall.h"

/**
 * 
 */
class PROCEDURALTEST_API MeshCreator
{
public:
	MeshCreator();
	~MeshCreator();

	UProceduralMeshComponent* mesh = nullptr;

	TArray<FVector> vertices;
	TArray<int32> Triangles;
	TArray<FVector> normals;
	TArray<FVector2D> UV0;
	TArray<FProcMeshTangent> tangents;
	TArray<FLinearColor> vertexColors;
	int triangleIndex = 0;
	float uvSize = 100;
	FColor currentColor = FColor::White;
	float wallThickness = 1;
	int numberOfDoors = 2;
	float doorHeight = 80;
	float doorWidth = 40.0f;


	void CreateTriangle(FVector p1, FVector p2, FVector p3, FVector2D uv1, FVector2D uv2, FVector2D uv3);
	void CreateTriangle(FVector p1, FVector p2, FVector p3, FVector2D uv1, FVector2D uv2, FVector2D uv3, FVector tangent, FVector normal);
	Wall CreateWall(FVector p1, FVector p2, float roomHeight);
	Wall CreateWall(FVector p1, FVector p2, float roomHeight, Wall startWall);
	Wall CreateWall(FVector p1, FVector p2, float roomHeight, float thickness);
	Wall CreateWall(FVector p1, FVector p2, float roomHeight, float thickness, Wall startWall);
	void AddWallToMesh(Wall wall);
	void AddWallToMesh(Wall wall, bool addBothSides);
	void CreateWallWithCorners(FVector p1, FVector p2, float roomHeight, Wall startWall);
	void CreateWallsWithDoors(FVector p1, FVector p2, float roomHeight, float thickness, int numberOfDoors, float doorHeight, float doorWidth);

	Wall CreateSimpleFloor(FVector p1, FVector p2, bool up);
	Wall CreateSimpleFloor(FVector middlePosition, FVector2D dimensions, bool up);
	void CreateSimpleRoom(FVector p1, FVector p2, float height);
	void CreateSimpleHallway(FVector p1, FVector p2, float height);
	virtual void ClearMesh();
	FVector GetNormal(FVector v1, FVector v2, FVector v3);
	void GenerateMesh();

	void SetMesh(UProceduralMeshComponent* mesh);
	void SetMaterial(UMaterial * wallMaterial);
};
