// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VisibleObject.h"
#include "MeshCreator.h"
#include "DoorFrame.generated.h"
class ADoor;
class UMaterial;

UCLASS()
class PROCEDURALTEST_API ADoorFrame : public AVisibleObject
{
	GENERATED_BODY()

public:
	ADoorFrame();

	void Init(int roomID, FVector position, float frameWidth, float frameHeight, UMaterial * material, ADoor * door);
	void Init(int roomID, FVector position, float frameWidth, float frameHeight, UMaterial * material, float doorWidth, float doorHeight, FVector doorForward);

private:
	UPROPERTY(EditAnywhere)
		UProceduralMeshComponent* wallMesh = nullptr;
	UPROPERTY(EditAnywhere)
		UMaterial * _material;
	UPROPERTY(EditAnywhere)
		float _frameHeight = 200;
	UPROPERTY(EditAnywhere)
		float _frameWidth = 100;
	UPROPERTY(EditAnywhere)
		int _roomID = 0;

	UPROPERTY(EditAnywhere)
		float _doorWidth = 85;
	UPROPERTY(EditAnywhere)
		float _doorHeight = 190;
	UPROPERTY(EditAnywhere)
		FVector _doorForward = FVector(1,0,0);
	UPROPERTY(EditAnywhere)
		bool test = false;
	void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent);

	MeshCreator meshCreator;

	void CreateMesh();

};
