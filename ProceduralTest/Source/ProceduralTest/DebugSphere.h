// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "GameFramework/Actor.h"
#include "DebugSphere.generated.h"

UCLASS()
class PROCEDURALTEST_API ADebugSphere : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADebugSphere();
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* sphereVisual;
	void ChangeColorIntensity(float intensity);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UMaterialInstanceDynamic* myMaterialDynamic;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	
};
