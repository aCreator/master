// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MeshCreator.h"
#include "VisibleObject.h"
#include "Corner.generated.h"

UCLASS()
class PROCEDURALTEST_API ACorner : public AVisibleObject
{
	GENERATED_BODY()

private:
	MeshCreator meshCreator;
	UPROPERTY(EditAnywhere)
		UProceduralMeshComponent* wallMesh = nullptr;
	UPROPERTY(EditAnywhere)
		UProceduralMeshComponent* floorMesh = nullptr;
	UPROPERTY(EditAnywhere)
		UProceduralMeshComponent* ceilingMesh = nullptr;
	Wall CreateWallInDirection(FVector direction);
	void MakeWallsIntangibleToggle();
	bool wallsIntangible = false;

public:
	// Sets default values for this actor's properties
	ACorner();
	UFUNCTION(BlueprintCallable, Category = "PathCreator")
		void TestMethod();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterial * _wallMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterial * _floorMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterial * _ceilingMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float _corridorWidth = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float _height = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector _start = FVector(0, 1, 0);
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector _end = FVector(1, 0, 0);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool test;
	void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void Init(FVector position, FVector start, FVector end, float width, float height, UMaterial * floorMaterial, UMaterial * wallMaterial, UMaterial * ceilingMaterial);
	void CreateMesh();
};
