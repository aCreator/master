// Fill out your copyright notice in the Description page of Project Settings.

#include "PathCreator.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Core/Public/Math/NumericLimits.h"
#include "Runtime/Engine/Public/DrawDebugHelpers.h"
#include "Runtime/Core/Public/Misc/DateTime.h"
#include "Runtime/Engine/Classes/Components/ShapeComponent.h"
#include "Room.h"
#include "Hallway.h"
#include "Wall.h"
#include "Door.h"
#include "Corner.h"
#include "Corridor.h"
#include "DebugSphere.h"
#include "DebugCube.h"
#include "Obstacle.h"
#include "Engine/Engine.h"


struct NODE {
	int cost;   //how expensive it is to get here
	float distanceToLastCorner = 0.0f;
	int corners = 0;
	int corridorLengthInSteps = 0;
	TSharedPtr<NODE> predecessor;
	FIntVector direction;
	FIntVector previousDirection = FIntVector::ZeroValue;
	FIntVector mapPosition;
	NODE(float cost, FIntVector direction, FIntVector mapPosition, TSharedPtr<NODE> predecessor) : cost(cost), direction(direction), mapPosition(mapPosition), predecessor(predecessor) {}
	NODE(float cost, FIntVector direction, FIntVector mapPosition) : cost(cost), direction(direction), mapPosition(mapPosition) {}
	NODE() : mapPosition(FIntVector::ZeroValue) {};
};
// the top of the priority queue is the greatest element by default,
// but we want the smallest, so flip the sign
bool operator<(const NODE &n1, const NODE &n2) {
	return n1.cost < n2.cost;
}
bool operator==(const NODE &n1, const NODE &n2) {
	return n1.mapPosition == n2.mapPosition && n1.direction == n2.direction;
}

enum NodeDirection {left, right};

struct SOLUTIONSCORE {
	int score = 0;
	TSharedPtr<SOLUTIONSCORE> leftSolution;
	TSharedPtr<SOLUTIONSCORE> rightSolution;
	SOLUTIONSCORE(){};

	NodeDirection GetNextDirection(TSharedPtr<NODE> node) {
		FIntVector previousDirection = node->predecessor->direction;
		FIntVector previousDirectionLeft(-previousDirection.Y, previousDirection.X, 0);
		FIntVector currentDirection = node->direction;
		if (currentDirection == previousDirectionLeft)
			return NodeDirection::left;
		return NodeDirection::right;
	}
	
	int GetScore(TSharedPtr<NODE> node) {
		if (node->predecessor.IsValid()) {
			if (GetNextDirection(node) == NodeDirection::left) {
				if (!leftSolution.IsValid())
					return score;
				return score + leftSolution->GetScore(node->predecessor);
			}
			else {
				if (!rightSolution.IsValid())
					return score;
				return score + rightSolution->GetScore(node->predecessor);
			}
		}
		return score;
	}

	void AddToScore(TSharedPtr<NODE> node) {
		score++;
		if (node->predecessor.IsValid()) {
			if (GetNextDirection(node) == NodeDirection::left) {
				if (!leftSolution.IsValid())
					leftSolution = TSharedPtr<SOLUTIONSCORE>(new SOLUTIONSCORE());
				leftSolution->AddToScore(node->predecessor);
			}
			else {
				if (!rightSolution.IsValid())
					rightSolution = TSharedPtr<SOLUTIONSCORE>(new SOLUTIONSCORE());
				rightSolution->AddToScore(node->predecessor);
			}
		}
	}

	FString GetOrder(TSharedPtr<NODE> node){
		if (node->predecessor.IsValid()) {
			if (GetNextDirection(node) == NodeDirection::left) {
				if (leftSolution.IsValid())
					return FString::FromInt(score) + ", left " + leftSolution->GetOrder(node->predecessor);
			}
			else {
				if (rightSolution.IsValid())
					return FString::FromInt(score) + ", right " + rightSolution->GetOrder(node->predecessor);
			}
		}
		return FString::FromInt(score);
	}
};

TSharedPtr<SOLUTIONSCORE> solutionScoreGenerator(new SOLUTIONSCORE());

void APathCreator::GetFreeNodes(float stepsize)
{
	if (nodeMap == nullptr) {
		nodeMap = new NodeMap(GetWorld());
	}

	nodeMap->CreateNodeMap(stepSize, corridorWidth, trackingSpaceSize);
}

// Sets default values
APathCreator::APathCreator()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	SetRootComponent(mesh);
	mesh->bUseAsyncCooking = true;
	meshCreator.SetMesh(mesh);
	middle = 0;//wallHeight / 2.f;
	Initialize();

}

void APathCreator::GenerateIpoint(int i, FVector * arr, FVector trackingAreaSize, float corridorWidth)
{
	FVector point = FVector(FMath::FRandRange(-trackingAreaSize.X / 2.0f + corridorWidth, trackingAreaSize.X / 2.0f - corridorWidth), FMath::FRandRange(-trackingAreaSize.Y / 2.0f + corridorWidth, trackingAreaSize.Y / 2.0f - corridorWidth), trackingAreaSize.Z);
	arr[i] = point;
}

bool APathCreator::CheckPntDist(FVector pnt1, FVector pnt2)
{

	bool chX = (FMath::Abs(pnt1.X - pnt2.X) < 4 * corridorWidth / 2.0f);
	bool chY = (FMath::Abs(pnt1.Y - pnt2.Y) < 4 * corridorWidth / 2.0f);

	bool eqX = FMath::IsNearlyEqual(pnt1.X, pnt2.X);
	bool eqY = FMath::IsNearlyEqual(pnt1.Y, pnt2.Y);

	if (chX || chY)
	{
		if (chX && eqY || chY && eqX)
		{
			return false;
		}
		else
		{
			if (chX && eqX && !chY || chY && eqY && !chX)
			{
				return true;
			}

			else
			{
				return false;
			}
		}
	}
	else
	{
		return true;
	}
}

bool APathCreator::PointInsideRoom(ARoom* room, FVector pointToCheck)
{
	if (room->_yRotation == 0.0f || room->_yRotation == 180.0f)
	{
		if (pointToCheck.X > room->_position.X - room->_length / 2 - corridorWidth / 2.0f && pointToCheck.X < room->_position.X + room->_length / 2 + corridorWidth / 2.0f &&
			pointToCheck.Y > room->_position.Y - room->_width / 2 - corridorWidth / 2.0f && pointToCheck.Y < room->_position.Y + room->_width / 2 + corridorWidth / 2.0f)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (room->_yRotation == 90.0f || room->_yRotation == 270.0)
	{
		if (pointToCheck.X > room->_position.X - room->_width / 2 - corridorWidth / 2.0f && pointToCheck.X < room->_position.X + room->_width / 2 + corridorWidth / 2.0f &&
			pointToCheck.Y > room->_position.Y - room->_length / 2 - corridorWidth / 2.0f && pointToCheck.Y < room->_position.Y + room->_length / 2 + corridorWidth / 2.0f)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Wrong room rotation"));
		return false;
	}
	return false;
}

bool APathCreator::PointInTheRoomOrBehind(ARoom * room, FVector targetEntry, FVector pointToCheck, float ignoreFactor)
{
	if (room->_yRotation == 0.0f || room->_yRotation == 180.0f) // a-side is aligned with x and b-side with z
	{
		if (FMath::IsNearlyEqual(FMath::Abs(targetEntry.Y - room->_position.Y), (room->_width / 2.0f + corridorWidth / 2.0f))) // if the door is on the upper or on the lower side
		{
			// if x-coord is between left and right borders
			if (pointToCheck.X >= targetEntry.X - room->_length / 2.0f - ignoreFactor*corridorWidth / 2.0f && pointToCheck.X <= targetEntry.X + room->_length / 2.0f + ignoreFactor*corridorWidth / 2.0f)
			{
				if (room->_position.Y > targetEntry.Y) //lower door
				{
					if (pointToCheck.Y > targetEntry.Y)
						return true;
				}
				else //upper door
				{
					if (pointToCheck.Y < targetEntry.Y)
						return true;
				}
			}
		}
		if (FMath::IsNearlyEqual(FMath::Abs(targetEntry.X - room->_position.X), (room->_length / 2.0f + corridorWidth / 2.0f))) // if the door is on the left or on the right side
		{
			if (pointToCheck.Y >= targetEntry.Y - room->_width / 2.0f - ignoreFactor*corridorWidth / 2.0f && pointToCheck.Y <= targetEntry.Y + room->_width / 2.0f + ignoreFactor*corridorWidth / 2.0f)
			{
				if (room->_position.X > targetEntry.X) //left door
				{
					if (pointToCheck.X > targetEntry.X)
						return true;
				}
				else //right door
				{
					if (pointToCheck.X < targetEntry.X)
						return true;
				}
			}
		}
	}
	else if (room->_yRotation == 90.0f || room->_yRotation == 270.0)
	{
		if (FMath::IsNearlyEqual(FMath::Abs(targetEntry.Y - room->_position.Y), (room->_length / 2.0f + corridorWidth / 2.0f))) // if the door is on the upper or on the lower side
		{
			if (pointToCheck.X >= targetEntry.X - room->_width / 2.0f - ignoreFactor*corridorWidth / 2.0f && pointToCheck.X <= targetEntry.X + room->_width / 2.0f + ignoreFactor*corridorWidth / 2.0f)
			{
				if (room->_position.Y > targetEntry.Y) //lower door
				{
					if (pointToCheck.Y > targetEntry.Y)
						return true;
				}
				else //upper door
				{
					if (pointToCheck.Y < targetEntry.Y)
						return true;
				}
			}
		}
		if (FMath::IsNearlyEqual(FMath::Abs(targetEntry.X - room->_position.X), (room->_width / 2.0f + corridorWidth / 2.0f))) // if the door is on the left or on the right side
		{
			if (pointToCheck.Y >= targetEntry.Y - room->_length / 2.0f - ignoreFactor*corridorWidth / 2.0f && pointToCheck.Y <= targetEntry.Y + room->_length / 2.0f + ignoreFactor*corridorWidth / 2.0f)
			{
				if (room->_position.X > targetEntry.X) //left door
				{
					if (pointToCheck.X > targetEntry.X)
						return true;
				}
				else //right door
				{
					if (pointToCheck.X < targetEntry.X)
						return true;
				}
			}
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Wrong room rotation"));
	}
	/*
	GameObject c = GameObject.CreatePrimitive(PrimitiveType.Sphere);
	c.transform.position = pointToCheck;
	c.transform.localScale = new Vector3(0.4f,0.4f,0.4f);
	c.name = "Ipoint";
	*/
	return false;
}

bool APathCreator::DoorChoice(FVector * arr, int maxPnum, ARoom* room, ADoor* destDoorID)
{
	TArray<ADoor*> doorsRanged = room->GetDoorsRangedByDistance(arr[maxPnum]);

	//List<Vector3> doorsRanged = room.GetDoorsFace();
	//Debug.Log("DOOR CHOICE");
	FVector chosenDoorPosition = doorsRanged[0]->_position + doorsRanged[0]->_forward * corridorWidth / 2.0f; // source point assigned 

	arr[maxPnum + 1] = chosenDoorPosition;
	//room.SetRotation(destDoorID, arr[maxPnum+1]); // if the door choice didnt work the doorFace[0] will be taken, rotation will correspond to 0	
	bool check = CheckPntDist(arr[maxPnum], arr[maxPnum + 1]);


	if (!check)
	{
		UE_LOG(LogTemp, Warning, TEXT("Second try"));

		for (int k = 1; k < 2; k++)
		{
			arr[maxPnum + 1] = doorsRanged[k]->_position + doorsRanged[k]->_forward * corridorWidth / 2.0f; // source point assigned 
			
			check = CheckPntDist(arr[maxPnum], arr[maxPnum + 1]);

			if (check)
			{
				//Debug.Log ("DOOR FACE " + k + " FITS");
				//Debug.Log("SETTING THE DOOR  FROM DOOR CHOICE DOOR FACE > 0");
				room->SetRotation(destDoorID, arr[maxPnum + 1]);
				return true;
			}
			else
			{
				if (k + 1 == doorsRanged.Num()) // never happens
					UE_LOG(LogTemp, Warning, TEXT(" Doors problem!"));

					//Debug.LogWarning(System.DateTime.Now + " Doors problem!");
				continue;
			}
		}
	}

	if (!check)
	{
		//Debug.Log("DOOR CHOICE DIDNT WORK");
		room->SetRotation(destDoorID, arr[maxPnum + 1]);
		return false;
	}
	else
	{
		//Debug.Log("DOOR FACE FOUND");
		room->SetRotation(destDoorID, arr[maxPnum + 1]);
		return true;
	}
}

bool APathCreator::CheckAddPnt(ARoom* roomA, FVector doorA, ARoom* roomB, TArray<FVector> listV, int pListNum, int cnt, int maxPnum, FVector old)
{
	bool ret = true;
	if (cnt == 1)
	{
		if (PointInTheRoomOrBehind(roomA, doorA, listV[pListNum], 2.0f)) // ignore factor * corridorWidth / 2.0f
		{
			ret = false;
		}

	}

	if (cnt == maxPnum + 1)
	{
		bool back = DoesWayGoBack(listV), limit = PointInsideRoom(roomB, listV[pListNum]);
		//for debug
		if (back)
			UE_LOG(LogTemp, Warning, TEXT("!!_Going back! 1 but last point"));

		if (/*back ||*/ limit) //check for backwards is temporary taken out and handled with 3 in line correction for walls 24.03.14
			ret = false;

		/*if (back) //was for DEBUG
		{
		if (PointInsideRoom(roomB, old))
		{
		ret =  false;
		}
		else
		{
		ret =  false;
		}
		}
		if (limit)
		{
		if (PointInsideRoom(roomB, old)) //if the room is too big????
		{
		ret =  false;
		}
		else
		{
		ret =  false;
		}
		}*/

	}
	else
	{
		ret = DoesWayGoBack(listV);
	}

	return ret;
}

bool APathCreator::DoesWayGoBack(TArray<FVector> listV)
{
	if (listV.Num() >= 3) //don't go back!
	{
		float x1 = listV[listV.Num() - 3].X, x2 = listV[listV.Num() - 2].X, x3 = listV[listV.Num() - 1].X;
		float z1 = listV[listV.Num() - 3].Y, z2 = listV[listV.Num() - 2].Y, z3 = listV[listV.Num() - 1].Y;

		if ((FMath::IsNearlyEqual(x1, x2) && ((z1<z2 && z3<z2) || (z1>z2 && z3>z2))) || (FMath::IsNearlyEqual(z1, z2) && ((x1<x2 && x3<x2) || (x1>x2 && x3>x2))))
		{
			return true;
		}
		else return false;
	}
	else return false;
}

bool APathCreator::TryNextDoor(FVector * arr, int maxPnum, ARoom * room, FVector prevDoor, ADoor* destDoor)
{
	bool res = false;
	TArray<ADoor*> doorsRanged = room->GetDoorsRangedByDistance(arr[maxPnum]);
	//	List<Vector3> doorsRanged = room.GetDoorsFace();
	//Debug.Log("TRY NEXT DOOR");


	for (int k = 1; k < doorsRanged.Num(); k++)
	{
		if (prevDoor == doorsRanged[k]->_position && k < doorsRanged.Num() - 1)
		{
			arr[maxPnum + 1] = doorsRanged[k + 1]->_position + doorsRanged[k + 1]->_forward * corridorWidth / 2.0f; // source point assigned 
			
			if (CheckPntDist(arr[maxPnum], arr[maxPnum + 1]))
			{
				int fn = k + 1;
				//Debug.Log("SETTING THE FACE FROM TRY NEXT DOOR DOOR FACE " + fn);
				room->SetRotation(destDoor, arr[maxPnum + 1]);
				res = true;
			}
			else
				continue;
		}
		else
			continue;
	}

	if (!res)
		UE_LOG(LogTemp, Warning, TEXT("All doors are too close?!!"));

	return res;
}

FVector GetSimpleDirection(FVector p1, FVector p2) {
	FVector direction = p1 - p2;

	if (FMath::Abs(direction.X) > 0.0001f) {
		direction = FVector(FMath::Sign(direction.X), 0, 0);
	}
	else
	{
		direction = FVector(0, FMath::Sign(direction.Y), 0);
	}
	return direction;
}

void APathCreator::Initialize()
{
	meshCreator.ClearMesh();
	mesh->SetMaterial(0, trackingAreaMaterial);
	//FVector p1 = FVector(trackingSpaceSize.X / 2.0f, -trackingSpaceSize.Y / 2.0f, trackingSpaceSize.Z);
	//FVector p2 = FVector(-trackingSpaceSize.X / 2.0f, trackingSpaceSize.Y / 2.0f, trackingSpaceSize.Z);
	meshCreator.AddWallToMesh(meshCreator.CreateSimpleFloor(FVector::ZeroVector, FVector2D(trackingSpaceSize.X, trackingSpaceSize.Y), true));
	meshCreator.GenerateMesh();
}

void APathCreator::RunTests()
{
	for (int i = 0; i < testRuns - 1; i++)
	{
		if (!CreateRandomRoomWithPath(false)) {
			break;
		}
	}
	CreateRandomRoomWithPath(true);
}

void APathCreator::PlayerEnteredRoom(int roomID)
{
	//UE_LOG(LogTemp, Warning, TEXT("Player EnteredRoom"));
	//GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Red, TEXT("ROOOOM"));
	if (roomID >= rooms.Num()) {
		UE_LOG(LogTemp, Warning, TEXT("Room ID does not exist"));

	}

	if (currentRoomFS == nullptr || currentRoomFS->id != roomID) {
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Magenta, FString::Printf(TEXT("NEW Room: %i"), roomID));
		
	}

	if (currentRoomFS != nullptr && currentRoomFS->id != roomID) {
		//New room, remove connections from old room
		//Find connection to keep
		ACorridor* toKeep = nullptr;
		for (ACorridor* c : currentRoomFS->corridors) {
			if (c->_endRoom->id == roomID || c->_startRoom->id == roomID) {
				toKeep = c;
			}
		}
		if(toKeep != nullptr)
			RemoveCorridors(toKeep, *currentRoomFS);

		currentRoomFS = &rooms[roomID];

		ExpandRoom(*currentRoomFS);

	}

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Magenta, FString::Printf(TEXT("entered Room: %i"), roomID));

	/*for (FROOMSTRUCT & room : rooms) {
		if (room.roomVM == nullptr || !room.roomVM->active)
			continue;
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Magenta, FString::Printf(TEXT("Active ROOOOM = %i"), room.id));

		if (room.roomVM->playerInside)
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Magenta, FString::Printf(TEXT("Inside ROOOOM = %i"), room.id));

		if (room.roomVM->playerInside && &room != currentRoomFS) {
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("NEEEEW ROOOOM = %i"), room.id));

			currentRoomFS = &room;
		}
	}*/
}

ACorridor * APathCreator::GetCurrentCorridor(FROOMSTRUCT * room, ADoor * door)
{
	for (ACorridor * c : room->corridors) {
		if (c->_startDoor == door || c->_endDoor == door)
			return c;
	}
	return nullptr;
}

void APathCreator::CreateRandomRoomWithPath()
{
	CreateRandomRoomWithPath(true);
}
bool APathCreator::CreateRandomRoomWithPath(bool createMesh)
{
	Initialize();
	//if (freeNodes.Num() == 0)
	//	freeNodes = GetFreeNodes(stepSize);
	FVector Location(0.0f, 0.0f, 0.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	UWorld* world = GetWorld();
	if (_corridor != nullptr)
		_corridor->Destroy();
	for (int i = 0; i < actorsToDestroy.Num(); i++) {
		actorsToDestroy[i]->Destroy();
	}
	actorsToDestroy.Empty();

	if (rooms.Num() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Add one room to rooms list"));
		return false;
	}
	
	if (rooms[0].rTemplate == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("initialize Map first"));
		return false;
	}

	if (_currentRoom != nullptr) {
		_currentRoom->Destroy();
		_currentRoomVM->Destroy();
	}

	if (_nextRoom != nullptr) {
		_currentRoom = _nextRoom;
		_currentRoomVM = _nextRoomVM;
	}
	else {

		if (rooms[0].rTemplate->possibleRoomPositions.Num() < 1) {
			UE_LOG(LogTemp, Warning, TEXT("no Room Positions in template"));
			return false;
		}
		
		FVector roompos = rooms[0].rTemplate->GetClosestRoomPosition(player->GetPawn()->GetActorLocation());
		_currentRoom = CreateRoom(createMesh, 1, roompos, rooms[0].rTemplate, _amountOfDoors, rooms[0].floorMaterial, rooms[0].wallMaterial, rooms[0].ceilingMaterial, rooms[0].roomColor);
		_currentRoomVM = CreateVisibilityManagerForRoom(_currentRoom);
		
		//_currentRoomVM->mainObjects.Add(
		/*currentRoom = GetWorld()->SpawnActor<ARoom>(Location, Rotation, SpawnInfo);
		currentRoom->wallMaterial = wallMaterial;
		currentRoom->ceilingMaterial = ceilingMaterial;
		currentRoom->floorMaterial = floorMaterial;
		currentRoom->RoomInitRandomPosition(1, roomTemplate, createMesh, amountOfDoors);
	*/}
	_currentRoom->ChangeDoorColor(FLinearColor::Green);

	//return false;

	ADoor* startDoor;
	FVector normal;
	FIntVector startArrayPosition;
	FIntVector startArrayForward;

	do {
		startDoor = _currentRoom->getRandomDoor();
		normal = FVector(startDoor->_forward.Y, startDoor->_forward.X, 0.0f);
		startArrayForward = FIntVector(startDoor->_forward.X, startDoor->_forward.Y, 0);
		startArrayPosition = nodeMap->GetPositionNodeArray(startDoor->_position);
		
	} while (CheckIfBreakOutNecessary(startDoor->_position + startDoor->_forward * corridorWidth / 2.0f, normal, corridorLengthLowerLimit + corridorWidth / 2.0f, corridorWidth));
	
	
		
	AddTestSphere(startDoor->_position, "Startdoor", 0);
	TArray<FVector> corridorPoints;
	corridorPoints.Add(startDoor->_position);

	FVector roomPosition = FVector::ZeroVector;

	FIntVector intDir(startDoor->_forward.X, startDoor->_forward.Y, startDoor->_forward.Z);
	FIntVector endDoorPositionInt(0, 0, 0);
	FIntVector endDoorDirectionInt(0, 0, 0);

	//corridorPoints = CreatePathPointsToPossibleDoorNoLimit(startDoor->_arrayPosition, intDir, _currentRoom, _roomTemplate, roomPosition, endDoorPositionInt, endDoorDirectionInt );
	if (corridorPoints.Num() <= 1) {
		return false;
	}

	if (roomPosition != FVector::ZeroVector) {
		_nextRoom = CreateRoom(createMesh, _currentRoom->id + 1, roomPosition, rooms[0].rTemplate, _amountOfDoors, endDoorDirectionInt, endDoorPositionInt, rooms[0].floorMaterial, rooms[0].wallMaterial, rooms[0].ceilingMaterial, rooms[0].roomColor);
		//nextRoom->RoomInitFixedPosition(currentRoom->id + 1, roomTemplate, createMesh, roomPosition, amountOfDoors, endDoorDirectionInt, endDoorPositionInt);
		_nextRoomVM = CreateVisibilityManagerForRoom(_nextRoom);
		_nextRoom->ChangeDoorColor(FLinearColor::Red);
		UE_LOG(LogTemp, Warning, TEXT("solution was valid"));
	}
	else {
		_nextRoom = _currentRoom;
		_currentRoom = nullptr;

		//nextRoom->RoomInitFixedPosition(currentRoom->id + 1, roomTemplate, createMesh, currentRoom->_position);
		//UE_LOG(LogTemp, Warning, TEXT("no positions found for pos x: %f, y: %f, z: %f"), endDoorPosition.X, endDoorPosition.Y, endDoorPosition.Z);

	}

	bool success = true;

	FVector corridorLimit = trackingSpaceSize / 2.0f;
	corridorLimit = corridorLimit - FVector::OneVector * corridorWidth/2.0f;
	

	//check if all corridors are inside the tracking area
	for (int i = 0; i < corridorPoints.Num() - 1; i++)
	{
		if (FMath::Abs(corridorPoints[i].X) > corridorLimit.X || FMath::Abs(corridorPoints[i].Y) > corridorLimit.Y) {
			//success = false;
			UE_LOG(LogTemp, Warning, TEXT("Corridor outside tracking area"));
		}
	}

	//check if first corridor goes into first room
	if (corridorPoints.Num() > 2) {
		FVector lastCorridorDirection = corridorPoints[corridorPoints.Num() - 1] - corridorPoints[corridorPoints.Num() - 2];
		lastCorridorDirection.Normalize();
		FVector firstCorridorDirection = corridorPoints[1] - corridorPoints[0];
		firstCorridorDirection.Normalize();
		if ((firstCorridorDirection - startDoor->_forward).SizeSquared() > 0.1f) {
			//UE_LOG(LogTemp, Warning, TEXT("Start Corridor goes into room"));
			//success = false;
		}

		if (!CheckIfRoomFitsAfterWayPoint(lastCorridorDirection, corridorPoints[corridorPoints.Num() - 1])) {
			//UE_LOG(LogTemp, Warning, TEXT("Room doesn't fit, no success"));
			//success = false;
		}
	}
	if(createMesh)
		_corridor = CreatePathMesh(corridorPoints, _currentRoom, _currentRoomVM, _nextRoom, _nextRoomVM, true);
		
	if(success)
		UE_LOG(LogTemp, Warning, TEXT("Successfully created new Room and Path"))
	else if (!createMesh){
		//create Meshes for testing
		_nextRoom->CreateMesh();
		_currentRoom->CreateMesh();
		_corridor = CreatePathMesh(corridorPoints, _currentRoom, _currentRoomVM, _nextRoom, _nextRoomVM, false);
	}
	return success;
}

void APathCreator::CreateRandomRoom()
{
	if (rooms.Num() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Add one room to rooms list"));
		return;
	}
	if (rooms[0].rTemplate == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("initialize Map first"));
		return;
	}

	//Multitasking* thread = Multitasking::JoyInit(&i);
	if (_currentRoom != nullptr) {
		_currentRoom->Destroy();
	}

	FVector Location(0.0f, 0.0f, 0.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	UWorld* world = GetWorld();

	_currentRoom = GetWorld()->SpawnActor<ARoom>(Location, Rotation, SpawnInfo);
	_currentRoom->wallMaterial = rooms[0].wallMaterial;
	_currentRoom->ceilingMaterial = rooms[0].ceilingMaterial;
	_currentRoom->floorMaterial = rooms[0].floorMaterial;
	_currentRoom->RoomInitRandomPosition(1, rooms[0].rTemplate, true, 0, doorSpeed);
	
}

void APathCreator::AddDoor()
{
	if (_currentRoom == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("Room does not exist"));
		return;
	}
	_currentRoom->AddDoors(1);
	_currentRoom->VisualizePossibleDoors();
	_currentRoom->CreateMesh();
}

void APathCreator::DoNothing()
{
	UE_LOG(LogTemp, Warning, TEXT("Multitask Pathcreator do stuff"));
}

void APathCreator::RebuildMap()
{
	//Multitasking* thread = Multitasking::JoyInit(&i);
	for (int i = 0; i < actorsToManuallyDestroy.Num(); i++) {
		if (actorsToManuallyDestroy[i] != nullptr)
			actorsToManuallyDestroy[i]->Destroy();
	}
	actorsToManuallyDestroy.Empty();
	GetFreeNodes(stepSize);
	if (rooms.Num() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Add one room to rooms list"));
		return;
	}
	if (rooms[0].rTemplate != nullptr)
		delete rooms[0].rTemplate;
	rooms[0].rTemplate = new RoomTemplate(GetWorld(), nodeMap, rooms[0].size, corridorWidth, doorWidth, doorHeight, _minimumDistanceBetweenDoors, wallThickness, corridorLengthLowerLimit / stepSize);
	rooms[0].rTemplate->FindAllPossibleRoomAndDoorPositions(trackingSpaceSize, stepSize);
	//setUpRoom->FreeNodesTest(freeNodes);
}

void APathCreator::AddTestSphere(FVector position, FString name, float intensity)
{
	if (!showAlgorithmVisualization)
		return;
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	UWorld* world = GetWorld();
	ADebugSphere* debugSphere;
	debugSphere = GetWorld()->SpawnActor<ADebugSphere>(position, Rotation, SpawnInfo);
	debugSphere->SetActorScale3D(FVector(0.5f, 0.5f, 0.5f));
	debugSphere->SetActorLabel(name);
	debugSphere->ChangeColorIntensity(intensity);
	actorsToDestroy.Add(debugSphere);

}
void APathCreator::AdjustRoomsList()
{
	for (int rID = 0; rID < rooms.Num(); rID++){
		FROOMSTRUCT& rs = rooms[rID];
		rs.id = rID;
		for (int i = 0; i < rs.connections.Num(); i++)
		{
			int id = rs.connections[i];
			if (id >= rooms.Num() || id < 0) {
				rs.connections.RemoveAt(i);
				i--;
				continue;
			}
			FROOMSTRUCT& otherRoom = rooms[id];
			if (!otherRoom.connections.Contains(rID)) {
				otherRoom.connections.Add(rID);
			}
		}
	}
}
void APathCreator::AddTestSphere(FVector position, FString name) {
	AddTestSphere(position, name, 1);
}
ACorridor * APathCreator::CreatePathMesh(TArray<FVector> corridorPoints, ARoom * currentRoom, AVisibilityManagerRoom * currentRoomVM, ARoom * nextRoom, AVisibilityManagerRoom * nextRoomVM, bool addVisibilityManager)
{
	//CreatedPathEvent();
	if(currentRoom != nullptr && nextRoom != nullptr)
		AdjustCorridorPointsToRooms(corridorPoints, currentRoom, nextRoom);
	ACorridor * corridor;
	FVector Location(0.0f, 0.0f, 0.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	UWorld* world = GetWorld();
	corridor = GetWorld()->SpawnActor<ACorridor>(Location, Rotation, SpawnInfo);
	corridor->InitCorridor(corridorPoints, corridorWidth, corridorHeight, _corridorFloorMaterial, _corridorWallMaterial, _corridorCeilingMaterial);
	if (currentRoom != nullptr && nextRoom != nullptr) {
		corridor->AddRooms(currentRoom, currentRoomVM, nextRoom, nextRoomVM);
		ADoor * endDoor = nextRoom->getClosestDoor(corridorPoints.Last());
		ADoor * startDoor = currentRoom->getClosestDoor(corridorPoints[0]);
		corridor->AddDoors(startDoor, endDoor, addDoorFrame, _minimumDistanceBetweenDoors);
	}
	if (spawnCorridor && !curvedCorridors) {
		float stepsize = 1.0f / (corridorPoints.Num()-1);
		//Show points for visualization
		/*for (int i = 0; i < corridorPoints.Num(); i++) {
			ADebugSphere* debugSphere = GetWorld()->SpawnActor<ADebugSphere>(Location, Rotation, SpawnInfo);
			debugSphere->AttachToActor(corridor, FAttachmentTransformRules::KeepRelativeTransform);
			debugSphere->SetActorLocation(corridorPoints[i]);
			debugSphere->SetActorLabel(*FString("Corridor Point " + FString::FromInt(i)));
			debugSphere->ChangeColorIntensity(stepsize * i);
			actorsToDestroy.Add(debugSphere);
		}*/

		UE_LOG(LogTemp, Warning, TEXT("Creating Mesh, corridorPoints length: %i"), corridorPoints.Num());
		//if (corridorPoints.Num() < 2)
			//return;
		FVector previousPoint = corridorPoints[0];
		FVector previousDirection;

		corridor->CreateCorridor(addVisibilityManager);
	}
	if (spawnCorridor && curvedCorridors)
		PathCalculatedEvent(corridor);
	return corridor;
	//corridor->DeactivateAllExceptFirstHallway();
}

void APathCreator::AdjustCorridorPointsToRooms(TArray<FVector>& corridorPoints, ARoom * startRoom, ARoom * endRoom)
{
	FVector startPoint = corridorPoints[0];
	FVector startDirection = corridorPoints[0] - corridorPoints[1];
	startDirection.Normalize();
	if (FMath::Abs(startDirection.X) > 0.1) 
		if (startDirection.X > 0) 
			startPoint.X = startRoom->_position.X - startRoom->_width/2.0f;
		else 
			startPoint.X = startRoom->_position.X + startRoom->_width / 2.0f;
	else
		if (startDirection.Y > 0)
			startPoint.Y = startRoom->_position.Y - startRoom->_length / 2.0f;
		else
			startPoint.Y = startRoom->_position.Y + startRoom->_length / 2.0f;


	FVector endPoint = corridorPoints.Last();
	FVector endDirection = corridorPoints.Last() - corridorPoints.Last(1);
	endDirection.Normalize();

	if (FMath::Abs(endDirection.X) > 0.1)
		if (endDirection.X > 0)
			endPoint.X = endRoom->_position.X - endRoom->_width / 2.0f;
		else
			endPoint.X = endRoom->_position.X + endRoom->_width / 2.0f;
	else
		if (endDirection.Y > 0)
			endPoint.Y = endRoom->_position.Y - endRoom->_length / 2.0f;
		else
			endPoint.Y = endRoom->_position.Y + endRoom->_length / 2.0f;

	//AdjustWayPointsToPoints(corridorPoints, startPoint, endPoint);

	corridorPoints[0] = startPoint;
	corridorPoints[corridorPoints.Num() - 1] = endPoint;
	
}

FVector AdjustPointToPoint(FVector adjustmentPoint1, FVector adjustmentPoint2, FVector pointToBeAdjusted) {
	FVector difference = adjustmentPoint1 - adjustmentPoint2;
	FVector adjustmentVector = adjustmentPoint2 - pointToBeAdjusted;

	if (FMath::Abs(adjustmentVector.X) > 0.0001f) {
		adjustmentVector = FVector(1, 0, 0);
	}
	else
	{
		adjustmentVector = FVector(0, 1, 0);
	}
	FVector adjustedPoint = pointToBeAdjusted + difference * adjustmentVector;
	return adjustedPoint;

}

void APathCreator::AdjustWayPointsToPoints(TArray<FVector> & corridorPoints, FVector startPoint, FVector endPoint) {
	/*corridorPoints[0] = startPoint;
	corridorPoints[1] = AdjustPointToPoint(startPoint, corridorPoints[0], corridorPoints[1]);

	if (FVector::DistSquared(corridorPoints[0], corridorPoints[1]) < corridorWidth * corridorWidth / 4.0f) {
		FVector direction = corridorPoints[1] - corridorPoints[0];
		direction.Normalize();
		FVector newPoint = corridorPoints[0] + direction * corridorWidth / 2.0f;
		corridorPoints[1] = newPoint;
		corridorPoints[2] = AdjustPointToPoint(newPoint, corridorPoints[1], corridorPoints[2]);
	}

	int num = corridorPoints.Num();
	corridorPoints[num - 1] = endPoint;
	corridorPoints[num - 2] = AdjustPointToPoint(endPoint, corridorPoints[num - 1], corridorPoints[num - 2]);

	if (FVector::DistSquared(corridorPoints[num - 1], corridorPoints[num - 2]) < corridorWidth * corridorWidth / 4.0f) {
		FVector direction = corridorPoints[num - 2] - corridorPoints[num - 1];
		direction.Normalize();
		FVector newPoint = corridorPoints[num - 1] + direction * corridorWidth / 2.0f;
		corridorPoints[2] = newPoint;
		corridorPoints[num - 3] = AdjustPointToPoint(newPoint, corridorPoints[num - 2], corridorPoints[num - 3]);
	}*/
	int num = corridorPoints.Num();
	if (num < 3)
		return;
	FVector difference = startPoint - corridorPoints[0];
	FVector adjustmentVector = corridorPoints[0] - corridorPoints[1];
	if (FMath::Abs(adjustmentVector.X) > 0.0001f) {
		adjustmentVector = FVector(0, 1, 0);
	}
	else
	{
		adjustmentVector = FVector(1, 0, 0);
	}

	corridorPoints[0] = startPoint;
	corridorPoints[1] += difference * adjustmentVector;


	difference = endPoint - corridorPoints[num - 1];
	adjustmentVector = corridorPoints[num - 1] - corridorPoints[num - 2];
	if (FMath::Abs(adjustmentVector.X) > 0.0001f) {
		adjustmentVector = FVector(0, 1, 0);
	}
	else
	{
		adjustmentVector = FVector(1, 0, 0);
	}
	FVector direction = GetSimpleDirection(corridorPoints[num - 2], corridorPoints[num - 1]);

	corridorPoints[num - 1] = endPoint;
	corridorPoints[num - 2] += difference * adjustmentVector;
	for (int i = 1; i < num; i++) {
		if (FVector::DistSquared(corridorPoints[num - i], corridorPoints[num - (i + 1)]) < corridorWidth * corridorWidth / 4.0f) {

			FVector newPosition = corridorPoints[num - i] + direction * corridorWidth / 2.0f;
			difference = newPosition - corridorPoints[num - (i + 1)];
			corridorPoints[num - (i + 1)] = newPosition;
			corridorPoints[num - (i + 2)] = corridorPoints[num - (i + 2)] + difference;
		}
		else {
			break;
		}
	}
}

void APathCreator::ExpandRoom(FROOMSTRUCT & roomstruct)
{
	TArray<ADoor*> doorsToExpand = roomstruct.room->_doors;

	for (ACorridor* corridor : roomstruct.corridors)
	{
		if (corridor == nullptr)
			continue;

		doorsToExpand.Remove(corridor->_endDoor);
		doorsToExpand.Remove(corridor->_startDoor);
	}

	for (ADoor* door : doorsToExpand){
		FROOMSTRUCT& nextRoom = rooms[door->_roomToID];
		CreatePathAndRoom(roomstruct, rooms[door->_roomToID], door);
		if (nextRoom.roomVM == nullptr)
			continue;
		nextRoom.roomVM->DeactivateAllMainObjects();
		nextRoom.roomVM->DeactivateAllNeighbours();
		nextRoom.roomVM->Deactivate();
	}

}

void APathCreator::RemoveCorridors(ACorridor* doNotRemove, FROOMSTRUCT & roomstruct) {
	for (int i = 0; i < roomstruct.corridors.Num(); i++) {
		ACorridor* c = roomstruct.corridors[i];

		if (c != nullptr && c != doNotRemove) {
			if (c->_startRoom->id != roomstruct.id) {
				rooms[c->_startRoom->id].corridors.Remove(c);
				rooms[c->_startRoom->id].Clean();
			}
			if (c->_endRoom->id != roomstruct.id) {
				rooms[c->_endRoom->id].corridors.Remove(c);
				rooms[c->_endRoom->id].Clean();
			}
			DestroyCorridor(c);
			c->Destroy();
		}
	}
	roomstruct.corridors.Empty();
	roomstruct.corridors.Add(doNotRemove);
}


ADoor * APathCreator::CreatePathAndRoom(FROOMSTRUCT & startRoom, FROOMSTRUCT & endRoom, ADoor * startDoor)
{
	if (endRoom.rTemplate == nullptr) {
		endRoom.rTemplate = new RoomTemplate(GetWorld(), nodeMap, endRoom.size, corridorWidth, doorWidth, doorHeight, _minimumDistanceBetweenDoors, wallThickness, corridorLengthLowerLimit / stepSize);
		endRoom.rTemplate->FindAllPossibleRoomAndDoorPositions(trackingSpaceSize, stepSize);
		if (endRoom.rTemplate->possibleRoomPositions.Num() < 1) {
			UE_LOG(LogTemp, Warning, TEXT("no endroom Positions in template"));
			return nullptr;
		}
	}
	startDoor->ChangeColor(endRoom.doorColor);
	bool createMesh = true;
	TArray<FVector> corridorPoints;
	if (endRoom.room != nullptr || endRoom.roomVM != nullptr) {
		endRoom.Clean();
	}
	
	FVector roomPosition = FVector::ZeroVector;

	FIntVector intDir(startDoor->_forward.X, startDoor->_forward.Y, startDoor->_forward.Z);
	FIntVector endDoorPositionInt(0, 0, 0);
	FIntVector endDoorDirectionInt(0, 0, 0);

	corridorPoints = CreatePathPointsToPossibleDoorNoLimit(startDoor->_arrayPosition, intDir, startRoom, endRoom, roomPosition, endDoorPositionInt, endDoorDirectionInt);
	if (endRoom.rTemplate->_size != endRoom.size) {
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("room map size does not fit room size")));
		UE_LOG(LogTemp, Warning, TEXT("room map size does not fit room size"));
	}
	if (corridorPoints.Num() <= 1) {
		return false;
	}

	if (roomPosition != FVector::ZeroVector) {
		CreateRoom(endRoom, roomPosition, endDoorPositionInt, endDoorDirectionInt, startRoom.id, endRoom.id);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("no path found for room"));
		return nullptr;
		//nextRoom->RoomInitFixedPosition(currentRoom->id + 1, roomTemplate, createMesh, currentRoom->_position);
		//UE_LOG(LogTemp, Warning, TEXT("no positions found for pos x: %f, y: %f, z: %f"), endDoorPosition.X, endDoorPosition.Y, endDoorPosition.Z);

	}
	_corridor = CreatePathMesh(corridorPoints, startRoom.room, startRoom.roomVM, endRoom.room, endRoom.roomVM, true);
	
	startRoom.corridors.Add(_corridor);
	endRoom.corridors.Add(_corridor);

	ADoor * endDoor = endRoom.room->getClosestDoor(corridorPoints.Last());
	endDoor->ChangeColor(startRoom.doorColor);

	return endDoor;
}

FBoxSphereBounds APathCreator::GetClosestObstacle(FVector direction, FVector position)
{
	// in both directions
	FBoxSphereBounds closestObstacle;
	FVector closestPoint;
	float closestPointDistance = TNumericLimits< float >::Max();

	for (size_t j = 0; j < 2; j++)
	{
		direction *= -1;
		float corridorDistance;
		float xUpperLimit = trackingSpaceSize.X / 2.0f;
		//xUpperLimit -= corridorWidth / 2.0f;
		float yUpperLimit = trackingSpaceSize.Y / 2.0f;
		//yUpperLimit -= corridorWidth / 2.0f;

		int directionSign;

		if (FMath::Abs(direction.X) > FMath::Abs(direction.Y)) {
			directionSign = FMath::Sign(direction.X);
			corridorDistance = xUpperLimit - directionSign * position.X;
		}
		else {
			directionSign = FMath::Sign(direction.Y);
			corridorDistance = yUpperLimit - directionSign * position.Y;
		}

		FVector* points = new FVector[2];
		FVector endPoint = position + direction * corridorDistance;
		FVector normal = FVector(direction.Y, direction.X, 0.0f);

		points[0] = position + normal * corridorWidth / 2.0f;
		//points[1] = position - normal * corridorWidth / 2.0f;
		//points[2] = endPoint + normal * corridorWidth / 2.0f;
		points[1] = endPoint - normal * corridorWidth / 2.0f;
		points[1].Z = corridorHeight;

		FBoxSphereBounds boundingBox(points, 2);

		AddTestCube(boundingBox, 1, 0.3f, "bbox closest obstacle");

		TArray<AActor*> FoundActors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AObstacle::StaticClass(), FoundActors);
		bool intersects = false;



		for (int i = 0; i < FoundActors.Num(); i++)
		{
			AObstacle* o = (AObstacle *)FoundActors[i];
			if (FBoxSphereBounds::BoxesIntersect(o->GetBoundingBox(), boundingBox)) {

				FVector newClosestPoint = o->GetBoundingBox().GetBox().GetClosestPointTo(position);
				float newDistance = FVector::DistSquared(position, newClosestPoint);
				if (closestPointDistance > newDistance) {
					closestPoint = newClosestPoint;
					closestPointDistance = newDistance;
					closestObstacle = o->GetBoundingBox();
				}
				//UE_LOG(LogTemp, Warning, TEXT("Intersections are happening"));
			}
			else {
				//UE_LOG(LogTemp, Warning, TEXT("no intersections here"));
			}
		}
	}
	return closestObstacle;
}
float APathCreator::GetCorridorDistanceToObstacle(FVector direction, FVector position) {
	return GetDistanceToObstacle(direction, position, corridorWidth);
}
float APathCreator::GetDistanceToObstacle(FVector direction, FVector position, float width)
{
	float corridorDistance;
	float xUpperLimit = trackingSpaceSize.X / 2.0f;
	//xUpperLimit -= corridorWidth / 2.0f;
	float yUpperLimit = trackingSpaceSize.Y / 2.0f;
	//yUpperLimit -= corridorWidth / 2.0f;

	int directionSign;

	if (FMath::Abs(direction.X) > FMath::Abs(direction.Y)) {
		directionSign = FMath::Sign(direction.X);
		corridorDistance = xUpperLimit - directionSign * position.X;
	}
	else {
		directionSign = FMath::Sign(direction.Y);
		corridorDistance = yUpperLimit - directionSign * position.Y;
	}

	FVector* points = new FVector[2];
	FVector endPoint = position + direction * corridorDistance;
	FVector normal = FVector(direction.Y, direction.X, 0.0f);

	points[0] = position + normal * width / 2.0f;
	//points[1] = position - normal * corridorWidth / 2.0f;
	//points[2] = endPoint + normal * corridorWidth / 2.0f;
	points[1] = endPoint - normal * width / 2.0f;
	points[1].Z = corridorHeight;


	FVector absDirection = FVector(FMath::Abs(direction.X), FMath::Abs(direction.Y), 0.0f);
	FVector absNormal = FVector(absDirection.Y, absDirection.X, 0.0f);

	FBoxSphereBounds boundingBox(points, 2);

	AddTestCube(boundingBox, 0, 0.2f, "bbox distance to obstacle");

	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AObstacle::StaticClass(), FoundActors);
	bool intersects = false;

	FVector closestPoint;
	float closestPointDistance = TNumericLimits< float >::Max();
	for (int i = 0; i < FoundActors.Num(); i++)
	{
		AObstacle* o = (AObstacle *)FoundActors[i];
		if (FBoxSphereBounds::BoxesIntersect(o->GetBoundingBox(), boundingBox)) {
			
			FVector newClosestPoint = position * absNormal + o->GetActorLocation() * absDirection - direction * o->GetBoundingBox().GetBox().GetExtent(); // o->GetActorLocation() * absDirection + position * absNormal;
			//FVector newClosestPoint = o->GetBoundingBox().GetBox().GetClosestPointTo(comparePosition);
			float newDistance = FVector::DistSquared(position, newClosestPoint);
			if (closestPointDistance > newDistance) {
				closestPoint = newClosestPoint;
				closestPointDistance = newDistance;
			}

			intersects = true;
			//UE_LOG(LogTemp, Warning, TEXT("Intersections are happening"));
		}
		else {
			//UE_LOG(LogTemp, Warning, TEXT("no intersections here"));
		}
	}
	if (intersects) {
		corridorDistance = FMath::Sqrt(closestPointDistance);
	}
	AddTestSphere(position + direction * corridorDistance, "edge point");
	return corridorDistance - 0.01f;
}

void APathCreator::AddTestCube(FBoxSphereBounds box, float intensity, float height, FString name)
{
	FLinearColor color = FLinearColor::LerpUsingHSV(FLinearColor::Green, FLinearColor::Red, intensity);
	AddTestCube(box, color, height, name, true);
}

void APathCreator::CreateRoomClosestToPlayer(FROOMSTRUCT & roomStruct)
{
	roomStruct.rTemplate = new RoomTemplate(GetWorld(), nodeMap, roomStruct.size, corridorWidth, doorWidth, doorHeight, _minimumDistanceBetweenDoors, wallThickness, corridorLengthLowerLimit / stepSize);
	roomStruct.rTemplate->FindAllPossibleRoomAndDoorPositions(trackingSpaceSize, stepSize);
	if (roomStruct.rTemplate->possibleRoomPositions.Num() < 1) {
		UE_LOG(LogTemp, Warning, TEXT("no Room Positions in template"));
		return;
	}
	FVector roompos = roomStruct.rTemplate->GetClosestRoomPosition(player->GetPawn()->GetActorLocation());

	TArray<int> connections = roomStruct.connections;

	roomStruct.room = CreateRoom(true, roomStruct.id, roompos, roomStruct.rTemplate, roomStruct.connections.Num(), roomStruct.floorMaterial, roomStruct.wallMaterial, roomStruct.ceilingMaterial, roomStruct.roomColor);

	for (ADoor* d : roomStruct.room->_doors) {
		if (connections.Num() < 1) {
			UE_LOG(LogTemp, Warning, TEXT("more doors than connections"));
			break;
		}
		d->_roomToID = connections.Pop();
		d->_roomInID = roomStruct.id;
	}


	roomStruct.roomVM = CreateVisibilityManagerForRoom(roomStruct.room);
	AddPropsToRoom(roomStruct);
	roomStruct.room->ChangeDoorColor(FLinearColor::Green);
}

void APathCreator::CreateRoom(FROOMSTRUCT & roomStruct, FVector roomPos, FIntVector doorIntPos, FIntVector doorIntDir, int doorToRoomID, int doorInRoomID)
{
	roomStruct.room = CreateRoom(true, roomStruct.id, roomPos, roomStruct.rTemplate, roomStruct.connections.Num(), doorIntDir, doorIntPos, roomStruct.floorMaterial, roomStruct.wallMaterial, roomStruct.ceilingMaterial, roomStruct.roomColor);
	
	ADoor * endDoor = roomStruct.room->getClosestDoor(nodeMap->GetRealPosition(doorIntPos));
	endDoor->_roomInID = doorInRoomID;
	endDoor->_roomToID = doorToRoomID;
	
	TArray<int> connections = roomStruct.connections;
	connections.Remove(doorToRoomID);

	for (ADoor* d : roomStruct.room->_doors) {
		if (connections.Num() < 1) {
			UE_LOG(LogTemp, Warning, TEXT("more doors than connections"));
			break;
		}
		if (d == endDoor)
			continue;
		d->_roomToID = connections.Pop();
		d->_roomInID = doorInRoomID;
		d->ChangeColor(rooms[d->_roomToID].doorColor);
	}

	//nextRoom->RoomInitFixedPosition(currentRoom->id + 1, roomTemplate, createMesh, roomPosition, amountOfDoors, endDoorDirectionInt, endDoorPositionInt);
	roomStruct.roomVM = CreateVisibilityManagerForRoom(roomStruct.room);

	AddPropsToRoom(roomStruct);
}

void APathCreator::AddPropsToRoom(FROOMSTRUCT & roomStruct)
{
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	if (roomStruct.lightBlueprint) {
		ACeilingLight* light = GetWorld()->SpawnActor<ACeilingLight>(roomStruct.lightBlueprint,FVector::UpVector * roomStruct.room->_height, Rotation, SpawnInfo);
		//light->ChangeLightColor(roomStruct.roomColor);
		//roomStruct.roomVM->mainObjects.Add(light);
		light->AttachToActor(roomStruct.room, FAttachmentTransformRules::KeepRelativeTransform);
	}
	for (const TPair<FVector, TSubclassOf<class AVisibleObject>>& pair : roomStruct.props)
	{
		AVisibleObject* prop = GetWorld()->SpawnActor<AVisibleObject>(pair.Value, pair.Key, Rotation, SpawnInfo);
		//roomStruct.roomVM->mainObjects.Add(prop);
		prop->AttachToActor(roomStruct.room, FAttachmentTransformRules::KeepRelativeTransform);
	}
}

ARoom * APathCreator::CreateRoom(bool createMesh, int id, RoomTemplate * roomTemplate, int amountOfdoors)
{
	FVector Location(0.0f, 0.0f, 0.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	UWorld* world = GetWorld();
	ARoom * room = GetWorld()->SpawnActor<ARoom>(Location, Rotation, SpawnInfo);
	room->wallMaterial = rooms[0].wallMaterial;
	room->ceilingMaterial = rooms[0].ceilingMaterial;
	room->floorMaterial = rooms[0].floorMaterial;
	room->_roomColor = rooms[0].roomColor;
	room->RoomInitRandomPosition(id, roomTemplate, createMesh, amountOfdoors, doorSpeed);

	return room;
}

ARoom * APathCreator::CreateRoom(bool createMesh, int id, FVector position, RoomTemplate * roomTemplate, int amountOfdoors, UMaterial * floorMaterial, UMaterial * wallMaterial, UMaterial * ceilingMaterial, FColor roomColor)
{
	FVector Location(0.0f, 0.0f, 0.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	UWorld* world = GetWorld();
	ARoom * room = GetWorld()->SpawnActor<ARoom>(Location, Rotation, SpawnInfo);
	room->wallMaterial = wallMaterial;
	room->ceilingMaterial = ceilingMaterial;
	room->floorMaterial = floorMaterial;
	room->_roomColor = roomColor;
	room->RoomInitFixedPosition(id, roomTemplate, createMesh, position, amountOfdoors, doorSpeed);
	room->ChangeDoorColor(FLinearColor::Red);

	return room;
}

ARoom * APathCreator::CreateRoom(bool createMesh, int id, FVector position, RoomTemplate * roomTemplate, int amountOfDoors, FIntVector endDoorDirectionInt, FIntVector endDoorPositionInt, UMaterial * floorMaterial, UMaterial * wallMaterial, UMaterial * ceilingMaterial, FColor roomColor)
{
	FVector Location(0.0f, 0.0f, 0.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	UWorld* world = GetWorld();
	ARoom * room = GetWorld()->SpawnActor<ARoom>(Location, Rotation, SpawnInfo);
	room->wallMaterial = wallMaterial;
	room->ceilingMaterial = ceilingMaterial;
	room->floorMaterial = floorMaterial;
	room->_roomColor = roomColor;
	room->RoomInitFixedPosition(id, roomTemplate, createMesh, position, amountOfDoors, doorSpeed, endDoorDirectionInt, endDoorPositionInt);
	room->ChangeDoorColor(FLinearColor::Red);

	return room;
}

AVisibilityManagerRoom * APathCreator::CreateVisibilityManagerForRoom(ARoom * room)
{
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	AVisibilityManagerRoom* vism = GetWorld()->SpawnActor<AVisibilityManagerRoom>(room->_position, Rotation, SpawnInfo);
	vism->Init(room, this);
	return vism;
}

void APathCreator::AddTestCube(FBoxSphereBounds box, FLinearColor color, float height, FString name, bool destroy)
{
	if (!showAlgorithmVisualization)
		return;
	FVector Location = box.GetBoxExtrema(0) + (box.GetBoxExtrema(1) - box.GetBoxExtrema(0)) / 2.0f;
	Location.Z = 0;
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	UWorld* world = GetWorld();
	ADebugCube* boundingBoxVisualization;
	boundingBoxVisualization = GetWorld()->SpawnActor<ADebugCube>(Location, Rotation, SpawnInfo);
	FVector scale = (box.GetBoxExtrema(1) - box.GetBoxExtrema(0));
	scale.X = FMath::Abs(scale.X) / 100.0f;
	scale.Y = FMath::Abs(scale.Y) / 100.0f;
	scale.Z = height;

	//scale.Z = 1;
	boundingBoxVisualization->SetActorScale3D(scale);
	boundingBoxVisualization->SetActorLabel(name);
	boundingBoxVisualization->ChangeColor(color);
	if (destroy)
		actorsToDestroy.Add(boundingBoxVisualization);
	else
		actorsToManuallyDestroy.Add(boundingBoxVisualization);

}

TArray<FVector> APathCreator::CreateRandomPathPoints(FVector startPoint, FVector startDirection, float cornerAmount)
{
	if (nodeMap == nullptr)
		GetFreeNodes(stepSize);

	TArray<FVector> wayPoints;
	FVector wayVector = startDirection;
	wayVector.Normalize();
	FVector previousWayVector;
	FVector currentPoint = startPoint;
	wayPoints.Add(currentPoint);

	int direction = 1;
	float lowerLimit = corridorWidth;
	float upperLimit;
	float xUpperLimit = trackingSpaceSize.X / 2.0f;
	xUpperLimit -= corridorWidth / 2.0f;
	float yUpperLimit = trackingSpaceSize.Y / 2.0f;
	yUpperLimit -= corridorWidth / 2.0f;
	float corridorLength;

	bool triedPreviousDirection = false;
	bool triedBreakOut = false;
	// make all hallways except last one

	int loop = 0;
	for (size_t i = 0; i <= cornerAmount; i++)
	{
		loop++;
		if (loop > 100) {
			UE_LOG(LogTemp, Warning, TEXT("too many loops random halways"));
		}
		if (i == 0)
			lowerLimit = corridorWidth / 2.0f;
		else
			lowerLimit = corridorLengthLowerLimit;

		upperLimit = GetCorridorDistanceToObstacle(wayVector, currentPoint) - corridorWidth / 2.0f - 0.1f;

		if (upperLimit < lowerLimit) {
			if (triedPreviousDirection) {
				UE_LOG(LogTemp, Warning, TEXT("Breakout not possible"));
				wayVector = GetDirectionWithMoreSpace(wayVector, currentPoint);
				upperLimit = GetCorridorDistanceToObstacle(wayVector, currentPoint) - corridorWidth / 2.0f - 0.1f;
				lowerLimit = upperLimit;
				/*UE_LOG(LogTemp, Warning, TEXT("Both sides: %f"), upperLimit);
				if (triedBreakOut) {
					UE_LOG(LogTemp, Warning, TEXT("BreakOut Did not work x: %f y: %f"), currentPoint.X, currentPoint.Y);
					break;
				}
				wayPoints = BreakOut(wayPoints, previousWayVector, wayVector);
				triedBreakOut = true;
				currentPoint = wayPoints[wayPoints.Num() - 1];
				triedPreviousDirection = false;
				continue;*/
			}
			else {
				i--;
				wayVector *= -1.0f;
				triedPreviousDirection = true;
				continue;
			}
		}

		corridorLength = FMath::FRandRange(lowerLimit, upperLimit);

		if (upperLimitOnly)
			corridorLength = upperLimit;
		if (lowerLimitOnly)
			corridorLength = lowerLimit;

		currentPoint = currentPoint + wayVector * corridorLength;

		//UE_LOG(LogTemp, Warning, TEXT("waypoint x: %f, y: %f"), nextPoint.X, nextPoint.Y);
		currentPoint = nodeMap->GetClosestPossiblePoint(currentPoint);
		wayPoints.Add(currentPoint);
		previousWayVector = wayVector;
		wayVector = FVector(wayVector.Y, wayVector.X, 0.0f);
		if (CheckIfBreakOutNecessary(currentPoint, wayVector, corridorLengthLowerLimit + corridorWidth / 2.0f, corridorWidth)) {
			wayPoints = BreakOut(wayPoints, previousWayVector, wayVector);
			currentPoint = wayPoints[wayPoints.Num() - 1];
		}

		if (i >= cornerAmount-1) {
			FVector closestPoint = nodeMap->GetClosestPossiblePoint(currentPoint);
			FVector prevAdjust = FVector(FMath::Abs(previousWayVector.X), FMath::Abs(previousWayVector.Y), 0.0f);
			FVector wayAdjust = FVector(FMath::Abs(wayVector.X), FMath::Abs(wayVector.Y), 0.0f);
			FVector tempPoint = currentPoint * wayAdjust + closestPoint * prevAdjust;
			if (CheckIfFree(FVector(corridorWidth, corridorWidth, corridorWidth), tempPoint, false)) {
				tempPoint -= previousWayVector * stepSize;
			}
			currentPoint = tempPoint;
			wayPoints[wayPoints.Num() - 1] = currentPoint;
		}
		int leftRight = FMath::Sign(FMath::FRandRange(-1.0f, 1.0f));
		wayVector *= leftRight;
		triedPreviousDirection = false;
	}

	return wayPoints;
}

TArray<FVector> APathCreator::CreateRandomPathPoints(FIntVector startPoint, FIntVector startDirection, int cornerAmount)
{
	TArray<FVector> wayPoints;
	TArray<FIntVector> wayIntPoints;

	FIntVector currentPoint = startPoint;
	FIntVector wayVector = startDirection;
	
	wayPoints.Add(nodeMap->GetRealPosition(currentPoint));
	wayIntPoints.Add(currentPoint);

	int lowerLimit;
	int doorLowerLimit = (corridorWidth / 2.0f) / stepSize + 1;
	int corridorLowerLimit = corridorLengthLowerLimit / stepSize;
	int upperLimit;

	FIntVector previousWayVector;
	FIntVector previousCurrentPoint;


	for (int i = 0; i < cornerAmount; i++)
	{
		if (i == 0) {
			lowerLimit = doorLowerLimit;
		}
		else {
			lowerLimit = corridorLowerLimit;
		}
		upperLimit = nodeMap->NodeRayCast(currentPoint, wayVector);
		/*
		if ((wayVector.X + wayVector.Y) < 0) {
			if (wayVector.X < 0)
				upperLimit = currentPoint.X - 2;
			else
				upperLimit = currentPoint.Y - 2;
		}
		else {
			if (wayVector.X > 0)
				upperLimit = nodeArrayWidth - 3 - currentPoint.X;
			else
				upperLimit = nodeArrayHeight - 3 - currentPoint.Y;
		}
		*/
		if (nodeMap->CheckIfBreakOutNecessary(currentPoint, FIntVector(wayVector.Y, wayVector.X, 0), corridorLowerLimit)) {
			//return CreateRandomPathPoints(startPoint, startDirection, cornerAmount);
			UE_LOG(LogTemp, Warning, TEXT("break out necessary, even though we should have checked earlier"));
			UE_LOG(LogTemp, Warning, TEXT("wayVector x: %i, y: %i, z: %i point: x: %i, y: %i, z: %i"), wayVector.Y, wayVector.X, 0, currentPoint.X, currentPoint.Y, currentPoint.Z);
			UE_LOG(LogTemp, Warning, TEXT("preVector x: %i, y: %i, z: %i prept: x: %i, y: %i, z: %i"), previousWayVector.X, previousWayVector.Y, previousWayVector.Z, previousCurrentPoint.X, previousCurrentPoint.Y, previousCurrentPoint.Z);

			return wayPoints;
		}
		if (lowerLimit > upperLimit) {
			i--;
			wayVector *= -1;
			continue;
		}
		int corridorLength = FMath::RandRange(lowerLimit, upperLimit);
		if (lowerLimitOnly)
			corridorLength = lowerLimit;
		if (upperLimitOnly)
			corridorLength = upperLimit;
		currentPoint += wayVector * corridorLength;
		previousCurrentPoint = currentPoint;
		if (nodeMap->CheckIfBreakOutNecessary(currentPoint, wayVector, corridorLowerLimit)) {
			//BreakOut()
			if (!TryBreakThrough(currentPoint, wayVector, corridorLowerLimit)) {
				UE_LOG(LogTemp, Warning, TEXT("Breakthrough did not work, try fitting corridor"));

				if (!TryFittingCorridor(currentPoint, corridorLength, wayVector, previousWayVector, corridorLowerLimit, wayPoints, wayIntPoints)){
					UE_LOG(LogTemp, Warning, TEXT("Fitting did not work, try different direction"));
					i--;
					wayVector *= -1;
					continue;
				}
			}
			//return CreateRandomPathPoints(startPoint, startDirection, cornerAmount);
		}

		previousWayVector = wayVector;
		int leftRight = FMath::Sign(FMath::FRandRange(-1.0f, 1.0f));
		wayVector = FIntVector(wayVector.Y, wayVector.X, 0) * leftRight;
		wayIntPoints.Add(currentPoint);
		wayPoints.Add(nodeMap->GetRealPosition(currentPoint));
	}

	return wayPoints;
}


bool APathCreator::CheckIfBreakOutNecessary(FVector position, FVector wayVector, float minimumDistance, float widthCheck)
{
	bool oneSideTooSmall = false;
	FVector normal = FVector(wayVector.Y, wayVector.X, 0.0f);
	for (int i = 0; i < 2; i++)
	{
		wayVector *= -1;

		FVector* points = new FVector[2];
		FVector endPoint = position + wayVector * minimumDistance;
		FVector normal = FVector(wayVector.Y, wayVector.X, 0.0f);

		points[0] = position + normal * widthCheck / 2.0f;
		//points[1] = position - normal * widthCheck / 2.0f;
		//points[2] = endPoint + normal * widthCheck / 2.0f;
		points[1] = endPoint - normal * widthCheck / 2.0f;
		points[1].Z = corridorHeight;
		FBoxSphereBounds boundingBox(points, 2);

		AddTestCube(boundingBox, 0.5f, 0.4f, "bbox check for breakout");
		
		float distanceToTrackingAreaBorder = GetDistanceToTrackingAreaBorder(wayVector, position);
		if (distanceToTrackingAreaBorder < minimumDistance) {
			if (oneSideTooSmall) {
				UE_LOG(LogTemp, Warning, TEXT("Break out necessary"));
				return true;
			}
			oneSideTooSmall = true;
			continue;
		}
		TArray<AActor*> FoundActors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AObstacle::StaticClass(), FoundActors);

		for (int i = 0; i < FoundActors.Num(); i++)
		{
			AObstacle* o = (AObstacle *)FoundActors[i];
			if (FBoxSphereBounds::BoxesIntersect(o->GetBoundingBox(), boundingBox)) {
				if (oneSideTooSmall) {
					UE_LOG(LogTemp, Warning, TEXT("Break out necessary"));
					return true;
				}
				oneSideTooSmall = true;
				//UE_LOG(LogTemp, Warning, TEXT("Intersections are happening"));
			}
			else {
				//UE_LOG(LogTemp, Warning, TEXT("no intersections here"));
			}
		}
	}
	return false;
}

TArray<FVector> APathCreator::BreakOut(TArray<FVector> wayPoints, FVector previousWayVector, FVector wayVector)
{
	FVector currentPoint = wayPoints[wayPoints.Num() - 1];
	FBoxSphereBounds closestBox1 = GetClosestObstacle(wayVector, currentPoint);

	FVector outestPointInDirectionClosestBox1 = closestBox1.GetBox().GetCenter();
	outestPointInDirectionClosestBox1 += previousWayVector * closestBox1.GetBox().GetExtent();

	//outestPointInDirectionClosestBox1 += previousWayVector * closestBox1.GetBox().GetExtent() / 2.0f;
	AddTestSphere(outestPointInDirectionClosestBox1, "outest Point");

	//UE_LOG(LogTemp, Warning, TEXT("BreakOut outestPointInDirectionClosestBox2 x: %f y: %f"), outestPointInDirectionClosestBox2.X, outestPointInDirectionClosestBox2.Y);
	//UE_LOG(LogTemp, Warning, TEXT("BreakOut outestPointInDirectionClosestBox1 x: %f y: %f"), outestPointInDirectionClosestBox1.X, outestPointInDirectionClosestBox1.Y);


	FVector vector = FVector(FMath::Abs(previousWayVector.X), FMath::Abs(previousWayVector.Y), 0);
	FVector negaVector = FVector(FMath::Abs(wayVector.X), FMath::Abs(wayVector.Y), 0);
	//UE_LOG(LogTemp, Warning, TEXT("BreakOut vector x: %f y: %f"), vector.X, vector.Y);
	//UE_LOG(LogTemp, Warning, TEXT("BreakOut negaVector x: %f y: %f"), negaVector.X, negaVector.Y);

	FVector newPoint = currentPoint * negaVector + outestPointInDirectionClosestBox1 * vector;

	newPoint += previousWayVector * corridorWidth / 2.0f + previousWayVector * 0.1f;
	//UE_LOG(LogTemp, Warning, TEXT("newPoint x: %f y: %f"), newPoint.X, newPoint.Y);

	FVector corridorLimit = trackingSpaceSize / 2.0f;
	corridorLimit = corridorLimit - FVector::OneVector * corridorWidth / 2.0f;

	//UE_LOG(LogTemp, Warning, TEXT("corridorLimit x: %f y: %f"), corridorLimit.X, corridorLimit.Y);

	if (FMath::Abs(newPoint.X) > corridorLimit.X || FMath::Abs(newPoint.Y) > corridorLimit.Y) {
		//move previous point
		FVector previousPreviousWayVector;
		FVector previousPoint = wayPoints[wayPoints.Num() - 2];

		if (wayPoints.Num() >= 3) {
			previousPreviousWayVector = previousPoint - wayPoints[wayPoints.Num() - 3];
			previousPreviousWayVector.Normalize();
		}
		else {
			return wayPoints;
			//previousPreviousWayVector = GetDirectionWithMoreSpace(wayVector, previousPoint);
		}
		float distance = GetCorridorDistanceToObstacle(previousPreviousWayVector, currentPoint) - corridorWidth / 2.0f;
		newPoint = currentPoint += previousPreviousWayVector * distance;
		if (CheckIfBreakOutNecessary(newPoint, wayVector, corridorLengthLowerLimit + corridorWidth / 2.0f, corridorWidth)) {
			//no space left, try different direction
			previousWayVector *= -1.0f;
			newPoint = currentPoint + previousWayVector * corridorLengthLowerLimit;
		}
		else {
			previousPoint = previousPoint += previousPreviousWayVector * distance;
			wayPoints[wayPoints.Num() - 2] = previousPoint;
			//wayPoints[wayPoints.Num() - 1] = newPoint;
		}
	}
	wayPoints[wayPoints.Num() - 1] = newPoint;
	if (CheckIfBreakOutNecessary(newPoint, wayVector, corridorLengthLowerLimit + corridorWidth / 2.0f, corridorWidth)) {
		wayPoints = BreakOut(wayPoints, previousWayVector, wayVector);
	}
	//UE_LOG(LogTemp, Warning, TEXT("BreakOut x: %f y: %f"), newPoint.X, newPoint.Y);

	return wayPoints;
}

bool APathCreator::TryBreakThrough(FIntVector & currentPoint, FIntVector wayVector, int lowerLimit)
{
	FIntVector point = currentPoint;

	while (nodeMap->CheckIfBreakOutNecessary(point, wayVector, lowerLimit)) {
		point += wayVector;
		if (point.X > nodeMap->nodeArrayDimensions.X - 1 || point.X < 0 ||
			point.Y > nodeMap->nodeArrayDimensions.Y - 1 || point.Y < 0)
			return false;
	}
	currentPoint = point;
	return true;
}

bool APathCreator::TryFittingCorridor(FIntVector &currentPoint, int corridorDistance, FIntVector wayVector, FIntVector previousWayVector, int lowerLimit, TArray<FVector> & wayPoints, TArray<FIntVector> & wayIntPoints)
{
	FIntVector point = currentPoint;
	FIntVector previousPoint = wayIntPoints.Last();
	while (nodeMap->CheckIfBreakOutNecessary(point, wayVector, lowerLimit)) {
		previousPoint += previousWayVector;
		if (!CheckIfCorridorPointFree(previousPoint))
			return false;
		point += previousWayVector;
		if (!CheckIfCorridorPointFree(point))
			return false;
		if (nodeMap->NodeRayCast(previousPoint, wayVector) < corridorDistance)
			return false;
	}
	currentPoint = point;
	wayIntPoints[wayIntPoints.Num() - 1] = previousPoint;
	wayPoints[wayPoints.Num() - 1] = nodeMap->GetRealPosition(previousPoint);
	return true;
}


TArray<FVector> APathCreator::CreateEndPathPoints(FVector startPoint, FVector startDirection)
{

	//create endPoint
	FVector lastPoint = startPoint;
	FVector lastWayVector = FVector(startDirection.Y, startDirection.X, 0.0f);;
	FVector previousWayVector = startDirection;

	FVector currentPoint = lastPoint;
	FVector wayVector = lastWayVector;
	float corridorLength;
	float upperLimit;
	float lowerLimit;

	float minimumRoomSpaceX = rooms[0].size.X + 2 * corridorWidth;
	float minimumRoomSpaceY = rooms[0].size.Y + 2 * corridorWidth;

	float distanceToAreaEndInDirection;
	int loop = 0;

	TArray<FVector> endPoints;
	while (!CheckIfRoomFitsAfterWayPoint(previousWayVector, currentPoint)) {
		loop++;
		endPoints.Empty();
		wayVector = lastWayVector;
		currentPoint = lastPoint;
		for (size_t i = 3; i <= loop; i++) {
			//Max Distance (goes all the way to the end of the tracking area
			wayVector = GetDirectionWithMoreSpace(wayVector, currentPoint);
			distanceToAreaEndInDirection = GetCorridorDistanceToObstacle(wayVector, currentPoint);
			corridorLength = distanceToAreaEndInDirection - corridorWidth / 2.0f;
			currentPoint = currentPoint + wayVector * corridorLength;
			previousWayVector = wayVector;
			wayVector = FVector(wayVector.Y, wayVector.X, 0.0f);
			endPoints.Add(currentPoint);
		}

		if (loop >= 2) {
			//Half Distance (goes far enough so that the wayPoint is at half the width of the room away from tracking area border)
			wayVector = GetDirectionWithMoreSpace(wayVector, currentPoint);
			float minimumRoomWidth = minimumRoomSpaceX * FMath::Abs(wayVector.Y) + minimumRoomSpaceY * FMath::Abs(wayVector.X);
			distanceToAreaEndInDirection = GetCorridorDistanceToObstacle(wayVector, currentPoint);
			float distanceToAreaEndInOtherDirection = GetCorridorDistanceToObstacle(-wayVector, currentPoint);
			float minimumRoomLength = minimumRoomSpaceX * FMath::Abs(wayVector.X) + minimumRoomSpaceY * FMath::Abs(wayVector.Y);

			lowerLimit = minimumRoomLength / 2.0f + corridorWidth / 2.0f - distanceToAreaEndInOtherDirection;
			upperLimit = distanceToAreaEndInDirection - minimumRoomLength / 2.0f - corridorWidth / 2.0f + 1.0f;

			corridorLength = FMath::FRandRange(lowerLimit, upperLimit);

			if (upperLimitOnly)
				corridorLength = upperLimit;
			if (lowerLimitOnly)
				corridorLength = lowerLimit;

			//happens if both directions are almost the same distance away from border
			if (corridorLength < corridorLengthLowerLimit)
				corridorLength = corridorLengthLowerLimit;

			currentPoint = currentPoint + wayVector * corridorLength;
			previousWayVector = wayVector;
			wayVector = FVector(wayVector.Y, wayVector.X, 0.0f);
			endPoints.Add(currentPoint);
		}

		if (loop >= 1) {
			//Room Distance (goes short enough to fit room after wayPoint)
			wayVector = GetDirectionWithMoreSpace(wayVector, currentPoint);
			distanceToAreaEndInDirection = GetCorridorDistanceToObstacle(wayVector, currentPoint);
			lowerLimit = corridorWidth / 2.0f;
			float minimumRoomSpace = minimumRoomSpaceX * FMath::Abs(wayVector.X) + minimumRoomSpaceY * FMath::Abs(wayVector.Y);
			upperLimit = distanceToAreaEndInDirection - minimumRoomSpace + corridorWidth - 1.0f;
			if (upperLimit < lowerLimit)
				upperLimit = lowerLimit;
			corridorLength = FMath::FRandRange(lowerLimit, upperLimit);

			if (upperLimitOnly)
				corridorLength = upperLimit;
			if (lowerLimitOnly)
				corridorLength = lowerLimit;

			currentPoint = currentPoint + wayVector * corridorLength;
			previousWayVector = wayVector;
			wayVector = FVector(wayVector.Y, wayVector.X, 0.0f);
			endPoints.Add(currentPoint);
		}
		if (loop > 10) {
			UE_LOG(LogTemp, Warning, TEXT("too many loops end points"));
			break;
		}
	}


	return endPoints;
}

struct NODECONTAINER {
	TArray<TSharedPtr<NODE>> nodes;
	FIntVector direction;
	FIntVector mapPosition;
	float id;
	bool empty;
	bool addNode(TSharedPtr<NODE> newNode, float limitLength) {
		//UE_LOG(LogTemp, Warning, TEXT("nodes in container length: %i"), nodes.Num());
		//UE_LOG(LogTemp, Warning, TEXT("length successor: %f"), newNode.distanceToLastCorner);
		int nodeNum = nodes.Num();
		TArray<TSharedPtr<NODE>> nodesToRemove;
		for (int i = 0; i < nodeNum; i++)
		{
			TSharedPtr<NODE> existingNode = nodes[i];
			float existingSuccessorCorridorDistanceToLastCorner = existingNode->distanceToLastCorner;
			//UE_LOG(LogTemp, Warning, TEXT("length existing: %f"), existingSuccessorCorridorDistanceToLastCorner);

			if (existingSuccessorCorridorDistanceToLastCorner >= newNode->distanceToLastCorner && existingNode->cost < newNode->cost){
			//	UE_LOG(LogTemp, Warning, TEXT("node wont be added"), nodes.Num());

				return false;
			}
			if (existingNode->cost < newNode->cost && existingSuccessorCorridorDistanceToLastCorner > limitLength) {
			//	UE_LOG(LogTemp, Warning, TEXT("node wont be added"), nodes.Num());
				return false;
			}

			if (existingSuccessorCorridorDistanceToLastCorner <= newNode->distanceToLastCorner && existingNode->cost > newNode->cost) {
			//	UE_LOG(LogTemp, Warning, TEXT("removing existing Node"), nodes.Num());
				nodesToRemove.Add(existingNode);
			}
		}
		for (int i = 0; i < nodesToRemove.Num(); i++) {
			nodes.Remove(nodesToRemove[i]);
		}
		nodes.Add(newNode);

		return true;
	}
	NODECONTAINER() { empty = false; };
	NODECONTAINER(TSharedPtr<NODE> node) {
		mapPosition = node->mapPosition;
		direction = node->direction;
		nodes.Add(node);
		id = 1 * mapPosition.Z + 10 * mapPosition.Y + 100 * mapPosition.Z + 1000 * mapPosition.Z + 10000 * mapPosition.Y + 10000 * mapPosition.X;
		empty = true;
	};
};


FORCEINLINE uint32 GetTypeHash(const NODE& b)
{
	return FCrc::MemCrc32(&b, sizeof(NODE));
}

bool operator<(const NODECONTAINER &n1, const NODECONTAINER &n2) {
	return n1.id < n2.id;
}
bool operator==(const NODECONTAINER &n1, const NODECONTAINER &n2) {
	return n1.mapPosition == n2.mapPosition && n1.direction == n2.direction;
}

FORCEINLINE uint32 GetTypeHash(const NODECONTAINER& b)
{
	return FCrc::MemCrc32(&b, sizeof(NODECONTAINER));
}

struct NODEDIRECTIONMAP {
	NODECONTAINER* nodeContainter100;
	NODECONTAINER* nodeContainter010;
	NODECONTAINER* nodeContainterN00;
	NODECONTAINER* nodeContainter0N0;

	int _height;
	int _width;

	NODEDIRECTIONMAP(int height, int width) {
		nodeContainter100 = new NODECONTAINER[height * width];
		nodeContainter010 = new NODECONTAINER[height * width];
		nodeContainterN00 = new NODECONTAINER[height * width];
		nodeContainter0N0 = new NODECONTAINER[height * width];
		_height = height;
		_width = width;
	}

	void Destroy() {
		delete[] nodeContainter100;
		delete[] nodeContainter010;
		delete[] nodeContainterN00;
		delete[] nodeContainter0N0;
	}

	bool AddNode(TSharedPtr<NODE> node, float limitLength) {
		if (node->direction.X == 1 && node->direction.Y == 0) {
			return nodeContainter100[node->mapPosition.X + _width * node->mapPosition.Y].addNode(node, limitLength);
		}
		else if (node->direction.X == 0 && node->direction.Y == 1) {
			return nodeContainter010[node->mapPosition.X + _width * node->mapPosition.Y].addNode(node, limitLength);
		}
		else if (node->direction.X == -1 && node->direction.Y == 0) {
			return nodeContainterN00[node->mapPosition.X + _width * node->mapPosition.Y].addNode(node, limitLength);
		}
		else if (node->direction.X == 0 && node->direction.Y == -1) {
			return nodeContainter0N0[node->mapPosition.X + _width * node->mapPosition.Y].addNode(node, limitLength);
		}
		return false;
	}
};


NODE* APathCreator::ExpandNode(TSharedPtr<NODE>currentNode, FIntVector direction) {

	FIntVector mapPosition = currentNode->mapPosition + direction;


	if (FMath::Abs(direction.X) == FMath::Abs(direction.Y)) {
		//UE_LOG(LogTemp, Warning, TEXT("node diagonal or middle"));

		return nullptr;
	}
	
	//if (curvedCorridors &&  currentNode->distanceToLastCorner > (3.0f / 2.0f) * corridorLengthLowerLimit)
	//	return nullptr;

	if (mapPosition.X >= nodeMap->nodeArrayDimensions.X || mapPosition.Y >= nodeMap->nodeArrayDimensions.Y || mapPosition.X < 0 || mapPosition.Y < 0) {
		return nullptr;
	}


	if (!nodeMap->CheckIfNodeIsValid(mapPosition)) {
		return nullptr;
	}

	if (currentNode->direction == direction * -1) {
		//AddTestSphere(successor->position, "node wrong direction");

		//UE_LOG(LogTemp, Warning, TEXT("node not legal"));

		return nullptr;
	}
	//check for neighbours


	//UE_LOG(LogTemp, Warning, TEXT("expanding Node x: %f, y: %f"), successor->position.X, successor->position.Y); 
	FIntVector previousDirection = currentNode->previousDirection;
	TSharedPtr<NODE> predecessor = currentNode->predecessor;
	if (!predecessor.IsValid())
		predecessor = currentNode;
	float distanceToLastCorner = currentNode->distanceToLastCorner + stepSize;
	int cost = currentNode->cost + 1;
	int corners = currentNode->corners;

	//check if next node would create a curve
	if (currentNode->direction != direction) {
		predecessor = currentNode;
		previousDirection = currentNode->direction;
		cost += cornerCost;
		distanceToLastCorner = stepSize;
		//UE_LOG(LogTemp, Warning, TEXT("corners amount origingal %i"), currentNode->corners);

		corners = currentNode->corners + 1;
		//	UE_LOG(LogTemp, Warning, TEXT("corners amount successor %i"), successor->corners);

		float corridorDistanceToLastCorner = currentNode->distanceToLastCorner;

		if (corridorDistanceToLastCorner < corridorWidth)
			return nullptr;

		float zigZagLimit = zigZagCorridorLengthLowerLimit;
		if (randomizeZigZagLength) {
			zigZagLimit = FMath::FRandRange(zigZagCorridorLengthLowerLimit, corridorLengthLowerLimit);
		}
		if (corridorDistanceToLastCorner < corridorLengthLowerLimit) {
			if (allowZigZags && direction == currentNode->previousDirection) {
				if (corridorDistanceToLastCorner < zigZagLimit) {
					return nullptr;
				}
			}
			else {
				return nullptr;
			}
		}
	}

	if (curvedCorridors) {
		if (corners > 3) {
			if (CheckIfPointTouchesCorridor(currentNode, mapPosition))
				return nullptr;
		}
	}

	NODE* successor = new NODE(cost, direction, mapPosition, predecessor);
	successor->distanceToLastCorner = distanceToLastCorner;
	successor->cost = cost;
	successor->corners = corners;
	successor->previousDirection = previousDirection;
	successor->corridorLengthInSteps = currentNode->corridorLengthInSteps + 1;
	float successorDistanceToLastCorner = successor->distanceToLastCorner;

	return successor;
}

bool APathCreator::CheckIfPointTouchesCorridor(TSharedPtr<NODE> node, FIntVector point)
{
	

	TSharedPtr<NODE> current = node;
	FIntVector previousPosition = FIntVector(0, 0, 0);
	FIntVector currentPosition;

	int layer = 0;
	while (current.Get()) {
		currentPosition = current->mapPosition;
		
		if (layer >= 4) {
			if (previousPosition != FIntVector(0, 0, 0)) {
				//check if between two corners
				FIntVector direction = current->direction;
				int first;
				int second;
				if (FMath::Abs(direction.X) > FMath::Abs(direction.Y)) {
					first = FMath::Min(previousPosition.X, currentPosition.X);
					second = FMath::Max(previousPosition.X, currentPosition.X);
					if (first <= point.X && point.X <= second) {
						//is between
						if (FMath::Abs(point.Y - currentPosition.Y) * stepSize < corridorWidth) {
							//touching corridor
							if (showAlgorithmVisualization)
								AddTestSphere(nodeMap->GetRealPosition(currentPosition), "allowedNode");
							return true;

						}
					}
				}
				else {
					first = FMath::Min(previousPosition.Y, currentPosition.Y);
					second = FMath::Max(previousPosition.Y, currentPosition.Y);
					if (first <= point.Y && point.Y <= second) {
						//is between
						if (FMath::Abs(point.X - currentPosition.X) * stepSize < corridorWidth) {
							//touching corridor
							if (showAlgorithmVisualization)
								AddTestSphere(nodeMap->GetRealPosition(currentPosition), "allowedNode");
							return true;

						}
					}
				}
			}
		}
		layer++;
		previousPosition = currentPosition;
		current = current->predecessor;
	}

	return false;
}

void APathCreator::ExpandNode(TSharedPtr<NODE> currentNode, TArray<NODE> &openlist)
{
	//UE_LOG(LogTemp, Warning, TEXT("expanding node"));

	//create successors
	for (int x = -1; x <= 1; x++)
	{
		for (int y = -1; y <= 1; y++)
		{
			FIntVector direction(x, y, 0);
			NODE* successor = ExpandNode(currentNode, direction);
			if (successor == nullptr)
				continue;
			NODE helperNodeSuccessor(*successor);
			TSharedPtr<NODE> successorShared(successor);
			helperNodeSuccessor.predecessor = successorShared;
			openlist.HeapPush(helperNodeSuccessor);

		}
	}
}

float APathCreator::GetSolutionScore(TSharedPtr<NODE> node)
{
	float score = TNumericLimits< float >::Max();
	FIntVector allDirectionsAddedUp(0,0,0);

	TSharedPtr<NODE> current = node;
	while (current.Get()) {
		allDirectionsAddedUp += current->direction;
		current = current->predecessor;
	}
	FVector allDirectionsAddedUpFloat(allDirectionsAddedUp.X, allDirectionsAddedUp.Y, allDirectionsAddedUp.Z);
	return allDirectionsAddedUpFloat.SizeSquared();
}

TSharedPtr<NODE> APathCreator::FindBetterSolution(TSharedPtr<NODE> node1, TSharedPtr<NODE> node2)
{

	if (GetSolutionScore(node1) > GetSolutionScore(node2))
		return node1;

	return node2;
}




TArray<FVector> APathCreator::ExtractWaypoints(TSharedPtr<NODE> lastNode) {
	TArray<FVector> wayPoints;
	TSharedPtr<NODE> current = lastNode;
	while (current.Get()) {
		wayPoints.Add(nodeMap->GetRealPosition(current->mapPosition));
		current = current->predecessor;
	}
	TArray<FVector> reverseWayPoints;
	for (int i = wayPoints.Num()-1; i >= 0 ; i--)
	{
		reverseWayPoints.Add(wayPoints[i]);
	}
	return reverseWayPoints;
}
TArray<FVector> APathCreator::CreatePathPointsToPossibleDoor(FVector startPoint, FVector startDirection, RoomTemplate* exampleRoom)
{

	NODEDIRECTIONMAP nodeDirectionMap(nodeMap->nodeArrayDimensions.X, nodeMap->nodeArrayDimensions.Y);
	TArray<FVector> wayPoints;

	TArray<NODE> openlist;

	FIntVector startIntDirection(FMath::RoundToInt(startDirection.X), FMath::RoundToInt(startDirection.Y), 0);

	FVector startNodePosition = nodeMap->GetClosestPossiblePoint(startPoint);

	FIntVector startNodeArrayPosition = nodeMap->GetPositionNodeArray(startNodePosition);
	TSharedPtr<NODE> startNode(new NODE(0, startIntDirection, startNodeArrayPosition));
	startNode->distanceToLastCorner = corridorLengthLowerLimit;
	NODE startHelperNode(*startNode.Get());
	startHelperNode.predecessor = startNode;
	//startNode.predecessor = nullptr;
	openlist.HeapPush(startHelperNode);
	//startContainer.addNode(startNode);
	nodeDirectionMap.AddNode(startNode, corridorLengthLowerLimit);

	//	float timeSpentAverageExpandNode = 0.0f;
	int aterations = 0;
	do
	{
		//float realtimeSeconds = UGameplayStatics::GetRealTimeSeconds(GetWorld());
		aterations++;

		NODE helperNode;
		TSharedPtr<NODE> currentNode;
		openlist.HeapPop(helperNode);
		currentNode = helperNode.predecessor;

		if (exampleRoom->CheckIfValidDoorPosition(currentNode->mapPosition, currentNode->direction * -1)) {
			if (helperNode.distanceToLastCorner >= corridorWidth / 2.0f) {

				//	UE_LOG(LogTemp, Warning, TEXT("way found"));
				//float corridorDistanceToLastCorner = GetLengthToLastCorner(currentNode);
				//UE_LOG(LogTemp, Warning, TEXT("distance for last node: %f"), corridorDistanceToLastCorner);
				//UE_LOG(LogTemp, Warning, TEXT("cost for last node: %f"), currentNode.cost);
				wayPoints = ExtractWaypoints(currentNode);
				wayPoints = RemoveRedundantPoints(wayPoints);
				//wayPoints = AdjustWayPointsToPoints(wayPoints, startPoint, endPoint);
				//	UE_LOG(LogTemp, Warning, TEXT("time for expand node: %f"), timeSpentAverageExpandNode);
				//	UE_LOG(LogTemp, Warning, TEXT("time for expand node average: %f"), timeSpentAverageExpandNode/aterations);
				//	UE_LOG(LogTemp, Warning, TEXT("tsegsegsegseg%f"), UGameplayStatics::GetRealTimeSeconds(GetWorld()));

				//UE_LOG(LogTemp, Warning, TEXT("aterations: %i"), aterations);
				/*if (showNodes) {
				TArray<NODE> keys;
				nodeMap.GetKeys(keys);
				for (int i = 0; i < keys.Num(); i++)
				{
				NODE n = keys[i];
				FVector position = GetRealPosition(n.mapPosition);
				FString name = "testNode dir x: ";
				name.AppendInt(n.direction.X);
				name += ", y: ";
				name.AppendInt(n.direction.Y);
				name += ", z: ";
				name.AppendInt(n.direction.Z);
				name += "; pos x: ";
				name.AppendInt(n.mapPosition.X);
				name += ", y: ";
				name.AppendInt(n.mapPosition.Y);
				name += ", z: ";
				name.AppendInt(n.mapPosition.Z);
				AddTestSphere(position, name, 0.5f);
				}
				}*/
				nodeDirectionMap.Destroy();
				return wayPoints;
			}
		}
		if (!nodeDirectionMap.AddNode(currentNode, corridorLengthLowerLimit)) {
			continue;
		}

		//closedlist.Add(currentNode);
		ExpandNode(currentNode, openlist);
		//		float toorealtootimeSecondstokyodrift = UGameplayStatics::GetRealTimeSeconds(GetWorld()) - realtimeSeconds;
		//	timeSpentAverageExpandNode += toorealtootimeSecondstokyodrift;
	} while (openlist.Num() > 0);
	//UE_LOG(LogTemp, Warning, TEXT("no path found"));
	nodeDirectionMap.Destroy();
	//UE_LOG(LogTemp, Warning, TEXT("aterations: %i"), aterations);

	/*FVector nextPoint = FVector(endPoint.X, startPoint.Y, 0.0f);
	FVector directionBetweenPoints = nextPoint - startPoint;
	FVector directionBetweenPointsNormalized = directionBetweenPoints;
	directionBetweenPointsNormalized.Normalize();
	UE_LOG(LogTemp, Warning, TEXT("startDirection x: %f, y: %f, direction x: %f, y: %f"), startDirection.X, startDirection.Y, directionBetweenPointsNormalized.X, directionBetweenPointsNormalized.Y);

	if (FVector::DistSquared(directionBetweenPointsNormalized, startDirection * -1.0f) < 0.1f ||
	FVector::DistSquared(directionBetweenPointsNormalized, startDirection) < 0.1f) {
	nextPoint = FVector(startPoint.X, endPoint.Y, 0.0f);
	UE_LOG(LogTemp, Warning, TEXT("different way"));
	}

	wayPoints.Add(nextPoint);
	wayPoints.Add(endPoint);
	*/



	return wayPoints;


}
TArray<FVector> APathCreator::CreatePathPointsToPossibleDoorNoLimit(FVector startPoint, FVector startDirection, FROOMSTRUCT & startRoom, FROOMSTRUCT & endRoom, FVector & endRoomPosition, FIntVector & endDoorPosition, FIntVector & endDoorDirection)
{
	FVector startNodePosition = nodeMap->GetClosestPossiblePoint(startPoint);

	FIntVector startIntDirection(FMath::RoundToInt(startDirection.X), FMath::RoundToInt(startDirection.Y), 0);
	FIntVector startNodeArrayPosition = nodeMap->GetPositionNodeArray(startNodePosition);


	//AddTestSphere(endPointGoalAdjusted, "endPointGoalAdjusted", 0.7f);
	//AddTestSphere(endPointGoal, "endPointGoalnonadjusted", 0.2f);

	return CreatePathPointsToPossibleDoorNoLimit(startNodeArrayPosition, startIntDirection, startRoom, endRoom, endRoomPosition, endDoorPosition, endDoorDirection );
}
TArray<FVector> APathCreator::CreatePathPointsToPossibleDoorNoLimit(FIntVector startPoint, FIntVector startDirection, FROOMSTRUCT & startRoom, FROOMSTRUCT & endRoom, FVector & endRoomPosition, FIntVector & endDoorPosition, FIntVector & endDoorDirection)
{
	if (!endRoom.rTemplate->_size.Equals(endRoom.size, 0.1)) {
		UE_LOG(LogTemp, Warning, TEXT("roomtemplate does not equal room size"));
		endRoom.rTemplate = new RoomTemplate(GetWorld(), nodeMap, endRoom.size, corridorWidth, doorWidth, doorHeight, _minimumDistanceBetweenDoors, wallThickness, corridorLengthLowerLimit / stepSize);
		endRoom.rTemplate->FindAllPossibleRoomAndDoorPositions(trackingSpaceSize, stepSize);
	}
	if (endRoom.rTemplate->_size != endRoom.size) {
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("room map size does not fit room size")));
		UE_LOG(LogTemp, Warning, TEXT("room map size does not fit room size"));
	}

	if (zigZagCorridorLengthLowerLimit < corridorWidth) {
		zigZagCorridorLengthLowerLimit = corridorWidth;
	}

	int solutionScore = TNumericLimits< int >::Max();;

	GetFreeNodes(stepSize);

	TArray<FVector> wayPoints;

	TQueue<TSharedPtr<NODE>> queue;

	TSharedPtr<NODE> solutionNode;
	
	int solutionsFound = 0;
	//AddTestSphere(endPointGoalAdjusted, "endPointGoalAdjusted", 0.7f);
	//AddTestSphere(endPointGoal, "endPointGoalnonadjusted", 0.2f);


	FIntVector checkPos = startPoint + startDirection * ((corridorWidth / stepSize) -1);

	if (!nodeMap->CheckIfNodeIsValid(checkPos)) {
		UE_LOG(LogTemp, Warning, TEXT("Not Valid Start Position X: %f, Y: %f, Z: %f"), startPoint.X, startPoint.Y, startPoint.Z);
		//return wayPoints;
	}

	if (endRoom.rTemplate->possibleRoomPositions.Num() == 0) {
		UE_LOG(LogTemp, Warning, TEXT("No doors to reach"), startPoint.X, startPoint.Y, startPoint.Z);
		return wayPoints;
	}

	TSharedPtr<NODE> startNode(new NODE(0, startDirection, startPoint));
	startNode->distanceToLastCorner = corridorLengthLowerLimit - corridorWidth;//+ stepSize;
	//startNode.predecessor = nullptr;
	queue.Enqueue(startNode);
	//startContainer.addNode(startNode);

	//	float timeSpentAverageExpandNode = 0.0f;
	int aterations = 0;
	TArray<FIntVector> directions = GetCardinalDirections();
	TArray<FIntVector> directionsLeft;
	float realtimeSeconds = UGameplayStatics::GetRealTimeSeconds(GetWorld());
	do
	{
		aterations++;

		TSharedPtr<NODE> currentNode;
		queue.Dequeue(currentNode);
		if (currentNode->corners > maximumCornerAmount)
			continue;
		if (currentNode->corners >= minimumCornerAmount && endRoom.rTemplate->CheckIfValidDoorPosition(currentNode->mapPosition, currentNode->direction * -1)) {
			
			float corridorLength = currentNode->corridorLengthInSteps * stepSize;

			if (currentNode->distanceToLastCorner >= (corridorWidth + corridorWidth) / 2.0f && corridorLength > hallwayMinimumLength) {
				if (solutionsFound == 0) {
					TArray<FVector> & possibleRoomPositions = endRoom.rTemplate->GetPossibleRoomPositions(currentNode->mapPosition, currentNode->direction * -1);
					if (!CheckIfCorridorCrossesRoomOverAllRoomPositions(endRoom.size.X, endRoom.size.Y, currentNode, possibleRoomPositions, endRoomPosition)) {
						solutionNode = currentNode;
						solutionScore = solutionScoreGenerator->GetScore(solutionNode);
						FString order = solutionScoreGenerator->GetOrder(currentNode);
					//	UE_LOG(LogTemp, Warning, TEXT("%s"), *order);
					//	UE_LOG(LogTemp, Warning, TEXT("solution %i score: %i"), solutionsFound, solutionScore);
						solutionsFound++;
					}

				}
				else {
					TArray<FVector> & possibleRoomPositions = endRoom.rTemplate->GetPossibleRoomPositions(currentNode->mapPosition, currentNode->direction * -1);
					FVector currentRoomPosition;
					int currentScore = solutionScoreGenerator->GetScore(currentNode);
					if (currentScore < solutionScore) {
						if (!CheckIfCorridorCrossesRoomOverAllRoomPositions(endRoom.size.X, endRoom.size.Y, currentNode, possibleRoomPositions, currentRoomPosition)) {
							solutionScore = currentScore;
							solutionNode = currentNode;
							endRoomPosition = currentRoomPosition;
						}
					}
					FString order = solutionScoreGenerator->GetOrder(currentNode);
					//	UE_LOG(LogTemp, Warning, TEXT("%s"), *order);
					//	UE_LOG(LogTemp, Warning, TEXT("solution %i score: %i"), solutionsFound, currentScore);
					solutionsFound++;
					
				}
				if (solutionsFound > solutionsToCompare) {
					break;
				}
			}
		}
		if (randomizeDirections) {
			directionsLeft.Empty();
			directionsLeft.Append(directions);
			while (directionsLeft.Num() > 0) {
				int directionId = FMath::RandRange(0, directionsLeft.Num() - 1);
				FIntVector direction = directionsLeft[directionId];
				directionsLeft.RemoveAt(directionId);
				NODE* successor = ExpandNode(currentNode, direction);
				if (successor == nullptr)
					continue;
				if (startRoom.room != nullptr) {
					if (successor->corners == 2) {
						if (CheckIfPointInsideRoom(startRoom.room->_position, startRoom.room->_width + corridorWidth, startRoom.room->_length + corridorWidth, nodeMap->GetRealPosition(successor->mapPosition))) {
							continue;
						}
					}
				}
				TSharedPtr<NODE> successorShared(successor);
				queue.Enqueue(successorShared);
			}
		}
		else {
			for (FIntVector direction : directions)
			{
				NODE* successor = ExpandNode(currentNode, direction);
				if (successor == nullptr)
					continue;
				if (startRoom.room != nullptr) {
					if (successor->corners == 2) {
						if (CheckIfPointInsideRoom(startRoom.room->_position, startRoom.room->_width + corridorWidth, startRoom.room->_length + corridorWidth, nodeMap->GetRealPosition(successor->mapPosition))) {
							continue;
						}
					}
				}
				TSharedPtr<NODE> successorShared(successor);
				queue.Enqueue(successorShared);
			}
		}
	} while (!queue.IsEmpty());
	if (!solutionNode.IsValid()) {
		UE_LOG(LogTemp, Warning, TEXT("no path found"));
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("No Path Found")));

		return wayPoints;
	}
	//exampleRoom->CheckIfValidDoorPosition(currentNode->mapPosition, currentNode->direction * -1);
	wayPoints = ExtractWaypoints(solutionNode);
	//wayPoints = RemoveRedundantPoints(wayPoints);
	//TArray<FVector> roomPositions = endRoom.rTemplate->GetPossibleRoomPositions(solutionNode->mapPosition, solutionNode->direction * -1);
	/*if (roomPositions.Num() > 0) {
		int roomPosId = FMath::FRandRange(0, roomPositions.Num());
		endRoomPosition.X = roomPositions[roomPosId].X;
		endRoomPosition.Y = roomPositions[roomPosId].Y;
		endRoomPosition.Z = roomPositions[roomPosId].Z;
		UE_LOG(LogTemp, Warning, TEXT("no rooms found"));
	}*/
	if (!endRoom.rTemplate->possibleRoomPositions.Contains(endRoomPosition)) {
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("room position not valid")));
		//UE_LOG(LogTemp, Warning, TEXT("room position not valid"));
		TArray<FVector> & possibleRoomPositions = endRoom.rTemplate->GetPossibleRoomPositions(solutionNode->mapPosition, solutionNode->direction * -1);
		FVector currentRoomPosition;
		if (!CheckIfCorridorCrossesRoomOverAllRoomPositions(endRoom.size.X, endRoom.size.Y, solutionNode, possibleRoomPositions, currentRoomPosition)) {
			if (currentRoomPosition != endRoomPosition)
				endRoomPosition = currentRoomPosition;
		}
	}

	UE_LOG(LogTemp, Warning, TEXT("aterations: %i"), aterations);
	endDoorPosition.X = solutionNode->mapPosition.X;
	endDoorPosition.Y = solutionNode->mapPosition.Y;
	endDoorPosition.Z = solutionNode->mapPosition.Z;
	endDoorDirection.X = -solutionNode->direction.X;
	endDoorDirection.Y = -solutionNode->direction.Y;
	endDoorDirection.Z = 0;

	FString order = solutionScoreGenerator->GetOrder(solutionNode);
//	UE_LOG(LogTemp, Warning, TEXT("%s"), *order);
//	UE_LOG(LogTemp, Warning, TEXT("final solution %i score: %i"), solutionsFound, solutionScore);
//	UE_LOG(LogTemp, Warning, TEXT("Time spent searching: %f"), UGameplayStatics::GetRealTimeSeconds(GetWorld()) - realtimeSeconds);

	solutionScoreGenerator->AddToScore(solutionNode);

	return wayPoints;
}

TArray<FIntVector> APathCreator::GetCardinalDirections() {
	TArray<FIntVector> directions;
	for (int x = -1; x <= 1; x++)
	{
		for (int y = -1; y <= 1; y++)
		{
			if (FMath::Abs(x) == FMath::Abs(y)) {
				continue;
			}
			directions.Add(FIntVector(x, y, 0));
		}
	}
	return directions;
};

TArray<FVector> APathCreator::CreatePathPointsToPointNoLimit(FVector startPoint, FVector startDirection, FVector endPoint, FVector endDirection)
{
	GetFreeNodes(stepSize);
	TArray<FVector> wayPoints;

	TQueue<TSharedPtr<NODE>> queue;

	FIntVector startIntDirection(FMath::RoundToInt(startDirection.X), FMath::RoundToInt(startDirection.Y), 0);
	FIntVector endIntDirection(FMath::RoundToInt(endDirection.X), FMath::RoundToInt(endDirection.Y), 0);
	
	if (startIntDirection.X > 0 && startIntDirection.Y > 0) {
		UE_LOG(LogTemp, Warning, TEXT("StartDirection must only go in cardinal directions"));
		return wayPoints;
	}

	if (endIntDirection.X > 0 && endIntDirection.Y > 0) {
		UE_LOG(LogTemp, Warning, TEXT("EndDirection must only go in cardinal directions"));
		return wayPoints;
	}
	
	

	FVector startNodePosition = nodeMap->GetClosestPossiblePoint(startPoint);
	FVector endNodePosition = nodeMap->GetClosestPossiblePoint(endPoint);


	FVector endPointGoal = nodeMap->GetClosestPossiblePoint(endPointGoal);
	//AddTestSphere(endPointGoalAdjusted, "endPointGoalAdjusted", 0.7f);
	//AddTestSphere(endPointGoal, "endPointGoalnonadjusted", 0.2f);


	AddTestSphere(endNodePosition, "endNodePosition", 1);

	FIntVector startNodeArrayPosition = nodeMap->GetPositionNodeArray(startNodePosition);
	TSharedPtr<NODE> startNode(new NODE(0, startIntDirection, startNodeArrayPosition));
	startNode->distanceToLastCorner = corridorLengthLowerLimit - corridorWidth;

	FIntVector checkPos = startNodeArrayPosition + startIntDirection * (corridorWidth / stepSize);

	if (!nodeMap->CheckIfNodeIsValid(checkPos)) {
		
		UE_LOG(LogTemp, Warning, TEXT("Not Valid Start Position X: %f, Y: %f, Z: %f"), startPoint.X, startPoint.Y, startPoint.Z);
		return wayPoints;
	}

	//startNode.predecessor = nullptr;
	queue.Enqueue(startNode);
	//startContainer.addNode(startNode);
	FIntVector endNodeArrayPosition = nodeMap->GetPositionNodeArray(endNodePosition);

	if (!nodeMap->CheckIfNodeIsValid(endNodeArrayPosition + endIntDirection * (corridorWidth / stepSize))) {
		UE_LOG(LogTemp, Warning, TEXT("Not Valid End Position"));
		return wayPoints;
	}

	NODE endNode(0, endIntDirection * -1, endNodeArrayPosition);
	//	float timeSpentAverageExpandNode = 0.0f;
	int aterations = 0;
	do
	{
		//float realtimeSeconds = UGameplayStatics::GetRealTimeSeconds(GetWorld());
		aterations++;

		TSharedPtr<NODE> currentNode;
		queue.Dequeue(currentNode);

		if (currentNode->corners >= minimumCornerAmount && *currentNode.Get() == endNode) {
			if (currentNode->distanceToLastCorner >= corridorWidth / 2.0f) {
				wayPoints = ExtractWaypoints(currentNode);
				wayPoints = RemoveRedundantPoints(wayPoints);

				UE_LOG(LogTemp, Warning, TEXT("aterations: %i"), aterations);

				return wayPoints;
			}
		}

		for (int x = -1; x <= 1; x++)
		{
			for (int y = -1; y <= 1; y++)
			{
				FIntVector direction(x, y, 0);
				NODE* successor = ExpandNode(currentNode, direction);
				if (successor == nullptr)
					continue;
				TSharedPtr<NODE> successorShared(successor);
				queue.Enqueue(successorShared);
			}
		}
	} while (!queue.IsEmpty());
	UE_LOG(LogTemp, Warning, TEXT("no path found"));

	return wayPoints;


}


TArray<FVector> APathCreator::CreatePathPointsToPoint(FVector startPoint, FVector startDirection, FVector endPoint, FVector endDirection)
{
	GetFreeNodes(stepSize);
	NODEDIRECTIONMAP nodeDirectionMap(nodeMap->nodeArrayDimensions.X, nodeMap->nodeArrayDimensions.Y);
	TArray<FVector> wayPoints;

	TArray<NODE> openlist;

	FIntVector startIntDirection(FMath::RoundToInt(startDirection.X), FMath::RoundToInt(startDirection.Y), 0);
	FIntVector endIntDirection(FMath::RoundToInt(endDirection.X), FMath::RoundToInt(endDirection.Y), 0);

	FVector startNodePosition = nodeMap->GetClosestPossiblePoint(startPoint);
	FVector endNodePosition = nodeMap->GetClosestPossiblePoint(endPoint);


	FVector endPointGoal = endNodePosition + endDirection;
	FVector endPointGoalAdjusted = nodeMap->GetClosestPossiblePoint(endPointGoal);
	//AddTestSphere(endPointGoalAdjusted, "endPointGoalAdjusted", 0.7f);
	//AddTestSphere(endPointGoal, "endPointGoalnonadjusted", 0.2f);
	
	AddTestSphere(endNodePosition, "endNodePosition", 1);

	FIntVector startNodeArrayPosition = nodeMap->GetPositionNodeArray(startNodePosition);
	TSharedPtr<NODE> startNode(new NODE(0, startIntDirection, startNodeArrayPosition));
	startNode->distanceToLastCorner = corridorLengthLowerLimit - corridorWidth;
	NODE startHelperNode(*startNode.Get());
	startHelperNode.predecessor = startNode;
	//startNode.predecessor = nullptr;
	openlist.HeapPush(startHelperNode);
	//startContainer.addNode(startNode);
	nodeDirectionMap.AddNode(startNode, corridorLengthLowerLimit);
	FIntVector endNodeArrayPosition = nodeMap->GetPositionNodeArray(endNodePosition);

	NODE endNode(0, endIntDirection * -1, endNodeArrayPosition);
//	float timeSpentAverageExpandNode = 0.0f;
	int aterations = 0;
	do
	{
		//float realtimeSeconds = UGameplayStatics::GetRealTimeSeconds(GetWorld());
		aterations++;

		NODE helperNode;
		TSharedPtr<NODE> currentNode;
		openlist.HeapPop(helperNode);
		currentNode = helperNode.predecessor;

		if (helperNode == endNode) {
			if (helperNode.distanceToLastCorner >= corridorWidth/2.0f) {
			//	UE_LOG(LogTemp, Warning, TEXT("way found"));
				//float corridorDistanceToLastCorner = GetLengthToLastCorner(currentNode);
				//UE_LOG(LogTemp, Warning, TEXT("distance for last node: %f"), corridorDistanceToLastCorner);
				//UE_LOG(LogTemp, Warning, TEXT("cost for last node: %f"), currentNode.cost);
				wayPoints = ExtractWaypoints(currentNode);
				wayPoints = RemoveRedundantPoints(wayPoints);
				//wayPoints = AdjustWayPointsToPoints(wayPoints, startPoint, endPoint);
			//	UE_LOG(LogTemp, Warning, TEXT("time for expand node: %f"), timeSpentAverageExpandNode);
			//	UE_LOG(LogTemp, Warning, TEXT("time for expand node average: %f"), timeSpentAverageExpandNode/aterations);
			//	UE_LOG(LogTemp, Warning, TEXT("tsegsegsegseg%f"), UGameplayStatics::GetRealTimeSeconds(GetWorld()));

				UE_LOG(LogTemp, Warning, TEXT("aterations: %i"), aterations);
				/*if (showNodes) {
					TArray<NODE> keys;
					nodeMap.GetKeys(keys);
					for (int i = 0; i < keys.Num(); i++)
					{
						NODE n = keys[i];
						FVector position = GetRealPosition(n.mapPosition);
						FString name = "testNode dir x: ";
						name.AppendInt(n.direction.X);
						name += ", y: ";
						name.AppendInt(n.direction.Y);
						name += ", z: ";
						name.AppendInt(n.direction.Z);
						name += "; pos x: ";
						name.AppendInt(n.mapPosition.X);
						name += ", y: ";
						name.AppendInt(n.mapPosition.Y);
						name += ", z: ";
						name.AppendInt(n.mapPosition.Z);
						AddTestSphere(position, name, 0.5f);
					}
				}*/
				nodeDirectionMap.Destroy();
				return wayPoints;
			}
		}
		if (!nodeDirectionMap.AddNode(currentNode, corridorLengthLowerLimit)) {
			continue;
		}

		//closedlist.Add(currentNode);
		ExpandNode(currentNode, openlist);
//		float toorealtootimeSecondstokyodrift = UGameplayStatics::GetRealTimeSeconds(GetWorld()) - realtimeSeconds;
	//	timeSpentAverageExpandNode += toorealtootimeSecondstokyodrift;
	} while (openlist.Num() > 0);
	UE_LOG(LogTemp, Warning, TEXT("no path found"));
	nodeDirectionMap.Destroy();

	/*FVector nextPoint = FVector(endPoint.X, startPoint.Y, 0.0f);
	FVector directionBetweenPoints = nextPoint - startPoint;
	FVector directionBetweenPointsNormalized = directionBetweenPoints;
	directionBetweenPointsNormalized.Normalize();
	UE_LOG(LogTemp, Warning, TEXT("startDirection x: %f, y: %f, direction x: %f, y: %f"), startDirection.X, startDirection.Y, directionBetweenPointsNormalized.X, directionBetweenPointsNormalized.Y);

	if (FVector::DistSquared(directionBetweenPointsNormalized, startDirection * -1.0f) < 0.1f ||
		FVector::DistSquared(directionBetweenPointsNormalized, startDirection) < 0.1f) {
		nextPoint = FVector(startPoint.X, endPoint.Y, 0.0f);
		UE_LOG(LogTemp, Warning, TEXT("different way"));
	}

	wayPoints.Add(nextPoint);
	wayPoints.Add(endPoint);
	*/


	
	return wayPoints;


}

TArray<FVector> APathCreator::RemoveRedundantPoints(TArray<FVector> wayPoints) {
	/*TArray<FVector> wayPoints;
	NODE* currentNode = &lastNode;
	int loopcheck = 50;
	while (currentNode != nullptr) {
		wayPoints.Add(currentNode->position);
		currentNode = currentNode->predecessor;
		UE_LOG(LogTemp, Warning, TEXT("predecessor x: %f, y; %f"), currentNode->position.X, currentNode->position.Y);

		loopcheck--;
		if (loopcheck < 0) {
			UE_LOG(LogTemp, Warning, TEXT("Too many loops extract waypoints"));
			break;
		}
	}
	return wayPoints;*/
	if (wayPoints.Num() <= 1)
		return wayPoints;

	TArray<FVector> corridorPoints;
	corridorPoints.Add(wayPoints[0]);
	FVector previousDirection = GetSimpleDirection(wayPoints[1], wayPoints[0]);
	//lastNode.wayPoints[1] - lastNode.wayPoints[0];//	//previousDirection.Normalize();
	FVector previousNode = wayPoints[0];
	
	float stepsize = 1.0f / (wayPoints.Num() - 1);
	//Show points for visualization
	
	for (int i = 0; i < wayPoints.Num(); i++) {
		AddTestSphere(wayPoints[i], "node Path", stepsize * i);
	}

	for (int i = 1; i < wayPoints.Num() - 1; i++)
	{
		if (previousNode == wayPoints[i])
			continue;
		FVector direction = GetSimpleDirection(wayPoints[i + 1], wayPoints[i]);
		//lastNode.wayPoints[i + 1] - lastNode.wayPoints[i];//		direction.Normalize();
		if (direction != previousDirection) {
			if (wayPoints[i] == previousNode) {
				corridorPoints[corridorPoints.Num() - 1] = wayPoints[i];
			}
			else {
				corridorPoints.Add(wayPoints[i]);
			}
			previousDirection = direction;
			previousNode = wayPoints[i];
		}

	}
	corridorPoints.Add(wayPoints[wayPoints.Num() - 1]);

	return corridorPoints;
}

TArray<FVector> APathCreator::CreatePathPoints(FVector startPoint, FVector startDirection)
{
	TArray<FVector> wayPoints;
	TArray<FVector> endPoints;

	wayPoints = CreateRandomPathPoints(startPoint, startDirection, minimumCornerAmount);
	FVector lastPoint = wayPoints[wayPoints.Num() - 1];
	FVector lastDirection = wayPoints[wayPoints.Num() - 1] - wayPoints[wayPoints.Num() - 2];
	lastDirection.Normalize();
	endPoints = CreateEndPathPoints(lastPoint, lastDirection);

	for (int i = 0; i < endPoints.Num(); i++)
	{
		wayPoints.Add(endPoints[i]);
	}
	return wayPoints;
}



float APathCreator::GetDistanceToTrackingAreaBorder(FVector direction, FVector position)
{
	float distanceToBorder;
	int directionSign;
	if (FMath::Abs(direction.X) > 0.f) {
		directionSign = FMath::Sign(direction.X);
		distanceToBorder = trackingSpaceSize.X / 2.0f - directionSign * position.X;
	}
	else {
		directionSign = FMath::Sign(direction.Y);
		distanceToBorder = trackingSpaceSize.Y / 2.0f - directionSign * position.Y;
	}
	return distanceToBorder;
}

FVector APathCreator::GetDirectionWithMoreSpace(FVector direction, FVector position)
{
	float distanceToAreaEndInDirection = GetCorridorDistanceToObstacle(direction, position);
	float distanceToAreaEndInOtherDirection = GetCorridorDistanceToObstacle(direction * -1.0f, position);

	if (distanceToAreaEndInOtherDirection > distanceToAreaEndInDirection) {
		return direction *= -1.0f;
	}
	return direction;
}

bool APathCreator::CheckIfRoomFitsAfterWayPoint(FVector wayVector, FVector position)
{
	bool fits = false;
	float minimumRoomSpaceX = rooms[0].size.X + 2 * corridorWidth;
	float minimumRoomSpaceY = rooms[0].size.Y + 2 * corridorWidth;

	int direction;
	if (FMath::Abs(wayVector.X) > 0.f) {
		direction = FMath::Sign(wayVector.X);
		float distanceToAreaEndInDirection = trackingSpaceSize.X / 2.0f - direction * position.X;
		//check if Room fits in lastWayVector direction
		if (distanceToAreaEndInDirection >= minimumRoomSpaceX - corridorWidth) {
			float distanceToAreaEnd = trackingSpaceSize.Y / 2.0f - FMath::Abs(position.Y);
			//check if room fits in other directions
			if (distanceToAreaEnd >= minimumRoomSpaceY / 2.0f) {
				fits = true;
			}

		}
	}
	else {
		direction = FMath::Sign(wayVector.Y);
		float distanceToAreaEndInDirection = trackingSpaceSize.Y / 2.0f - direction * position.Y;
		//check if Room fits in lastWayVector direction
		if (distanceToAreaEndInDirection >= minimumRoomSpaceY - corridorWidth) {
			float distanceToAreaEnd = trackingSpaceSize.X / 2.0f - FMath::Abs(position.X);
			//check if room fits in other directions
			if (distanceToAreaEnd >= minimumRoomSpaceX / 2.0f) {
				fits = true;
			}
		}
	}

	FVector* points = new FVector[4];
	FVector endPoint = position + wayVector * (minimumRoomSpaceY - corridorWidth);
	FVector normal = FVector(wayVector.Y, wayVector.X, 0.0f);

	points[0] = position + normal * minimumRoomSpaceX / 2.0f;
	points[1] = position - normal * minimumRoomSpaceX / 2.0f;
	points[2] = endPoint + normal * minimumRoomSpaceX / 2.0f;
	points[3] = endPoint - normal * minimumRoomSpaceX / 2.0f;

	FBoxSphereBounds boundingBox(points, 4);
	AddTestCube(boundingBox, 0.8, 0.1, "bbox room");

	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AObstacle::StaticClass(), FoundActors);

	for (int i = 0; i < FoundActors.Num(); i++)
	{
		AObstacle* o = (AObstacle *)FoundActors[i];
		if (FBoxSphereBounds::BoxesIntersect(o->GetBoundingBox(), boundingBox)) {
			fits = false;
			break;
			//UE_LOG(LogTemp, Warning, TEXT("Intersections are happening"));
		}
		else {
			//UE_LOG(LogTemp, Warning, TEXT("no intersections here"));
		}
	}
	if (fits) {
		UE_LOG(LogTemp, Warning, TEXT("Room fits"));
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Room doesn't fit"));
	}
	return fits;
}

bool APathCreator::CheckIfFree(FVector size, FVector position, bool addVisualization)
{
	FVector absPosition = FVector(FMath::Abs(position.X), FMath::Abs(position.Y), 0);
	if (absPosition.X + size.X / 2.0f > trackingSpaceSize.X / 2.0f ||
		absPosition.Y + size.Y / 2.0f > trackingSpaceSize.Y / 2.0f)
		return false;

	FVector* points = new FVector[2];
	points[0] = position + size / 2.0f;
	points[1] = position - size / 2.0f;
	FBoxSphereBounds boundingBox(points, 2);
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AObstacle::StaticClass(), FoundActors);

	UWorld * world = GetWorld();

	for (int i = 0; i < FoundActors.Num(); i++)
	{
		AObstacle* o = (AObstacle *)FoundActors[i];
		if (FBoxSphereBounds::BoxesIntersect(o->GetBoundingBox(), boundingBox)) {
			//if(addVisualization)
			//	AddTestCube(boundingBox, FLinearColor::Black, 0.1f, "not free", false);
			if(showAlgorithmVisualization)
				DrawDebugSolidBox(GetWorld(), position, FVector(size.X / 2.0f, size.Y / 2.0f, 4.0f) - FVector::OneVector * 3, FColor::Black, false, 10, 0);
			
			return false;
		}
	}
	//if (addVisualization)
		//AddTestCube(boundingBox, FLinearColor::Blue, 0.15f, "free", false);
	if (showAlgorithmVisualization)
		DrawDebugSolidBox(GetWorld(), position, FVector(size.X / 2.0f, size.Y / 2.0f, 4.0f) - FVector::OneVector * 3, FColor::Blue, false, 10, 0);

	return true;
}

bool APathCreator::CheckIfCorridorPointFree(FIntVector point)
{
	if (point.X > nodeMap->nodeArrayDimensions.X - 1 || point.X < 0 ||
		point.Y > nodeMap->nodeArrayDimensions.Y - 1 || point.Y < 0)
		return false;
	return true;
}

bool APathCreator::CheckIfPointInsideRoom(FVector roomPosition, float roomWidth, float roomLength, FVector point)
{
	FVector pointRoomVector = roomPosition - point;
	pointRoomVector.X = FMath::Abs(pointRoomVector.X);
	pointRoomVector.Y = FMath::Abs(pointRoomVector.Y);
	//point can be inside one third of the room -> 2/3 * 1/2 = 0.333
	if (pointRoomVector.X > roomWidth / 2.0f)
		return false;
	if (pointRoomVector.Y > roomLength / 2.0f)
		return false;
	return true;
}

bool APathCreator::CheckIfCorridorCrossesRoom(FVector roomPosition, float roomWidth, float roomLength, TSharedPtr<NODE> endNode)
{
	TSharedPtr<NODE> corner1 = endNode->predecessor;
	TSharedPtr<NODE> corner2 = corner1->predecessor;
	TSharedPtr<NODE> corner3 = corner2->predecessor;
	FIntVector hallwayDirection = corner2->direction;
	FIntVector directionToRoom = endNode->direction;
	//check if corridor moves away from room
	if (directionToRoom == hallwayDirection)
		return false;
	//check if corridor stops before touching room
	if (endNode->distanceToLastCorner > (corner2->distanceToLastCorner + corridorWidth / 2.0f)) {
		return false;
	}

	FVector realPositionCorner3 = nodeMap->GetRealPosition(corner3->mapPosition);

	FVector pointRoomVector = roomPosition - realPositionCorner3;
	pointRoomVector.X = FMath::Abs(pointRoomVector.X);
	pointRoomVector.Y = FMath::Abs(pointRoomVector.Y);

	if (FMath::Abs(hallwayDirection.X) > 0) {
		if (pointRoomVector.Y < roomLength / 2.0f + corridorWidth / 2.0f)
			return true;
	}
	else {
		if (pointRoomVector.X < roomWidth / 2.0f + corridorWidth / 2.0f)
			return true;
	}
	return false;
}

bool APathCreator::CheckIfCorridorCrossesRoomOverAllRoomPositions(float roomWidth, float roomLength, TSharedPtr<NODE> endNode, TArray<FVector>& roomPositions, FVector & endRoomPosition)
{
	for (int i = 0; i < roomPositions.Num(); i++)
	{
		if (!CheckIfCorridorCrossesRoom(roomPositions[i], roomWidth, roomLength, endNode)) {
			endRoomPosition.X = roomPositions[i].X;
			endRoomPosition.Y = roomPositions[i].Y;
			endRoomPosition.Z = roomPositions[i].Z;
			return false;
		}
	}
	return true;
}

TArray<FVector> APathCreator::CalcWay(ADoor* doorStart, ARoom* roomA, ARoom* roomB, ADoor* destDoor)
{
	int mainPnum = 1;//(int)Random.Range(1,2); //tecnically can be also 0 => 1 to 3 turns, but causes to much troubles to fix fast. =1
	if (mainPnum == 0)
	{
		if (FVector::Distance(roomA->_position, roomB->_position) < 5 * corridorWidth / 2.0f)
			mainPnum = 1;
	}

	//WHAT IF THERE WILL BE 5 CORNERS?
	FVector* arr = new FVector[mainPnum + 3];  //originally +2, +1 added in case additional point needed (mostly mainPnum = 0 case). 
	
	TArray<FVector> listV;  // all points for corridor
	arr[0] = doorStart->_position + doorStart->_forward * corridorWidth / 2.0f; // source point assigned 

	int runsTotal = 0, runsAdd = 0, runsMinus = 0, runsA = 0, runsB = 0;

	// I point generation
	// generate arr[1] - 2nd corner point?
	for (int i = 1; i <= mainPnum; i++)	//arr[mainPnum +1] = b
	{
		runsTotal++;
		// WRITES RANDOMLY SELECTED POINT TO THE arr[i]
		GenerateIpoint(i, arr, trackingSpaceSize, 2 * minimum*corridorWidth / 2.0f); // ARGUMENTS CHANGED



		//arr[i] = GenerateIpoint2(area, 2*min*halfWidth, doorAFace, roomA, roomB, 4*corridorWidth / 2.0f);
		// CHECKS
		if (runsTotal >60 || runsMinus > 60)
		{
			UE_LOG(LogTemp, Warning, TEXT("Too many runs for corners! total: %d minus: %d"), runsTotal, runsMinus);
			break;
		}
		if (runsTotal >50 || runsMinus > 50)
		{
			UE_LOG(LogTemp, Warning, TEXT("OVER 50! total: %d minus: %d"), runsTotal, runsMinus);

			// OVERWRITE RANDOM POINT SELECTION WITH THE FOLLOWING: AT THE BORDER OF THE TRACKING SPACE IN THE MIDDLE OF THE CORRIDOR
			//FMath::FRandRange(0, 5);
			switch (FMath::RandRange(0, 5))
				//switch(FMath::RandRange(0, 5))
			{
			case 0: arr[i] = FVector(trackingSpaceSize.X / 2.0f - minimum*corridorWidth / 2.0f, trackingSpaceSize.Y / 2.0f - minimum*corridorWidth / 2.0f, middle);
				break;
			case 1: arr[i] = FVector(-trackingSpaceSize.X / 2.0f + minimum*corridorWidth / 2.0f, trackingSpaceSize.Y / 2.0f - minimum*corridorWidth / 2.0f, middle);
				break;
			case 2: arr[i] = FVector(trackingSpaceSize.X / 2.0f - minimum*corridorWidth / 2.0f, -trackingSpaceSize.Y / 2.0f + minimum*corridorWidth / 2.0f, middle);
				break;
			default: arr[i] = FVector(-trackingSpaceSize.X / 2.0f + minimum*corridorWidth / 2.0f, -trackingSpaceSize.Y / 2.0f + minimum*corridorWidth / 2.0f, middle);
				break;
			}
		}
		if (!CheckPntDist(arr[i], arr[i - 1]))
		{
			i--;
			runsMinus++;
			continue;
		}
		
		if (i == mainPnum ? PointInsideRoom(roomB, arr[i]) : false)
		{
			i--;
			runsB++;
			runsMinus++;
			continue;
		}
		//if ((i == 1 ? RoomLimitsBroken(roomA, doorAFace, arr[i], 2f) : false))
		if ((i == 1 ? PointInTheRoomOrBehind(roomA, doorStart->_position, arr[i], 2.0f) : false))
		{
			runsA++;
			i--;
			runsMinus++;
			continue;

		}
		if (i == mainPnum)
		{
			//roomB->UpdateDoorsFace(destDoorID);
			DoorChoice(arr, mainPnum, roomB, destDoor); // DEST DOOR POSITION IS WRITTEN TO arr[2]
			//arr[mainPnum + 1] = destDoor->_pos + destDoor->_forward * corridorWidth / 2.0f;;

		}
	}
	listV.Add(doorStart->_position + doorStart->_forward * corridorWidth / 2.0f); // add 1st corner point to the end list
	int zCount = 0, xCount = 0;
	for (int i = 1; i < mainPnum + 2; i++)
	{
		int tt = listV.Num() - 1;
		//runsTotal++;
		runsAdd++;

		// if it doesn't fit within reach of the corridor => add point!	(WHAT DOESNT FIT ??)			 
		
		if (!FMath::IsNearlyEqual(arr[i].X, (listV[tt]).X) && !FMath::IsNearlyEqual(arr[i].Y, (listV[tt]).Y))
		{
			if (FMath::RandRange(0.0f, 1.0f) > 0.5f)
			{
				zCount++;

				listV.Add(FVector(listV[tt].X, arr[i].Y, listV[tt].Z));

				if ((i == 1 ? PointInTheRoomOrBehind(roomA, doorStart->_position, listV[tt + 1], 2.0f) : false) || DoesWayGoBack(listV) || (i == mainPnum + 1 ? PointInsideRoom(roomB, listV[tt + 1]) : false))
				{

					FVector old = listV[tt + 1];

					listV.RemoveAt(tt + 1);
					listV.Add(FVector(arr[i].X, listV[tt].Y, listV[tt].Z));

					if (!CheckAddPnt(roomA, doorStart->_position, roomB, listV, tt + 1, i, mainPnum, old))
					{
						if (i == mainPnum + 1)
						{
							listV.RemoveAt(tt + 1);
							if (TryNextDoor(arr, mainPnum, roomB, arr[mainPnum + 1], destDoor))
							{
								//		Debug.Log("Trying next door!");
								i--;
								continue;
							}
							else
								UE_LOG(LogTemp, Warning, TEXT("ERROR!"));
						}
					}
				}
			}
			else
			{
				xCount++;

				listV.Add(FVector(arr[i].X, listV[tt].Y, listV[tt].Z));

				if ((i == 1 && PointInTheRoomOrBehind(roomA, doorStart->_position, listV[tt + 1], 2.0f)) || DoesWayGoBack(listV) || (i == mainPnum + 1 && PointInsideRoom(roomB, listV[tt + 1])))
				{

					FVector old = listV[tt + 1];

					listV.RemoveAt(tt + 1);
					listV.Add(FVector(listV[tt].X, arr[i].Y, listV[tt].Z));

					if (!CheckAddPnt(roomA, doorStart->_position, roomB, listV, tt + 1, i, mainPnum, old))
					{

						if (i == mainPnum + 1)
						{
							listV.RemoveAt(tt + 1);
							if (TryNextDoor(arr, mainPnum, roomB, arr[mainPnum + 1], destDoor))
							{
								UE_LOG(LogTemp, Warning, TEXT("Trying next door!"));
								i--;
								continue;
							}
							else
								UE_LOG(LogTemp, Warning, TEXT("ERROR!"));
						}
					}
				}
			}
			listV.Add(arr[i]);
		}
		else
		{
			if (FMath::IsNearlyEqual(arr[i].X, (listV[tt]).X) ^ FMath::IsNearlyEqual(arr[i].Y, (listV[tt]).Y))
				listV.Add(arr[i]);
		}

		if (listV.Num() > 98)
		{
			break;
		}
	}

	float length = 0;
	for (int i = 0; i < listV.Num(); i++)
	{
		if (i != 0)
			length += FVector::Dist(listV[i - 1], listV[i]);
	}

	//NumWays++;
	/*    Destroy(GameObject.Find("CorridorMarkers"));
	GameObject emptyy = new GameObject();
	emptyy.name = "CorridorMarkers";
	List<GameObject> dots = new List<GameObject>();
	*/
	for (int i = 1; i < listV.Num() - 3; i++) //eliminating 3 in line situation.
	{
		if (FMath::IsNearlyEqual(listV[i].X, listV[i + 2].X) && FMath::IsNearlyEqual(listV[i].X, listV[i + 1].X))
		{
			//For Debug!
			/*              GameObject tmp = GameObject.CreatePrimitive(PrimitiveType.Cube);
			tmp.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
			tmp.transform.position = new Vector3(listV[i + 1].x, 3f, listV[i + 1].Y);//listV[i + 1];
			tmp.transform.parent = emptyy.transform;
			tmp.renderer.material.color = Color.red;
			tmp.transform.collider.enabled = false;
			tmp.name = "rm " + (i + 1).ToString();
			dots.Add(tmp);
			*/            //END Debug!

			listV.RemoveAt(i + 1);
		}
		else if (FMath::IsNearlyEqual(listV[i].Y, listV[i + 2].Y) && FMath::IsNearlyEqual(listV[i].Y, listV[i + 1].Y))
		{
			//For Debug!
			/*              GameObject tmp = GameObject.CreatePrimitive(PrimitiveType.Cube);
			tmp.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
			tmp.transform.position = new Vector3(listV[i+1].x, 3f, listV[i+1].Y);//listV[i + 1];
			tmp.transform.parent = emptyy.transform;
			tmp.renderer.material.color = Color.red;
			tmp.transform.collider.enabled = false;
			tmp.name = "rm " + (i + 1).ToString();
			dots.Add(tmp);
			*/             //END Debug!


			listV.RemoveAt(i + 1);
		}
	}
	delete[](arr);

	return listV;
}

void NodeTest(TSharedPtr<NODE> lastNode) {
	TSharedPtr<NODE> current = lastNode;
	while (current.IsValid()) {
		UE_LOG(LogTemp, Warning, TEXT("curret cost: %f"), current->cost);
		current = current->predecessor;
	}
}

#if WITH_EDITOR  
void APathCreator::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	//UE_LOG(LogTemp, Warning, TEXT("butnc"));
	//mesh = GetOwner()->FindComponentByClass<UProceduralMeshComponent>();

	//Get the name of the property that was changed  
	FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(APathCreator, rooms))) {
		AdjustRoomsList();
	}

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(APathCreator, trackingSpaceSize))) {
		Initialize();
	}
	if ((PropertyName == GET_MEMBER_NAME_CHECKED(APathCreator, trackingAreaMaterial))) {
		Initialize();
	}
	if ((PropertyName == GET_MEMBER_NAME_CHECKED(APathCreator, randomRoom))) {
		//Multitasking* thread = Multitasking::JoyInit(&i);
		CreateRandomRoomWithPath(spawnCorridor);
	}	
	if ((PropertyName == GET_MEMBER_NAME_CHECKED(APathCreator, singleRoomTest))) {
		CreateRandomRoom();
	}
	if ((PropertyName == GET_MEMBER_NAME_CHECKED(APathCreator, addDoor))) {
		AddDoor();
	}
	
	if ((PropertyName == GET_MEMBER_NAME_CHECKED(APathCreator, runMultipleTests))) {
		//Multitasking* thread = Multitasking::JoyInit(&i);

		RunTests();
	}
	if ((PropertyName == GET_MEMBER_NAME_CHECKED(APathCreator, rebuildMap))) {
		RebuildMap();
	}

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(APathCreator, removeEverything))) {
		if (_corridor != nullptr)
			_corridor->Destroy();
		for (int i = 0; i < actorsToManuallyDestroy.Num(); i++) {
			if (actorsToManuallyDestroy[i] != nullptr)
				actorsToManuallyDestroy[i]->Destroy();
		}
		actorsToManuallyDestroy.Empty();
		for (int i = 0; i < actorsToDestroy.Num(); i++) {
			if (actorsToDestroy[i] != nullptr)
				actorsToDestroy[i]->Destroy();
		}
		actorsToDestroy.Empty();
		if (_currentRoom != nullptr) {
			_currentRoom->Destroy();
		}
		if (_nextRoom != nullptr) {
			_nextRoom->Destroy();
		}
		if (_currentRoomVM != nullptr) {
			_currentRoomVM->Destroy();
		}
		if (_nextRoomVM != nullptr) {
			_nextRoomVM->Destroy();
		}
	}

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(APathCreator, fixedCorridorTest))) {

		FixedCorridorTest();

	}
	if ((PropertyName == GET_MEMBER_NAME_CHECKED(APathCreator, randomCorridorTest))) {
		if (rooms.Num() == 0)
		{
			UE_LOG(LogTemp, Warning, TEXT("Add one room to rooms list"));
		}
		if (rooms.Num() > 0 && rooms[0].rTemplate != nullptr && rooms[0].rTemplate->possibleRoomPositions.Num() > 0) {


			if (_corridor != nullptr)
				_corridor->Destroy();
			for (int i = 0; i < actorsToDestroy.Num(); i++) {
				actorsToDestroy[i]->Destroy();
			}
			actorsToDestroy.Empty();

			FVector Location(0.0f, 0.0f, 0.0f);
			FRotator Rotation(0.0f, 0.0f, 0.0f);
			FActorSpawnParameters SpawnInfo;
			UWorld* world = GetWorld();
			if (testCorridorStart == nullptr) {

				testCorridorStart = GetWorld()->SpawnActor<ADebugSphere>(Location, Rotation, SpawnInfo);
				testCorridorStart->SetActorScale3D(FVector(1.2f, 1.2f, 1.2f));
				testCorridorStart->SetActorLabel(*FString("Test Corridor Start "));
				//actorsToManuallyDestroy.Add(testCorridorStart);
				//testCorridorStart->ChangeColorIntensity(0);
			}

			//if(freeNodes.Num() == 0)
			//	freeNodes = GetFreeNodes(stepSize);
			FVector startDirection = testCorridorStart->GetActorForwardVector();
			FIntVector startDirectionInt = FIntVector(startDirection.X, startDirection.Y, 0);
			FVector startPoint = nodeMap->GetClosestPossiblePoint(testCorridorStart->GetActorLocation());
			FIntVector startPointInt = nodeMap->GetPositionNodeArray(startPoint);
			FVector roomPosition = FVector::ZeroVector;
			TSharedPtr<NODE> solutionNode;
			FIntVector tempInt(0, 0, 0);
			FVector temp(0, 0, 0);

			TArray<FVector> corridorPoints;
			corridorPoints = CreatePathPointsToPossibleDoorNoLimit(testCorridorStart->GetActorLocation(), testCorridorStart->GetActorForwardVector(), rooms[0], rooms[0], temp, tempInt, tempInt);
			UE_LOG(LogTemp, Warning, TEXT("Test Corridor Start Direction x: %f y: %f z: %f"), startDirection.X, startDirection.Y, startDirection.Z);
			//FVector endStartPoint = corridorPoints[corridorPoints.Num() - 1];
			//FVector endDirection = endStartPoint - corridorPoints[corridorPoints.Num() - 2];
			//endDirection.Normalize();

			//TArray<FVector> endPoints = CreatePathPointsToPossibleDoor(endStartPoint, endDirection, setUpRoom);
			//corridorPoints.Append(endPoints);
			corridorPoints = RemoveRedundantPoints(corridorPoints);
			if (corridorPoints.Num() > 0)
				_corridor = CreatePathMesh(corridorPoints, nullptr, nullptr, nullptr, nullptr, false);
			else
				UE_LOG(LogTemp, Warning, TEXT("No way found"));
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Initialize Map first"));
		}
	}

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(APathCreator, nodeTest))) {

		NODECONTAINER* nodeContainterMap100 = new NODECONTAINER[nodeMap->nodeArrayDimensions.X * nodeMap->nodeArrayDimensions.Y];

	//	NODECONTAINER* nodeMap010 = new NODECONTAINER[nodeArrayWidth * nodeArrayHeight];
	//	NODECONTAINER* nodeMapN00 = new NODECONTAINER[nodeArrayWidth * nodeArrayHeight];
	//	NODECONTAINER* nodeMap0N0 = new NODECONTAINER[nodeArrayWidth * nodeArrayHeight];



		NODECONTAINER nodecontainer = nodeContainterMap100[0];
		if (nodecontainer.empty) {
			UE_LOG(LogTemp, Warning, TEXT("nodecontainer empty: true"));
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("nodecontainer empty: false"));
		}
		UE_LOG(LogTemp, Warning, TEXT("1nodecontainer empty: %B"), nodecontainer.empty);
		UE_LOG(LogTemp, Warning, TEXT("2nodecontainer empty: %d"), nodecontainer.empty);

		/*
		for (int i = 0; i < 6; i++)
		{
			TSharedPtr<NODE> node(new NODE(i, FVector::ForwardVector, FVector::ZeroVector + FVector::ForwardVector * i));
			if (i > 0)
				node->predecessor = nodes[i-1];

			nodes.Add(node);
		}
		

		TSharedPtr<NODE> node1(new NODE(1, FVector::ForwardVector, FVector::ZeroVector + FVector::ForwardVector));

		TSharedPtr<NODE> node2(new NODE(2, FVector::RightVector, FVector::ForwardVector));
		TSharedPtr<NODE> node3(new NODE(3, FVector::ForwardVector * -1, FVector::RightVector));
		TSharedPtr<NODE> node4(new NODE(4, FVector::RightVector * -1, FVector::RightVector*-1));
		node2->predecessor = node1;
		node3->predecessor = node2;
		node4->predecessor = node3;
		
		nodes.HeapPush(node4);
		nodes.HeapPush(node2);
		nodes.HeapPush(node3);
		nodes.HeapPush(node1);

		while (nodes.Num() > 0) {

			TSharedPtr<NODE> node;
			nodes.HeapPop(node);
			UE_LOG(LogTemp, Warning, TEXT("curret cost: %f"), node->cost);

		}
		*/
		//NodeTest(nodes[5]);
	}

	// Call the base class version  
	Super::PostEditChangeProperty(PropertyChangedEvent);
}
void APathCreator::FixedCorridorTest()
{
	if (_corridor != nullptr)
		_corridor->Destroy();
	for (int i = 0; i < actorsToDestroy.Num(); i++) {
		actorsToDestroy[i]->Destroy();
	}
	actorsToDestroy.Empty();

	FVector Location(0.0f, 0.0f, 0.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	UWorld* world = GetWorld();
	if (testCorridorStart == nullptr) {

		testCorridorStart = GetWorld()->SpawnActor<ADebugSphere>(Location, Rotation, SpawnInfo);
		testCorridorStart->SetActorScale3D(FVector(1.2f, 1.2f, 1.2f));
		testCorridorStart->SetActorLabel(*FString("Test Corridor Start "));
		//actorsToManuallyDestroy.Add(testCorridorStart);
		//testCorridorStart->ChangeColorIntensity(0);
	}
	if (testCorridorEnd == nullptr) {
		Location = FVector(trackingSpaceSize.X / 2.0f - corridorWidth * 2.0f, trackingSpaceSize.Y / 2.0f - corridorWidth * 2.0f, 0.0f);
		testCorridorEnd = GetWorld()->SpawnActor<ADebugSphere>(Location, Rotation, SpawnInfo);
		testCorridorEnd->SetActorScale3D(FVector(1.2f, 1.2f, 1.2f));
		testCorridorEnd->SetActorLabel(*FString("Test Corridor End "));
		testCorridorEnd->ChangeColorIntensity(1.0f);
		//actorsToManuallyDestroy.Add(testCorridorEnd);
	}
	//if(freeNodes.Num() == 0)
	//	freeNodes = GetFreeNodes(stepSize);
	FVector startDirection = testCorridorStart->GetActorForwardVector();

	TArray<FVector> corridorPoints = CreatePathPointsToPointNoLimit(testCorridorStart->GetActorLocation(), testCorridorStart->GetActorForwardVector(), testCorridorEnd->GetActorLocation(), testCorridorEnd->GetActorForwardVector());
	UE_LOG(LogTemp, Warning, TEXT("Test Corridor Start Direction x: %f y: %f z: %f"), startDirection.X, startDirection.Y, startDirection.Z);

	if (corridorPoints.Num() > 0)
		_corridor = CreatePathMesh(corridorPoints, nullptr, nullptr, nullptr, nullptr, false);
	else
		UE_LOG(LogTemp, Warning, TEXT("No way found"));
}
#endif

// Called when the game starts or when spawned
void APathCreator::BeginPlay()
{
	Super::BeginPlay();
	if (!runOnStart)
		return;
	player = GetWorld()->GetFirstPlayerController();

	AdjustRoomsList();
	GetFreeNodes(stepSize);

	player->InputComponent->BindAction("RebuildMap", IE_Pressed, this, &APathCreator::RebuildMap);
	//myCharacter->InputComponent->BindAction("NewRoom", IE_Pressed, this, &APathCreator::CreateRandomRoomWithPath);
	//RebuildMap();
	//CreateRandomRoomWithPath();
	FROOMSTRUCT &roomStruct = rooms[0];
	CreateRoomClosestToPlayer(roomStruct);
	if (roomStruct.rTemplate->possibleRoomPositions.Num() == 0) {
		UE_LOG(LogTemp, Warning, TEXT("No valid positions found for room 0"));

	}
	else {
		currentRoomFS = &roomStruct;
		ExpandRoom(roomStruct);
		roomStruct.roomVM->playerInside = true;
	}
}

// Called every frame
void APathCreator::Tick(float DeltaTime)
{
	if (!runOnStart)
		return;
	if (currentRoomFS != nullptr) {

		bool playerInCorridor = false;
		if (_currentCorridor != nullptr)
			playerInCorridor = _currentCorridor->playerInside;


		FVector playerPos = player->GetPawn()->GetActorLocation();
		ADoor * closestDoor = nullptr;
		float closestDistance = doorOpeningDistance * doorOpeningDistance;
		if (!playerInCorridor) {
			for (ADoor* door : currentRoomFS->room->_doors) {
				float currentDistance = FVector::DistSquared2D(door->_position, playerPos);
				if (currentDistance <= closestDistance) {
					closestDistance = currentDistance;
					closestDoor = door;
				}
			}

		}
		else {
			//there is a corridor and player is inside
			float distanceToStartDoor = FVector::DistSquared2D(_currentCorridor->_startDoor->_position, playerPos);
			float distanceToEndDoor = FVector::DistSquared2D(_currentCorridor->_endDoor->_position, playerPos);
			if (_currentCorridor->_startDoor->active) {
				if (closestDistance > distanceToStartDoor) {
					closestDistance = distanceToStartDoor;
					closestDoor = _currentCorridor->_startDoor;
				}
			}
			if (_currentCorridor->_endDoor->active) {
				if (closestDistance > distanceToEndDoor) {
					closestDoor = _currentCorridor->_endDoor;
				}
			}
		}
		if (closestDoor != _closestDoor) {
			//New door entered
			if (_closestDoor != nullptr) {
				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, FString::Printf(TEXT("Closing door: %i"), _closestDoor->_roomToID));
				_closestDoor->Close();
				
			}
			_closestDoor = closestDoor;
			if (closestDoor != nullptr) {
				closestDoor->Open();

				//GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Green, TEXT("Activate new corridor"));
				//if (!curvedCorridors) {
					ACorridor * currentCorridor = GetCurrentCorridor(currentRoomFS, _closestDoor);

					if (currentCorridor != _currentCorridor) {
						if (_currentCorridor != nullptr) {
							//DeactivateCorridor(_currentCorridor, currentRoomFS->roomVM);
							//_currentCorridor->DeactivateCorridor(currentRoomFS->roomVM);
						}
						_currentCorridor = currentCorridor;

						if (_currentCorridor != nullptr) {
							//ActivateCorridor(_currentCorridor, currentRoomFS->roomVM);
							//_currentCorridor->ActivateCorridor(currentRoomFS->roomVM);
						}
					}
				//}
			}
		}
		
	}
	Super::Tick(DeltaTime);
}

