// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "PathCreator.h"
#include "Runtime/Core/Public/HAL/Runnable.h"
/**
 * 
 */
class PROCEDURALTEST_API Multitasking : public FRunnable
{
private:
	APathCreator* pathCreator;
public:
	static Multitasking* Runnable;
	FRunnableThread* Thread;
	FThreadSafeCounter StopTaskCounter;
	Multitasking(APathCreator *p);
	virtual ~Multitasking();
	// Begin FRunnable interface.
	virtual bool Init() override;
	virtual uint32 Run() override;
	virtual void Stop() override;
	// End FRunnable interface

	bool IsFinished() const
	{
		return (StopTaskCounter.GetValue() == 1);
	}

	void EnsureCompletion();

	static Multitasking* JoyInit(APathCreator *p);

	static void Shutdown();

	static bool IsThreadFinished();
	
	void DoTestLoop();
};
