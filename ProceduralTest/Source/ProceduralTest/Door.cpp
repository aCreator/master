// Fill out your copyright notice in the Description page of Project Settings.

#include "Door.h"
#include "ConstructorHelpers.h"
#include "VisibilityManager.h"
#include "Corridor.h"
#include "PathCreator.h"
#include "Engine/Engine.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"


// Sets default values
ADoor::ADoor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	USceneComponent* root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Object"));
	root->SetupAttachment(RootComponent);
	SetRootComponent(root);

	doorVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Debug Door Visualisation"));
	doorVisual->SetupAttachment(root);
	
	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube"));
	if (CubeVisualAsset.Succeeded()) {
		doorVisual->SetStaticMesh(CubeVisualAsset.Object);
		myMaterialDynamic = UMaterialInstanceDynamic::Create(doorVisual->GetMaterial(0), doorVisual);
		myMaterialDynamic->SetVectorParameterValue("Color", FLinearColor::Yellow);
		doorVisual->SetMaterial(0, myMaterialDynamic);
	}
	SetActorEnableCollision(false);
	deactivateCollisions = true;
}

void ADoor::Init(int id, FVector position, FVector forward, FIntVector arrayPosition)
{
	_roomInID = id;
	_position = position;
	_forward = forward;
	_arrayPosition = arrayPosition;
	SetActorLocation(position);
	SetActorLabel(*FString("Door " + FString::FromInt(_roomInID)));
}

void ADoor::Init(int id, FVector position, FVector forward, FIntVector arrayPosition, float height, float width, float speed)
{
	Init(id, position, forward, arrayPosition);
	_doorWidth = width;
	_height = height;
	if (FMath::Abs(forward.Y) > 0) {
		_width = width;
		_length = 20;
	}
	else {
		_width = 20;
		_length = width;
	}
	//doorVisual->AddWorldOffset(FVector(0, 0, _height/2.0f));
	_speed = speed;
	if(FMath::Abs(forward.Y) > 0)
		SetActorScale3D(FVector(_width / 100.0f, _length / 100.0f, _height / 100.0f));
	else
		SetActorScale3D(FVector(_width / 100.0f, _length / 100.0f, _height / 100.0f));
		
}

void ADoor::Open()
{
	closed = false;
	open = true;
	if (_room != nullptr && _pathCreator != nullptr && _corridor != nullptr) {
		if (!_room->playerInside) {
			_room->ActivateAllMainObjects();
			_room->Activate();
		}
		if (!_corridor->playerInside) {
			_corridor->ActivateCorridor(_room);
			_pathCreator->ActivateCorridor(_corridor, this);
		}
	}

}

void ADoor::Close()
{
	open = false;
	if(!_room->IsValidLowLevel())
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Magenta, FString::Printf(TEXT("Door closed but no room was registered")));

}

void ADoor::ActivateObject()
{
	//Super::ActivateObject();
	active = true;
	SetActorHiddenInGame(false);

	if (_room != nullptr && _pathCreator != nullptr && _corridor != nullptr) {
		if (_doorFrame != nullptr && _doorFrame->IsValidLowLevel())
			_doorFrame->SetActorHiddenInGame(false);
	}
}

void ADoor::DeactivateObject()
{
	//Super::DeactivateObject();
	active = false;
	SetActorHiddenInGame(true);

	if (_room != nullptr && _pathCreator != nullptr && _corridor != nullptr) {
		if (_doorFrame != nullptr && _doorFrame->IsValidLowLevel())
			_doorFrame->SetActorHiddenInGame(true);
	}
}

void ADoor::ChangeColor(FLinearColor color)
{
	myMaterialDynamic->SetVectorParameterValue("Color", color);
}

// Called when the game starts or when spawned
void ADoor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (open) {
		FVector newPosition = GetActorLocation();
		if (newPosition.Z + _speed * DeltaTime < _position.Z + _height + 0.1)
			newPosition.Z = newPosition.Z + _speed * DeltaTime;
		else {
			// Door is entirely opened
			newPosition.Z = _position.Z + _height + 0.1;
		}
		SetActorLocation(newPosition);
	}
	else {
		if (!closed) {
			FVector newPosition = GetActorLocation();
			if (newPosition.Z - _speed * DeltaTime > _position.Z)
				newPosition.Z = newPosition.Z - _speed * DeltaTime;
			else {
				// Door is entirely closed
				newPosition.Z = _position.Z;
				closed = true;
				if (_room != nullptr && _pathCreator != nullptr && _corridor != nullptr) {
					if (_room->playerInside) {
						if (_corridor->IsValidLowLevel())
							_corridor->DeactivateCorridor(_room);
						_pathCreator->DeactivateCorridor(_corridor, this);
					}
					else {
						_room->Deactivate();
						_room->DeactivateAllMainObjects();
						_room->Deactivate();
					}
					ActivateObject();
					if(_doorFrame->IsValidLowLevel())
						_doorFrame->ActivateObject();
				}
			}
			SetActorLocation(newPosition);
		}
	}

}

void ADoor::Destroy()
{
	for (AActor* h : _hallWay) {
		if(h != nullptr)
			h->Destroy();
	}
	Super::Destroy();
}

