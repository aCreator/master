// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PathCreator.h"
#include "Multitasking.h"
#include "GameFramework/Actor.h"
#include "Testing.generated.h"

UCLASS()
class PROCEDURALTEST_API ATesting : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATesting();
	APathCreator * pathCreator;
	UPROPERTY(EditAnywhere)
		bool startTest;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable, Category = "PathCreator")
		void TestMethod();
	void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent);	
};
