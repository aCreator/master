// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "GameFramework/Actor.h"
#include "MeshCreator.h"
#include "DebugSphere.h"
#include "Obstacle.generated.h"


UCLASS()
class PROCEDURALTEST_API AObstacle : public AActor
{
	GENERATED_BODY()
	
private:
	MeshCreator meshCreator;
	UPROPERTY(EditAnywhere)
		UProceduralMeshComponent* mesh = nullptr;
	UPROPERTY(EditAnywhere)
		float _height = 100;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* obstacleVisual;
public:	
	// Sets default values for this actor's properties
	AObstacle();

	//UPROPERTY(EditAnywhere)
		TArray<FVector> corners;
	UPROPERTY(EditAnywhere)
		bool reset;
	UPROPERTY(EditAnywhere)
		float _width = 100;
	UPROPERTY(EditAnywhere)
		float _length = 100;
	UPROPERTY(EditAnywhere)
		bool test;
	UPROPERTY(EditAnywhere)
		bool showBoundingBox;
	FBoxSphereBounds GetBoundingBox();
	void AddTestSphere(FVector position);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void CreateMesh();
	void CreateWallMesh(FVector p1, FVector p2);
	TArray<AActor*> actorsToDestroy;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent);
	
	
};
