// Fill out your copyright notice in the Description page of Project Settings.

#include "VisibilityManagerRoom.h"
#include "PathCreator.h"


void AVisibilityManagerRoom::Init(ARoom* room, APathCreator * pathCreator) {
	Super::Init(room->_position, room->_width, room->_length, FVector::ZeroVector);
	for (ADoor* d : room->_doors) {
		AddMainObject(d, true);
		d->_pathCreator = pathCreator;
	}	
	AddMainObject(room, true);
	_room = room;
	_pathCreator = pathCreator;
}

void AVisibilityManagerRoom::OnOverlapBegin(AActor * OverlappedActor, AActor * OtherActor)
{
	Super::OnOverlapBegin(OverlappedActor, OtherActor);
	if (!active)
		return;
	if (OtherActor->ActorHasTag("Player")) {
		if (playerInside && _pathCreator != nullptr) {
			_pathCreator->PlayerEnteredRoom(_room->id);
		}
	}
}

void AVisibilityManagerRoom::OnOverlapEnd(AActor * OverlappedActor, AActor * OtherActor)
{
	Super::OnOverlapEnd(OverlappedActor, OtherActor);
	if (!active)
		return;
	if (OtherActor->ActorHasTag("Player")) {
		DeactivateAllEmptyNeighbours();
		Activate();
		ActivateAllMainObjects();
	}
}
